<?php

class Public_function{

	function __construct(){
		$this->ci=&get_instance();
	}

	public function reArrayFiles(&$file_post, $id = null) {
        $file_ary = array();
        $file_keys = array_keys($file_post);
		if($id === null){
			$file_count = count($file_post['name']);
			for ($i=0; $i<$file_count; $i++) {
				foreach ($file_keys as $key) {
					$file_ary[$i][$key] = $file_post[$key][$i];
				}
			}
		} else {
			$file_count = count($file_post['name'][$id]);
			for ($i=0; $i<$file_count; $i++) {
				foreach ($file_keys as $key) {
					$file_ary[$i][$key] = $file_post[$key][$id][$i];
				}
			}
		}
        return $file_ary;
    }

    public function format_date(&$date){
        $r_bulan = $this->ref_month();
        $tahun = (int) substr($date,0,4);
        $bulan = (int) substr($date,5,2);
        $tanggal = (int) substr($date,8,2);
        $jam = (strlen($date) > 10)?" ".date('H:i:s',strtotime($date)):"";
        return ((empty($tanggal))?"":$tanggal." ").((empty($bulan))?"":$r_bulan[$bulan]." ").((empty($tahun))?"":((empty($tanggal) && empty($bulan))?"Tahun ":"").$tahun).$jam;
    }

    public function ref_bulan(){
        $r_bulan = array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        return $r_bulan;
    }

    public function ref_month(){
        $r_bulan = array(1=>"January","February","March","April","May","June","July","August","September","October","November","December");
        return $r_bulan;
    }

    public function format_month($month){
        switch ($month) {
            case "1": $nama_bulan = "Januari"; 
            break;
            case "2": $nama_bulan = "Februari"; 
            break;
            case "3": $nama_bulan = "Maret"; 
            break;
            case "4": $nama_bulan = "April"; 
            break;
            case "5": $nama_bulan = "Mei"; 
            break;
            case "6": $nama_bulan = "Juni"; 
            break;
            case "7": $nama_bulan = "Juli"; 
            break;
            case "8": $nama_bulan = "Agustus"; 
            break;
            case "9": $nama_bulan = "September"; 
            break;
            case "10": $nama_bulan = "Oktober"; 
            break;
            case "11": $nama_bulan = "November"; 
            break;
            case "12": $nama_bulan = "Desember"; 
            break;
            default:
            break;
        }
        return $nama_bulan;
    }

    public function create_array($id_name,$value = array()){
        $arr = array();
        foreach ($value as $val) {
            $x = array("$id_name" => $val);
            array_push($arr,$x);
        }
        return $arr;
    }

    public function replace_month($date){
        foreach ($ref_bulan as $key => $value) {
            if (strpos($date, $value) !== false){
                return str_replace($value,substr($value, 0, 3),$date);
            }
        }
    }

    public function status_order($status, $user){
        switch ($status) {
            case '1':
                $text = 'Pending Confirmation';
                break;
            case '2':
                if ($user == 'tourist'){
                    $text = 'Accepted';
                }else{
                    $text = 'Pending Payments';
                }
                break;
            case '3':
                $text = 'Declined';
                break;
            case '4':
                $text = 'Booking Completed';
                break;
            case '5':
                $text = 'Started';
                break;
            case '6':
                $text = 'Finished';
                break;
            case '7':
                $text = 'Canceled by tourist';
                break;
            case '8':
                $text = 'Canceled by guide';
                break;
        }

        return $text;
    }

    public function status_book($status) {
        switch ($status) {
            case '1':
                $text = 'Pending';
                break;
            case '2':
                $text = 'Accepted';
                break;
            case '3':
                $text = 'Rejected';
                break;
            case '4':
                $text = 'Paid';
                break;
            case '5':
                $text = 'Finished';
                break;
        }
        return $text;
    }

    public function status_account($status) {
        switch ($status) {
            case '0':
                $text = 'Not Activated';
                break;
            case '1':
                $text = 'Active';
                break;
            case '2':
                $text = 'Suspended';
                break;
        }
        return $text;
    }

}
?>