<?php
    class Request_dto {
        var $amount; //The amount to pay without the decimal fraction.
        var $amountExtra; //The tip amount to pay without the decimal fraction.
        var $authorizationType; // Required to identify what authorization type is used in online payment. Option : 'PRE_AUTH' or 'PURCHASE'. The authorization type "PRE_AUTH" is used when the merchant wants to use credit card pre-auth transaction. The authorization type "PURCHASE" is used when the merchant wants to use do purchase.
        var $billingAddressLine1; //The billing address 1 to process the authorization.
        var $billingAddressLine2; //The billing address 2 to process the authorization.
        var $billingCity; //The billing city to process the authorization.
        var $billingCountry; //The billing country to process the authorization.
        var $billingState; //The billing state to process the authorization.
        var $billingDistrict; //The billing district to process the authorization.
        var $billingVillage; //The billing village to process the authorization.
        var $billingPostcode; //The billing postcode to process the authorization.
        var $currencyCode; //The currency code.
        var $customerEmailAddress; //The email address where receipt will be send.
        var $customerMobileNumber; //The phone number of the customer.
        var $customerName; //The name of the customer.
        var $decimalAmount; //The decimal amount fraction only.
        var $decimalAmountExtra; //The decimal tip amount fraction only.
        var $description; //The description of the purchase payment.
        var $integrationType; //Required to identify what integration type to process online payment. Option: "CASHLEZ_URL" or "MERCHANT_URL". The integration type "CASHLEZ_URL" is used when merchant integration is using Cashlez's page to input credit card number. The integration type "MERCHANT_URL" is used when merchant integration is using their own page to input credit card number.
        var $invoiceUrl; //The invoice URL.
        var $merchantPaymentFailedURL; //When the payment failed, the system will redirect (HTTP GET) to this URL and passing some values.
        var $merchantPaymentSuccessURL; //When the payment succeed, the system will redirect (HTTP GET) to this URL and pass some values.
        var $merchantTransactionId; //The unique identifier given by the merchant to identify the transaction.
        var $paymentPageConfigDTO; //The configuration detail of the payment page.
        var $productImageUrl; //The product image URL.
        var $requestRecurringToken; //The flag decides whether the response should return card token.
        var $sendReceipt; //The type of receipt to send.
        var $shippingAddressLine1; //The shipping address line 1 to process the authorization.
        var $shippingAddressLine2;  //The shipping address line 2 to process the authorization.
        var $shippingCity; //The shipping city to process the authorization.
        var $shippingCountry; //The shipping country to process the authorization.
        var $shippingDistrict; //The shipping district to process the authorization.
        var $shippingVillage; //The shipping village to process the authorization.
        var $shippingPostcode; //The shipping postcode to process the authorization.
        var $shippingState; //The shipping state to process the authorization.
        var $endpointId; // The endpoint id name - providd by cashlez
        var $endpointType; // "AGGREGATOR-HASH"
        var $username;  // The username - provided by cashlez
        var $timestamp; // Generated
        var $version; // "1.3.0"
        var $additionalData;

        var $response = []; //Return the response from process function

        public function process($serverKey) {
            $fields = [
                "integrationType"=>$this->integrationType,
                "authorizationType"=>$this->authorizationType,
                "merchantPaymentSuccessURL"=>$this->authorizationType,
                "merchantPaymentFailedURL"=>$this->merchantPaymentFailedURL,
                "currencyCode"=>$this->currencyCode,
                "amount"=>$this->amount,
                "decimalAmount"=>$this->decimalAmount,
                "amountExtra"=>$this->amountExtra,
                "decimalAmountExtra"=>$this->decimalAmountExtra,
                "description"=>$this->merchantTransactionId,
                "merchantTransactionId"=>$this->merchantTransactionId,
                "customerName"=>$this->customerName,
                "customerEmailAddress"=>$this->customerEmailAddress,
                "customerMobileNumber"=>$this->customerMobileNumber,
                "shippingAddressLine1"=>$this->shippingAddressLine1,
                "shippingAddressLine2"=>$this->shippingAddressLine2,
                "shippingCity"=>$this->shippingCity,
                "shippingState"=>$this->shippingState,
                "shippingCountry"=>$this->shippingCountry,
                "shippingDistrict"=>$this->shippingDistrict,
                "shippingVillage"=>$this->shippingVillage,
                "shippingPostcode"=>$this->shippingPostcode,
                "billingAddressLine1"=>$this->billingAddressLine1,
                "billingAddressLine2"=>$this->billingAddressLine2,
                "billingCity"=>$this->billingCity,
                "billingState"=>$this->billingState,
                "billingCountry"=>$this->billingCountry,
                "billingDistrict"=>$this->billingDistrict,
                "billingVillage"=>$this->billingVillage,
                "billingPostcode"=>$this->billingPostcode,
                "requestRecurringToken"=>$this->requestRecurringToken,
                "auth"=>[
                    "endpointId"=>$this->endpointId,
                    "endpointType"=>$this->endpointType,
                    "username"=>$this->username,
                    "timestamp"=>$this->timestamp,
                ],
                "version"=>$this->version,
                "additionalData"=>$this->additionalData,
                "invoiceUrl"=>$this->invoiceUrl,
                "paymentPageConfigDTO"=>$this->paymentPageConfigDTO,
                "productImageUrl"=>$this->productImageUrl,
                "sendReceipt"=>$this->sendReceipt
            ];

            $token = new createAuthToken;
            $token->generate($fields,$serverKey);

            $this->response = json_encode($token->response);
            return $this->response;
        }
    }

    class createAuthToken {
        var $response = [];

        function generate($fields,$serverKey){
            $temp_key = array();
            foreach ($fields as $key => $value) {
                $key = (string) $key;
                array_push($temp_key, $key);
            }
            sort($temp_key);

            $data = [];
            foreach ($temp_key as $tempKey) {
                $tempKey = (string) $tempKey;
                foreach ($fields as $key => $value) {
                    if((string) $tempKey == (string) $key){
                        //sort child
                        if(count($value) > 1){
                            $temp_key_child = [];
                            foreach ($value as $keyc => $valuec) {
                                $keyc = (string) $keyc;
                                array_push($temp_key_child, $keyc);
                            }
                            sort($temp_key_child);

                            //get data base on sorting child key
                            $data_child = [];
                            foreach ($temp_key_child as $tempKeyChild) {
                                $tempKeyChild = $tempKeyChild;
                                foreach ($value as $keychild => $valuechild) {
                                    if((string) $tempKeyChild == (string) $keychild){
                                        $tempchild = [$tempKeyChild => $valuechild];
                                        $data_child = array_merge($data_child,$tempchild);
                                    }
                                }
                            }
                            //end sort
                            $value = $data_child;
                        }
                        //end sort child
                        $temp = [$tempKey => $value];
                        $data = array_merge($data,$temp);
                    }
                }
            }

            //get the value
            $val = '';
            foreach ($data as $keydata => $valuedata) {
                if(count($valuedata) > 1){
                    foreach ($valuedata as $keychild => $valuechild) {
                      $val .= $valuechild;
                    }
                }else{
                    $val .= (string)$valuedata;
                }
            }

            $first_key_len = round(strlen($serverKey)/2,0,PHP_ROUND_HALF_DOWN);
            $last_key_len = strlen($serverKey) - $first_key_len;
            $first_key = substr($serverKey,0,$first_key_len);
            $last_key = substr($serverKey,$first_key_len,$last_key_len);
            $token = strtoupper(hash('sha256', (string) $first_key . $val . $last_key));

            $data_token = [];
            foreach ($data as $key => $value) {
                if(count($value) > 1){
                    if($key == 'auth'){
                        $temp_data = ['token' => $token];
                        $value = array_merge($value,$temp_data);
                    }
                }
                $temp = [$key => $value];
                $data_token = array_merge($data_token,$temp);
            }

            $this->response = $data_token;
            return $this->response;
        }
    }
?>