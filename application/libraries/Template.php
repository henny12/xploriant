<?php
	class Template{
		protected $_ci;
		function __construct() {
	        $this->ci = &get_instance();
	        $this->ci->load->library(array('session'));
	        $this->ci->load->model(array('ConnectChatModel','NotifBookingModel'));
	    }

		function display($template,$data=null){
			$myid = $this->ci->session->userdata('id');
			$notif_message = $this->ci->ConnectChatModel->fetch(null, null, null, false, null, array('cm_user_id' => $myid, 'read_status' => 0), null, true);
			$notif_booking = $this->ci->NotifBookingModel->fetch(null, null, null, false, null, array('user_id' => $myid, 'read_status' => 0), null, true);
			$data['notif_message'] = $notif_message;
			$data['notif_booking'] = $notif_booking;
			$data['_content']=$this->ci->load->view($template,$data,true);
			$this->ci->load->view('/template.php',$data);
		}

	}
?>