<?php
	class Tables{
		//exploriant
		PUBLIC STATIC $user_account = "user_account";
		PUBLIC STATIC $user_data = "user_data";
		PUBLIC STATIC $personal_interest = "personal_interest";
		PUBLIC STATIC $not_recommended = "not_recommended";
		PUBLIC STATIC $temp_image = "temp_image";
		PUBLIC STATIC $itinerary = "itinerary";
		PUBLIC STATIC $itinerary_image = "itinerary_image";
		PUBLIC STATIC $booking = "booking";
		PUBLIC STATIC $connect_message = "connect_message";
		PUBLIC STATIC $itinerary_detail = "itinerary_detail";
		PUBLIC STATIC $review = "review";
		PUBLIC STATIC $chat = "chat";
		PUBLIC STATIC $top_destination = "top_destination";
		PUBLIC STATIC $notif_booking = "notif_booking";
		PUBLIC STATIC $admin = "admin";
		PUBLIC STATIC $user_miles = "user_miles";
		PUBLIC STATIC $kurs = "kurs";
		//old
		PUBLIC STATIC $role = "role";
		PUBLIC STATIC $menu = "menu";
		PUBLIC STATIC $role_menu = "role_menu";
		PUBLIC STATIC $user_skill = "user_skill";
		PUBLIC STATIC $user_profile = "user_profile";
		PUBLIC STATIC $transaction = "transaction";
		PUBLIC STATIC $withdrawal = "withdrawal";
		PUBLIC STATIC $top_up = "top_up";
		PUBLIC STATIC $level = "level";
	}
?>