<?php
    class Response_dto {
        var $response = []; //return the response from get payment URL
  
        function process($data, $urlAPI) {
            $ch = curl_init();
        
            // // TO BE CHANGED FOR PRODUCTION
            // $urlAPI = 'https://staging.cashlez.com:8080/ecom-migs-server-1.2.0-SNAPSHOT/ws/getPaymentURL';
        
            curl_setopt($ch, CURLOPT_URL, $urlAPI);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            $response = curl_exec($ch);
            curl_close($ch);

            return $this->response = $response;
        }
    }
?>