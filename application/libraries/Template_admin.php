<?php
    class Template_admin{
        protected $_ci;
        function __construct() {
            $this->ci = &get_instance();
            $this->ci->load->library(array('session'));
        }

        function display($template,$data=null){
            $data['_content']=$this->ci->load->view($template,$data,true);
            $this->ci->load->view('/admin/template.php',$data);
        }

    }
?>