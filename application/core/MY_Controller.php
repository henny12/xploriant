<?php

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_generic_dao();
        $this->load->library(array('user_agent'));
        $this->load->model(array('NotifBookingModel','BookingModel'));
    }

    public function logged_in_member() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth/signin");
        }
    }

    public function logged_in_admin() {
        if (!($this->session->userdata('logged_in_admin'))) {
            redirect(base_url() . "admin/auth/login");
        }
    }

    public function config_email() {
        $config_email = "henny.novelite@gmail.com";
        $config_password = "Novelite8899";    
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $config_email,
            'smtp_pass' => $config_password,
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config_email, 'Xploriant');
    }

    public function send_notif_booking($title, $book_id, $user_id, $book_type) {
        $obj = array(
            'notif_title' => $title,
            'book_id' => $book_id,
            'user_id' => $user_id,
            'book_type' => $book_type,
            'read_status' => 0,
            'created_at' => date('Y-m-d h:i:s')
        );
        $this->NotifBookingModel->insert($obj);
    }

    public function send_email($email, $book_id, $type) {
        $account = $this->UserAccountModel->by_id(['user_email' => $email]);
        $data = $this->UserDataModel->by_id(['user_id' => $account->user]);
        $this->data['firstname'] = $data->user_fname;
        $this->data['email'] = urlencode($email);
        $this->data['code'] = $code;
        $this->email->to($email); 
        $this->email->subject('Verify Your Email Address');
        $this->email->message($this->load->view('template_email/account_verification', $this->data, true));  
        $this->email->send();
    }

    public function send_notif_email($subject, $book_id, $email, $type) {
        $booking = $this->BookingModel->by_id(['book_id' => $book_id]);
        $this->data['traveler'] = $this->UserDataModel->by_id(['user_id' => $booking->tourist_id]);
        $this->data['guide'] = $this->UserDataModel->by_id(['user_id' => $booking->guide_id]);
        $this->data['booking'] = $booking;
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($this->load->view('template_email/'.$type, $this->data, true));
        $this->email->send();
    }
}

?>