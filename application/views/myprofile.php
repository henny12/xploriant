<div class="container-fluid">
    <div class="row">
        <img src="<?= base_url()?>assets/images/profilepage.png" class="img-responsive img-home">
        <img src="<?= (!empty($user->user_photo))?base_url().'media/profpic_thumb/'. $user->user_photo:base_url().'media/profpic_thumb/default.png'; ?>" class="photoprofile">
        <center><span class="btn btn-primary btn-file btn-xs btn-profpic"><i class="fa fa-camera" aria-hidden="true"></i> Change Picture <input type="hidden"><input type="file" name="file_photo" class="item-img file" required></span></center>
        <input type="hidden" class="profpic" value="1">
        <figure id="fig">
            <img src="" class="img-thumbnail" id="item-img-output" />
        </figure>
        <p class="nameprofile" align="center"><?= $user->user_fname .' '. $user->user_lname; ?></p>
        <p class="locprofile" align="center"><?= $user->user_residence; ?></p>
    </div>
</div>
<div class="container">
    <div class="row margin1">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills m-b-30">
                        <li class=" nav-item">
                            <a href="#navpills-1" class="nav-link nav-profile active" data-toggle="tab">My Account</a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-2" class="nav-link nav-profile" data-toggle="tab">About Me</a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-3" class="nav-link nav-profile" data-toggle="tab">My Itineraries</a>
                        </li>
                        <li class="nav-item">
                            <a href="#navpills-4" class="nav-link nav-profile" data-toggle="tab">Account Balance</a>
                        </li>
                    </ul>
                    <div class="tab-content br-n pn margin5">
                        <div id="navpills-1" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="collapse-init1" class="btn btn-primary btn-sm">Edit Your Profile</button><br><br>
                                    <form action="<?= base_url(); ?>myprofile/edit_account/<?= $user->user_id?>" method="post">
                                        <input type="hidden" name="">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-user-circle-o"></i></span>
                                                </div>
                                                <input type="text" name="fname" class="form-control fname" placeholder="First Name" value="<?= $user->user_fname?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-user-circle-o"></i></span>
                                                </div>
                                                <input type="text" name="lname" class="form-control lname" placeholder="Last Name" value="<?= $user->user_lname?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-envelope"></i></span>
                                                </div>
                                                <input type="text" name="email" class="form-control email" placeholder="Email Address" value="<?= $user->user_email?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-address-card"></i></span>
                                                </div>
                                                <input type="text" name="personalid" class="form-control personalid" placeholder="Enter Your Personal Id" value="<?= $user->user_personal_id ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" name="register" class="btn btn-danger btn-x reg-btn" value="Update Profile" style="display: none">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <hr>
                            <button id="collapse-changepass" class="btn btn-primary btn-sm">Change Your Password</button>
                            <hr>
                            <div class="row changepass" style="display: none">
                                <div class="col-md-12">
                                    <form action="<?= base_url(); ?>myprofile/change_password/<?= $user->user_id?>" method="post">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                                                </div>
                                                <input type="password" name="currentpassword" class="form-control" placeholder="Enter Current Password" aria-describedby="basic-addon2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                                                </div>
                                                <input type="password" name="newpassword" id="cp-password" class="form-control" placeholder="Enter New Password" aria-describedby="basic-addon2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                                                </div>
                                                <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm New Password" id="cp-confirm" aria-describedby="basic-addon2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" name="register" class="btn btn-danger btn-x" value="Update Password">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-2" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="collapse-about" class="btn btn-primary btn-sm">Edit Your Profile</button><br><br>
                                    <form action="<?= base_url(); ?>myprofile/edit_about/<?= $user->user_id; ?>" method="post">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-venus-mars"></i></span>
                                                </div>
                                                <select name="gender" class="form-control gender" disabled>
                                                    <option>Choose Gender</option>
                                                    <option value="M" <?= ($user->user_gender == 'M')?'selected':''; ?>>Male</option>
                                                    <option value="F" <?= ($user->user_gender == 'F')?'selected':''; ?>>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <input type="text" name="dob" class="form-control dob" placeholder="Date" id="datepicker" placeholder="Date of Birth" value="<?= $user->user_dob; ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-flag"></i></span>
                                                </div>
                                                <input type="text" name="nationality" class="form-control nationality" placeholder="Nationality" value="<?= ucwords($user->user_nationality);?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-map-pin"></i></span>
                                                </div>
                                                <input type="text" name="residence" class="form-control residence" placeholder="Residence" value="<?= ucwords($user->user_residence); ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-language"></i></span>
                                                </div>
                                                <input type="text" name="language" class="form-control language" placeholder="Language" value="<?= ucwords($user->user_lang); ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-user-circle-o"></i></span>
                                                </div>
                                                <textarea class="form-control desc" name="desc" placeholder="Personal Description" rows="4" disabled><?= $user->user_desc; ?></textarea>
                                            </div>
                                        </div>
                                        <b>Personal Interest</b>
                                        <hr>
                                        <div class="row">
                                            <?php if(!empty($personal_interest_list)){
                                                $i = 0;
                                                $data_interest = explode('-',$user->user_interests);
                                                foreach ($personal_interest_list as $pe) { ?>
                                                    <div class="col-md-4">
                                                        <span class="button-checkbox">
                                                            <button type="button" class="btn btn-opt <?= ($pe->pe_id == @$data_interest[$i])?'btn-aktif':'';?>" data-color="primary" disabled><?= ucwords($pe->pe_name); ?></button>
                                                            <input type="checkbox" class="hidden" name="interest[]" value="<?= $pe->pe_id; ?>" />
                                                        </span>
                                                    </div>
                                            <?php $i++; }} ?>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <input type="submit" name="register" class="btn btn-danger btn-x abt-btn" value="Update Profile" style="display: none">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-3" class="tab-pane">
                            <div class="row">
                                <?php if(!empty($itinerary)){
                                    foreach($itinerary as $val){ ?>
                                    <div class="col-md-3 margin2">
                                        <a href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>">
                                            <?php if(!empty($val->itin_thumb)){ ?>
                                                <img src="<?=base_url().'media/itin_thumb/'.$val->itin_thumb; ?>" class="img-responsive img-border">
                                            <?php }else{ ?>
                                                <img src="<?=base_url().'media/itin_thumb/no-image.png';?>" class="img-responsive img-border">
                                            <?php } ?>
                                        </a>
                                        <p class="text1"><?= ucwords($val->itin_location); ?></p>
                                        <a class="l-title" href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>"><p class="text2"><?= ucwords(substr($val->itin_title,0,16).'...'); ?></p></a>
                                        <a href="#" data-href="<?= base_url().'itinerary/delete/'.$val->itin_id ?>" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger btn-xs" style="width: 100%">Remove Itinerary</a>
                                    </div>
                                <?php }} ?>
                                <div class="col-md-3 margin2">
                                    <a href="<?= base_url().'guide';?>">
                                        <img src="<?=base_url().'assets/images/icon/add2.png';?>" class="img-responsive img-border">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-4" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="prof-text1">Your X-miles balance is 0 point.</p>
                                    <hr class="hr-prof">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.hr-prof{
    border: 2px solid #042441;
}
.prof-text1{
    color: #042441;
    margin-bottom: 5px;
    font-weight: 400;
    text-align: center;
}
.photoprofile{
    width: 120px;
    height: 120px;
    position: absolute;
    top: 125px;
    left: 0px;
    right: 0px;
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-radius: 50%;
}
.prof-pic{
    width: 120px;
    height: 120px;
}
.nameprofile{
    position: absolute;
    top: 280px;
    left: 0px;
    right: 0px;
    color: white;
    font-size: 17pt;
    font-weight: 400;
}
.locprofile{
    position: absolute;
    top: 310px;
    left: 0px;
    right: 0px;
    color: white;
    font-size: 10pt;
}
.btn-profpic{
    width: 120px;
    position: absolute !important;
    top: 255px;
    left: 0px;
    right: 0px;
    display: block;
    margin-left: auto;
    margin-right: auto;
}
.btn-file {
    /*position: relative;
    overflow: hidden;*/
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;   
    cursor: inherit;
    display: block;
}
.btn-aktif {
    background-color: #042441 !important;
    color: #fff !important;
    border-radius: 0 !important;
}
</style>