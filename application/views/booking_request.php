<div class="container margin7">
    <div class="row">
        <div class="col-md-12">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <p class="bs-text1">Booking Request</p>
            <h3 class="bs-title"><?= ucwords($itin->itin_title); ?></h3>
            <p class="bs-text1">By <a href="#" class="bs-link"><?= $guide->user_fname.' '.$guide->user_lname; ?></a></p>
            <br>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/location.png')?>" class="bs-icon"> <?= ucwords($itin->itin_location)?>
            </p>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/user2.png')?>" class="bs-icon"> <?= $book->total_tourist; ?> Tourists
            </p>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/calender.png')?>" class="bs-icon"> <?= date('d/m/Y', strtotime($book->start_date)); ?> <?= (!empty($book->end_date))?'- '. date('d/m/Y', strtotime($book->end_date)):''; ?>
            </p>
            <br>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/money.png')?>" class="bs-icon"> <?= strtoupper($book->total_price_currency).' '.number_format($book->total_price_nominal,0,'.',','); ?>
            </p>
            <p class="bs-text2">
                Booking Code : <?= $book->book_code; ?><br>
                <span style="font-size: 10pt">
                    <?= (!empty($book->book_accepted_date)?'Accepted on '.$this->public_function->format_date($book->book_accepted_date).'<br>':'');?>
                    <?= (!empty($book->book_paid_date)?'Paid on '.$this->public_function->format_date($book->book_paid_date).'<br>':'');?>
                </span>
            </p>
            <br>
            <div class="card bs-card">
                <div class="card-body">
                    <h3>About Your Traveler</h3>
                    <div class="row">
                        <div class="col-md-2">   
                            <img src="<?= (!empty($turis->user_photo))?base_url().'media/profpic_thumb/'. $turis->user_photo:base_url().'assets/images/icon/user.png'; ?>" class="foto-profil">
                        </div>
                        <div class="col-md-5">
                            <a href="<?= base_url().'guide/profile/'.$turis->user_id; ?>" style="color: #727577"><span class="nama-profil"><?= $turis->user_fname.' '.$turis->user_lname; ?></span></a><br>
                            <form method="get" action="<?= base_url().'messages';?>">
                                <?php $id_encrypt = $this->OauthModel->Encryptor('encrypt', $turis->user_id)?>
                                <input type="hidden" name="guide_id" value="<?= $id_encrypt; ?>">
                                <input type="hidden" name="guide_name" value="<?= $turis->user_fname.' '.$turis->user_lname; ?>">
                                <input type="submit" value="Contact Traveler" class="contact">
                            </form>
                        </div>
                    </div>
                    <textarea class="form-control bs-ta" name="book_request" placeholder="Descriptions" rows="6" disabled><?= $book->book_request; ?></textarea>
                </div>
            </div>
            <?php if ($book->book_status == 1){ ?>
                <div class="form-group">
                    <a href="<?= base_url().'booking/accept_book/'.$book->book_id; ?>" class="btn btn-danger btn-x">Accept Request</a>
                </div>
                <p align="center"><a href="<?= base_url().'booking/reject_book/'.$book->book_id; ?>">Decline</a></p>
            <?php } if ($book->book_status == 2) {?>
                <div class="form-group">
                    <a href="#" class="btn btn-danger btn-xr" disabled>Waiting for payment</a>
                </div>
            <?php } if ($book->book_status == 4 && ($book->start_date == date('Y-m-d'))) {?>
                <div class="form-group" id="start-tour">
                    <a href="<?= base_url().'booking/end_tour/'.$book->book_id.'/2'; ?>" class="btn btn-danger btn-x btn-start">Start Tour</a>
                </div>
            <?php } if ($book->book_status == 5) { ?>
                <p align="center">The tour is still running...</p>
                <?php if ($book->end_date <= date('Y-m-d')) { ?>
                <div class="form-group" id="start-tour">
                    <a href="<?= base_url().'booking/end_tour/'.$book->book_id.'/2'; ?>" class="btn btn-danger btn-x btn-start">End Tour</a>
                </div>
            <?php }} if ($book->book_status != 1 && $book->book_status != 3 && $book->book_status != 5 && $book->book_status != 6) { ?>
                <p align="center"><a href="#" data-href="<?= base_url().'booking/cancel_book/'.$book->book_id ?>" data-toggle="modal" data-target="#cancelModal">Cancel Booking</a></p>
            <?php } ?>
        </div>
    </div>
</div>
<style type="text/css">
    .bs-ta{
        border: 2px solid #36648e;
        border-radius: 0;
    }
    .bs-card{
        border-radius: 0;
        border-bottom: 1px solid rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 5px 2px rgba(0, 0, 0, 0.15);
    }
    .bs-text2{
        color: #042441;
        margin-bottom: 5px;
        font-weight: 400;
    }
    .bs-text1{
        color: #042441;
        margin-bottom: 5px;
        font-weight: 600;
    }
    .bs-link{
        color: red;
    }
    .bs-title{
        font-weight: bold;
        font-size: 35px;
        margin-top: 5px;
        margin-bottom: 5px;
        line-height: 35px;
    }
    .bs-icon{
        width: 20px;
        margin-right: 5px;
    }
</style>