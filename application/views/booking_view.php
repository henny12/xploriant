<div class="container" style="margin-top: 100px;">
    <div class="row margin1">
        <div class="col-md-12">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills m-b-30">
                        <li class="nav-item nav-booking">
                            <a href="#navpills-1" class="nav-link nav-profile button-badge active" data-toggle="tab">My Bookings
                                <?php if ($notif_mybook > 0){ ?>
                                    <span class="notif"></span>
                                <?php } ?>
                            </a>
                        </li>
                        <li class="nav-item nav-booking">
                            <a href="#navpills-2" class="nav-link nav-profile button-badge" data-toggle="tab">Tourists Bookings
                                <?php if ($notif_mytour > 0){ ?>
                                    <span class="notif-mytour"></span>
                                <?php } ?>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content br-n pn margin5">
                        <div id="navpills-1" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <?php 
                                        if (!empty($mybook)){
                                            foreach ($mybook as $val) {
                                    ?>
                                        <table class="table table-bordered border-table">
                                            <tr>
                                                <td colspan="2" class="pad-table"><?= ucwords($val->itin_title); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Guide Name</td>
                                                <td class="border-table pad-table"><?= $val->user_fname.' '.$val->user_lname; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table"># of Tourists</td>
                                                <td class="border-table pad-table"><?= $val->total_tourist; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Total Price</td>
                                                <td class="pad-table"><?= strtoupper($val->total_price_currency).' '.$val->total_price_nominal; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Date</td>
                                                <td class="border-table pad-table"><?= ($val->itin_day == 'md')?$this->public_function->format_date($val->start_date).' / '. date('Y-m-d', strtotime('+'.$val->itin_duration.' days', strtotime($val->start_date))):$this->public_function->format_date($val->start_date); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Status</td>
                                                <td class="border-table pad-table <?= ($val->book_status == 2?'pending-req':'')?>"><?= $this->public_function->status_order($val->book_status,'tourist');?></td>
                                            </tr>
                                        </table>
                                        <?php if ($val->book_status == 1 || $val->book_status == 4 || $val->book_status == 5){ ?>
                                            <a href="<?= base_url().'booking/detail/'.$val->itin_id.'/'.$val->book_id; ?>" class="btn btn-danger btn-x">View Booking</a>
                                        <?php } elseif ($val->book_status == 2){ ?>
                                            <a href="<?= base_url().'booking/detail/'.$val->itin_id.'/'.$val->book_id; ?>" class="btn btn-danger btn-x">Make Payment</a>
                                        <?php } elseif ($val->book_status == 3){ ?>
                                            <a href="<?= base_url().'booking/detail/'.$val->itin_id.'/'.$val->book_id; ?>" class="btn btn-danger btn-xr">Declined</a>
                                        <?php } ?>
                                        <hr class="hr-booking">
                                    <?php }} else { ?>
                                        <center><img src="<?= base_url().'assets/images/noresultfound.jpg';?>" class="img-responsive"></center>
                                        <br><br>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-2" class="tab-pane">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <?php 
                                        if (!empty($mytour)){
                                            foreach ($mytour as $val) {
                                    ?>
                                        <table class="table table-bordered border-table">
                                            <tr>
                                                <td colspan="2" class="pad-table"><?= ucwords($val->itin_title); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Guide Name</td>
                                                <td class="border-table pad-table"><?= $val->user_fname.' '.$val->user_lname; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table"># of Tourists</td>
                                                <td class="border-table pad-table"><?= $val->total_tourist; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Total Price</td>
                                                <td class="pad-table"><?= strtoupper($val->total_price_currency).' '.$val->total_price_nominal; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Date</td>
                                                <td class="border-table pad-table"><?= ($val->itin_day == 'md')?$this->public_function->format_date($val->start_date).' / '. date('Y-m-d', strtotime('+'.$val->itin_duration.' days', strtotime($val->start_date))):$this->public_function->format_date($val->start_date); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="border-table title-table pad-table">Status</td>
                                                <td class="border-table pad-table <?= ($val->book_status == 1?'pending-req':'')?>"><?= $this->public_function->status_order($val->book_status, 'guide');?></td>
                                            </tr>
                                        </table>
                                        <?php if ($val->book_status == 1 || $val->book_status == 4 || $val->book_status == 5){ ?>
                                            <a href="<?= base_url().'booking/reqdetail/'.$val->itin_id.'/'.$val->book_id; ?>" class="btn btn-danger btn-x">View Request</a>
                                        <?php } elseif ($val->book_status == 2){ ?>
                                            <a href="<?= base_url().'booking/reqdetail/'.$val->itin_id.'/'.$val->book_id; ?>" class="btn btn-danger btn-x">Waiting For Payment</a>
                                        <?php } elseif ($val->book_status == 3){ ?>
                                            <a href="#" class="btn btn-danger btn-xr">Declined</a>
                                        <?php } ?>
                                        <hr class="hr-booking">
                                    <?php }} else { ?>
                                        <center><img src="<?= base_url().'assets/images/noresultfound.jpg';?>" class="img-responsive"></center>
                                        <br><br>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.hr-booking{
    border: 2px solid #042441;
}
.nav-booking {
    width: 50%;
    text-align: center;
}
.title-table {
    background-color: #042441;
    color: white;
    width: 25%;
}
.pad-table {
    padding: 6px 20px !important;
}
.border-table {
    border: 2px solid #042441 !important;
}
.pending-req {
    color: red;
    font-weight: 700;
}
</style>