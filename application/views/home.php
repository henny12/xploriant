<!-- <div class="container-fluid">
    <div class="row">
        <img src="<?= base_url()?>assets/images/homepage.png" class="img-responsive img-home">
    </div>
</div> -->
<!-- <div class="row car-slider">
    <div class="col-lg-12">          
        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators2" data-slide-to="0"></li>
                    <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators2" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item">
                        <img class="img-responsive" src="<?= base_url().'assets/images/slider/slider1.jpg'; ?>" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3>kdfhkfdjghfkg</h3>
                            <p>kdfhkfdjghfkg</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="img-responsive" src="<?= base_url().'assets/images/slider/slider2.jpg'; ?>" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>...</h5>
                            <p>...</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="img-responsive" src="<?= base_url().'assets/images/slider/slider3.jpg'; ?>" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>...</h5>
                            <p>...</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="img-responsive" src="<?= base_url().'assets/images/slider/slider4.jpg'; ?>" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>...</h5>
                            <p>...</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>     
    </div>
</div> -->
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-top: 35px;">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-12 margin1">
            <h3>Top Destinations</h3>
        </div>
    </div>
    <div class="row">
        <?php if (!empty($top_desti)){ 
            foreach ($top_desti as $td){ ?>
                <div class="col-md-2 margin2">
                    <a href="<?= base_url().'itinerary/top_destination/'.$td->td_title; ?>"><img src="<?=base_url().'assets/images/top_destination/'.$td->td_image; ?>" class="img-responsive img-border">
                    <p class="text-topdest"><?= strtoupper($td->td_title); ?></p></a>
                </div>
        <?php }} ?>
        <div class="col-md-2 margin2">
            <img src="<?=base_url()?>assets/images/top_destination/black.png" class="img-responsive img-border">
            <p class="text-topdest">MORE CITIES</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 margin4">
            <h3>New Xplorations</h3>
        </div>
    </div>
    <div class="row">
        <?php if(!empty($itinerary)){
            foreach($itinerary as $val){ ?>
            <div class="col-md-2 margin8">
                <a href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>">
                    <?php if(!empty($val->itin_thumb)){ ?>
                        <img src="<?=base_url().'media/itin_thumb/'.$val->itin_thumb; ?>" class="img-responsive img-border">
                    <?php }else{ ?>
                        <img src="<?=base_url().'media/itin_thumb/no-image.png';?>" class="img-responsive img-border">
                    <?php } ?>
                </a>
                <p class="text1"><?= ucwords($val->itin_location); ?> <br> <?= ($val->itin_day == 'sd')?($val->itin_duration).' Hours':($val->itin_accom_night+1).'D/'.$val->itin_accom_night.'N'; ?></p>
                <a class="l-title" href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>"><p class="text2"><?= ucwords($val->itin_title); ?></p></a>
                <p class="text3">IDR <?= number_format($val->itin_price_nominal,0,',','.'); ?>/person</p>

                <span class="pull-left">
                    <?php
                        $starNumber = $val->itin_avg_ratings;
                        for($x=1;$x<=$starNumber;$x++) {
                            echo '<img src='.base_url()."assets/images/fullstar.png".' class="img-star" />';
                        }
                        if (strpos($starNumber,'.')) {
                            echo '<img src='.base_url()."assets/images/halfstar.png".' class="img-star" />';
                            $x++;
                        }
                        while ($x<=5) {
                            echo '<img src='.base_url()."assets/images/emptystar.png".' class="img-star" />';
                            $x++;
                        }
                    ?>
                </span>
                <br>
                <p class="text-review"><span class="text-rate"><?= (empty($val->itin_avg_ratings))?0:$val->itin_avg_ratings;?></span> (<?= $val->total_review; ?> Reviews)</p>
            </div>
        <?php }} ?>
    </div>
</div>
<br><br><br><br>