<div class="container">
    <div class="row margin3">
        <div class="col-md-3"></div>
        <div class="col-md-6 box-sign">
            <h3 align="center"><b>Xploriant - Sign Up</b></h3>
            <br>
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
            <form action="<?= base_url(); ?>auth/register" method="post">
                <input type="hidden" class="type" value="su">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email Address" required>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" name="fname" class="form-control" placeholder="First Name" required>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-user-circle-o"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-user-circle-o"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control password" placeholder="Password" id="su-password" required>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="password" class="form-control confirm_password" placeholder="Confirm Password" id="su-confirm" required>
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <span>
                        <b>Birthday</b><br>
                        You must be 17 or older to sign up.<br>
                        Your birthday will not be visible to other users.<br><br>
                    </span>
                    <div class="input-group">
                        <div class="dropdate">
                            <input type="hidden" id="su-dob" name="dob">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" name="register" class="btn btn-danger btn-x" value="Sign Up">
                </div>
            </form>
            <hr>
            <p align="center">Already a member? <a href="#login-modal" data-toggle="modal">Login Now</a></p>
        </div>
    </div>
    <br><br>
</div>
<style type="text/css">

</style>