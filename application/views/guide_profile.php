<div class="container-fluid">
    <div class="row">
        <img src="<?= base_url()?>assets/images/profilepage.png" class="img-responsive img-home">
        <img src="<?= (!empty($user->user_photo))?base_url().'media/profpic_thumb/'. $user->user_photo:base_url().'media/profpic_thumb/default.png'; ?>" class="photoprofile">
        <p class="nameprofile" align="center"><?= $user->user_fname .' '. $user->user_lname; ?></p>
        <p class="locprofile" align="center"><?= $user->user_residence; ?></p>
    </div>
</div>
<div class="container">
    <div class="row margin1">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills m-b-30">
                        <li class="nav-item" style="width: 33%">
                            <a href="#navpills-1" class="nav-link nav-profile active" data-toggle="tab"><center>About</center></a>
                        </li>
                        <li class="nav-item" style="width: 33%">
                            <a href="#navpills-2" class="nav-link nav-profile" data-toggle="tab"><center>Itineraries</center></a>
                        </li>
                        <li class="nav-item" style="width: 33%">
                            <a href="#navpills-3" class="nav-link nav-profile" data-toggle="tab"><center>Reviews</center></a>
                        </li>
                    </ul>
                    <div class="tab-content br-n pn margin5">
                        <div id="navpills-1" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="<?= base_url(); ?>myprofile/edit_account/<?= $user->user_id?>" method="post">
                                        <input type="hidden" name="">
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-user-circle-o"></i></span>
                                                </div>
                                                <textarea class="form-control desc" name="desc" placeholder="Personal Description" rows="5" disabled><?= $user->user_desc; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-heart"></i></span>
                                                </div>
                                                <input type="text" name="lname" class="form-control lname" placeholder="Personal Interest" value="<?= $user->user_interests?>" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-language"></i></span>
                                                </div>
                                                <input type="text" name="language" class="form-control language" placeholder="Language" value="<?= $user->user_lang; ?>" disabled>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="navpills-2" class="tab-pane">
                            <div class="row">
                                <?php if(!empty($itinerary)){
                                    foreach($itinerary as $val){ ?>
                                        <div class="col-md-3 margin2">
                                            <a href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>">
                                                <?php if(!empty($val->itin_thumb)){ ?>
                                                    <img src="<?=base_url().'media/itin_thumb/'.$val->itin_thumb; ?>" class="img-responsive img-border">
                                                <?php }else{ ?>
                                                    <img src="<?=base_url().'media/itin_thumb/no-image.png';?>" class="img-responsive img-border">
                                                <?php } ?>
                                            </a>
                                            <p class="text1"><?= ucwords($val->itin_location); ?> - <?= ($val->itin_day == 'sd')?($val->itin_duration).' Hours':($val->itin_accom_night+1).'D/'.$val->itin_accom_night.'N'; ?></p>
                                            <a class="l-title" href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>"><p class="text2"><?= ucwords($val->itin_title); ?></p></a>
                                            <p class="text3">IDR <?= number_format($val->itin_price_nominal,0,',','.'); ?>/person</p>

                                            <span class="pull-left">
                                                <?php
                                                    $starNumber = 2.2;
                                                    for($x=1;$x<=$starNumber;$x++) {
                                                        echo '<img src='.base_url()."assets/images/fullstar.png".' class="img-star" />';
                                                    }
                                                    if (strpos($starNumber,'.')) {
                                                        echo '<img src='.base_url()."assets/images/halfstar.png".' class="img-star" />';
                                                        $x++;
                                                    }
                                                    while ($x<=5) {
                                                        echo '<img src='.base_url()."assets/images/emptystar.png".' class="img-star" />';
                                                        $x++;
                                                    }
                                                ?>
                                            </span>
                                            <span class="text-rate"><?= (empty($val->itin_avg_ratings))?0:$val->itin_avg_ratings;?></span>
                                            <p class="text-review">(17 Reviews)</p>
                                        </div>
                                <?php }} ?>
                            </div>
                        </div>
                        <div id="navpills-3" class="tab-pane">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4><?= count($review); ?> Review(s)</h4>
                                </div>
                                <div class="col-md-9">
                                    <?php if (!empty($review)){?>
                                        <span class="pull-left">
                                            <?php
                                                $starNumber = 2.2;
                                                for($x=1;$x<=$starNumber;$x++) {
                                                    echo '<img src='.base_url()."assets/images/fullstar.png".' class="img-star-review" />';
                                                }
                                                if (strpos($starNumber,'.')) {
                                                    echo '<img src='.base_url()."assets/images/halfstar.png".' class="img-star-review" />';
                                                    $x++;
                                                }
                                                while ($x<=5) {
                                                    echo '<img src='.base_url()."assets/images/emptystar.png".' class="img-star-review" />';
                                                    $x++;
                                                }
                                            ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <?php if (!empty($review)){ 
                                    foreach ($review as $val){ ?>
                                        <div class="col-md-2">
                                            <img src="<?= (!empty($val->user_photo))?base_url().'media/profpic_thumb/'. $user->user_photo:base_url().'media/profpic_thumb/default.png'; ?>" class="foto-reviewer">    
                                        </div>
                                        <div class="col-md-10">
                                            <b><?= $val->user_fname.' '. $val->user_lname; ?></b>
                                            <p><?= $val->review ?></p>
                                        </div>
                                <?php } } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
.hr-prof{
    border: 2px solid #042441;
}
.prof-text1{
    color: #042441;
    margin-bottom: 5px;
    font-weight: 400;
    text-align: center;
}
.foto-reviewer{
    width: 60px;
    border-radius: 50%;
}
.photoprofile{
    width: 120px;
    height: 120px;
    position: absolute;
    top: 125px;
    left: 0px;
    right: 0px;
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-radius: 50%;
}
.prof-pic{
    width: 120px;
    height: 120px;
}
.nameprofile{
    position: absolute;
    top: 280px;
    left: 0px;
    right: 0px;
    color: white;
    font-size: 17pt;
    font-weight: 400;
}
.locprofile{
    position: absolute;
    top: 310px;
    left: 0px;
    right: 0px;
    color: white;
    font-size: 10pt;
}
.btn-profpic{
    width: 120px;
    position: absolute !important;
    top: 255px;
    left: 0px;
    right: 0px;
    display: block;
    margin-left: auto;
    margin-right: auto;
}
.btn-file {
    /*position: relative;
    overflow: hidden;*/
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;   
    cursor: inherit;
    display: block;
}
.btn-aktif {
    background-color: #042441 !important;
    color: #fff !important;
    border-radius: 0 !important;
}
</style>