<div class="container margin7">
    <div class="row">
        <div class="col-md-12">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <p class="bs-text1">Booking Review</p>
            <h3 class="bs-title"><?= ucwords($itin->itin_title); ?></h3>
            <p class="bs-text1">By <a href="#" class="bs-link"><?= $guide->user_fname.' '.$guide->user_lname; ?></a></p>
            <br>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/location.png')?>" class="bs-icon"> <?= ucwords($itin->itin_location)?>
            </p>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/user2.png')?>" class="bs-icon"> <?= $book->total_tourist; ?> Tourists
            </p>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/calender.png')?>" class="bs-icon"> <?= date('d/m/Y', strtotime($book->start_date)); ?> <?= (!empty($book->end_date))?'- '. date('d/m/Y', strtotime($book->end_date)):''; ?>
            </p>
            <br>
            <p class="bs-text2">
                <img src="<?= base_url('assets/images/icon/money.png')?>" class="bs-icon"> <?= strtoupper($book->total_price_currency).' '.number_format($book->total_price_nominal,0,'.',','); ?>
            </p>
            <p class="bs-text2">
                Booking Code : <?= $book->book_code; ?><br>
            </p>
            <br>
            <form method="post" action="<?= base_url().'booking/send_review'; ?>">
                <input type="hidden" name="book_id" value="<?= $book->book_id; ?>">
                <input type="hidden" name="review_as" value="tourist">
                <input type="hidden" name="user_id" value="<?= $myid; ?>">
                <input type="hidden" name="review_status" value="1">
                <input type="hidden" name="review_to" value="<?= $guide->user_id; ?>">
                <input type="hidden" name="itin_id" value="<?= $book->itin_id; ?>">
                <input type="hidden" name="role_type" value="<?= $role_type; ?>">
                <div class="card bs-card">
                    <div class="card-body">
                        <h3>Tell us about your Xploration</h3>
                        <div class="row">
                            <div class="col-md-2">   
                                <img src="<?= (!empty($guide->user_photo))?base_url().'media/profpic_thumb/'. $guide->user_photo:base_url().'assets/images/icon/user.png'; ?>" class="foto-profil">
                            </div>
                            <div class="col-md-5">
                                    <?= ($role_type == 1)?'Your Guide :':'Your Travelers :'; ?><br>
                                    <a href="<?= base_url().'guide/profile/'.$guide->user_id; ?>" style="color: #727577"><span class="nama-profil"><?= $guide->user_fname.' '.$guide->user_lname; ?></span></a><br>
                            </div>
                            <div class="col-md-5"></div>
                            <div class="col-md-8">
                                <div id="minrate">
                                    <div class="rate"><input id="rating-input" type="text" name="rating" title="" required value="<?= @$review->rating; ?>" /></div>
                                </div>
                            </div>
                        </div>
                        <textarea class="form-control bs-ta" name="review" placeholder="Your Review" rows="6" required <?= (!empty($review))?'readonly':''; ?>><?= @$review->review; ?></textarea>
                    </div>
                </div>
                <?php if (empty($review)) { ?>
                    <div class="form-group">
                        <input type="submit" class="btn btn-danger btn-x" value="Submit Review">
                    </div>
                <?php } ?>
                    <div class="form-group">
                        <a href="<?= base_url().''; ?>" class="btn btn-danger btn-x">Back to main menu</a>
                    </div>
            </form>
        </div>
    </div>
</div>
<style type="text/css">
    .bs-ta{
        border: 2px solid #36648e;
        border-radius: 0;
    }
    .bs-card{
        border-radius: 0;
        border-bottom: 1px solid rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 5px 2px rgba(0, 0, 0, 0.15);
    }
    .bs-text2{
        color: #042441;
        margin-bottom: 5px;
        font-weight: 400;
    }
    .bs-text1{
        color: #042441;
        margin-bottom: 5px;
        font-weight: 600;
    }
    .bs-link{
        color: red;
    }
    .bs-title{
        font-weight: bold;
        font-size: 35px;
        margin-top: 5px;
        margin-bottom: 5px;
        line-height: 35px;
    }
    .bs-icon{
        width: 20px;
        margin-right: 5px;
    }
</style>