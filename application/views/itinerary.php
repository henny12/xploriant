<div class="container container-itin">
    <div class="row margin6">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <center><h2>Itinerary Creator</h2></center>
            <hr class="hr-guide">
        </div>
    </div>
    <form method="post" action="<?= base_url()?>guide/add_itinerary">
        <input type="hidden" name="day" class="form-control" value="<?= $day; ?>">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Where are we going ?</h4>
                        <input type="text" name="location" class="form-control" placeholder="Location Format : City, Country (eg. Bali, Indonesia)" required>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Categories</h4>
                        <div class="row">
                            <?php if(!empty($personal_interest_list)){
                                foreach ($personal_interest_list as $pe) {  ?>
                                <div class="col-md-3">
                                    <span class="button-checkbox">
                                        <button type="button" class="btn btn-opt" data-color="primary"><?= ucwords($pe->pe_name); ?></button>
                                        <input type="checkbox" class="hidden" name="category[]" value="<?= $pe->pe_id; ?>" />
                                    </span>
                                </div>
                            <?php }} ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Add Title</h4>
                        <input type="text" name="title" class="form-control" placeholder="Title" required>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Add Description</h4>
                        <textarea class="form-control" name="overview" placeholder="Description" required></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Duration</h4>
                            <div class="row">
                                <div class=" col-md-11">
                                    <input class="tch3" id="duration" type="text" value="0" name="duration" data-bts-button-down-class="btn btn-secondary btn-outline" data-bts-button-up-class="btn btn-secondary btn-outline" required> 
                                </div>
                                <div class="col-md-1"><p class="label-itin"><?= ($day == 'md')?'Day(s)':'Hours'; ?></p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Travelers</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Minimum</label>
                                    <input class="tch3" type="text" id="mintourist" value="0" name="min_tourist" data-bts-button-down-class="btn btn-secondary btn-outline" data-bts-button-up-class="btn btn-secondary btn-outline" required readonly>
                                </div>
                                <div class="col-md-6">
                                    <label>Maximum</label>
                                    <input class="tch3" type="text" id="maxtourist" value="0" name="max_tourist" data-bts-button-down-class="btn btn-secondary btn-outline btn-min-for-maxtour" data-bts-button-up-class="btn btn-secondary btn-outline" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Places We Go</h4>
                            <table class="table-added">
                                <tbody id="data_place"></tbody>
                            </table>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Add Place" id="place" name="place">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="placewego">+</button>
                                </div>
                            </div>
                            <!-- <div id="errorplace" class="alert alert-danger">Place must be filled in.</div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Photos</h4>
                            <div class="dropzone">
                                <div class="dz-message" style="display: none;">
                                    <!-- <h3>Drop files here or click to upload</h3> -->

                                </div>
                                <a id="testbutton"><img src="<?= base_url().'assets/images/icon/add.png'?>" class="add-image"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Upload Thumbnail</h4>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-secondary btn-file"> 
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="hidden"><input type="file" name="file_photo" class="item-img file" required>
                                </span>
                            </div>
                            <figure id="fig">
                                <img src="" class="img-thumbnail" id="item-img-output" />
                            </figure>
                            <input type="hidden" name="thumb" id="thumb">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Meals</h4>
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-opt" data-color="primary">Breakfast</button>
                                <input type="checkbox" class="hidden" name="meals[]" value="breakfast" />
                            </span>
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-opt" data-color="primary">Lunch</button>
                                <input type="checkbox" class="hidden" name="meals[]" value="lunch" />
                            </span>
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-opt" data-color="primary">Dinner</button>
                                <input type="checkbox" class="hidden" name="meals[]" value="lunch" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Included Fees</h4>
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-opt" data-color="primary">Entrance Tickets</button>
                                <input type="checkbox" class="hidden" name="fees[]" value="ticket"/>
                            </span>
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-opt" data-color="primary">Transportation</button>
                                <input type="checkbox" class="hidden" name="fees[]" value="transport"/>
                            </span>
                            <span class="button-checkbox">
                                <button type="button" class="btn btn-opt" data-color="primary">Insurance</button>
                                <input type="checkbox" class="hidden" name="fees[]" value="insurance"/>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>What To Bring</h4>
                            <table class="table-added">
                                <tbody id="data_tobring"></tbody>
                            </table>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Add What To Bring" id="tobring" name="tobring">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" id="whattobring" type="button">+</button>
                                </div>
                            </div>
                            <!-- <div id="errorwtb" class="alert alert-danger">What to bring must be filled in.</div>  -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4>Detailed Itinerary</h4>
                        <?php if($day == 'sd'){ ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td width="20%">
                                        <input class="form-control" type="time" name="start_time[]">
                                    </td>
                                    <td width="20%">
                                        <input class="form-control" type="time" name="end_time[]">
                                    </td>
                                    <td><input type="text" class="form-control" name="activity_title[]" placeholder="Activity Title"></td>
                                    <td width="8%" rowspan="2" class="td-remove"><button class="btn remove-table" type="button">-</button></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><textarea class="form-control" name="activity_desc[]" placeholder="Add Description (option)"></textarea></td>
                                </tr>
                            </table>
                            <div id="data_activity"></div>
                            <button type="button" class="btn btn-primary btn-x" id="addactivity"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Activity</button>
                        <?php } else { ?>
                            <div id="data_activitymd">
                                <table class="table table-bordered" id="">
                                    <tr>
                                        <td width="15%">
                                            <input class="vertical-spin dday" type="text" data-bts-button-down-class="btn btn-secondary btn-outline btn-min-for-maxday" data-bts-button-up-class="btn btn-secondary btn-outline" placeholder="Day" name="activity_day[]" data-con="1" readonly>
                                        </td>
                                        <td width="15%">
                                            <input class="form-control" type="time" name="start_time[]">
                                        </td>
                                        <td width="15%">
                                            <input class="form-control" type="time" name="end_time[]">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="activity_title[]" placeholder="Activity Title">
                                        </td>
                                        <td width="8%" rowspan="2" class="td-remove"><button class="btn remove-table" type="button">-</button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><textarea class="form-control" name="activity_desc[]" placeholder="Add Description (option)"></textarea></td>
                                    </tr>
                                </table>
                            </div>
                            <button type="button" class="btn btn-primary btn-x" id="addactivitymd"><i class="fa fa-plus-circle" aria-hidden="true"></i> New Activity</button>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if($day == 'md'){ ?>
            <div class=" col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Accomodations</h4>
                            <div class="row" id="night">
                                <div class=" col-md-10">
                                    <input class="tch3" id="accom_night" type="text" value="0" name="accom_night" data-bts-button-down-class="btn btn-secondary btn-outline" data-bts-button-up-class="btn btn-secondary btn-outline"> 
                                </div>
                                <div class="col-md-2"><p class="label-itin">Night</p></div>
                            </div>
                            <br>
                            <div class="row" id="desc">
                                <div class=" col-md-12">
                                    <input type="text" class="form-control" id="accom_desc" name="accom_desc" placeholder="Name or Description"><br><br>
                                </div>
                            </div>
                            <div class="row" id="minrate">
                                <div class="col-md-12">
                                    <div class="rate">
                                        <center>
                                            Minimum Rating<br>
                                            <input id="rating-input" type="text" name="accom_min_rate" title=""/>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="demo-checkbox">
                                <input type="checkbox" id="basic_checkbox_1" class="filled-in" value="true" name="accom_included" />
                                <label for="basic_checkbox_1">Not Included</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <p align="center">Not Recommended for tourists...</p>
                            <div class="row">
                                <?php if(!empty($not_recommended_list)){
                                    foreach($not_recommended_list as $nr){?>
                                    <div class="col-md-6">
                                        <span class="button-checkbox">
                                            <button type="button" class="btn btn-opt" data-color="primary"><?= $nr->nr_title; ?></button>
                                            <input type="checkbox" class="hidden" name="not_recomm[]" value="<?= $nr->nr_id; ?>" />
                                        </span>        
                                    </div>
                                <?php }} ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Cancellation</h4>
                            <div class="row">
                                <div class=" col-md-10">
                                    <input class="tch3" type="text" value="0" data-bts-button-down-class="btn btn-secondary btn-outline" data-bts-button-up-class="btn btn-secondary btn-outline" name="cancellation"> 
                                </div>
                                <div class="col-md-2"><p class="label-itin">Days</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <h4>Price/<small>tourist</small></h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <select class="form-control" name="currency">
                                        <option value="idr">IDR</option>
                                        <option value="usd">USD</option>
                                    </select>
                                </div>
                                <div class="col-md-8">
                                    <input type="number" name="price" class="form-control" placeholder="Enter Price" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <input type="submit" value="Submit Itinerary" class="btn btn-danger btn-x">
                <br><br>
                <center><a href="#">Cancel</a></center><br>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    
</script>
<style type="text/css">
    .container-itin{
        padding: 0 90px;
    }
    .option-meals{
        width: 100%;
        margin-bottom:10px;
    }
    .option-meals2{
        width: 50%;
        margin-bottom:10px;
    }
    .table-added{
        margin-bottom: 10px;
        width: 100%;
    }
    .label-itin{
        margin-top: 8px;
    }
    .rate{
        border: 1px solid #042441;
        padding-top: 20px;
        padding-bottom: 20px;
        margin-bottom: 20px;
    }
    .td-remove{
        vertical-align: middle !important;
        text-align: center;
        background-color: #042441;
    }
    .remove-table {
        font-size:25pt;
        font-weight:bold;
        color: white !important;
    }
    .btn-outline-secondary {
        background-color: #545b62;
        color: white;
    }
    #data_activitymd .bootstrap-touchspin {
        width: 85%;
    }
    .add-image {
        margin: 13px 4px;
        max-width: 120px;
    }
</style>



