<div class="container">
    <div class="row">
        <div class="col-md-11 margin7">
            <?php $key = (object) $this->session->userdata('filter_itin'); ?>
            <h3>Xplore <?= ucfirst($key->itin_location); ?></h3>
        </div>
    </div>
    <div class="row">
        <?php if(!empty($itinerary)){
            foreach($itinerary as $val){ ?>
            <div class="col-md-2 margin2">
                <a href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>">
                    <?php if(!empty($val->itin_thumb)){ ?>
                        <img src="<?=base_url().'media/itin_thumb/'.$val->itin_thumb; ?>" class="img-responsive img-border">
                    <?php }else{ ?>
                        <img src="<?=base_url().'media/itin_thumb/no-image.png';?>" class="img-responsive img-border">
                    <?php } ?>
                </a>
                <p class="text1"><?= ucwords($val->itin_location); ?> - <?= ($val->itin_day == 'sd')?($val->itin_duration).' Hours':($val->itin_accom_night+1).'D/'.$val->itin_accom_night.'N'; ?></p>
                <a class="l-title" href="<?= base_url().'itinerary/detail/'.$val->itin_id; ?>"><p class="text2"><?= ucwords($val->itin_title); ?></p></a>
                <p class="text3">IDR <?= number_format($val->itin_price_nominal,0,',','.'); ?>/person</p>

                <span class="pull-left">
                    <?php
                        $starNumber = $val->itin_avg_ratings;
                        for($x=1;$x<=$starNumber;$x++) {
                            echo '<img src='.base_url()."assets/images/fullstar.png".' class="img-star" />';
                        }
                        if (strpos($starNumber,'.')) {
                            echo '<img src='.base_url()."assets/images/halfstar.png".' class="img-star" />';
                            $x++;
                        }
                        while ($x<=5) {
                            echo '<img src='.base_url()."assets/images/emptystar.png".' class="img-star" />';
                            $x++;
                        }
                    ?>
                </span>
                <br>
                <p class="text-review"><span class="text-rate"><?= (empty($val->itin_avg_ratings))?0:$val->itin_avg_ratings;?></span> (<?= $val->total_review; ?> Reviews)</p>
            </div>
        <?php }}else{ ?>
            <div class="col-md-12">
                <center><img src="<?= base_url().'assets/images/noresultfound.jpg';?>" ><br><a href="<?= base_url(); ?>" class="btn btn-primary">Back To Home</a></center>
                <br><br>
            </div>
        <?php }?>
    </div>
</div>
<style type="text/css">
    .l-title{
        color: #67757c;
    }
</style>