<section id='modal'>
    <!--Login Modal -->
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title-modal">Welcome To Xploriant</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-1 col-md-offset-1">
                            <form action="<?php echo base_url(); ?>auth/login" method="post">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="email" class="form-control" placeholder="Email Address" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="password" id="password2" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <div class="demo-checkbox">
                                            <input type="checkbox" id="basic_checkbox_3" class="filled-in" name="remember_me" />
                                            <label for="basic_checkbox_3">Remember Me</label>
                                        </div>
                                    </label>
                                    <label class="checkbox-inline pull-right">
                                        <div class="demo-checkbox">
                                            <input type="checkbox" id="basic_checkbox_4" class="filled-in" name="show_password" onclick="show_pass();" />
                                            <label for="basic_checkbox_4">Show Password</label>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="login" class="btn btn-danger btn-x" value="Login">
                                </div>
                                <div class="form-group">
                                    <center><a href="#forgotpassword-modal" data-toggle="modal">Forgot password?</a></center>
                                </div>
                            </form>
                            <hr>
                            <p align="center">Don't have an account ? <a href="#register-modal" data-toggle="modal">Sign Up Now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Register Modal -->
    <div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title-modal">Xploriant - Sign Up</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-1 col-md-offset-1">
                            <form action="<?= base_url(); ?>auth/register" method="post">
                                <input type="hidden" class="type" value="sum">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="email" class="form-control" placeholder="Email Address" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="fname" class="form-control" placeholder="First Name" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-user-circle-o"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-user-circle-o"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="password" name="password" class="form-control" placeholder="Password" id="sum-password" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="password" class="form-control" placeholder="Confirm Password" id="sum-confirm" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span>
                                        <b>Birthday</b><br>
                                        You must be 17 or older to sign up.<br>
                                        Your birthday will not be visible to other users.<br><br>
                                    </span>
                                    <div class="input-group">
                                        <div class="dropdate">
                                            <input type="hidden" id="sum-dob" name="dob">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="register" class="btn btn-danger btn-x" value="Sign Up">
                                </div>
                            </form>
                            <hr>
                            <p align="center">Already a member? <a href="#login-modal" data-toggle="modal">Login Now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Forgot Password Modal -->
    <div class="modal fade" id="forgotpassword-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title-modal">Forgot Password</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-1 col-md-offset-1">
                            <form action="<?php echo base_url(); ?>auth/forgot_password" method="post">
                                <p>Enter your email address and we'll send you the instructions to reset your password.</p>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" name="email" class="form-control" placeholder="Email Address" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="login" class="btn btn-danger btn-x" value="Send">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title-modal">Delete Data</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
                </div>
                <div class="modal-body">
                  <p class="text-center"><i class="fa fa-exclamation-triangle fa-4x"></i></p>
                  <p class="text-center">There are still bookings to this Itinerary. All bookings to this Itinerary will be suspended and you will not receive any payout.</p>
                  <p class="text-center"><strong>Are you sure you want to remove this Itinerary?</strong></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-primary danger">Yes, Delete Data</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Cancel Modal -->
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title-modal">Cancel Booking</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
                </div>
                <div class="modal-body">
                  <p class="text-center"><i class="fa fa-exclamation-triangle fa-4x"></i></p>
                  <p class="text-center"><strong>Are you sure you want to cancel this booking ?</strong></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-primary danger">Yes, Cancel Booking</button>
                </div>
            </div>
        </div>
    </div>
    <!--Croping Image Modal -->
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="max-width: 435px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"> </h4>
                </div>
                <div class="modal-body">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Crop and Upload</button>
                </div>
            </div>
        </div>
    </div>
    <!--Enter Code Modal -->
    <div class="modal fade" id="starttour-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title-modal">Enter Code</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-1 col-md-offset-1">
                            <form action="<?php echo base_url(); ?>booking/start_tour" method="post">
                                <p>Enter the code from the tourist to start the tour.</p>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="hidden" name="book_id">
                                        <input type="hidden" name="itin_id">
                                        <input type="text" name="code" class="form-control" placeholder="Enter Code">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-qrcode"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="login" class="btn btn-danger btn-x" value="Start Tour">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>