<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: #042441 !important">
    <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url().'assets/images/xnegative.png'?>" width="160"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="col-md-6 search-bar">
        <form  method="post" action="<?= base_url() . 'itinerary/search'; ?>">
            <?php $key = (object) $this->session->userdata('filter_itin'); ?>
            <div class="input-group">
                <input type="text" class="form-control my-0 py-1 white-border" name="location" placeholder="Search Destination">
                <div class="input-group-append">
                    <input type="hidden" id="search" name="search" value="true">
                    <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-search text-grey" aria-hidden="true"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <?php if (!$this->session->userdata('logged_in')) {?>
                <li class="nav-item">
                    <a class="nav-link" href="#register-modal" data-toggle="modal">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#login-modal" data-toggle="modal">Log In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Help</a>
                </li>
            <?php } else { ?>
                <li class="nav-item menu-top">
                    <a class="nav-link active" href="<?= base_url()?>guide">Be a Guide</a>
                </li>
                <li class="nav-item menu-top">
                    <a class="nav-link button-badge" href="<?= base_url()?>messages">Messages
                        <?php if ($notif_message > 0){ ?>
                            <span class="notif"></span>
                        <?php } ?>
                    </a>
                </li>
                <li class="nav-item menu-top">
                    <a class="nav-link" href="#">Help</a>
                </li>
                <li class="nav-item">
                    <div class="dropdown">
                      <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?= base_url()?>assets/images/icon/user.png" width="30">
                      </a>
                      <div class="dropdown-menu drop-profile" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="<?= base_url()?>myprofile">My Profile</a>
                        <a class="dropdown-item" href="<?= base_url()?>guide">Be a Guide</a>
                        <a class="dropdown-item button-badge" href="<?= base_url()?>booking/mybook">Bookings 
                            <?php if (@$notif_booking > 0){ ?>
                                <span class="notif-book"></span>
                            <?php } ?>
                        </a>
                        <a class="dropdown-item" href="#">Invite Friends</a>
                        <a class="dropdown-item" href="<?= base_url()?>auth/logout">Logout</a>
                      </div>
                    </div>
                </li>
            <?php } ?>
            
        </ul>
    </div>
</nav>