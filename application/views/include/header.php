<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="<?= base_url().'assets/images/icon/xicon.png'?>">
    <title>Xploriant</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Fonts and icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/custom/css/app.css">
    <!-- Material Pro -->
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/material-pro/material/css/style.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/material-pro/material/css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- page CSS -->
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet"/>
    <link href="<?= base_url(); ?>assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet"/>
    <link href="<?= base_url(); ?>assets/plugins/rating/css/star-rating.css" rel="stylesheet"/>
    <link href="<?= base_url(); ?>assets/plugins/rating/css/bootstrap.css" rel="stylesheet"/>
    <link rel='stylesheet prefetch' href='https://foliotek.github.io/Croppie/croppie.css'>
    <link href="<?=base_url('public')?>/plugins/timepicker/bootstrap-timepicker.css"/>
    <link rel="stylesheet" href="<?=base_url('public')?>/dist/css/AdminLTE.min.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" /> -->
</head>
