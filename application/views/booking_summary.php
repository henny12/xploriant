<div class="container margin7">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form action="<?= base_url().'booking/add'; ?>" method="post">
                <input type="hidden" name="book_code" value="<?= $code; ?>">
                <input type="hidden" name="itin_id" value="<?= $itin->itin_id; ?>">
                <input type="hidden" name="guide_id" value="<?= $guide->user_id; ?>">
                <input type="hidden" name="start_date" value="<?= $book['start_date']; ?>">
                <input type="hidden" name="duration" value="<?= $itin->itin_duration; ?>">
                <input type="hidden" name="itin_day" value="<?= $itin->itin_day; ?>">
                <input type="hidden" name="total_tourist" value="<?= $book['jmltourist']; ?>">
                <input type="hidden" name="total_price_currency" value="<?= $itin->itin_price_currency; ?>">
                <input type="hidden" name="total_price_nominal" value="<?= $book['total_price']; ?>">

                <p class="bs-text1">Booking Request</p>
                <h3 class="bs-title"><?= $itin->itin_title; ?></h3>
                <p class="bs-text1">By <a href="#" class="bs-link"><?= $guide->user_fname.' '.$guide->user_lname; ?></a></p>
                <br>
                <p class="bs-text2">
                    <img src="<?= base_url('assets/images/icon/location.png')?>" class="bs-icon"> <?= ucwords($itin->itin_location)?>
                </p>
                <p class="bs-text2">
                    <img src="<?= base_url('assets/images/icon/user2.png')?>" class="bs-icon"> <?= $book['jmltourist']; ?> Tourists
                </p>
                <p class="bs-text2">
                    <img src="<?= base_url('assets/images/icon/calender.png')?>" class="bs-icon"> <?= date('d/m/Y', strtotime($book['start_date'])); ?> <?= (!empty($book['end_date']))?'- '. date('d/m/Y', strtotime($book['end_date'])):''; ?>
                </p>
                <br>
                <p class="bs-text2">
                    <img src="<?= base_url('assets/images/icon/money.png')?>" class="bs-icon"> <?= strtoupper($itin->itin_price_currency).' '.number_format($book['total_price'],0,'.',','); ?>
                </p>
                <p class="bs-text2">
                    Booking Code : <?= $code; ?>
                </p>
                <br>
                <div class="card bs-card">
                    <div class="card-body">
                        <h3>Introduce yourself to your Guide.</h3>
                        <p>
                            Tell them why you are coming and who is coming with you.
                            Your Guide will review your request before you can pay and complete the reservation process.
                        </p>
                        <textarea class="form-control bs-ta" name="book_request" placeholder="Add Descriptions Here" rows="6"></textarea>
                        
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-danger btn-x" value="Submit Request">
                </div>
            </form>
        </div>
    </div>
</div>
<style type="text/css">
    .bs-ta{
        border: 2px solid #36648e;
        border-radius: 0;
    }
    .bs-card{
        border-radius: 0;
        border-bottom: 1px solid rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 5px 2px rgba(0, 0, 0, 0.15);
    }
    .bs-text2{
        color: #042441;
        margin-bottom: 5px;
        font-weight: 400;
    }
    .bs-text1{
        color: #042441;
        margin-bottom: 5px;
        font-weight: 600;
    }
    .bs-link{
        color: red;
    }
    .bs-title{
        font-weight: bold;
        font-size: 35px;
        margin-top: 5px;
        margin-bottom: 5px;
        line-height: 35px;
    }
    .bs-icon{
        width: 20px;
        margin-right: 5px;
    }
</style>