<div class="container margin7">
    <div class="row">
        <div class="col-md-8">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>

            <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php for($i=0;$i<count($itin_image);$i++){ ?>
                        <li data-target="#carouselExampleIndicators2" data-slide-to="<?= $i ?>"></li>
                    <?php } ?>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <?php foreach($itin_image as $val){ ?>
                        <div class="carousel-item">
                            <img class="img-responsive" src="<?= base_url('media/itin_image/'.$val->itin_img_name); ?>" alt="First slide">
                        </div>
                    <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <h3 class="itin-title"><?= $itinerary->itin_title; ?></h3>
                <h3>Overview</h3>
                <p align="justify"><?= $itinerary->itin_overview; ?></p>
            <hr class="hr-border">  
                <h3>About Your Guide</h3>
                <div class="row">
                    <div class="col-md-2">
                        <img src="<?= base_url('assets/images/icon/user.png')?>" class="foto-profil">    
                    </div>
                    <div class="col-md-5">
                        <span class="nama-profil"><?= $guide->user_fname.' '.$guide->user_lname; ?></span><br>   
                        <a href="#" class="contact">Contact Guide</a> 
                    </div>
                </div>
                <p><?= $guide->user_desc; ?></p>
            <hr class="hr-border">
                <h3>Place We Go</h3>
                <p><?= str_replace('-',', ',$itinerary->itin_places); ?></p>
            <hr class="hr-border">
                <h3>What To Bring</h3>
                <p><?= str_replace('-',', ',$itinerary->itin_what_to_bring); ?></p>
            <hr class="hr-border">
                <h3>Meal</h3>
                <div class="row">
                    <?php if (!empty($itinerary->itin_included_meals)){
                        $meals = explode('-', $itinerary->itin_included_meals);
                        foreach ($meals as $meal) { ?>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-opt btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i> <?= ucfirst($meal); ?></button>
                        </div>
                    <?php }}else{ ?>
                        <div class="col-md-12">
                            Not Included
                        </div>
                    <?php } ?>
                </div>
            <?php if ($itinerary->itin_accom_included){ ?>
                <hr class="hr-border">
                    <h3>Accomodation</h3>
                    <p class="itin-mb"><?= $itinerary->itin_accom_night; ?> Nights</p>
                    <p class="itin-mb"><?= $itinerary->itin_accom_desc; ?></p>
                    <?php
                        $starNumber = $itinerary->itin_accom_min_rate;
                        for($x=1;$x<=$starNumber;$x++) {
                            echo '<img src='.base_url()."assets/images/fullstar.png".' class="itin-star" />';
                        }
                        while ($x<=5) {
                            echo '<img src='.base_url()."assets/images/emptystar.png".' class="itin-star" />';
                            $x++;
                        }
                    ?>
            <?php } ?>
            <hr class="hr-border">
                <h3>Included Fees</h3>
                <div class="row">
                    <?php if (!empty($itinerary->itin_included_fees)){
                        $fees = explode('-', $itinerary->itin_included_fees);
                        foreach ($fees as $fee) { ?>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-opt btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i> <?= ucfirst($fee); ?></button>
                        </div>
                    <?php }}else{ ?>
                        <div class="col-md-12">
                            Not Included
                        </div>
                    <?php } ?>
                </div>
            <hr class="hr-border">
                <h3>Cancellation</h3>
                <p><?= $itinerary->itin_cancellation; ?> Days</p>
            <hr class="hr-border">
                <h3>Additional Information</h3>
                <?php if (!empty($additional_info)){ 
                    foreach ($additional_info as $add){ ?>
                    <p class="itin-mb"><?= $add; ?></p>
                <?php }} ?>
            <hr class="hr-border">
                <h3>0 Reviews</h3>
        </div>
        <div class="col-md-4">
            <div class="book" id="myBook">
                <h3 align="center">Book Status</h3>
                <hr class="hr-border">
                <h4><b><?= $itinerary->itin_title; ?></b></h4>
                <p class="lbl-loc">
                    <img src="<?= base_url('assets/images/icon/location.png')?>" class="icon-img"> <?= ucwords($itinerary->itin_location)?>
                </p>
                <p class="lbl-tourist">
                    <img src="<?= base_url('assets/images/icon/user2.png')?>" class="icon-img"> <?= $itinerary->itin_min_tourist; ?> - <?= $itinerary->itin_max_tourist; ?> Tourists
                </p>
                <p class="lbl-tourist">
                    <img src="<?= base_url('assets/images/icon/calender.png')?>" class="icon-img"> <?= ($itinerary->itin_day == 'md')?$book->start_date.'/'.$book->start_date:$book->start_date; ?>
                </p>
                <br>
                <p class="lbl-tourist">
                    <input type="hidden" id="itin-price" value="<?= $itinerary->itin_price_nominal; ?>">
                    <img src="<?= base_url('assets/images/icon/money.png')?>" class="icon-img"> <?= strtoupper($book->total_price_currency).' '.number_format($book->total_price_nominal,0,'.',','); ?>
                </p>
                <p class="lbl-tourist">
                    Booking Code : <?= ($book->book_code) ?><br><br>
                    <span style="font-size: 10pt">
                        <?= (!empty($book->book_accepted_date)?'Accepted on '.$this->public_function->format_date($book->book_accepted_date).'<br>':'');?>
                        <?= (!empty($book->book_rejected_date)?'Rejected on '.$this->public_function->format_date($book->book_rejected_date).'<br>':'');?>
                        <?= (!empty($book->book_paid_date)?'Paid on '.$this->public_function->format_date($book->book_paid_date).'<br>':'');?>
                    </span>
                </p>
                <br>
                <?php if ($book->book_status == 1){ ?>
                    <a href="#" class="btn btn-danger btn-xr" disabled>Waiting for confirmation..</a>
                <?php } elseif ($book->book_status == 2){ 
                        if (!empty($xmiles)) { ?>
                        <p class="lbl-xmiles" style="font-size: 10pt">You have x-miles :<br> <?= $xmiles->x_pts_now.' PTS - IDR '.number_format($xmiles->x_idr_now,0,",","."); ?></p>
                        <input type="hidden" id="total-price" value="<?= $book->total_price_nominal; ?>">
                        <input type="hidden" name="xmiles" id="xmiles" value="<?= $xmiles->x_idr_now; ?>">
                        <input type="checkbox" id="bookid"  class="chk-col-black" value="<?= $book->book_id; ?>" />
                        <label for="bookid">Use your x-miles in this transaction.</label><br><br>
                    <?php } ?>

                    <!-- <form method="post" name="ePayment" action="https://sandbox.ipay88.co.id/epayment/entry.asp"> -->
                    <!-- <form method="post" name="ePayment" action="https://sandbox.ipay88.co.id/ePayment/WebService/PaymentAPI/Checkout"> -->
                    <form method="post" name="ePayment" action="<?= base_url().'booking/seamless'; ?>">
                        <input type="hidden" name="itin_id" value="<?= $itinerary->itin_id; ?>">
                        <input type="hidden" name="MerchantCode" value="<?= $merchantcode; ?>">
                        <input type="hidden" name="PaymentId" value="55">
                        <input type="hidden" name="RefNo" value="<?= $book->book_code; ?>">
                        <input type="hidden" name="Amount" value="<?= preg_replace('/[\.\,]/', '', $book->total_price_nominal); ?>">
                        <input type="hidden" name="Currency" value="<?= strtoupper($itinerary->itin_price_currency); ?>">
                        <input type="hidden" name="ProdDesc" value="<?= $itinerary->itin_title; ?>">
                        <input type="hidden" name="UserName" value="<?= $turis->user_fname; ?>">
                        <input type="hidden" name="UserEmail" value="<?= $turis_akun->user_email; ?>">
                        <input type="hidden" name="UserContact" value="<?= $turis->user_phone; ?>">
                        <input type="hidden" name="Remark" value="Xploriant">
                        <input type="hidden" name="Lang" value="UTF-8">
                        <input type="hidden" name="Signature" value="<?= $signature; ?>">
                        <input type="hidden" name="ResponseURL" value="<?= base_url().'booking/response'; ?>">
                        <input type="hidden" name="BackendURL" value="<?= base_url().'booking/backend'; ?>">
                        <!-- <input type="hidden" name="xfield1" value="||IPP:3||"> -->
                        <input type="submit" value="Make Payment" name="Submit" class="btn btn-danger btn-x"> 
                    </form>
                    <!-- <a href="<?= base_url().'booking/make_payment/'.$book->book_id; ?>" class="btn btn-danger btn-x">Make Payment</a> -->
                    <!-- <input type="hidden" name="book_paid" id="book_paid" value="<?= $book->total_price_nominal ?>"> -->
                    <br>
                    <a id="makepayment" href="<?= base_url().'booking/simulation/'.$book->book_id; ?>" class="btn btn-danger btn-x">Simulation Payment</a>
                    <!-- <input type="submit" class="btn btn-danger btn-x" value="Simulation Payment"> -->
                <?php } elseif ($book->book_status == 3){ ?>
                    <a href="#" class="btn btn-danger btn-xr">Declined</a>
                <?php } elseif ($book->book_status == 4){ ?>
                    <a class="btn btn-danger btn-x get-token">Get Code</a>
                <?php } elseif ($book->book_status == 5){ ?>
                    <?php if (empty($book->tourist_end_tour)){ ?>
                        <p align="center">The tour is still running...</p>
                    <?php } else { ?>
                        <div class="form-group">
                            <a href="<?= base_url().'booking/review_tour/'.$book->book_id.'/1'; ?>" class="btn btn-danger btn-x btn-start">Review Your Tour</a>
                        </div>
                    <?php } if ($book->end_date <= date('Y-m-d')) { ?>
                        <div class="form-group" id="start-tour">
                            <a href="<?= base_url().'booking/end_tour/'.$book->book_id.'/1'; ?>" class="btn btn-danger btn-x btn-start">End Tour</a>
                        </div>    
                <?php } elseif ($book->book_status == 6) { ?>
                    <div class="form-group">
                        <a href="<?= base_url().'booking/review_tour/'.$book->book_id.'/1'; ?>" class="btn btn-danger btn-x btn-start">Review Your Tour</a>
                    </div>
                <?php }} elseif ($book->book_status == 7 || $book->book_status == 8){ ?>
                    <a href="#" class="btn btn-danger btn-xr">Booking Canceled</a>
                <?php } ?>
                <div class="token" style="display: none"><?= @$book->book_token; ?></div>
                <div class="ket-token" style="display: none">give this code to the guide when you start the tour.</div>
                <hr class="hr-booking">
                <?php if ($book->book_status != 5 || $book->book_status != 6 || $book->book_status != 7 || $book->book_status != 8){ ?>
                    <p align="center"><a href="#" data-href="<?= base_url().'booking/cancel_book/'.$book->book_id.'/1' ?>" data-toggle="modal" data-target="#cancelModal">Cancel Booking</a></p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .datepicker{
        top:323px !important;
    }
    .itin-note{
        font-size: 9pt;
        text-align: center;
    }
    .itin-star{
        width: 27px;
        margin-bottom: 10px;
        margin-right: 5px;
    }
    .itin-mb{
        margin-bottom: 5px;
    }
    .contact{
        color: red;
        text-decoration: underline;
        font-size: 9pt;
        font-weight: bold;
    }
    .nama-profil{
        font-weight: bold;
    }
    .foto-profil{
        width: 80px;
    }
    .hr-border{
        border: 2px solid #042441;
    }
    .lbl-loc{
        font-weight: bold;
        color: red;
        margin-bottom: 5px;
    }
    .lbl-tourist{
        /*font-weight: bold;*/
        color: #042441;
        margin-bottom: 5px;
    }
    .icon-img{
        width: 20px;
        margin-right: 5px;
    }
    .itin-title{
        font-weight: bold;
        font-size: 35px;
        margin-top: 15px;
        margin-bottom: 20px;
    }
    .carousel-item{
        width: 100%; /*width you want*/
        height: 600px; /*height you want*/
        overflow: hidden;
    }
    .carousel-item img{
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .token{
        margin-top: 10px;
        text-align: center;
        background-color: wheat;
        padding: 15px 10px;
        font-size: 25pt;
        font-weight: 600;
        letter-spacing: 5px;
    }
    .ket-token{
        font-size: 12px;
        font-style: italic;
        margin-top: 5px;
        text-align: center;
    }
</style>