<div class="container mt-term">
    <p align="right"><a href="<?= base_url().'termofservice?lang=in' ?>" class="link-lang"><img src="<?= base_url().'assets/images/ind.png' ?>" class="flag-lang">Bahasa Indonesia</a> | <a href="<?= base_url().'termofservice?lang=en' ?>" class="link-lang"><img src="<?= base_url().'assets/images/eng.png' ?>" class="flag-lang">English</a></p>
    <?php if (empty($lang) || $lang == 'en') { ?>
        <div class="wrap">
            <div class="row">
                <div class="col-md-12">
                    <h3 align="center">TERMS OF SERVICE</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <ol align="justify">
                        <li>Definitions
                            <ol type="a">
                                <li>“Xploriant Services” is defined as any content or services provided within the Xploriant web or mobile app platform.</li>
                                <li>“Registered User” is defined as any person who creates an Xploriant account or registers using a third-party service to access certain features of the platform.</li>
                                <li>“User Account” is defined as an Xploriant personal account that enables Registered Users to be a Guide and/or a Traveler.</li>
                                <li>“Guide” is defined as a Registered User acting as the provider of a travel service in the Xploriant platform.</li>
                                <li>“Traveler” is defined as a Registered User acting as a client of a travel service in the Xploriant platform.</li>
                                <li>“Itinerary” is defined as any product uploaded by one or more Guide to the Xploriant platform, which includes the data of a travel timetable alongside its descriptions and pricing.</li>
                                <li>“X-Miles” is defined as Xploriant’s promotional program through which Registered Users can accumulate points from their past transactions in the Xploriant platform, eligible for the next transaction.</li>
                            </ol>
                        </li>
                        <li>Xploriant Services
                            <ol type="a">
                                <li>Xploriant is an online platform that empowers Registered Users to create and/or book an Itinerary.</li>
                                <li>Xploriant does not own, produce, sell, resell, provide, organize, or deliver any intellectual or material property related to the aforementioned Itineraries.</li>
                            </ol>
                        </li>
                        <li>Language
                            <ol type="a">
                                <li>Should there be ambiguity due to deviation of meaning between the English version and versions in other languages, the English version shall prevail.</li>
                            </ol>
                        </li>
                        <li>Account Registration
                            <ol type="a">
                                <li>Users should register an Xploriant account to access certain features of the platform.</li>
                                <li>Registered Users should be at least 17 years old to register an Xploriant account and access certain features of the platform.</li>
                                <li>Registered Users should input their personal data (name, date of birth, email address) accurately and completely.</li>
                                <li>Registered Users should not disclose their personal data to a third party.</li>
                                <li>Registered Users should report to Xploriant should they suspect that their personal data is lost, stolen, or misappropriated by a third party. Registered Users are held accountable for any activity performed by their account in Xploriant platform unless such activities are not done by the rightful user and the rightful user reports the misappropriation of their account to Xploriant.</li>
                                <li>Xploriant may, but is not obliged to, request verification of personal data through official documents such as passport or social security card.</li>
                                <li>Xploriant is responsible for the privacy of personal data, such that it would not grant, sell, or exchange user’s personal data to a third party, unless such action is in accordance with our Privacy Policy.</li>
                            </ol>
                        </li>
                        <li>Content
                            <ol type="a">
                                <li>Registered Users should input the information regarding their Itineraries accurately and completely, and update them immediately should there be any change.</li>
                                <li>Media content (photos and videos) uploaded by user should accurately mirror the content of the Itinerary. Xploriant may request a minimum and/or maximum amount and resolution of media contents.</li>
                                <li>The copyright of the aforementioned Itineraries, alongside its text and media contents, does not belong to Xploriant and can be reproduced in other sites by its rightful owner. Guides are held accountable for the plagiarism of Itinerary contents that is unrightfully taken from other sites.</li>
                                <li>Xploriant is not responsible for the content of any Itinerary should it contain activities that (i) do not exist, (ii) do not fulfill the quality of the descriptions, (iii) are dangerous, (iv) are not suitable, or (v) violate the regional or national law of the destination country. Guides are held accountable for the content of their Itineraries.</li>
                                <li>Registered Users may not publish contents that (i) are misleading or fraudulent, (ii) incorporate pornographic material, (iii) endorse discrimination, racism or bigotry, (iv) contain violence or threats, (v) endorse illegal activities, or (vi) market illegal substances or endorse substance abuse. Failing to abide to Xploriant’s content guidelines may result in account termination.</li>
                                <li>The act of copying, publishing, selling, or exchanging Xploriant Itinerary contents by a third party without the creator’s permission is considered plagiarism.</li>
                                <li>Xploriant may publish media content (photos and videos) contained in Itineraries for promotional purposes.</li>
                            </ol>
                        </li>
                        <li>Pricing
                            <ol type="a">
                                <li>Itinerary price displayed on Xploriant platforms is equivalent to those assigned by the Guide.</li>
                                <li>Xploriant charges 20% Service Fee from all sales within our platform. Guides would receive 80% of the assigned price, regardless of any promotional discount given by Xploriant to Travelers.</li>
                            </ol>
                        </li>
                        <li>Payment Services
                            <ol type="a">
                                <li>Xploriant Payment Services is only available for Registered Users only.</li>
                                <li>Payment in Xploriant is performed through third-party payment service providers. Third-party payment service providers may charge additional fees; Xploriant is not responsible for any such fees. Users are also subject to the terms and conditions of the third-party payment service provider.</li>
                                <li>Currency conversion rates are determined and updated by Xploriant.</li>
                                <li>Travelers are compelled to pay the total booking fee before the reservation is considered finalized.</li>
                            </ol>
                        </li>
                        <li>Payout
                            <ol type="a">
                                <li>Guides receive booking fees in their account balance. Guides may request to transfer the balance to their personal bank or e-wallet account 48 hours after the tour is completed. Transfer requests will be processed within 1 business day, however, the timing to receive the balance may vary based on the policies of the chosen third-party payment service provider. Xploriant is not responsible for any such additional transaction fees between Xploriant and Guides subject to the policies of the third party payment service providers.</li>
                                <li>Xploriant may temporarily suspend or permanently cancel a payout for Guides should there be any reports of violation of terms from corresponding Travelers.</li>
                                <li>Travelers receive refunds in their account balance. Travelers may request to transfer the balance to their own personal bank or e-wallet account immediately after cancellation. Transfer requests will be processed within 1 business day, however, the timing to receive the balance may vary based on the policies of the chosen third-party payment service provider. Xploriant is not responsible for any such additional transaction fees between Xploriant and Travelers subject to the policies of the third party payment service providers.</li>
                                <li>Registered Users should inform Xploriant should there be any unwanted recurring payment. Xploriant may refund users only if reservation and billing evidence are sufficient and in line.</li>
                            </ol>
                        </li>
                        <li>Cancellation Policies
                            <ol type="a">
                                <li>Guides may determine the cancellation period of each of their Itineraries.</li>
                                <li>Cancellation can only be requested before the tour starts.</li>
                                <li>Travelers will receive a full refund of the paid amount should they request cancellation before the predetermined cancellation period of the Itinerary or within 24 hours after the booking is finalized. Refund is not applicable to cancellation requested within the cancellation period determined by the Guide. Travelers will receive the refund in their account balance. Payout policies apply to refunds.</li>
                                <li>Should Travelers cancel a booking prior to the cancellation date predetermined by the Guide or within 24 hours after the booking is finalized, the Guide will not receive any payout.</li>
                                <li>Should the Guide cancels a paid booking, Travelers will receive a full refund of the paid amount. In such cases, Guides will not receive any payout. Guides may no longer cancel a reservation contract within 24 hours before the supposed start date. Travelers will receive the refund in their account balance. Payout policies apply to refunds.</li>
                                <li>Should a Guide unrightfully terminates a reservation contract by cancelling an Itinerary without prior notice to the corresponding Traveler, Xploriant has the right to temporarily suspend or permanently terminate the Guide’s User Account. Travelers should report immediately to support@xploriant.com in order to receive a full refund and compensation of approximately 10% of the paid amount in the form of X-Miles credit. Full refund and/or compensation is not applicable should the Traveler fails to report the case to Xploriant within 48 hours after the supposed last travel date. Should the Traveler provides a misleading report regarding a cancellation case, Xploriant has the right to temporarily suspend or permanently terminate the Traveler’s User Account.</li>
                            </ol>
                        </li>
                        <li>During the Tour
                            <ol type="a">
                                <li>Both Travelers and Guides must confirm the start of the tour on the supposed start date. Travelers will be given a personalized OTP via email or in the Xploriant mobile app. Guides must request the OTP at meetup and enter it on a confirmation form sent via email or in the Xploriant mobile app.</li>
                                <li>Guides may not request additional payment from their Travelers, unless (i) the fee is declared in Xploriant platform as an additional fee, or (ii) the fee is for facilities not included in Xploriant platform and/or requested by Travelers on the spot. Travelers’ reports of such actions may result in Guide’s account termination.</li>
                                <li>Xploriant is not responsible for any violations of the law committed by the Guide(s) and/or the Traveler(s).</li>
                                <li>Xploriant is not responsible for the loss of any personal belongings, purposefully or incidentally, throughout the tour.</li>
                                <li>Xploriant is not responsible for any case of accidents causing any form of injuries and/or death to the Guide(s) and/or the Traveler(s) throughout the tour.</li>
                                <li>Both Travelers and Guides must confirm the end of the tour on the supposed end date by following the instructions sent via email or in the Xploriant mobile app.</li>
                            </ol>
                        </li>
                        <li>Reviewing
                            <ol type="a">
                                <li>Guides and Travelers are recommended to rate and review one another after the tour as a form of improving Xploriant community.</li>
                                <li>Rating and review can be submitted immediately after check-out. The procedure is closed 7 days after the end of the tour.</li>
                                <li>Rating and review should not be biased or misleading.</li>
                                <li>Reviews should not (i) be misleading or fraudulent, (ii) incorporate pornographic material, (iii) endorse discrimination, racism or bigotry, (iv) contain violence or threats, (v) endorsing illegal activities, or (vi) market illegal substances or endorse substance abuse. Failing to abide to Xploriant’s reviewing guidelines may result in account termination.</li>
                                <li>Any complaints or requests regarding an Itinerary should be expressed within 48 hours after the end of the tour. Travelers or Guides may report to Xploriant by sending an email to support@xploriant.com. Xploriant is not obliged to respond to any complaints or requests reported after the aforementioned period of time.</li>
                            </ol>
                        </li>
                        <li>Communication
                            <ol type="a">
                                <li>Guides and Travelers are recommended to communicate only through the provided messaging platform.</li>
                                <li>Users may not misappropriate Xploriant’s messaging platform by sharing messages that (i) are misleading or fraudulent, (ii) incorporate pornographic material, (iii) endorse discrimination, racism or bigotry, (iv) contain violence or threats, (v) endorsing illegal activities, or (vi) market illegal substances or endorse substance abuse. Failing to abide to Xploriant’s messaging guidelines may result in account termination.</li>
                                <li>Xploriant is not responsible for any (i) discrimination, racism or bigotry, (ii) threats or contents of violence, (iii) pornographic contents or sexual harassment, (iv) illegal activities, (v) illegal substances or substance abuse, or (vi) fraudulence or deception, outside of Xploriant’s messaging platform.</li>
                            </ol>
                        </li>
                        <li>X-Miles
                            <ol type="a">
                                <li>For every purchase, Travelers receive X-Miles credits amounting to approximately 5% of booked Itinerary price assigned by the Guide, regardless of any promotional discount given by Xploriant to Travelers. X-Miles credits are transferred to the user’s X-Miles account immediately after the tour starts.</li>
                                <li>X-Miles credits may only be used after the start of the tour is confirmed.</li>
                                <li>X-Miles credits may be used to purchase the next reservation.</li> 
                                <li>X-Miles credits can only be used to discount purchases in the Xploriant platform, and thus, cannot be transferred to any personal account.</li>
                                <li>X-Miles credits have no expiration date. However, X-Miles credits are dissolved should Xploriant terminates the user’s account.</li>
                            </ol>
                        </li>
                        <li>Account Suspension and Termination
                            <ol type="a">
                                <li>Xploriant has the right to temporarily suspend a User Account should the user fails to provide an official document to verify his or her personal data.</li>
                                <li>Xploriant has the right to temporarily suspend a User Account should the user is under the minimum age of 17. The suspended user may request the reactivation of his or her Xploriant account by sending one or more official documents proving their age. Xploriant has the right to reject such request.</li>
                                <li>Xploriant has the right to temporarily suspend or permanently terminate a User Account should the user does not accurately provide his or her personal data.</li>
                                <li>Xploriant has the right to temporarily suspend or permanently terminate a User Account should the user violates Xploriant’s content guidelines.</li>
                                <li>Xploriant has the right to temporarily suspend or permanently terminate a User Account should the user violates Xploriant’s reviewing guidelines.</li>
                                <li>Xploriant has the right to temporarily suspend or permanently terminate a User Account should the user violates Xploriant’s messaging guidelines.</li>
                                <li>Xploriant has the right to temporarily suspend or permanently terminate a User Account should the account is breached or misappropriated by a third party.</li>
                                <li>Xploriant has the right to temporarily suspend or permanently terminate a User Account should Xploriant believes with honest intentions that such action is necessary to protect the community.</li>
                            </ol>
                        </li>
                    </ol>
                    <br><br>
                </div>
            </div>
        </div>
    <?php } elseif ($lang == 'in') { ?>
        <div class="wrap">
            <div class="row">
                <div class="col-md-12">
                    <h3 align="center">KETENTUAN LAYANAN</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <ol align="justify">
                        <li>Definisi
                            <ol type="a">
                                <li>“Layanan Xploriant” didefinisikan sebagai semua konten dan layanan yang tersedia di platform web dan aplikasi seluler Xploriant.</li>
                                <li>“Pengguna” didefinisikan sebagai siapapun yang membuat akun Xploriant atau mendaftarkan diri melalui layanan pihak ketiga untuk mengakses fitur tertentu di platform Xploriant.</li>
                                <li>“Akun Pengguna” didefinisikan sebagai akun pribadi Xploriant yang memberi akses bagi Pengguna untuk menjadi Guide dan/atau Traveler.</li>
                                <li>“Guide” didefinisikan sebagai Pengguna yang menyediakan jasa pariwisata melalui platform Xploriant.</li>
                                <li>“Traveler” didefinisikan sebagai Pengguna yang membeli jasa pariwisata melalui platform Xploriant.</li>
                                <li>“Rencana Perjalanan” didefinisikan sebagai produk apapun yang diunggah oleh satu atau lebih Guide ke platform Xploriant, yang mencakup rincian perjalanan beserta deskripsi dan harga.</li>
                                <li>“X-Miles” didefinisikan sebagai program promosi Xploriant yang memberi akses bagi Pengguna untuk mengakumulasi poin dari transaksi sebelumnya, dan dapat digunakan untuk transaksi selanjutnya.</li>
                            </ol>
                        </li>
                        <li>Layanan Xploriant
                            <ol type="a">
                                <li>Xploriant adalah sebuah platform dalam jaringan yang memberi akses bagi Pengguna untuk membuat dan memesan Rencana Perjalanan.</li>
                                <li>Xploriant tidak memiliki, menghasilkan, menjual, memperjual-belikan, menyediakan, menyelenggarakan, atau mengantarkan hak milik intelektual dan material apapun yang berhubungan dengan Rencana Perjalanan yang disebutkan di atas.</li>
                            </ol>
                        </li>
                        <li>Bahasa
                            <ol type="a">
                                <li>Apabila terdapat ambiguitas akibat penyimpangan makna antara versi Bahasa Inggris dan versi bahasa lain, maka versi Bahasa Inggris akan berlaku.</li>
                            </ol>
                        </li>
                        <li>Pendaftaran Akun
                            <ol type="a">
                                <li>Pengguna harus mendaftarkan sebuah akun Xploriant untuk mengakses fitur tertentu dalam platform.</li>
                                <li>Pengguna harus berumur 17 tahun ke atas untuk mendaftarkan sebuah akun Xploriant dan mengakses fitur tertentu dalam platform.</li>
                                <li>Pengguna harus mencantumkan data pribadi (nama, tanggal lahir, alamat surel) dengan akurat dan lengkap.</li>
                                <li>Pengguna dilarang menyingkapkan data pribadi ke pihak ketiga.</li>
                                <li>Pengguna harus melapor kepada pihak Xploriant apabila mencurigai bahwa data pribadi mereka hilang, dicuri, atau disalahgunakan oleh pihak ketiga. Pengguna bertanggung jawab penuh atas semua aktivitas yang mengatasnamakan akun Xploriant mereka kecuali aktivitas tersebut tidak dilakukan oleh pemilik yang sebenarnya dan pemilik yang sebenarnya melaporkan penyalahgunaan akun mereka ke pihak Xploriant.</li>
                                <li>Xploriant berhak, tapi tidak diwajibkan, untuk meminta verifikasi data pribadi melalui dokumen resmi seperti paspor dan/atau kartu tanda penduduk (KTP).</li>
                                <li>Xploriant bertanggung jawab atas kerahasiaan data pribadi, sebagaimana kami tidak akan memberi, menjual, atau menukar data pribadi Pengguna dengan pihak ketiga, kecuali diperkenankan sesuai dengan Kebijakan Privasi kami.</li>
                            </ol>
                        </li>
                        <li>Konten
                            <ol type="a">
                                <li>Pengguna harus mencantumkan informasi mengenai Rencana Perjalanan mereka dengan akurat dan lengkap, serta memperbarui seketika jika ada perubahan.</li>
                                <li>Konten media (gambar dan video) harus merepresentasikan konten Rencana Perjalanan dengan akurat. Xploriant berhak untuk meminta jumlah dan resolusi minimum dan/atau maksimum untuk konten media.</li>
                                <li>Hak milik Rencana Perjalanan tersebut tadi, beserta teks dan konten media, bukan milik Xploriant dan boleh diperbanyak di situs-situs lain oleh pemilik yang sebenarnya. Guide bertanggung jawab penuh atas plagiarisme konten Rencana Perjalanan yang diambil dari situs lain.</li>
                                <li>Xploriant tidak bertanggung jawab atas konten Rencana Perjalanan seandainya memuat aktivitas yang (i) tidak nyata, (ii) tidak memenuhi kualitas yang dideskripsikan, (iii) berbahaya, atau (iv) melanggar hukum daerah atau nasional di negara tujuan. Guide bertanggung jawab penuh atas konten Rencana Perjalanan yang mereka unggah.</li>
                                <li>Registered Users may not publish contents that (i) are misleading or fraudulent, (ii) incorporate pornographic material, (iii) endorse discrimination, racism or bigotry, (iv) contain violence or threats, (v) endorse illegal activities, or (vi) market illegal substances or endorse substance abuse. Failing to abide to Xploriant’s content guidelines may result in account termination.</li>
                                <li>Pengguna tidak diperkenankan mempublikasikan konten yang (i) bersifat menyesatkan atau menipu, (ii) mengandung materi pornografi, (iii) mendukung diskriminasi atau rasialisme, (iv) mengandung kekerasan atau ancaman, (v) mendukung kegiatan ilegal, atau (vi) memperjual-belikan narkotika dan obat-obatan terlarang atau mendukung penyalahgunaan narkotika dan obat-obatan terlarang. Gagal mematuhi panduan konten Xploriant dapat mengakibatkan penghentian akun.</li>
                                <li>Meniru, mempublikasikan, menjual, atau menukar konten Rencana Perjalanan Xploriant dengan pihak ketiga tanpa izin pencipta adalah tindak plagiarisme. Xploriant berhak mempublikasikan konten media yang tercantum di Rencana Perjalanan untuk tujuan promosi.</li>
                            </ol>
                        </li>
                        <li>Harga
                            <ol type="a">
                                <li>Harga Rencana Perjalanan yang tertera di platform Xploriant sesuai dengan harga yang telah ditentukan oleh Guide.</li>
                                <li>Xploriant menerima 20% Biaya Layanan dari semua penjualan di platform kami. Guide akan menerima 80% dari harga yang telah mereka tentukan, tanpa dipengaruhi bentuk promosi apapun dari pihak Xploriant.</li>
                            </ol>
                        </li>
                        <li>Layanan Pembayaran
                            <ol type="a">
                                <li>Layanan Pembayaran Xploriant hanya tersedia bagi Pengguna.</li>
                                <li>Pembayaran di Xploriant dijalankan melalui penyedia layanan pembayaran pihak ketiga. Penyedia layanan pembayaran pihak ketiga dapat mengenakan biaya tambahan; Xploriant tidak bertanggung jawab atas biaya tambahan tersebut tadi. Pengguna juga menyetujui syarat dan ketentuan penyedia layanan pembayaran pihak ketiga.</li>
                                <li>Nilai tukar mata uang ditentukan dan diperbarui oleh pihak Xploriant.</li>
                                <li>Traveler diharuskan membayar penuh biaya pemesanan sebelum pemesanan dianggap selesai.</li>
                            </ol>
                        </li>
                        <li>Payout
                            <ol type="a">
                                <li>Guide menerima biaya pemesanan melalui saldo Xploriant mereka. Guide dapat mengajukan transfer saldo ke rekening bank pribadi atau rekening dompet elektronik 48 jam setelah tur secara resmi selesai. Pengajuan transfer saldo akan diproses dalam 1 hari kerja, namun, waktu penerimaan saldo dapat berubah sesuai dengan ketentuan penyedia layanan pembayaran pihak ketiga. Xploriant tidak bertanggung jawab atas biaya tambahan apapun dalam transaksi antara Xploriant dan Guide yang bergantung pada ketentuan penyedia layanan pembayaran pihak ketiga. </li>
                                <li>Xploriant berhak menunda atau membatalkan pembayaran Guide apabila terdapat laporan mengenai pelanggaran ketentuan yang dikirim oleh Traveler yang bersangkutan.</li>
                                <li>Traveler menerima pengembalian dana melalui saldo Xploriant mereka. Traveler dapat mengajukan transfer saldo ke rekening bank pribadi atau rekening dompet elektronik sesaat setelah pembatalan. Pengajuan transfer saldo akan diproses dalam 1 hari kerja, namun, waktu penerimaan saldo dapat berubah sesuai dengan ketentuan penyedia layanan pembayaran pihak ketiga. Xploriant tidak bertanggung jawab atas biaya tambahan apapun dalam transaksi antara Xploriant dan Traveler yang bergantung pada ketentuan penyedia layanan pembayaran pihak ketiga.</li>
                                <li>Pengguna harus melapor kepada pihak Xploriant apabila terjadi pembayaran berulang yang tidak dikehendaki. Xploriant akan melakukan pengembalian dana hanya jika bukti pemesanan dan bukti penagihan cukup dan sesuai.</li>
                            </ol>
                        </li>
                        <li>Pembatalan
                            <ol type="a">
                                <li>Guide berhak menentukan periode pembatalan untuk setiap Rencana Perjalanan mereka.</li>
                                <li>Pembatalan hanya dapat diajukan sebelum tur secara resmi dimulai.</li>
                                <li>Traveler akan menerima pengembalian dana penuh apabila mengajukan pembatalan sebelum periode pembatalan yang telah ditentukan atau dalam waktu 24 jam setelah pemesanan dianggap selesai. Pengembalian dana tidak berlaku untuk pembatalan yang diajukan setelah periode pembatalan yang telah ditentukan oleh Guide. Traveler akan menerima pengembalian dana melalui saldo Xploriant mereka. Ketentuan payout berlaku untuk proses pengembalian dana.</li>
                                <li>Apabila Traveler mengajukan pembatalan sebelum periode pembatalan atau dalam waktu 24 jam setelah pemesanan dianggap selesai, Guide tidak akan menerima biaya pemesanan.</li>
                                <li>Apabila Guide membatalkan pemesanan, Traveler yang bersangkutan akan menerima pengembalian dana penuh. Dalam kasus demikian, Guide tidak akan menerima biaya pemesanan. Guide tidak diperbolehkan mengajukan pembatalan dalam kurun waktu 24 jam sebelum tanggal mulai tur. Traveler akan menerima pengembalian dana melalui saldo Xploriant mereka. Ketentuan payout berlaku untuk proses pengembalian dana.</li>
                                <li>Apabila Guide membatalkan kontrak pemesanan tanpa pemberitahuan sebelumnya kepada Traveler yang bersangkutan, Xploriant berhak melakukan penghentian akun Guide. Traveler harus segera melapor dengan mengirim surat elektronik ke support@xploriant.com untuk menerima pengembalian dana penuh dan biaya kompensasi senilai kurang lebih 10% dari biaya pemesanan dalam bentuk poin X-Miles. Pengembalian dana penuh dan/atau biaya kompensasi tidak berlaku apabila Traveler tidak melaporkan perkara ke pihak Xploriant dalam kurun waktu 48 jam setelah hari terakhir tur sesuai kontrak pemesanan. Apabila Traveler mengajukan laporan yang bersifat menyesatkan atau fiktif mengenai pembatalan, Xploriant berhak melakukan penghentian akun Traveler.</li>
                            </ol>
                        </li>
                        <li>Selama Tur Berlangsung
                            <ol type="a">
                                <li>Traveler dan Guide harus mengkonfirmasi mulainya tur pada tanggal permulaan tur. Traveler akan menerima kode sandi satu kali (OTP) melalui surat elektronik atau di aplikasi seluler Xploriant. Guide harus meminta kode sandi satu kali (OTP) saat bertemu dan memasukkan kode tersebut ke formulir konfirmasi yang dikirim melalui surat elektronik atau di aplikasi seluler Xploriant.</li>
                                <li>Guide tidak diperkenankan meminta biaya tambahan dari Traveler yang bersangkutan kecuali jika (i) biaya tersebut dicantumkan sebagai biaya tambahan di platform Xploriant, atau (ii) biaya tersebut merupakan biaya fasilitas tambahan yang tidak dicantumkan sebagai termasuk di platform Xploriant dan/atau diminta oleh Traveler di tempat. Laporan Traveler mengenai pemerasan biaya tambahan dapat menyebabkan penghentian akun Guide.</li>
                                <li>Xploriant tidak bertanggung jawab atas pelanggaran hukum yang dilakukan oleh satu atau lebih Guide dan/atau Traveler.</li>
                                <li>Xploriant tidak bertanggung jawab atas kehilangan barang pribadi, baik secara sengaja atau tidak sengaja, selama tur berlangsung.</li>
                                <li>Xploriant tidak bertanggung jawab atas kecelakaan dalam bentuk apapun yang menyebabkan cedera dan/atau kematian pada Guide dan/atau Traveler selama tur berlangsung.</li>
                                <li>Traveler dan Guide harus mengkonfirmasi selesainya tur pada tanggal terakhir tur dengan mengikuti petunjuk yang dikirimkan melalui surat elektronik atau di aplikasi seluler Xploriant.</li>
                            </ol>
                        </li>
                        <li>Ulasan
                            <ol type="a">
                                <li>Guide dan Traveler disarankan untuk saling menilai dan mengulas setelah tur selesai sebagai bentuk membangun komunitas Xploriant.</li>
                                <li>Penilaian dan ulasan dapat disampaikan sesaat setelah tur selesai. Prosedur ini akan ditutup 7 hari setelah tur secara resmi selesai.</li>
                                <li>Penilaian dan ulasan harus objektif dan sesuai kenyataan.</li>
                                <li>Ulasan tidak diperkenankan (i) bersifat menyesatkan atau menipu, (ii) mengandung materi pornografi, (iii) mendukung diskriminasi atau rasialisme, (iv) mengandung kekerasan atau ancaman, (v) mendukung kegiatan ilegal, atau (vi) memperjual-belikan narkotika dan obat-obatan terlarang atau mendukung penyalahgunaan narkotika dan obat-obatan terlarang. Gagal mematuhi panduan ulasan Xploriant dapat mengakibatkan penghentian akun.</li>
                                <li>Keluhan dan permohonan apapun mengenai Rencana Perjalanan harus disampaikan dalam waktu 48 jam setelah tur secara resmi selesai. Traveler atau Guide dapat melapor ke pihak Xploriant dengan mengirimkan surat elektronik ke support@xploriant.com. Xploriant tidak diwajibkan menanggapi keluhan atau permohonan yang dilaporkan setelah batas waktu yang tertera di atas.</li>
                            </ol>
                        </li>
                        <li>Komunikasi
                            <ol type="a">
                                <li>Guide dan Traveler disarankan untuk berkomunikasi hanya melalui platform pengiriman pesan yang tersedia.</li>
                                <li>Pengguna tidak diperkenankan menyalahgunakan platform pengiriman pesan Xploriant dengan mengirim pesan yang (i) bersifat menyesatkan atau menipu, (ii) mengandung materi pornografi, (iii) mendukung diskriminasi atau rasialisme, (iv) mengandung kekerasan atau ancaman, (v) mendukung kegiatan ilegal, atau (vi) memperjual-belikan narkotika dan obat-obatan terlarang atau mendukung penyalahgunaan narkotika dan obat-obatan terlarang. Gagal mematuhi panduan pengiriman pesan Xploriant dapat mengakibatkan penghentian akun.</li>
                                <li>Xploriant tidak bertanggung jawab atas pesan yang mengandung (i) diskriminasi atau rasialisme, (ii) kekerasan atau ancaman, (iii) materi pornografi atau pelecehan seksual, (iv) kegiatan ilegal, (v) narkotika dan obat-obatan terlarang atau penyalahgunaannya, atau (vi) penipuan, di luar platform pengiriman pesan Xploriant.</li>
                            </ol>
                        </li>
                        <li>X-Miles
                            <ol type="a">
                                <li>Untuk setiap pemesanan, Traveler menerima poin X-Miles senilai kurang lebih 5% dari harga Rencana Perjalanan yang telah ditentukan oleh Guide, tanpa dipengaruhi bentuk promosi apapun dari pihak Xploriant. Poin X-Miles ditransfer ke saldo akun X-Miles milik Pengguna sesaat setelah tur dimulai.</li>
                                <li>Poin X-Miles hanya dapat digunakan sesaat setelah tur secara resmi dimulai.</li>
                                <li>Poin X-Miles dapat digunakan untuk pemesanan selanjutnya.</li> 
                                <li>Poin X-Miles hanya dapat digunakan untuk memotong harga pemesanan di platform Xploriant, dengan demikian, poin X-Miles tidak dapat diuangkan atau ditransfer ke akun pribadi.</li>
                                <li>Poin X-Miles tidak memiliki tanggal kadaluarsa. Akan tetapi, poin X-Miles akan dihapus apabila Xploriant melakukan penghentian Akun Pengguna.</li>
                            </ol>
                        </li>
                        <li>Penghentian Akun
                            <ol type="a">
                                <li>Xploriant berhak melakukan penghentian akun apabila Pengguna tidak dapat menunjukkan dokumen resmi untuk memverifikasi data pribadi mereka saat diminta oleh pihak Xploriant.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila Pengguna belum mencapai umur 17 tahun. Pengguna diperbolehkan memohon reaktivasi akun dengan menyediakan satu atau lebih dokumen resmi untuk membuktikan umur mereka. Xploriant berhak menolak permohonan tersebut.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila Pengguna tidak mencantum data pribadi dengan akurat.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila Pengguna melanggar panduan konten Xploriant.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila Pengguna melanggar panduan ulasan Xploriant.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila Pengguna melanggar panduan pengiriman pesan Xploriant.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila akun diretas atau disalahgunakan oleh pihak ketiga.</li>
                                <li>Xploriant berhak melakukan penghentian akun apabila pihak Xploriant menilai dengan itikad baik bahwa tindakan tersebut penting guna menjaga komunitas ini.</li>
                            </ol>
                        </li>
                    </ol>
                    <br><br>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
