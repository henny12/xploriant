<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$guide->user_fname; ?>
            <br><br>
            Today's the day! Request the authentic code from <?= @$tourist->user_fname .' '. @$tourist->user_lname; ?> upon meetup to confirm the start of your tour.
            <br><br>
            <a href="<?= base_url().'booking/detail/'.@$itin->itin_id.'/'.@$book->book_id ?>" class="btn-block">Confirm Now</a>
            <br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>
