<?php $this->load->view('template_email/header'); ?>
<body>
    <table border="1" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2" style="padding: 10px;text-align: center"> Test Email </td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/account_verification'?>" class="btn-block">Account Verification</a></td>
            <td><a href="<?= base_url().'testmail/reset_password'?>" class="btn-block">Reset Password</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/booking_req_guide'?>" class="btn-block">Booking Request Guide</a></td>
            <td><a href="<?= base_url().'testmail/booking_req_tourist'?>" class="btn-block">Booking Request Tourist</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/booking_accepted'?>" class="btn-block">Booking Accepted</a></td>
            <td><a href="<?= base_url().'testmail/booking_declined'?>" class="btn-block">Booking Declined</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/payment_completed_guide'?>" class="btn-block">Payment Completed Guide</a></td>
            <td><a href="<?= base_url().'testmail/payment_completed_tourist'?>" class="btn-block">Payment Completed Tourist</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/d_24hours_guide'?>" class="btn-block">Day-24 Hours Guide</a></td>
            <td><a href="<?= base_url().'testmail/d_24hours_tourist'?>" class="btn-block">Day-24 Hours Tourist</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/start_date_guide'?>" class="btn-block">Start Date Guide</a></td>
            <td><a href="<?= base_url().'testmail/start_date_tourist'?>" class="btn-block">Start Date Tourist</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/end_date_guide'?>" class="btn-block">End Date Guide</a></td>
            <td><a href="<?= base_url().'testmail/end_date_tourist'?>" class="btn-block">End Date Tourist</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/cancel_guide_no_payout'?>" class="btn-block">Cancel Guide No Payout</a></td>
            <td><a href="<?= base_url().'testmail/cancel_guide_payout'?>" class="btn-block">Cancel Guide Payout</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/cancel_guide'?>" class="btn-block">Cancel Guide</a></td>
            <td><a href="<?= base_url().'testmail/cancel_tourist'?>" class="btn-block">Cancel Tourist</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/cancel_tourist_no_refund'?>" class="btn-block">Cancel Tourist No Refund</a></td>
            <td><a href="<?= base_url().'testmail/cancel_tourist_refund'?>" class="btn-block">Cancel Tourist Refund</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/transfer_request_guide'?>" class="btn-block">Transfer Request Guide</a></td>
            <td><a href="<?= base_url().'testmail/transfer_completed_guide'?>" class="btn-block">Transfer Completed Guide</a></td>
        </tr>
        <tr>
            <td><a href="<?= base_url().'testmail/report'?>" class="btn-block">Report Auto Reply</a></td>
            <td></td>
        </tr>
    </table>
</body>
</html>