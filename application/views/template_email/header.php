<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Template Email</title>
    <style type="text/css">
        body {
            font-family: Arial
        }
        .btn-block {
            display: block;
            width: 100%;
            border: none;
            padding: 9px 0px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
            background-color: #042441; 
            color: #fff !important; 
            text-decoration: none;
        }
        .konten {
            margin: 10px 90px 0 90px;
            padding: 30px;
            background-color: #d9d9d9;
            line-height: 25px;
            font-size: 16px;
        }
        .logo {
            width: 300px;
        }
        .footer {
            text-align: center;
            /*line-height: 20px;*/
            /*color: black;*/
        }
        .f-title {
            font-weight: 700;
        }
        .btn-blockx {
            display: block;
            width: 100%;
            border: none;
            padding: 9px 0px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
            background-color: #cc0000; 
            color: #fff !important;  
            text-decoration: none;
        }
        .btn-blockxr {
            display: block;
            width: 100%;
            border: none;
            padding: 9px 0px;
            font-size: 16px;
            /*cursor: pointer;*/
            text-align: center;
            background-color: #999999; 
            color: #fff !important; 
            text-decoration: none;
        }
        .table-mail {
            border: 2px solid #042441;
            background: white;
            width: 100%;
        }
        .td-mail {
            border-bottom: 2px solid #042441;
            padding: 20px;
        }
        .td-no-border {
            padding: 20px;
        }
        .code {
            font-size: 15pt;
            font-weight: 600;
        }
    </style>
</head>