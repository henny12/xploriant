<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$firstname; ?>
            <br><br>
            You have cancelled your booking for [Itinerary Name]. You will receive a refund of [$$], which can be checked in the Profile section of our platform and transferred to your personal account on demand.
            <br><br>
            <table class="table-mail" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td-mail">
                        Traveler: [Traveler Fullname]<br>
                        Itinerary: [Itinerary Name]<br>
                        Date: [Start Date] - [End Date] OR [Start Date]<br>
                        Number of Travelers: [##]<br>
                        Total Price: [$$]<br><br>
                        Booking Code: 0000000000<br>
                    </td>
                </tr>
                <tr>
                    <td class="td-mail">
                        [Booking Request]
                    </td>
                </tr>
                <tr>
                    <td><a href="#" class="btn-blockxr">Cancelled</a></td>
                </tr>
            </table>
            <br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>