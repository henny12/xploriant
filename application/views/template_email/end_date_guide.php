<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$guide->user_fname; ?>
            <br><br>
            How’s your tour? We hope that you enjoy it. Confirm the end of your tour by clicking on the button below. Don’t forget to review your Guide to improve our community.
            <br><br>
            <a href="<?= base_url().'booking/detail/'.@$itin->itin_id.'/'.@$book->book_id ?>" class="btn-block">End Tour</a>
            <br>
            Should there be any problem throughout the tour, do not hesitate to send a report to support@xploriant.com. Your report should be submitted within 48 hours after the end of the tour, otherwise any form of refund or compensation may not be applicable
            <br><br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>
