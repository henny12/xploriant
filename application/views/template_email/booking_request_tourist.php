<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$guide->user_fname; ?>
            <br><br>
            Your booking request has been sent. Please wait for your Guide to review the booking request.
            <br><br>
            <table class="table-mail" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td-mail">
                        Traveler: <?= @$tourist->user_fname.' '.@$tourist->user_lname;?><br>
                        Itinerary: <?= ucwords(@$itin->itin_title); ?><br>
                        Date: <?= (@$book->start_date == @$book->end_date)?@$this->public_function->format_date(@$book->start_date):$this->public_function->format_date($book->start_date).' - '.@$this->public_function->format_date(@$book->end_date); ?><br>
                        Number of Travelers: <?= '#'.@$book->total_tourist; ?><br>
                        Total Price: <?= strtoupper(@$book->total_price_currency) .' '. number_format(@$book->total_price_nominal, 0,",","."); ?><br><br>
                        Booking Code: <?= @$book->book_code; ?><br>
                    </td>
                </tr>
                <tr>
                    <td class="td-mail">
                        <?= @$book->book_request; ?>
                    </td>
                </tr>
                <tr>
                    <td><a href="<?= base_url().'booking/detail/'.@$itin->itin_id.'/'.@$book->book_id ?>" class="btn-blockxr">Waiting for confirmation</a></td>
                </tr>
            </table>
            <br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>