<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$firstname; ?>
            <br><br>
            We have received your transfer request. You will receive the balance on your personal account within a few business days.
            <br><br>
            <table class="table-mail" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td-mail">
                        Traveler: [Traveler Fullname]<br>
                        Itinerary: [Itinerary Name]<br>
                        Date: [Start Date] - [End Date] OR [Start Date]<br>
                        Number of Travelers: [##]<br>
                        Total Price: [$$]<br><br>
                        Booking Code: 0000000000<br>
                    </td>
                </tr>
                <tr>
                    <td class="td-mail">
                        Date: MM-DD-YYYY 00:00:00<br>
                        Amount: [$$]<br>
                        Account Owner: [Owner Fullname]<br>
                        Account Number: **** **** **** 0000<br>
                        <br>
                        ITEM DETAILS<br>
                        [Itinerary Name]&emsp;&emsp;&emsp;&emsp;[$$]<br>
                        Total&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;[$$]<br>
                        <br>
                        CUSTOMER DETAILS<br>
                        Customer ID: 0000000000<br>
                        Email: example@gmail.com<br>
                    </td>
                </tr>
                <tr>
                    <td><a href="#" class="btn-blockxr">Transfer Requested</a></td>
                </tr>
            </table>
            <br>
            Should you not receive the balance after 5 business days, do not hesitate to contact support@xploriant.com for help.
            <br><br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>