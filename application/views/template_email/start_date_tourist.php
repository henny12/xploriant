<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$tourist->user_fname; ?>
            <br><br>
            A friendly reminder... You are traveling with <?= @$guide->user_fname .' '. @$guide->user_lname; ?> on <?= ucwords(@$itin->itin_title); ?> tomorrow. Start packing your bags and make sure you have everything you need. A great adventure awaits you!
            <br><br>
            <table class="table-mail" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td-no-border">
                        <span class="code"><center><?= $book->book_token; ?></center></span>
                    </td>
                </tr>
            </table>
            <br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>