<?php $this->load->view('template_email/header_email'); ?>
    <body>
        <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="center">
                    <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                        <!-- Logo -->
                        <tr>
                            <td class="email-masthead">
                              <a class="email-masthead_name">Xploriant</a>
                            </td>
                        </tr>
                        <!-- Email Body -->
                        <tr>
                            <td class="email-body" width="100%">
                                <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                                    <!-- Body content -->
                                    <tr>
                                        <td class="content-cell">
                                            <h1>Verify your email address</h1>
                                            <p>Thanks for signing up for Xploriant! We're excited to have you as an early user.</p>
                                            <a href="<?= base_url().'auth/verify_mail/'.@$email.'/'.@$code; ?>" class="button" style="color:white !important;">Verify Email</a>
                                            <br><br>
                                            <p>Thanks,<br>Xploriant Team</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>