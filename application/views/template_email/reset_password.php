<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$firstname; ?>
            <br><br>
            We received a request to reset your password for your Xploriant account: <?= urldecode(@$email); ?>. <br>
            Simply click on the button to set a new password.
            <br><br>
            <a href="<?= base_url().'auth/reset_password/'.@$email; ?>" class="btn-block">Reset Password</a>
            <br><br>
            If you didn't send this request, don't worry! You can ignore this email and rest assured that your password is still safe.
            <br><br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>