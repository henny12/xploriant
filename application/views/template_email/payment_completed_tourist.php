<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$tourist->user_fname; ?>
            <br><br>
            Thank you for completing your booking with Xploriant. Hope you enjoy your travels
            <br><br>
            <table class="table-mail" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="td-mail">
                        Traveler: <?= @$tourist->user_fname.' '.@$tourist->user_lname;?><br>
                        Itinerary: <?= ucwords(@$itin->itin_title); ?><br>
                        Date: <?= (@$book->start_date == @$book->end_date)?@$this->public_function->format_date(@$book->start_date):$this->public_function->format_date($book->start_date).' - '.@$this->public_function->format_date(@$book->end_date); ?><br>
                        Number of Travelers: <?= '#'.@$book->total_tourist; ?><br>
                        Total Price: <?= strtoupper(@$book->total_price_currency) .' '. number_format(@$book->total_price_nominal, 0,",","."); ?><br><br>
                        Booking Code: <?= @$book->book_code; ?><br>
                    </td>
                </tr>
                <tr>
                    <td class="td-mail">
                        Date: <?= @$book->book_paid_date; ?><br>
                        Amount: <?= @$book->total_price_nominal; ?><br>
                        <!-- Cardholder: [Cardholder Fullname]<br>
                        Card Number: **** **** **** 0000<br>
                        Account: Visa/Mastercard/etc.<br> -->
                        <br>
                        <table>
                            <tr>
                                <td colspan="2">ITEM DETAILS</td>
                            </tr>
                            <tr>
                                <td><?= ucwords(@$itin->itin_title); ?></td>
                                <td>&emsp;&emsp;<?= @$book->total_price_currency.' '.@$book->total_price_nominal; ?></td>
                            </tr>
                            <tr>
                                <td>TOTAL</td>
                                <td>&emsp;&emsp;<?= @$book->total_price_currency.' '.@$book->total_price_nominal; ?></td>
                            </tr>
                        </table>
                        <br>
                        <!-- ITEM DETAILS<br>
                        [Itinerary Name]&emsp;&emsp;&emsp;&emsp;[$$]<br>
                        Total&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;[$$]<br>
                        <br> -->
                        CUSTOMER DETAILS<br>
                        Customer ID: <?= @$tourist->user_id; ?> <br>
                        Email: <?= @$accountturis->user_email; ?><br>
                    </td>
                </tr>
                <tr>
                    <td><a href="<?= base_url().'booking/detail/'.@$itin->itin_id.'/'.@$book->book_id ?>" class="btn-blockxr">Paid</a></td>
                </tr>
            </table>
            <br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>