<?php $this->load->view('template_email/header'); ?>
    <body>
        <div class="header">
            <center><img src="<?= 'http://xploriant.com/assets/images/xpositive.png'?>" class="logo"></center>
        </div>
        <div class="konten">
            Hi, <?= @$firstname; ?>
            <br><br>
            Welcome to Xploriant. Please verify your email address to complete setting up your Xploriant account.
            <br><br>
            <a href="<?= base_url().'auth/verify_mail/'.@$email.'/'.@$code; ?>" class="btn-block">Verify Now</a>
            <br>
            Cheers,<br>Xploriant
        </div>
        <div class="footer">
            <p><span class="f-title">Xploriant</span><br>Setrasari Mall B8 | Bandung, Indonesia<br>support@xploriant.com</p>
            <p></p>
        </div>
    </body>
</html>