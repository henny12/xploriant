<div class="container">
    <div class="row margin6">
        <div class="col-md-12">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
        </div>
        <div class="col-md-12">
            <center><h2>Be A Guide</h2></center>
            <hr class="hr-guide">
        </div>
    </div>
    <div class="row margin1">
        <div class="col-md-4" align="center">
            <img src="<?= base_url() ?>assets/images/icon/user2.png" style="width:200px" class="img-responsive">
            <h3 class="title-guide">Socialize</h3>
            <p align="center">Guiding with Xploriant<br>allows you to share your<br>stories with people from<br> around the world.</p>
        </div>
        <div class="col-md-4" align="center">
            <img src="<?= base_url() ?>assets/images/icon/triangle.png" style="width:200px" class="img-responsive">
            <h3 class="title-guide">Promote</h3>
            <p align="center">Guiding with Xploriant<br>allows you to promote the<br>beauty of your hometown<br>and its unique culture.
            </p>
        </div>
        <div class="col-md-4" align="center">
            <img src="<?= base_url() ?>assets/images/icon/pig.png" style="width:200px" class="img-responsive">
            <h3 class="title-guide">Earn</h3>
            <p align="center">Guiding with Xploriant<br>allows you to earn extra<br> income as a freelancer.\</p>
        </div>
    </div>
    <?php if (!$complete_profile) { ?>
        <div class="row margin1">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <h3 align="center">You need to complete your profile to be a Guide<br><small>Your profile helps us introduce you to Tourists.</small></h3>
                        <a href="<?= base_url().'myprofile';?>" class="btn btn-danger btn-x">Complete Profile</a>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="row margin1">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <h3 align="left">Create New Itinerary<br><small>Start your Guiding experience by publishing an itinerary.<br>Choose the duration of your tour.</small></h3>
                        <a href="<?= base_url(); ?>guide/itinerary/sd" class="btn btn-danger btn-x">Single-Day</a>
                        <p class="line">or</p>
                        <a href="<?= base_url(); ?>guide/itinerary/md" class="btn btn-danger btn-x">Multi-Day</a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>