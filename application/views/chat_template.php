<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('include/header');?>
<style>
    .fileDiv {
  position: relative;
  overflow: hidden;
}
.upload_attachmentfile {
  position: absolute;
  opacity: 0;
  right: 0;
  top: 0;
}
.btnFileOpen {margin-top: -50px; }

.right > .direct-chat-text {
    background: #d2d6de !important;
    border-color: #d2d6de;
    color: #444;
    text-align: right;
}
.right > .direct-chat-text:after{
  border-left-color: #3c8dbc !important;
}
.right > .direct-chat-text {
    background: #3c8dbc !important;
    border-color: #3c8dbc;
    color: #fff;
    text-align: right;
}
.spiner{}
.spiner .fa-spin { font-size:24px;}
.attachmentImgCls{ width:450px; margin-left: -25px; cursor:pointer; }
/*.right > .direct-chat-text{
    background: #05728f !important;
}*/
</style>
 
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php $this->load->view('include/topbar');?>

<!-- Left side column. contains the logo and sidebar -->

<?php //$this->load->view('include/sidebar');?>

<!-- Content Wrapper. Contains page content -->

<div style="margin-top: 50px"> 
  
  <!-- Content Header (Page header) -->
  
<div class="content messaging">
    <div class="inbox_msg">
        <div class="inbox_people">
            <div class="headind_srch">
                <div class="recent_heading">
                    <h4>Recent Message</h4>
                </div>
            </div>
            <div class="inbox_chat">
                <?php if(!empty($vendorslist)){
                      foreach($vendorslist as $v){
                ?>
                    <div class="chat_list selectVendor <?= ($v['read_status'] == 0)?'unread':'';?>" id="<?=$v['id'];?>" title="<?=$v['name'];?>">
                        <div class="chat_people">
                            <div class="chat_img"><img src="<?=$v['picture_url'];?>" alt="<?=$v['name'];?>" title="<?=$v['name'];?>"></div>
                            <div class="chat_ib">
                                <h5><a class="users-list-name" href="#"><?=$v['name'];?></a><span class="chat_date"><?=$v['last_date'];?></span></h5>
                                <p><?=$v['last_message'];?></p>
                            </div>
                        </div>
                    </div>
                <?php }}else{?>
                    <div class="chat_list">
                        <div class="chat_people">
                            <div class="chat_ib">
                                <h5><a class="users-list-name" href="#">No message found...</a></h5>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="headind_srch">
            <div class="recent_heading">
                <h4 class="box-title" id="ReciverName_txt"><?= (empty($chatTitle)?'Select User':$chatTitle);?></h4>
            </div>
        </div>
        <div class="mesgs" id="chatSection" style="background-color: white">
            <div class="msg_history">
                <div class="direct-chat-messages" id="content">
                    <div id="dumppy"></div>
                </div>
            </div>
            <div class="input-group">
                <input type="hidden" id="ReciverId_txt">
                <input type="text" name="message" placeholder="Type Message ..." class="form-control message">
                    <span class="input-group-btn">
                       <button type="button" class="btn btn-success btn-flat btnSend" id="nav_down">Send</button>
                       <div class="fileDiv btn btn-info btn-flat"> <i class="fa fa-upload"></i> 
                       <input type="file" name="file" class="upload_attachmentfile"/></div>
                    </span>
            </div>
        </div>
    </div>
</div>   
  
  
</div>

<!-- /.content-wrapper --> 

<!-- Modal -->
<div class="modal fade" id="myModalImg">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="modelTitle">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <img id="modalImgs" src="uploads/attachment/21_preview.png" class="img-thumbnail" alt="Cinque Terre">
        </div>
        
        <!-- Modal footer -->
         
        
      </div>
    </div>
  </div>
<!-- Modal -->
<style type="text/css">
    .unread {
      background-color: #b7c7d6;
    }
    input.form-control.message {
      height: 34px !important;
    }
    .container{max-width:1170px; margin:auto; margin-top: 100px;}
    img{ max-width:100%;}
    .inbox_people {
      background: #f8f8f8 none repeat scroll 0 0;
      float: left;
      overflow: hidden;
      width: 40%; border-right:1px solid #c4c4c4;
    }
    .inbox_msg {
      border: 1px solid #c4c4c4;
      clear: both;
      overflow: hidden;
    }
    .top_spac{ margin: 20px 0 0;}


    .recent_heading {float: left; width:100%;}
    .srch_bar {
      display: inline-block;
      text-align: right;
      width: 60%; padding:
    }
    .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4; background-color: white}

    .recent_heading h4 {
      color: #05728f;
      font-size: 21px;
      margin: auto;
    }
    .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
    .srch_bar .input-group-addon button {
      background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
      border: medium none;
      padding: 0;
      color: #707070;
      font-size: 18px;
    }
    .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

    .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
    .chat_ib h5 span{ font-size:13px; float:right;}
    .chat_ib p{ font-size:14px; color:#989898; margin:auto}
    .chat_img {
      float: left;
      width: 11%;
    }
    .chat_ib {
      float: left;
      padding: 0 0 0 15px;
      width: 88%;
    }

    .chat_people{ overflow:hidden; clear:both;}
    .chat_list {
      border-bottom: 1px solid #c4c4c4;
      margin: 0;
      padding: 18px 16px 10px;
    }
    .inbox_chat { 
      height: 450px; 
      overflow-y: scroll;}

    .active_chat{ background:#ebebeb;}

    .incoming_msg_img {
      display: inline-block;
      width: 6%;
    }
    .received_msg {
      display: inline-block;
      padding: 0 0 0 10px;
      vertical-align: top;
      width: 92%;
     }
     .received_withd_msg p {
      background: #ebebeb none repeat scroll 0 0;
      border-radius: 3px;
      color: #646464;
      font-size: 14px;
      margin: 0;
      padding: 5px 10px 5px 12px;
      width: 100%;
    }
    .time_date {
      color: #747474;
      display: block;
      font-size: 12px;
      margin: 8px 0 0;
    }
    .received_withd_msg { width: 57%;}
    .mesgs {
      float: left;
      padding: 30px 15px 0 25px;
      width: 60%;
      /*height: 450px;*/
    }

     .sent_msg p {
      background: #05728f none repeat scroll 0 0;
      border-radius: 3px;
      font-size: 14px;
      margin: 0; color:#fff;
      padding: 5px 10px 5px 12px;
      width:100%;
    }
    .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
    .sent_msg {
      float: right;
      width: 46%;
    }
    .input_msg_write input {
      background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
      border: medium none;
      color: #4c4c4c;
      font-size: 15px;
      min-height: 48px;
      width: 100%;
    }

    .type_msg {border-top: 1px solid #c4c4c4;position: relative;}
    .msg_send_btn {
      background: #05728f none repeat scroll 0 0;
      border: medium none;
      border-radius: 50%;
      color: #fff;
      cursor: pointer;
      font-size: 17px;
      height: 33px;
      position: absolute;
      right: 0;
      top: 11px;
      width: 33px;
    }
    .messaging { padding:50px;}
    .msg_history {
      /*height: 1000px;*/
      overflow-y: auto;
    }
</style>  
<?php $this->load->view('include/footer');?>
<script src="<?=base_url('public/chat/chat.js');?>"></script> 
</body>
</html>
