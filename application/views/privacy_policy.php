<div class="container mt-term">
    <p align="right"><a href="<?= base_url().'privacypolicy?lang=in' ?>" class="link-lang"><img src="<?= base_url().'assets/images/ind.png' ?>" class="flag-lang">Bahasa Indonesia</a> | <a href="<?= base_url().'privacypolicy?lang=en' ?>" class="link-lang"><img src="<?= base_url().'assets/images/eng.png' ?>" class="flag-lang">English</a></p>
    <?php if (empty($lang) || $lang == 'en') { ?>
        <div class="wrap">
            <div class="row">
                <div class="col-md-12">
                    <h3 align="center">PRIVACY POLICY</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <ol align="justify">
                        <li>Introduction</li>
                            <ol type="a">
                                <li>Thank you for using Xploriant! Your trust is the foundation of our services and we are committed to ensuring the security of your privacy and personal information. The information we collect helps us develop our services and maintain a safe community. We are committed to make sure that your personal information is handled properly.
                                </li>
                            </ol>
                        <li>Definitions</li>
                            <ol type="a">
                                <li>All terms used in the Privacy Policy are previously defined in our Terms of Service.</li>
                            </ol>
                        <li>Language</li>
                            <ol type="a">
                                <li>Should there be ambiguity due to deviation of meaning between the English version and versions in other languages, the English version shall prevail.</li>
                            </ol>
                        <li>Information Xploriant Collects</li>
                            <ol type="a">
                                <li>
                                    <b>Account Information.</b> When signing up, Xploriant requires certain information such as your first name, last name, email address and date of birth. We request and collect the aforementioned personal information to comply with our legal obligations necessary to provide our services.
                                </li>
                                <li>
                                    <b>Identity Verification Information.</b> Xploriant requires you to provide an identity verification information such as but not limited to images of your government-issued ID and/or social security number or passport number. We request and collect the aforementioned personal information to help detect fraudulent activities and maintain a safe community.
                                </li>
                                <li>
                                    <b>Profile Information.</b> Additional profile information such as gender, nationality, city of residence, language, personal description and personal interests is mandatory for Guide and optional for Traveler. We request, collect, and make the aforementioned personal information available to the public to provide a better user experience.
                                </li>
                                <li>
                                    <b>Itinerary Information.</b> Xploriant requires Guides to provide information about the tour when creating their Itineraries. We request, collect, and make the aforementioned information available to the public to provide a better user experience.
                                </li>
                                <li>
                                    <b>Payment Information.</b> Xploriant requires you to provide certain payment information such as but not limited to bank account and credit card information to conduct payments.
                                </li>
                                <li>
                                    <b>Communications with Xploriant and Other Registered Users.</b> Xploriant records both contents of communication with Xploriant through reports and feedbacks and contents of communication with other Registered Users on our messaging platform. We collect the aforementioned information to help detect fraudulent activities and maintain a safe community.
                                </li>
                                <li>
                                    <b>Other Information.</b> Registered Users may provide additional information by filling out surveys, communicating with our customer support, posting on community forums, participating in promotions, and sharing your unique Xploriant experience with us. Information collected from the aforementioned sources will only be made available to the public with the user’s consent.
                                </li>
                            </ol>
                        <li>Why Xploriant Collects Data</li>
                            <ol type="a">
                                <li>Providing and Improving Our Services
                                    <ol type="i">
                                        <li>We may use the personal information you share to provide and improve our services such as:
                                            <ol>
                                                <li>To enable you to access features in the Xploriant platforms</li>
                                                <li>To enable you to connect and communicate with other members</li>
                                                <li>To optimize user experience by conducting research and analytics</li>
                                                <li>To provide customer support</li>
                                                <li>To detect and prevent fraudulent activities</li>
                                                <li>To personalize user experience by customizing Itinerary recommendations and  search results</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>Maintaining a Safe Community
                                    <ol type="i">
                                        <li>We may use the personal information you share to maintain a safe community such as:
                                            <ol>
                                                <li>To detect and prevent fraudulent activities and spam</li>
                                                <li>To anticipate any form of abuse of our services</li>
                                                <li>To eliminate contents that (i) are misleading or fraudulent, (ii) incorporate pornographic material, (iii) endorse discrimination, racism or bigotry, (iv) contain violence or threats, (v) endorse illegal activities, or (vi) market illegal substances or endorse substance abuse</li>
                                                <li>To verify the identity of our members and authenticate the information they provide</li>
                                                <li>To conduct security investigations and risk assessments</li>
                                                <li>To comply with legal obligations</li>
                                                <li>To enforce our Terms of Service</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>Improving Our Marketing
                                    <ol type="i">
                                        <li>We may use the personal information you share to improve our marketing such as:
                                            <ol>
                                                <li>To personalize user experience by customizing Itinerary recommendations and  search results</li>
                                                <li>To profile your preferences</li>
                                                <li>To send you promotional messages and advertising that may be of your interest</li>
                                                <li>To measure and improve our advertising</li>
                                                <li>To invite you to relevant events</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>Providing Payment Services
                                    <ol type="i">
                                        <li>We may use the personal information you share to provide Payment Services such as:
                                            <ol>
                                                <li>To enable you to access the Payment Services</li>
                                                <li>To detect and prevent fraudulent activities</li>
                                                <li>To conduct security investigations and risk assessments</li>
                                                <li>To comply with legal obligations</li>
                                                <li>To enforce our Terms of Service</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        <li>Sharing & Disclosure</li>
                            <ol type="a">
                                <li>Advertising
                                    <ol type="i">
                                        <li>
                                            Xploriant only shares your personal information, booking history and cookies within our platforms or with third party social media platforms with your consent and only for marketing purposes. Personal information shared includes (i) email address, (ii) full name, (iii) date of birth, (iv) gender, (v) nationality, (vi) city of residence, (vii) language, (viii) personal description, and (ix) personal interests. The disclosure of the aforementioned information, serves to increase the effectiveness of our marketing and provide you with personalized advertisements.
                                        </li>
                                        <li>
                                            Xploriant has the right to share the contents of any published Itinerary on any third party social media platforms for marketing purposes. Xploriant will not share removed Itineraries, but is not responsible to remove past advertisements that include Itineraries that are removed after the advertising campaign has started. Sharing the contents of published Itineraries may include related media contents and Guide’s information.
                                        </li>
                                    </ol>
                                </li>
                                <li>Sharing between Registered Users
                                    <ol type="i">
                                        <li>
                                            During the process of booking ─ including sending, accepting or declining booking request, and making payments ─ Xploriant may share some personal information and booking history to the corresponding Guide or Traveler. The sharing of the aforementioned information serves to provide a better user experience.
                                        </li>
                                    </ol>
                                </li>
                                <li>Profiles
                                    <ol type="i">
                                        <li>
                                            Parts of your profile are available to the public, including but not limited to (i) full name, (ii) profile picture, (iii) gender, (iv) nationality, (v) city of residence, (vi) language, (vii) personal description, and (viii) personal interests.  Visitors of the Xploriant platform, registered or not, may view the aforementioned fields by clicking on the links to your profile. The aforementioned fields are available to the public in order to assist potential Travelers in deciding their bookings.
                                        </li>
                                        <li>
                                            Parts of your profile are not available to the public, including but not limited to (i) email address, (ii) personal identification number, (iii) date of birth, and (iv) account balance. Xploriant is committed to ensuring the privacy and security of the aforementioned information.
                                        </li>
                                    </ol>
                                </li>
                                <li>Itinerary Contents
                                    <ol type="i">
                                        <li>All fields and descriptions in the Itinerary are available to the public in order to assist potential Travelers in deciding their bookings.</li>
                                    </ol>
                                </li>
                            </ol>
                        <li>Security
                            <ol type="a">
                                <li>Our team is committed to continuously improving our security measures to prevent your personal information from being stolen and/or misappropriated by another party. Should you believe that your personal information has been stolen and/or misappropriated by another party, please report immediately to support@xploriant.com.</li>
                            </ol>
                        </li>
                        <li>Changes to Xploriant Privacy Policy
                            <ol type="a">
                                <li>Xploriant reserves the right to modify this Privacy Policy. Should we make changes to this Privacy Policy, we will post the latest version on our platform, alongside the “Last Updated” date. We will also notify all Registered Users about the changes at least 30 days before the revisions become effective. Should any Registered User do not agree to the amendments to the Privacy Policy, please request the termination of your account by sending an email to support@xploriant.com. If you do not request the termination of your Xploriant account before the date the revised Privacy Policy becomes effective, your continued usage of our platform will be subject to our revised Privacy Policy.</li>
                            </ol>
                        </li>
                        <li>Contact Us
                            <ol type="a">
                                <li>Should you have any inquiries or concerns about this Privacy Policy or our data security practices, please contact support@xploriant.com.</li>
                            </ol>
                        </li>
                    </ol>
                    <br><br>
                </div>
            </div>
        </div>
    <?php } elseif ($lang == 'in') { ?>
        <div class="wrap">
            <div class="row">
                <div class="col-md-12">
                    <h3 align="center">KEBIJAKAN PRIVASI</h3>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <ol align="justify">
                        <li>Pengantar</li>
                            <ol type="a">
                                <li>Terima kasih atas kepercayaan Anda menggunakan layanan Xploriant. Kepercayaan Anda adalah dasar dari layanan kami dan kami berkomitmen untuk menjaga keamanan privasi dan data pribadi Anda. Data yang kami kumpulkan membantu kami untuk meningkatkan kualitas layanan kami dan menjaga keamanan komunitas Xploriant. Kami berkomitmen untuk memastikan bahwa data pribadi Anda ditangani dengan tepat.
                                </li>
                            </ol>
                        <li>Definisi</li>
                            <ol type="a">
                                <li>Semua istilah yang digunakan di Kebijakan Privasi ini sudah didefinisikan di Ketentuan Layanan kami.</li>
                            </ol>
                        <li>Bahasa</li>
                            <ol type="a">
                                <li>Apabila terdapat ambiguitas akibat penyimpangan makna antara versi Bahasa Inggris dan versi bahasa lain, maka versi Bahasa Inggris akan berlaku.</li>
                            </ol>
                        <li>Data yang Kami Kumpulkan</li>
                            <ol type="a">
                                <li>
                                    <b>Informasi Akun.</b> Saat registrasi, Xploriant mewajibkan beberapa informasi pribadi seperti nama penuh, alamat surel, dan tanggal lahir. Kami mewajibkan dan mengumpulkan data pribadi tersebut untuk mematuhi kewajiban hukum yang berlaku untuk menyediakan layanan kami.
                                </li>
                                <li>
                                    <b>Verifikasi Identitas.</b> Xploriant mewajibkan Anda untuk menyediakan informasi yang berhubungan dengan verifikasi identitas seperti foto identitas yang dikeluarkan pemerintah dan/atau nomor KTP atau paspor. Kami mewajibkan dan mengumpulkan data pribadi tersebut untuk mencegah penipuan dan menjaga keamanan komunitas kami.
                                </li>
                                <li>
                                    <b>Profil.</b> Informasi profil tambahan seperti jenis kelamin, kewarganegaraan, kota tempat tinggal, bahasa, deskripsi pribadi, dan minat merupakan wajib bagi Guide dan tidak wajib bagi Traveler. Kami mewajibkan, mengumpulkan, dan membuat data pribadi tersebut terbuka untuk umum untuk meningkatkan layanan kami.
                                </li>
                                <li>
                                    <b>Rencana Perjalanan.</b> Xploriant mewajibkan Guide untuk menyediakan informasi mengenai tur mereka saat membuat Rencana Perjalanan. Kami mewajibkan, mengumpulkan, dan membuat data pribadi tersebut terbuka untuk umum untuk meningkatkan layanan kami.
                                </li>
                                <li>
                                    <b>Pembayaran.</b> Xploriant mewajibkan Anda untuk menyediakan beberapa informasi pembayaran seperti akun bank dan/atau informasi kartu kredit untuk menyediakan layanan pembayaran.
                                </li>
                                <li>
                                    <b>Komunikasi dengan Xploriant dan Pengguna Lain.</b> Xploriant merekam semua komunikasi dengan Xploriant melalui laporan dan keluhan, serta semua komunikasi dengan pengguna lain melalui platform olahpesan kami. Kami mengumpulkan data tersebut untuk mencegah penipuan dan menjaga keamanan komunitas kami.
                                </li>
                                <li>
                                    <b>Lain-lain.</b> Pengguna dapat menyediakan informasi tambahan dengan mengisi formulir survei, berkomunikasi dengan pelayanan pelanggan kami, memposting di forum komunitas, berpartisipasi dalam promosi, dan berbagi pengalaman menarik Anda dengan Xploriant. Data yang dikumpulkan tersebut hanya akan dibuat terbuka untuk umum dengan izin pengguna.
                                </li>
                            </ol>
                        <li>Mengapa Kami Mengumpulkan Data</li>
                            <ol type="a">
                                <li>Menyediakan dan Memperbarui Layanan Kami
                                    <ol type="i">
                                        <li>Kami menggunakan data Anda untuk menyediakan dan meningkatkan kualitas layang kami dengan:
                                            <ol>
                                                <li>Memungkinkan Anda untuk mengakses fitur di platform Xploriant</li>
                                                <li>Memungkinkan Anda untuk terkoneksi dan berkomunikasi dengan pengguna lain</li>
                                                <li>Mengoptimalkan pengalaman pengguna dengan mengadakan riset dan analisis</li>
                                                <li>Menyediakan layanan pelanggan</li>
                                                <li>Mendeteksi dan mencegah penipuan</li>
                                                <li>Mempersonalisasi pengalaman pengguna dengan menyesuaikan rekomendasi Rencana Perjalanan dan hasil pencarian</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>Menjaga Keamanan
                                    <ol type="i">
                                        <li>Kami menggunakan data Anda untuk menjaga keamanan komunitas kami dengan:
                                            <ol>
                                                <li>Mendeteksi dan mencegah penipuan dan spam</li>
                                                <li>Mengantisipasi segala jenis penyalahgunaan layanan kami</li>
                                                <li>Menghapus konten yang (i) bersifat menyesatkan atau menipu, (ii) mengandung materi pornografi, (iii) mendukung diskriminasi atau rasialisme, (iv) mengandung kekerasan atau ancaman, (v) mendukung kegiatan ilegal, atau (vi) memperjual-belikan narkotika dan obat-obatan terlarang atau mendukung penyalahgunaan narkotika dan obat-obatan terlarang</li>
                                                <li>Memverifikasi identitas pengguna kami dan memastikan keaslian data yang mereka bagikan</li>
                                                <li>Menjalankan langkah-langkah keamanan  dan penilaian resiko</li>
                                                <li>Mematuhi kewajiban hukum</li>
                                                <li>Melaksanakan Ketentuan Layanan kami</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>Meningkatkan Pemasaran
                                    <ol type="i">
                                        <li>Kami menggunakan data Anda untuk meningkatkan kualitas pemasaran kami dengan:
                                            <ol>
                                                <li>Mempersonalisasi pengalaman pengguna dengan menyesuaikan rekomendasi Rencana Perjalanan dan hasil pencarian</li>
                                                <li>Mencatat preferensi Anda</li>
                                                <li>Mengirimkan pesan promosi dan iklan yang mungkin sesuai dengan minat Anda</li>
                                                <li>Mengukur dan meningkatkan iklan kami</li>
                                                <li>Mengundang Anda ke acara-acara yang bersangkutan</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li>Menyediakan Layanan Pembayaran
                                    <ol type="i">
                                        <li>Kami menggunakan data Anda untuk menyediakan layanan pembayaran dengan:
                                            <ol>
                                                <li>Memungkinkan Anda untuk mengakses Layanan Pembayaran</li>
                                                <li>Mendeteksi dan mencegah penipuan</li>
                                                <li>Menjalankan langkah-langkah keamanan  dan penilaian resiko</li>
                                                <li>Mematuhi kewajiban hukum</li>
                                                <li>Melaksanakan Ketentuan Layanan kami</li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                            </ol>
                        <li>Berbagi dan Mentransfer Data</li>
                            <ol type="a">
                                <li>Iklan
                                    <ol type="i">
                                        <li>
                                            Xploriant hanya membagikan data pribadi, sejarah pemesanan, dan cookie Anda di platform kami atau dengan platform media sosial pihak ketiga dengan persetujuan Anda. Data pribadi yang dibagikan antara lain (i) alamat surel, (ii) nama penuh, (iii) tanggal lahir, (iv) jenis kelamin, (v) kewarganegaraan, (vi) kota tempat tinggal, (vii) bahasa, (viii) deskripsi pribadi, dan (ix) minat. Penyingkapan data pribadi tersebut dimaksudkan untuk meningkatkan efektifitas pemasaran kami dan menyediakan iklan sesuai preferensi Anda.
                                        </li>
                                        <li>
                                            Xploriant berhak membagikan semua konten Rencana Perjalanan Anda dengan platform media sosial pihak ketiga manapun untuk kepentingan pemasaran. Xploriant tidak akan membagikan konten Rencana Perjalanan yang sudah dihapus, tetapi tidak berkewajiban untuk menghapus iklan yang mengandung konten Rencana Perjalanan yang dihapus setelah iklan dipasang. Membagikan konten Rencana Perjalanan termasuk konten media dan informasi umum tentang Guide yang terkait.
                                        </li>
                                    </ol>
                                </li>
                                <li>Berbagi dengan Pengguna Lain
                                    <ol type="i">
                                        <li>
                                            Selama proses pemesanan ─ termasuk proses mengirim, menerima, atau menolak permintaan pemesanan, dan proses pembayaran ─ Xploriant berhak membagikan beberapa data pribadi dan sejarah pemesanan dengan Guide atau Traveler yang berkaitan. Penyingkapan informasi tersebut dimaksudkan untuk meningkatkan layanan kami.
                                        </li>
                                    </ol>
                                </li>
                                <li>Profil
                                    <ol type="i">
                                        <li>
                                            Sebagian profil Anda terbuka untuk umum, antara lain (i) nama penuh, (ii) gambar profil, (iii) jenis kelamin, (iv) kewarganegaraan, (v) kota tempat tinggal, (vi) bahasa, (vii) deskripsi pribadi, dan (viii) minat. Semua pengguna platform Xploriant, baik yang terdaftar maupun tidak, dapat melihat data yang disebutkan di atas dengan mengklik tautan ke profil Anda. Data pribadi yang disebutkan di atas terbuka untuk publik untuk membantu calon Traveler memutuskan pemesanan mereka.
                                        </li>
                                        <li>
                                            Sebagian profil Anda tidak terbuka untuk umum, antara lain (i) alamat surel, (ii) nomor KTP atau paspor, (iii) tanggal lahir, dan (iv) saldo akun. Xploriant berkomitmen untuk memastikan keamanan dan kerahasiaan data pribadi yang disebut di atas.
                                        </li>
                                    </ol>
                                </li>
                                <li>Rencana Perjalanan
                                    <ol type="i">
                                        <li>Semua bidang dan deskripsi yang tertera di Rencana Perjalan terbuka untuk umum untuk membantu calon Traveler dalam memutuskan pemesanan mereka.</li>
                                    </ol>
                                </li>
                            </ol>
                        <li>Keamanan
                            <ol type="a">
                                <li>Tim kami berkomitmen untuk senantiasa meningkatkan keamanan data pribadi Anda untuk mencegah adanya pencurian atau penyalahgunaan oleh pihak lain. Apabila Anda merasa bahwa data pribadi Anda telah dicuri dan/atau disalahgunakan oleh pihak lain, laporkan seketika ke support@xploriant.com.</li>
                            </ol>
                        </li>
                        <li>Perubahan Kebijakan Privasi
                            <ol type="a">
                                <li>Xploriant berhak mengubah dan/atau memodifikasi Kebijakan Privasi ini. Apabila kami melakukan perubahan terhadap Kebijakan Privasi ini, kami akan memasang versi terakhir tersebut di platform kami, beserta tanggal “Terakhir Diperbarui”. Kami juga akan memberitahukan semua Pengguna mengenai perubahan tersebut setidaknya 30 hari sebelum versi Kebijakan Privasi tersebut berlaku. Apabila Anda tidak menyetujui perubahan pada Kebijakan Privasi, silahkan mengajukan penghentian akun dengan mengirim surat elektronik ke support@xploriant.com. Jika Anda tidak mengajukan penghentian akun Xploriant sebelum tanggal berlaku versi terbaru Kebijakan Privasi, Anda menyetujui versi terbaru Kebijakan Privasi kami.</li>
                            </ol>
                        </li>
                        <li>Hubungi Kami
                            <ol type="a">
                                <li>Jika Anda memiliki pertanyaan atau keluhan mengenai Kebijakan Privasi ini atau cara kami menangani data pribadi Anda, hubungi support@xploriant.com.</li>
                            </ol>
                        </li>
                    </ol>
                    <br><br>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
