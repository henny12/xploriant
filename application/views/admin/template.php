<?php $this->load->view('admin/include/header');?>
<body class="fix-header fix-sidebar card-no-border logo-center">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div id="main-wrapper">
        <?php $this->load->view('admin/include/topbar');?>
        <?php $this->load->view('admin/include/sidebar');?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <?php echo $_content; ?>
            </div>
            <footer class="footer">
                © <?= date('Y');?> - Xploriant.com
            </footer>
        </div>
    </div>
    <?php $this->load->view('admin/include/modal');?>
    <?php $this->load->view('admin/include/footer');?>
</body>
</html>
