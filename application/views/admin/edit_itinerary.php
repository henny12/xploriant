<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">Itinerary</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Itinerary</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="<?= base_url().'admin/itinerary/edit/'.$itin->itin_id; ?>" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <h4 class="card-title">Edit Itinerary</h4>
                        <h6 class="card-subtitle">(Itinerary Id : <?= $itin->itin_id; ?>)</h6>
                        <hr>
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('itin_creator_id') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Guide Id</label>
                                    <input type="text" name="itin_creator_id" class="form-control" value="<?= $itin->itin_creator_id; ?>" placeholder="Guide Id" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('itin_creator_id'); ?></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_fname') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Guide Name</label>
                                    <input type="text" name="user_fname" class="form-control" value="<?= $itin->user_fname.' '.$itin->user_lname; ?>" placeholder="Guide Name" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('user_fname'); ?></small>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update Data</button>
                        <a href="<?= base_url().'admin/account'; ?>" class="btn btn-inverse">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>