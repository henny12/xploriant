<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">User Admin</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">User Admin</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data User Admin</h4>
                <h6 class="card-subtitle">List User Admin</h6>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($user_admin)){
                                foreach ($user_admin as $val) { ?>
                            <tr>
                                <td><?= $val->admin_id; ?></td>
                                <td><?= $val->admin_user; ?></td>
                                <td><?= $val->admin_name; ?></td>
                                <td><?= ($val->admin_status == '1')?'Active':'Not Active'; ?></td>
                                <td>Delete | Edit</td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>