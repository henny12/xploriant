<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">Booking</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Booking</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
            $message = $this->session->flashdata('message');
            $type_message = $this->session->flashdata('type_message');
            echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Booking</h4>
                <h6 class="card-subtitle">List Booking</h6>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Booking Code</th>
                                <th>Itinerary Id</th>
                                <th>Guide Id</th>
                                <th>Traveler Id</th>
                                <th>Status</th>
                                <th>Last Updated</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($booking)){
                                foreach ($booking as $val) { ?>
                            <tr>
                                <td><?= $val->book_code; ?></td>
                                <td><?= $val->itin_id; ?></td>
                                <td><?= $val->guide_id; ?></td>
                                <td><?= $val->tourist_id; ?></td>
                                <td><?= $this->public_function->status_book($val->book_status);?></td>
                                <td><?= $val->updated_on; ?></td>
                                <td><a href="#changestatus-modal" class="btn btn-danger btn-xs chstat_book" data-toggle="modal" data-id="<?= $val->book_id; ?>" data-stat="<?php echo $val->book_status; ?>">Change Status</a>
                                    <a href="<?= base_url().'admin/booking/edit/'.$val->book_id; ?>" class="btn btn-primary btn-xs">Edit</a></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>