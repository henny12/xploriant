<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">Account</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Account</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
            $message = $this->session->flashdata('message');
            $type_message = $this->session->flashdata('type_message');
            echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Account</h4>
                <h6 class="card-subtitle">List Account</h6>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created</th>
                                <th>Last Active</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($account)){
                                foreach ($account as $val) { ?>
                            <tr>
                                <td><?= $val->user_id; ?></td>
                                <td><?= $val->user_fname .' '. $val->user_lname; ?></td>
                                <td><?= $val->user_email; ?></td>
                                <td><?= $val->created_on; ?></td>
                                <td><?= $val->last_login; ?></td>
                                <td><?= $this->public_function->status_account($val->user_status);?></td>
                                <td><a href="#changestatus-modal" class="btn btn-danger btn-xs chstat_account" data-toggle="modal" data-id="<?= $val->user_id; ?>" data-stat="<?php echo $val->user_status; ?>">Change Status</a>
                                    <a href="<?= base_url().'admin/account/edit/'.$val->user_id; ?>" class="btn btn-primary btn-xs">Edit</a></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>