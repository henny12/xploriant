<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">Account</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Account</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="<?= base_url().'admin/account/edit/'.$user->user_id; ?>" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <h4 class="card-title">Edit Account</h4>
                        <h6 class="card-subtitle">(User Id : <?= $user->user_id; ?>)</h6>
                        <hr>
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_fname') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">First Name</label>
                                    <input type="text" name="user_fname" class="form-control" value="<?= $user->user_fname; ?>" placeholder="First Name" required>
                                    <small class="form-control-feedback"><?php echo form_error('user_fname'); ?></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_lname') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Last Name</label>
                                    <input type="text" name="user_lname" class="form-control" value="<?= $user->user_lname; ?>" placeholder="Last Name" required>
                                    <small class="form-control-feedback"><?php echo form_error('user_lname'); ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_email') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Email Address</label>
                                    <input type="text" name="user_email" class="form-control" value="<?= $user->user_email; ?>" placeholder="Email Address" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('user_email'); ?></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_personal_id') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Identification Card</label>
                                    <input type="text" name="user_personal_id" class="form-control" value="<?= $user->user_personal_id; ?>" placeholder="Identification Card" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('user_personal_id'); ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_gender') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Gender</label>
                                    <input type="text" name="user_gender" class="form-control" value="<?= $user->user_gender; ?>" placeholder="Gender" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('user_gender'); ?></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_dob') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Date of birth</label>
                                    <input type="text" name="user_dob" class="form-control" value="<?= $user->user_dob; ?>" placeholder="Date of birth" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('user_dob'); ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_nationality') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Nationality</label>
                                    <input type="text" name="user_nationality" class="form-control" value="<?= $user->user_nationality; ?>" placeholder="Nationality">
                                    <small class="form-control-feedback"><?php echo form_error('user_nationality'); ?></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('user_residence') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">City of residence</label>
                                    <input type="text" name="user_residence" class="form-control" value="<?= $user->user_residence; ?>" placeholder="Date of birth">
                                    <small class="form-control-feedback"><?php echo form_error('user_residence'); ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php echo (form_error('user_lang') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Language</label>
                                    <input type="text" name="user_lang" class="form-control" value="<?= $user->user_lang; ?>" placeholder="Language">
                                    <small class="form-control-feedback"><?php echo form_error('user_lang'); ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php echo (form_error('user_desc') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Personal Description</label>
                                    <textarea name="user_desc" class="form-control" placeholder="Personal Description" rows="5"><?= $user->user_desc; ?></textarea>
                                    <small class="form-control-feedback"><?php echo form_error('user_desc'); ?></small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Personal Interest</label>
                                    <div class="row">
                                        <?php if(!empty($personal_interest_list)){
                                            $i = 0;
                                            $data_interest = explode('-',$user->user_interests);
                                            foreach ($personal_interest_list as $pe) {
                                                if ($pe->pe_id == @$data_interest[$i]) { ?>
                                                <div class="col-md-4">
                                                    <span class="button-checkbox">
                                                        <button type="button" class="btn btn-opt btn-aktif" data-color="primary"><?= ucwords($pe->pe_name); ?></button>
                                                    </span>
                                                </div>
                                        <?php } $i++; }} ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('created_on') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Created on</label>
                                    <input type="text" name="created_on" class="form-control" value="<?= $user->created_on; ?>" placeholder="Created On" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('created_on'); ?></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo (form_error('last_login') != "") ? "has-danger" : "" ?>">
                                    <label class="control-label">Last Active</label>
                                    <input type="text" name="last_login" class="form-control" value="<?= $user->last_login; ?>" placeholder="Last Active" disabled>
                                    <small class="form-control-feedback"><?php echo form_error('last_login'); ?></small>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update Data</button>
                        <a href="<?= base_url().'admin/account'; ?>" class="btn btn-inverse">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>