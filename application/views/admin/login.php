<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Xploriant - Administrator</title>
    <link href="<?= base_url()?>assets/material-pro/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/material-pro/horizontal/css/style.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/material-pro/horizontal/css/colors/blue.css" id="theme" rel="stylesheet">
</head>
<body>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <section id="wrapper">
        <div class="login-register" style="background-color:#042441;">        
            <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" action="<?= base_url().'admin/auth/login_process'; ?>" method="post">
                    <h3 class="box-title m-b-20">XPLORIANT - LOGIN</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="username" required placeholder="Username"> </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" required placeholder="Password"> </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Login</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </section>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/horizontal/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/horizontal/js/waves.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/horizontal/js/sidebarmenu.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/horizontal/js/custom.min.js"></script>
</body>
</html>