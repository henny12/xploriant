<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor">Itinerary</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Itinerary</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
            $message = $this->session->flashdata('message');
            $type_message = $this->session->flashdata('type_message');
            echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Itinerary</h4>
                <h6 class="card-subtitle">List Itinerary</h6>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Itinerary Id</th>
                                <th>Itinerary Title</th>
                                <th>Guide</th>
                                <th>Location</th>
                                <th>Created</th>
                                <th>Last Booked</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($itinerary)){
                                foreach ($itinerary as $val) { ?>
                            <tr>
                                <td><?= $val->itin_id; ?></td>
                                <td><?= wordwrap($val->itin_title,20,'<br>'); ?></td>
                                <td><?= 'Id : '.$val->itin_creator_id .' - '. $val->guide_name; ?></td>
                                <td><?= $val->itin_location; ?></td>
                                <td><?= $val->itin_last_booked; ?></td>
                                <td><?= $this->public_function->status_account($val->itin_status);?></td>
                                <td><a href="#changestatus-modal" class="btn btn-danger btn-xs chstat_itin" data-toggle="modal" data-id="<?= $val->itin_id; ?>" data-stat="<?php echo $val->itin_status; ?>">Change Status</a>
                                    <a href="<?= base_url().'admin/itinerary/edit/'.$val->itin_id; ?>" class="btn btn-primary btn-xs">Edit</a></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>