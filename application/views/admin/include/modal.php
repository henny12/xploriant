<!-- Modal Change Status Account -->
<div class="modal fade" id="changestatus-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span class="title-modal">Change Status</span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fa fa-times"></span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-md-offset-1 col-md-offset-1">
                        <div class="body-status">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>