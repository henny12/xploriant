<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">PERSONAL</li>
                <li class="<?= ($this->uri->segment(2) == 'dashboard')?'active':''; ?>">
                    <a class="has-arrow" href="<?= base_url().'admin/dashboard'; ?>" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                </li>
                <li class="<?= ($this->uri->segment(2) == 'account')?'active':''; ?>">
                    <a class="has-arrow " href="<?= base_url().'admin/account'; ?>" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Account</span></a>
                </li>
                <li class="<?= ($this->uri->segment(2) == 'itinerary')?'active':''; ?>">
                    <a class="has-arrow" href="<?= base_url().'admin/itinerary'; ?>" aria-expanded="false"><i class="mdi mdi-calendar"></i><span class="hide-menu">Itinerary</span></a>
                </li>
                <li class="<?= ($this->uri->segment(2) == 'booking')?'active':''; ?>">
                    <a class="has-arrow" href="<?= base_url().'admin/booking'; ?>" aria-expanded="false"><i class="mdi mdi-ticket"></i><span class="hide-menu">Bookings</span></a>
                </li>
                <!-- <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-history"></i><span class="hide-menu">Transaction</span></a>
                </li> -->
                <li class="<?= ($this->uri->segment(2) == 'user_admin')?'active':''; ?>">
                    <a class="has-arrow" href="<?= base_url().'admin/user_admin'; ?>" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu">User Admin</span></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>