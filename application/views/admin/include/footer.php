<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/horizontal/js/jquery.slimscroll.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/horizontal/js/waves.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/horizontal/js/sidebarmenu.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/horizontal/js/custom.min.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?= base_url(); ?>assets/material-pro/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>assets/custom/js/admin.js"></script>
<?php if ($this->uri->segment(2) == 'dashboard') { ?>
    <script src="<?= base_url(); ?>assets/material-pro/horizontal/js/dashboard2.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/d3/d3.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/c3-master/c3.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?= base_url(); ?>assets/material-pro/assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js"></script>
<?php } ?>