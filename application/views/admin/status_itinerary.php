<form action="<?= base_url().'admin/itinerary/change_status/'.$itin_id; ?>" method="POST">
    <label>Status</label><br>
    <div class="form-group">
        <div class="input-group">
            <input type="radio" name="status" id="active" class="form-control with-gap" value="1" <?= ($status->itin_status == 1)?'checked':''; ?> />
            <label for="active">Active</label>
        </div>
        <div class="input-group">
            <input type="radio" name="status" id="suspend" class="form-control with-gap" value="2" <?= ($status->itin_status == 2)?'checked':''; ?> />
            <label for="suspend">Suspend</label>
        </div>
    </div>
    <label>Comment</label><br>
    <div class="form-group">
        <div class="input-group mb-3">
            <textarea name="comment" class="form-control" placeholder="Comment (Hidden)"></textarea>
        </div>
    </div>
    <div class="form-group">
        <input type="submit" name="login" class="btn btn-danger btn-x" value="Save">
    </div>
</form>