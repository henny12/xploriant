<div class="container">
    <div class="row margin3">
        <div class="col-md-3"></div>
        <div class="col-md-6 box-sign">
            <h3 align="center"><b>Xploriant - Reset Your Password</b></h3>
            <hr>
            <form action="<?= base_url(); ?>auth/update_password" method="post">
                <input type="hidden" class="type" value="rp">
                <input type="hidden" name="user_id" value="<?= $account->user_id; ?>">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="New Password" id="rp-password" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Confirm New Password" id="rp-confirm" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" name="login" class="btn btn-danger btn-x" value="Reset Password">
                </div>
            </form>
        </div>
    </div>
    <br><br>
</div>
