<div class="container margin7">
    <div class="row">
        <div class="col-md-8">
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>

            <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php for($i=0;$i<count($itin_image);$i++){ ?>
                        <li data-target="#carouselExampleIndicators2" data-slide-to="<?= $i ?>"></li>
                    <?php } ?>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <?php foreach($itin_image as $val){ ?>
                        <div class="carousel-item">
                            <img class="img-responsive" src="<?= base_url('media/itin_image/'.$val->itin_img_name); ?>" alt="First slide">
                        </div>
                    <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <h3 class="itin-title"><?= ucwords($itinerary->itin_title); ?></h3>
            <p class="lbl-loc">
                <img src="<?= base_url('assets/images/icon/location.png')?>" class="icon-img"> <?= ucwords($itinerary->itin_location)?>
            </p>
            <p class="lbl-tourist">
                <img src="<?= base_url('assets/images/icon/user2.png')?>" class="icon-img"> <?= $itinerary->itin_min_tourist; ?> - <?= $itinerary->itin_max_tourist; ?> Tourists
            </p>
            <p class="lbl-tourist">
                <img src="<?= base_url('assets/images/icon/calender.png')?>" class="icon-img"> <?= ($itinerary->itin_day == 'md')? ($itinerary->itin_accom_night-1).'D/'.$itinerary->itin_accom_night.'N': $itinerary->itin_duration.' Hours'; ?>
            </p>
            <p class="lbl-tourist">
                <img src="<?= base_url('assets/images/icon/lang.png')?>" class="icon-img"> <?= $guide->user_lang; ?>
            </p>
            <br>
            <p class="lbl-tourist">
                <input type="hidden" id="itin-price" value="<?= $itinerary->itin_price_nominal; ?>">
                <img src="<?= base_url('assets/images/icon/money.png')?>" class="icon-img"> <?= strtoupper($itinerary->itin_price_currency).' '.number_format($itinerary->itin_price_nominal,0,'.',','); ?> per person
            </p>
            <hr class="hr-border">
                <h3>Overview</h3>
                <p align="justify"><?= $itinerary->itin_overview; ?></p>
            <hr class="hr-border">  
                <h3>About Your Guide</h3>
                <div class="row">
                    <div class="col-md-2">
                        <img src="<?= base_url('assets/images/icon/user.png')?>" class="foto-profil">    
                    </div>
                    <div class="col-md-5">
                        <a href="<?= base_url().'guide/profile/'.$guide->user_id; ?>" style="color: #727577"><span class="nama-profil"><?= $guide->user_fname.' '.$guide->user_lname; ?></span></a><br>
                        <?php if ($guide->user_id != $myid) { ?>
                            <form method="get" action="<?= base_url().'messages';?>">
                                <?php $id_encrypt = $this->OauthModel->Encryptor('encrypt', $guide->user_id)?>
                                <input type="hidden" name="guide_id" value="<?= $id_encrypt; ?>">
                                <input type="hidden" name="guide_name" value="<?= $guide->user_fname.' '.$guide->user_lname; ?>">
                                <input type="submit" value="Contact Guide" class="contact">
                            </form>
                        <?php } ?>
                    </div>
                </div>
                <p><?= $guide->user_desc; ?></p>
            <hr class="hr-border">
                <h3>Place We Go</h3>
                <p><?= ucwords($itinerary->itin_places); ?></p>
            <hr class="hr-border">
                <h3>What To Bring</h3>
                <p><?= ucwords($itinerary->itin_what_to_bring); ?></p>
            <hr class="hr-border">
                <h3>Meal</h3>
                <div class="row">
                    <?php if (!empty($itinerary->itin_included_meals)){
                        $meals = explode('-', $itinerary->itin_included_meals);
                        foreach ($meals as $meal) { ?>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-opt btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i> <?= ucfirst($meal); ?></button>
                            </div>
                    <?php }}else{ ?>
                        <div class="col-md-12">
                            Not Included
                        </div>
                    <?php } ?>
                </div>
            <?php if ($itinerary->itin_accom_included){ ?>
                <hr class="hr-border">
                    <h3>Accomodation</h3>
                    <p class="itin-mb"><?= $itinerary->itin_accom_night; ?> Nights</p>
                    <p class="itin-mb"><?= $itinerary->itin_accom_desc; ?></p>
                    <?php
                        $starNumber = $itinerary->itin_accom_min_rate;
                        for($x=1;$x<=$starNumber;$x++) {
                            echo '<img src='.base_url()."assets/images/fullstar.png".' class="itin-star" />';
                        }
                        while ($x<=5) {
                            echo '<img src='.base_url()."assets/images/emptystar.png".' class="itin-star" />';
                            $x++;
                        }
                    ?>
            <?php } ?>
            <hr class="hr-border">
                <h3>Included Fees</h3>
                <div class="row">
                    <?php if (!empty($itinerary->itin_included_fees)){
                        $fees = explode('-', $itinerary->itin_included_fees);
                        foreach ($fees as $fee) { ?>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-opt btn-default" data-color="primary"><i class="state-icon glyphicon glyphicon-unchecked"></i> <?= ucfirst($fee); ?></button>
                        </div>
                    <?php }}else{ ?>
                        <div class="col-md-12">
                            Not Included
                        </div>
                    <?php } ?>
                </div>
            <?php if (!empty($itinerary_detail)){ ?>
            <hr class="hr-border">
                <h3>Detailed Itinerary</h3>
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($itinerary->itin_day == 'sd'){ ?>
                            <table class="table table-bordered">
                                <?php foreach($itinerary_detail as $det) { ?>
                                <tr>
                                    <td width="20%" align="center">
                                    <?= substr($det->itd_start_time, 0,5) .' - '. substr($det->itd_end_time, 0,5); ?>
                                    </td>
                                    <td><?= ucwords($det->itd_title); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?= $det->itd_desc; ?></td>
                                </tr>
                                <?php }?>
                            </table>
                        <?php } else { ?>
                            <?php if (!empty($itinerary_detail_md)){
                                foreach($itinerary_detail_md as $det) { ?>
                                <h4>Day <?= $det->itd_day; ?></h4>
                                <table class="table table-bordered">
                                    <?php foreach ($det->detail as $val) { ?>
                                    <tr>
                                        <td width="20%" align="center">
                                        <?= substr($val->itd_start_time, 0,5) .' - '. substr($val->itd_end_time, 0,5); ?>
                                        </td>
                                        <td><?= $val->itd_title; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><?= $val->itd_desc; ?></td>
                                    </tr>
                                    <?php }?>
                                </table>
                        <?php }}} ?>
                    </div>
                </div>
            <?php } ?>
            <hr class="hr-border">
                <h3>Cancellation</h3>
                <p><?= $itinerary->itin_cancellation; ?> Days</p>
            <hr class="hr-border">
                <h3>Additional Information</h3>
                <?php if (!empty($additional_info)){ 
                    foreach ($additional_info as $add){ ?>
                    <p class="itin-mb"><?= $add; ?></p>
                <?php }} ?>
            <hr class="hr-border">
                <h3>0 Reviews</h3>
        </div>
        <div class="col-md-4">
            <div class="book" id="myBook">
                <h3 align="center">Book Itinerary</h3>
                <hr class="hr-border">
                <form autocomplete="off" method="post" action="<?= base_url().'booking/summary';?>">
                    <input type="hidden" name="itin_id" value="<?= $itinerary->itin_id; ?>">
                    <input type="hidden" name="guide_id" value="<?= $guide->user_id; ?>">
                    <input type="hidden" name="duration" value="<?= ($itinerary->itin_day == 'md')?$itinerary->itin_accom_night:''; ?>">
                    <div class="form-group">
                        <h4>Tourist</h4>
                        <input class="tch3" type="text" id="total-tourist" value="1" name="jmltourist" data-bts-button-down-class="btn btn-secondary btn-outline" data-bts-button-up-class="btn btn-secondary btn-outline" required>
                    </div>
                    <div class="form-group">
                        <h4>Start Date</h4>
                        <div class="input-group mb-3">
                            <input type="text" name="stardate" class="form-control" id="datepicker" placeholder="Start Date" required>
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <h4>Price</h4>
                        <input type="hidden" name="total_price" id="total-price" value="<?= $itinerary->itin_price_nominal?>">
                        <span id="txt-total-price">IDR <?= number_format($itinerary->itin_price_nominal,0,'.',',')?></span>
                    </div>
                    <div class="form-group">
                        <?php if ($this->session->userdata('logged_in')){ ?>
                            <input type="submit" class="btn btn-danger btn-x" value="Request To Book">
                        <?php } else { ?>
                            <a class="btn btn-danger btn-x" href="#login-modal" data-toggle="modal">Request To Book</a>
                        <?php } ?>
                    </div>
                    <p class="itin-note">You won't be charged yet.</p>
                </form>
            </div>
        </div>
    </div>
</div>