<div class="container">
    <div class="row margin3">
        <div class="col-md-3"></div>
        <div class="col-md-6 box-sign">
            <h3 align="center"><b>Xploriant - Login</b></h3>
            <br>
            <?php
                $message = $this->session->flashdata('message');
                $type_message = $this->session->flashdata('type_message');
                echo (!empty($message) && $type_message=="success") ? ' <div id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
                echo (!empty($message) && $type_message=="error") ? '   <div id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button>'.$message.'</div></div>': '';
            ?>
            <form action="<?= base_url(); ?>auth/login" method="post">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" name="email" class="form-control" placeholder="Email Address" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-key"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="basic_checkbox_1" class="filled-in" name="remember_me" />
                            <label for="basic_checkbox_1">Remember Me</label>
                        </div>
                    </label>
                    <label class="checkbox-inline pull-right">
                        <div class="demo-checkbox">
                            <input type="checkbox" id="basic_checkbox_2" class="filled-in" name="show_password" onclick="show_pass();" />
                            <label for="basic_checkbox_2">Show Password</label>
                        </div>
                    </label>
                    <!-- <span class="remember pull-right">Show Password</span> -->
                </div>
                <div class="form-group">
                    <input type="submit" name="login" class="btn btn-danger btn-x" value="Login">
                </div>
                <div class="form-group">
                    <center><a href="#forgotpassword-modal" data-toggle="modal">Forgot password?</a></center>
                </div>
            </form>
            <hr>
            <p align="center">Don't have an account ? <a href="#register-modal" data-toggle="modal">Sign Up Now</a></p>
        </div>
    </div>
    <br><br>
</div>
<script type="text/javascript">
    
</script>