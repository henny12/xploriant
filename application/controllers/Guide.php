<?php
ob_start();
class Guide extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'guide/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_member();
        $this->load->library('template');
        $this->load->model(array('PersonalInterestModel','NotRecomModel','TempImageModel','ItineraryModel','ItineraryImageModel','ItineraryDetailModel','UserDataModel','UserAccountModel','ReviewModel'));
        $this->data['page_title'] = "Be a Guide";
        $this->path_thumb = "media/itin_thumb/";
    }

    private function validate() {           
        $this->form_validation->set_rules('title', 'Title', 'trim|required');

        return $this->form_validation->run();
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['personal_interest_list'] = $this->PersonalInterestModel->fetch();
        $this->data['not_recommended_list'] = $this->NotRecomModel->fetch();
    }
    
    public function index(){
        $this->data['complete_profile'] = $this->complete_profile();
        // print_r($this->complete_profile());die();
        $this->template->display('guide',$this->data);
    }

    public function itinerary($day){
        $this->preload();

        $this->data['day'] = $day;
        $this->template->display('itinerary',$this->data);
    }

    public function complete_profile(){
        $user_id = $this->session->userdata('id');
        $data = $this->UserDataModel->by_id(['user_id' => $user_id]);
        if (!empty($data->user_fname) && !empty($data->user_lname) && !empty($data->user_gender) && !empty($data->user_nationality) && !empty($data->user_residence) && !empty($data->user_dob) && !empty($data->user_personal_id) && !empty($data->user_interests) && !empty($data->user_lang) && !empty($data->user_desc)){
            return true;
        } else {
            return false;
        }
    }

    private function fetch_input() {
        $data = array('itin_day' => $this->input->post('day'),
                    'itin_location' => strtolower($this->input->post('location')),
                    'itin_categories' => (!empty($this->input->post('category')))?implode("-",$this->input->post('category')):"",
                    'itin_title' => strtolower($this->input->post('title')),
                    'itin_overview' => $this->input->post('overview'),
                    'itin_duration' => $this->input->post('duration'),
                    'itin_min_tourist' => $this->input->post('min_tourist'),
                    'itin_max_tourist' => $this->input->post('max_tourist'),
                    'itin_included_meals' => (!empty($this->input->post('meals')))?implode("-",$this->input->post('meals')):"",
                    'itin_included_fees' => (!empty($this->input->post('fees')))?implode("-",$this->input->post('fees')):"",
                    'itin_places' => (!empty($this->input->post('dplace')))?implode(", ",$this->input->post('dplace')):"",
                    'itin_what_to_bring' => (!empty($this->input->post('dtobring')))?implode(", ",$this->input->post('dtobring')):"",
                    'itin_cancellation' => $this->input->post('cancellation'),
                    'itin_price_currency' => $this->input->post('currency'),
                    'itin_price_nominal' => $this->input->post('price'),
                    'itin_not_recom_opt' => (!empty($this->input->post('not_recomm')))?implode("-",$this->input->post('not_recomm')):"",
                    'itin_creator_id' => $this->session->userdata('id'),
                    'itin_thumb' => str_replace('"','',$this->input->post('thumb')),
                    'itin_status' => 1
        );

        return $data;
    }

    public function add_itinerary() {
        $obj = $this->fetch_input();
        if ($obj['itin_day'] == 'md'){
            $obj['itin_accom_night'] = $this->input->post('accom_night');
            $obj['itin_accom_desc'] = $this->input->post('accom_desc');
            $obj['itin_accom_min_rate'] = $this->input->post('accom_min_rate');
            $obj['itin_accom_included'] = (empty($this->input->post('accom_included'))?false:true);
        }

        if ($this->validate() != false) {
            $this->ItineraryModel->insert($obj);
            $id = $this->db->insert_id();

            $temp_photo = $this->TempImageModel->fetch();
            if (!empty($temp_photo)) {
                foreach ($temp_photo as $value) {
                    $obj_img['itin_id'] =  $id;
                    $obj_img['itin_img_name'] = $value->temp_img_name;
                    $this->ItineraryImageModel->insert($obj_img);
                }
                // $this->TempImageModel->delete(array());
                $this->db->empty_table('temp_image'); 
            }

            $itd_start_time = $this->input->post('start_time');
            if (!empty($itd_start_time)) {
                foreach ($itd_start_time as $key => $value) {
                    $det['itin_id'] = $id;
                    $det['itd_start_time'] = $value;
                    $det['itd_end_time'] = $this->input->post('end_time')[$key];
                    $det['itd_title'] = $this->input->post('activity_title')[$key];
                    $det['itd_desc'] = $this->input->post('activity_desc')[$key];
                    $det['itd_day'] = $this->input->post('activity_day')[$key];
                    $this->ItineraryDetailModel->insert($det);
                }
            }

            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(base_url('guide'));
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['news'] = (object) $obj;
            $this->template_admin->display('syspanel/news/news_insert', $this->data);
        }
    }

    public function upload_image(){
        $config['upload_path']   = FCPATH.'/media/itin_image';
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload',$config);

        if($this->upload->do_upload('userfile')){
            $token=$this->input->post('token_foto');
            $nama=$this->upload->data('file_name');
            $this->db->insert('temp_image',array('temp_img_name'=>$nama,'temp_img_token'=>$token));
        }
    }

    public function remove_img(){
        $token=$this->input->post('token');
        $img=$this->TempImageModel->by_id(array('temp_img_token' => $token));

        if(!empty($img)){
            $nama_img=$img->temp_img_name;
            $file=FCPATH.'/media/itin_image/'.$nama_img;
            if(file_exists($file)){
                unlink($file);
            }
            $this->db->delete('temp_image',array('temp_img_token'=>$token));
        }
        echo "{}";
    }

    public function upload(){
        $croped_image = $_POST['image'];
        list($type, $croped_image) = explode(';', $croped_image);
        list(, $croped_image)      = explode(',', $croped_image);
        $croped_image = base64_decode($croped_image);
        $image_name = md5(date('Y-m-d').time()).'.png';
        // upload cropped image to server 
        file_put_contents($this->path_thumb.$image_name, $croped_image);
        $data = json_encode($image_name);
        echo $data;
    }

    public function profile($user_id) {
        $data = $this->UserDataModel->by_id(['user_id' => $user_id]);
        $akun = $this->UserAccountModel->by_id(['user_id' => $user_id]);
        $interest_exp = explode("-", $data->user_interests);
        $personal_interest = [];
        if (!empty($interest_exp)) {
            foreach ($interest_exp as $val) {
                $pi = $this->PersonalInterestModel->by_id(['pe_id' => $val]);
                array_push($personal_interest, $pi->pe_name);
            }
        }
        $interest_imp = ucwords(implode(", ", $personal_interest));
        $this->data['user'] = (object) array_merge((array)$data,(array)$akun,array('user_interests' => $interest_imp));

        $key = array('itin_creator_id' => $user_id, 'itin_status' => 1);
        $itinerary = $this->ItineraryModel->fetch(null, null, null, true, null, $key);
        $arr_itin = [];
        if(!empty($itinerary)){
            foreach ($itinerary as $val) {
                $image = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id));
                $merge = array_merge((array)$val, array('image' => $image));
                array_push($arr_itin, (object)$merge);
            }
        }
        $this->data['itinerary'] = $arr_itin;

        $arr_review = [];
        $review_to = array('review_to' => $user_id);
        $review = $this->ReviewModel->fetch(null, null, null, true, null, $review_to);
        if (!empty($review)) {
            foreach ($review as $rev) {
                $review_from = $this->UserDataModel->by_id(['user_id' => $rev->user_id]);
                $rev_merge = array_merge((array)$rev, array('user_fname' => $review_from->user_fname, 'user_lname' => $review_from->user_lname, 'user_photo' => $review_from->user_photo));
                array_push($arr_review, (object) $rev_merge);
            }
        }
        // echo "<pre>";
        // print_r($arr_review);
        // die();
        $this->data['review'] = $arr_review;
        $this->template->display('guide_profile',$this->data);
    }

}

?>