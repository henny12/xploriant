<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ChatController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        init_generic_dao();
        $this->load->model(['MessageModel','OauthModel','ConnectChatModel','UserDataModel','ChatModel']);
        $this->SessionModel->not_logged_in();
        $this->load->helper('string');
    }

    public function index($userTo = null) {
        $myid = $this->session->userdata('id');
        $notif_message = $this->ConnectChatModel->fetch(null, null, null, false, null, array('cm_user_id' => $myid, 'read_status' => 0), null, true);
        $guide = $this->input->get('guide_id'); 
        $guideid = $this->OauthModel->Encryptor('decrypt', $guide);
        $list=[];
        $list = $this->ConnectChatModel->fetch(null, null, 'last_chat', false, null, array('cm_user_id' => $myid), null, false, false);
        $check = $this->ConnectChatModel->by_id(['cm_user_id' => $myid, 'cm_connected_to' => $guideid]);
        if (!empty($guide)){
            if (empty($check) && $myid != $guideid) {
                $obj1 = ['cm_user_id' => $myid, 'cm_connected_to' => $guideid];
                $obj2 = ['cm_user_id' => $guideid, 'cm_connected_to' => $myid];
                $this->ConnectChatModel->insert($obj1);
                $this->ConnectChatModel->insert($obj2);
            }
        }

        $list = $this->ConnectChatModel->fetch(null, null,'last_chat', false, null, array('cm_user_id' => $myid), null, false, false);
        $vendorslist=[];
        foreach($list as $u){
            $connect_to = $this->UserDataModel->by_id(['user_id' => $u->cm_connected_to]);  
            $lastchat = $this->ChatModel->get_last($myid,$u->cm_connected_to);
            if (!empty($lastchat->attachment_name)) {
                $lastmsg = '<i class="fa fa-file" aria-hidden="true"></i> image/file attachment';
            } else {
                $lastmsg = @$lastchat->message;
            }
            if (!empty($connect_to)){
                $vendorslist[]=
                [
                    'id' => $this->OauthModel->Encryptor('encrypt', @$connect_to->user_id),
                    'name' => @$connect_to->user_fname.' '.@$connect_to->user_lname,
                    'picture_url' => $this->UserDataModel->PictureUrlById(@$connect_to->user_id),
                    'last_message' => $lastmsg,
                    'last_date' => date("M d", strtotime(@$lastchat->message_date_time)),
                    'read_status' => $u->read_status
                ];
            }
        }
        $data['notif_message'] = $notif_message;
        $data['vendorslist']=$vendorslist;
        $this->parser->parse('chat_template',$data);
    }
    
    
    public function send_text_message(){
        $post = $this->input->post();
        $messageTxt='NULL';
        $attachment_name='';
        $file_ext='';
        $mime_type='';
        
        if(isset($post['type'])=='Attachment'){ 
            $AttachmentData = $this->ChatAttachmentUpload();
            //print_r($AttachmentData);
            $attachment_name = $AttachmentData['file_name'];
            $file_ext = $AttachmentData['file_ext'];
            $mime_type = $AttachmentData['file_type'];
             
        }else{
            $messageTxt = reduce_multiples($post['messageTxt'],' ');
        }   
            $sender_id = $this->session->userdata['id'];
            $receiver_id = $this->OauthModel->Encryptor('decrypt', $post['receiver_id']);
                $data=[
                    'sender_id' => $sender_id,
                    'receiver_id' => $receiver_id,
                    'message' =>   $messageTxt,
                    'attachment_name' => $attachment_name,
                    'file_ext' => $file_ext,
                    'mime_type' => $mime_type,
                    'message_date_time' => date('Y-m-d H:i:s'), //23 Jan 2:05 pm
                    'ip_address' => $this->input->ip_address(),
                ];
          
                $query = $this->MessageModel->SendTxtMessage($this->OauthModel->xss_clean($data));
                $obj_id = ['cm_user_id' => $receiver_id, 'cm_connected_to' => $sender_id];
                $this->ConnectChatModel->update(['read_status' => 0], $obj_id);
                $response='';
                if($query == true){
                    $response = ['status' => 1 ,'message' => '' ];
                }else{
                    $response = ['status' => 0 ,'message' => 'sorry we re having some technical problems. please try again !'                       ];
                }
             
           echo json_encode($response);
    }
    public function ChatAttachmentUpload(){
         
        
        $file_data='';
        if(isset($_FILES['attachmentfile']['name']) && !empty($_FILES['attachmentfile']['name'])){  
                $config['upload_path']          = './uploads/attachment';
                $config['allowed_types']        = 'jpeg|jpg|png|txt|pdf|docx|xlsx|pptx|rtf';
                //$config['max_size']             = 500;
                //$config['max_width']            = 1024;
                //$config['max_height']           = 768;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('attachmentfile'))
                {
                    echo json_encode(['status' => 0,
                    'message' => '<span style="color:#900;">'.$this->upload->display_errors(). '<span>' ]); die;
                }
                else
                {
                    $file_data = $this->upload->data();
                    //$filePath = $file_data['file_name'];
                    return $file_data;
                }
            }
         
    }
    
    public function get_chat_history_by_vendor(){
        $rec_id = $this->input->get('receiver_id');
        $receiver_id = $this->OauthModel->Encryptor('decrypt', $rec_id);
        $Logged_sender_id = $this->session->userdata['id'];
        $obj_id = ['cm_user_id' => $Logged_sender_id, 'cm_connected_to' => $receiver_id];
        $this->ConnectChatModel->update(['read_status' => 1],$obj_id);
        $history = $this->MessageModel->GetReciverChatHistory($receiver_id);

        foreach ($history as $chat) {
            $data_sender = $this->UserDataModel->by_id(['user_id' => $chat['sender_id']]);
            $message_id = $this->OauthModel->Encryptor('encrypt', $chat['id']);
            $sender_id = $chat['sender_id'];
            $userName = $data_sender->user_fname .' '. $data_sender->user_lname;
            $userPic = $this->UserDataModel->PictureUrlById($chat['sender_id']);
            $message = $chat['message'];
            $messagedatetime = date('d M H:i A',strtotime($chat['message_date_time']));

            $messageBody='';
            if ($message=='NULL'){ //fetach media objects like images,pdf,documents etc
                $classBtn = 'right';
                if ($Logged_sender_id==$sender_id) {
                    $classBtn = 'left';
                }
                    
                $attachment_name = $chat['attachment_name'];
                $file_ext = $chat['file_ext'];
                $mime_type = explode('/',$chat['mime_type']);
                
                $document_url = base_url('uploads/attachment/'.$attachment_name);
                    
                if ($mime_type[0]=='image') {
                    $messageBody.='<img src="'.$document_url.'" onClick="ViewAttachmentImage('."'".$document_url."'".','."'".$attachment_name."'".');" class="attachmentImgCls">';  
                } else {
                    $messageBody='';
                    $messageBody.='<div class="attachment">';
                    $messageBody.='<h4>Attachments:</h4>';
                    $messageBody.='<p class="filename">';
                    $messageBody.= $attachment_name;
                    $messageBody.='</p>';
                    $messageBody.='<div class="pull-'.$classBtn.'">';
                    $messageBody.='<a download href="'.$document_url.'"><button type="button" id="'.$message_id.'" class="btn btn-primary btn-sm btn-flat btnFileOpen">Open</button></a>';
                    $messageBody.='</div>';
                    $messageBody.='</div>';
                }                                
            }else{
                $messageBody = $message;    
            }
        ?>
        <?php if ($Logged_sender_id!=$sender_id){ ?>     
            <!-- Message. Default to the left -->
                <div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left"><?=$userName;?></span>
                        <span class="direct-chat-timestamp pull-right"><?=$messagedatetime;?></span>
                    </div>
                    <!-- /.direct-chat-info -->
                    <img class="direct-chat-img" src="<?=$userPic;?>" alt="<?=$userName;?>">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                        <?=$messageBody;?>
                    </div>
                    <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg -->
        <?php }else{?>
            <!-- Message to the right -->
            <div class="direct-chat-msg right">
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-right"><?=$userName;?></span>
                    <span class="direct-chat-timestamp pull-left"><?=$messagedatetime;?></span>
                </div>
                <!-- /.direct-chat-info -->
                <img class="direct-chat-img" src="<?=$userPic;?>" alt="<?=$userName;?>">
                <!-- /.direct-chat-img -->
                <div class="direct-chat-text">
                    <?=$messageBody;?>
                    <!--<div class="spiner">
                        <i class="fa fa-circle-o-notch fa-spin"></i>
                    </div>-->
                </div>
                <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->
        <?php }?>
        
        <?php } }

    public function get_user_history(){
        $myid = $this->session->userdata('id');
        // $guide = $this->input->get('guide_id'); 
        // $guideid = $this->OauthModel->Encryptor('decrypt', $guide);
        $list=[];
        // $list = $this->ConnectChatModel->fetch(null, null, null, false, null, array('cm_user_id' => $myid), null, false, false);
        // $check = $this->ConnectChatModel->by_id(['cm_user_id' => $myid, 'cm_connected_to' => $guideid]);
        // if (!empty($guide)){
        //     if (empty($check) && $myid != $guideid) {
        //         $obj1 = ['cm_user_id' => $myid, 'cm_connected_to' => $guideid];
        //         $obj2 = ['cm_user_id' => $guideid, 'cm_connected_to' => $myid];
        //         $this->ConnectChatModel->insert($obj1);
        //         $this->ConnectChatModel->insert($obj2);
        //     }
        // }

        $list = $this->ConnectChatModel->fetch(null, null, 'last_chat', false, null, array('cm_user_id' => $myid), null, false, false);
        if (!empty($list)){
            $vendorslist=[];
            foreach($list as $u){
                $connect_to = $this->UserDataModel->by_id(['user_id' => $u->cm_connected_to]);  
                $lastchat = $this->ChatModel->get_last($myid,$u->cm_connected_to);
                if (!empty($lastchat->attachment_name)) {
                    $lastmsg = '<i class="fa fa-file" aria-hidden="true"></i> image/file attachment';
                } else {
                    $lastmsg = @$lastchat->message;
                }
                if (!empty($connect_to)){
                    $id = $this->OauthModel->Encryptor('encrypt', @$connect_to->user_id);
                    $name = @$connect_to->user_fname.' '.@$connect_to->user_lname;
                    $picture_url = $this->UserDataModel->PictureUrlById(@$connect_to->user_id);
                    $last_message = $lastmsg;
                    $last_date = date("M d", strtotime(@$lastchat->message_date_time));
                    $read_status = ($u->read_status == 0)?'unread':'';

                    echo    '<div class="chat_list selectVendor '.$read_status.'" id="'.$id.'" title="'.$name.'">'.
                                '<div class="chat_people">'.
                                    '<div class="chat_img"><img src="'.$picture_url.'" alt="'.$name.'" title="'.$name.'"></div>'.
                                    '<div class="chat_ib">'.
                                        '<h5><a class="users-list-name" href="#">'.$name.'</a><span class="chat_date">'.$last_date.'</span></h5>'.
                                        '<p>'.$last_message.'</p>'.
                                    '</div>'.
                                '</div>'.
                            '</div>';
                }
            }
        } else { 
            echo    '<div class="chat_list">'.
                        '<div class="chat_people">'.
                            '<div class="chat_ib">'.
                                '<h5><a class="users-list-name" href="#">No message found...</a></h5>'.
                            '</div>'.
                        '</div>'.
                    '</div>';
        }
        
    }

    public function chat_clear_client_cs(){
        $receiver_id = $this->OauthModel->Encryptor('decrypt', $this->input->get('receiver_id') );
        
        $messagelist = $this->MessageModel->GetReciverMessageList($receiver_id);
        
        foreach($messagelist as $row){
            
            if($row['message']=='NULL'){
                $attachment_name = unlink('uploads/attachment/'.$row['attachment_name']);
            }
        }
        
        $this->MessageModel->TrashById($receiver_id); 
 
        
    }
    
}