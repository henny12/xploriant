<?php
ob_start();

class Auth extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'auth/');
        $this->data = array();
        init_generic_dao();
        $this->config_email();
        $this->load->library('template');
        $this->load->model(array('UserAccountModel','UserDataModel','OauthModel'));
        $this->data['page_title'] = "Login";
        $this->data['body_class'] = "signup-page sidebar-collapse";
    }
    
    public function signin() {
        $this->template->display('signin',$this->data);
    }

    public function login() {
        $this->OauthModel->CSRFVerify();
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $result = $this->UserAccountModel->Authentication_Check($email);
        if ($result != false){
            $login_user_id = $result['user_id'];
            $user = $this->UserAccountModel->Read_User_Information($login_user_id);
            $hashed = $user['user_password'];
            if($this->OauthModel->VerifyPassword($login_user_id,$password,$hashed) == 1) {
                $userdata = [
                        'id'  => $user['user_id'],
                        'fname'  => $user['user_fname'],
                        'lname'  => $user['user_lname'],
                        'email'     => $user['user_email'],
                        'last_logged' =>  $user['last_login'],
                        'logged_in' => 'TRUE'
                ];
                $this->session->set_userdata($userdata);
                $this->UserAccountModel->update(['last_login' => date('Y-m-d h:i:s')], ['user_id' => $user['user_id']]);
                redirect('home');
            } else {
                $this->session->set_flashdata(array('message'=>'Login failed. Your password is Incorrect!','type_message'=>'error'));
                $this->template->display('signin', $this->data);
            }
        } else {
            $this->session->set_flashdata(array('message'=>'Login failed. Your email is not registered!','type_message'=>'error'));
            $this->template->display('signin', $this->data);
        }
    }

    public function signup() {
        $this->template->display('signup',$this->data);
    }

    private function validate() { 
        $this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[255]');

        return $this->form_validation->run();
    }

    public function register() {
        $obj_login['user_email'] = $this->input->post('email');
        $obj_data['user_fname'] = $this->input->post('fname');
        $obj_data['user_lname'] = $this->input->post('lname');
        $obj_data['user_dob'] = $this->input->post('dob');
        $obj_login['created_by'] = "member";
        $obj_login['created_on'] = date('Y-m-d H:i:s');
        $obj_login['user_password'] = $this->OauthModel->HashPassword($this->input->post('password'));
        $obj_login['user_status'] = "0";
        $obj_login['code'] = $this->generate_code();
        $obj_login['ip_address'] = $this->input->ip_address();
        $age = $this->age_checking($obj_data['user_dob']);
        
        if ($age < 17) {
            $this->session->set_flashdata(array('message'=>'Registration failed. You must be 17 or older to sign up.','type_message'=>'error'));
            redirect('auth/signup');
        }

        if ($this->email_checking($obj_login['user_email'])) {
            $this->session->set_flashdata(array('message'=>'Registration failed. Email has been registered.','type_message'=>'error'));
            redirect('auth/signup');
        }

        if ($this->validate() != false) {
            $this->UserAccountModel->insert($this->OauthModel->xss_clean($obj_login));
            $id = $this->db->insert_id();
            $obj_data['user_id'] = $id;
            $this->UserDataModel->insert($this->OauthModel->xss_clean($obj_data));
            $account = $this->UserAccountModel->by_id(['user_id' => $id]);
            $this->send_verification($obj_login['user_email'], $account->code);
            $this->session->set_flashdata(array('message'=>'Registration complete. Please check your email.','type_message'=>'success'));
            redirect('auth/signin');
        } else {
            $this->session->set_flashdata(array('message'=>'Registration failed. Please check your input.','type_message'=>'error'));
            redirect('auth/signup');
        }
    } 

    public function generate_code() {
        $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = substr(str_shuffle($set), 0, 12);
        return $code;
    }

    public function age_checking($dob) {
        $bday = new DateTime($dob);
        $today = new DateTime();
        $diff = $today->diff($bday);
        return $diff->y;
    }

    public function email_checking($email) {
        $account = $this->UserAccountModel->by_id(['user_email' => $email]);
        if (!empty($account)){
            return true;
        }else{
            return false;
        }
    }

    public function send_verification($email, $code) {
        $account = $this->UserAccountModel->by_id(['user_email' => $email]);
        $data = $this->UserDataModel->by_id(['user_id' => $account->user_id]);
        $this->data['firstname'] = $data->user_fname;
        $this->data['email'] = urlencode($email);
        $this->data['code'] = $code;
        $this->email->to($email); 
        $this->email->subject('Verify Your Email Address');
        $this->email->message($this->load->view('template_email/account_verification', $this->data, true));  
        $this->email->send();
    }

    public function verify_mail($email=null, $code=null) {
        $email = urldecode($email);
        $account = $this->UserAccountModel->by_id(['user_email' => $email]);
        if ($code == $account->code) {
            $this->UserAccountModel->update(['user_status' => 1], ['user_email' => $email]);
            $this->session->set_flashdata(array('message'=>'Your mail has been verify.','type_message'=>'success'));
        } else {
            $this->session->set_flashdata(array('message'=>'Your email failed to be verified.','type_message'=>'error'));
        }
        redirect('auth/signin');
    }

    public function forgot_password() {
        $email = $this->input->post('email');
        $account = $this->UserAccountModel->by_id(['user_email' => $email]);
        $data = $this->UserDataModel->by_id(['user_id' => $account->user_id]);
        $this->data['firstname'] = $data->user_fname;
        $this->data['email'] = urlencode($email);
        $data = array('user_email'=> $email);
        $check = $this->UserAccountModel->fetch(null, null, null, true, $data, null, null, true);
        if($check != 0){
            $this->email->to($email); 
            $this->email->subject('Reset Your Password');
            $this->email->message($this->load->view('template_email/reset_password', $this->data, true));  
            $this->email->send();
            $this->session->set_flashdata(array('message' => 'Reset password request has been sent to your email. Please check your email.', 'type_message' => 'success'));
            redirect('auth/signin');
        }else{
            $this->session->set_flashdata(array('message' => 'Sorry, Your email is not registered.', 'type_message' => 'error'));
            redirect('auth/signup');
        }
    }

    public function reset_password($email) {
        $email = urldecode($email);
        $this->data['account'] = $this->UserAccountModel->by_id(['user_email' => $email]);
        $this->template->display('reset_password', $this->data);
    }

    public function update_password() {
        $obj['user_password'] = $this->OauthModel->HashPassword($this->input->post('password'));
        $obj['updated_by'] = 'member';
        $obj['updated_on'] = date('Y-m-d H:i:s');
        $this->UserAccountModel->update($obj, ['user_id' => $this->input->post('user_id')]);
        $this->session->set_flashdata(array('message' => 'Your password has been changed.', 'type_message' => 'success'));
            redirect('auth/signin');
    }

    public function logout() {
        $clear = array('id','email','fname','lname','logged_in','last_logged');
        $this->session->unset_userdata($clear);
        $this->session->set_flashdata(array('message'=>'Logout success','type_message'=>'success'));
        redirect('auth/signin');
    }
}

?>