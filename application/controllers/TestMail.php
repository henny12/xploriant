<?php

class TestMail extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'testmail/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        // $this->load->model(array('MessageModel','TempImageModel','UserDataModel','UserAccountModel'));
        // $this->data['page_title'] = "Message";
    }

    public function index(){
        $this->load->view('template_email/index',$this->data);
    }
    
    public function account_verification(){
        $this->load->view('template_email/account_verification',$this->data);
    }

    public function reset_password(){
        $this->load->view('template_email/reset_password',$this->data);
    }

    public function report(){
        $this->load->view('template_email/report_auto_reply',$this->data);
    }

    public function booking_req_guide(){
        $this->load->view('template_email/booking_request_guide',$this->data);
    }

    public function booking_req_tourist(){
        $this->load->view('template_email/booking_request_tourist',$this->data);
    }

    public function booking_accepted(){
        $this->load->view('template_email/booking_accepted',$this->data);
    }

    public function booking_declined(){
        $this->load->view('template_email/booking_declined',$this->data);
    }

    public function payment_completed_guide(){
        $this->load->view('template_email/payment_completed_guide',$this->data);
    }

    public function payment_completed_tourist(){
        $this->load->view('template_email/payment_completed_tourist',$this->data);
    }

    public function d_24hours_guide(){
        $this->load->view('template_email/d_24hours_guide',$this->data);
    }

    public function d_24hours_tourist(){
        $this->load->view('template_email/d_24hours_tourist',$this->data);
    }

    public function start_date_guide(){
        $this->load->view('template_email/start_date_guide',$this->data);
    }

    public function start_date_tourist(){
        $this->load->view('template_email/start_date_tourist',$this->data);
    }

    public function end_date_guide(){
        $this->load->view('template_email/end_date_guide',$this->data);
    }

    public function end_date_tourist(){
        $this->load->view('template_email/end_date_tourist',$this->data);
    }

    public function cancel_guide_no_payout(){
        $this->load->view('template_email/cancel_guide_no_payout',$this->data);
    }

    public function cancel_guide_payout(){
        $this->load->view('template_email/cancel_guide_payout',$this->data);
    }

    public function cancel_guide(){
        $this->load->view('template_email/cancel_guide',$this->data);
    }

    public function cancel_tourist(){
        $this->load->view('template_email/cancel_tourist',$this->data);
    }

    public function cancel_tourist_no_refund(){
        $this->load->view('template_email/cancel_tourist_no_refund',$this->data);
    }

    public function cancel_tourist_refund(){
        $this->load->view('template_email/cancel_tourist_refund',$this->data);
    }

    public function transfer_request_guide(){
        $this->load->view('template_email/transfer_request_guide',$this->data);
    }

    public function transfer_completed_guide(){
        $this->load->view('template_email/transfer_completed_guide',$this->data);
    }
}

?>