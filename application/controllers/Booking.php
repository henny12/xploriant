<?php
ob_start();

class Booking extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'booking/');
        $this->data = array();
        // sandbox
        // $this->merchantKey = 'qRAMPHfAmz';
        // $this->merchantCode = 'ID01246';
        // production
        $this->merchantKey = 'Pc40h7G81B';
        $this->merchantCode = 'ID01009';
        
        init_generic_dao();
        // $this->config_email();
        $this->load->library(array('template','Request_dto','Response_dto'));
        $this->load->model(array('BookingModel','UserDataModel','UserAccountModel','ItineraryModel','PersonalInterestModel','NotRecomModel','ItineraryImageModel','NotifBookingModel','ReviewModel','UserMilesModel','KursModel'));
        $this->data['page_title'] = "Booking";
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['personal_interest_list'] = $this->PersonalInterestModel->fetch();
        $this->data['not_recommended_list'] = $this->NotRecomModel->fetch();
    }

    public function mybook() {
        $this->logged_in_member();
        $myid = $this->session->userdata('id');
        $notif_mybook = $this->NotifBookingModel->fetch(null, null, null, false, null, array('user_id' => $myid, 'read_status' => 0, 'book_type' => 1), null, true);
        $notif_mytour = $this->NotifBookingModel->fetch(null, null, null, false, null, array('user_id' => $myid, 'read_status' => 0, 'book_type' => 2), null, true);

        $mybook = [];
        $book = $this->BookingModel->fetch(null, null, null, false, null, array('tourist_id' => $myid, 'book_hide_for_tourist' => 0), null, false, false);

        if(!empty($book)){
            foreach ($book as $val) {
                $review = $this->ReviewModel->by_id(array('booking_id' => $val->book_id, 'user_id' => $myid));
                $date = date('Y-m-d');
                $enddate = date('Y-m-d', strtotime('+7 days', strtotime($val->end_date)));
                if ((!empty($review) || ($date >= $enddate))){
                    $this->BookingModel->update(['book_hide_for_tourist' => 1], ['book_id' => $val->book_id]);
                }
                $itin = $this->ItineraryModel->by_id(array('itin_id' => $val->itin_id));
                $guide = $this->UserDataModel->by_id(array('user_id' => $val->guide_id));
                
                $merge = array_merge((array)$val, (array)$itin, (array)$guide);
                array_push($mybook, (object)$merge);
            }        
        }

        $mytour = [];
        $tour = $this->BookingModel->fetch(null, null, null, false, null, array('guide_id' => $myid, 'book_hide_for_guide' => 0), null, false, false);
        if(!empty($tour)){
            foreach ($tour as $val) {
                $review = $this->ReviewModel->by_id(array('booking_id' => $val->book_id, 'user_id' => $myid));
                $date = date('Y-m-d');
                $enddate = date('Y-m-d', strtotime('+7 days', strtotime($val->end_date)));
                if ((!empty($review) || ($date >= $enddate))){
                    $this->BookingModel->update(['book_hide_for_guide' => 1], ['book_id' => $val->book_id]);
                }
                $itin = $this->ItineraryModel->by_id(array('itin_id' => $val->itin_id));
                $guide = $this->UserDataModel->by_id(array('user_id' => $val->guide_id));
                $merge = array_merge((array)$val, (array)$itin, (array)$guide);
                array_push($mytour, (object)$merge);
            }
        }
        
        $this->data['mybook'] = $mybook;
        $this->data['mytour'] = $mytour;
        $this->data['notif_mytour'] = $notif_mytour;
        $this->data['notif_mybook'] = $notif_mybook;
        $this->template->display('booking_view',$this->data);
    }

    private function fetch_record($keys){
        $itinerary = $this->ItineraryModel->by_id($keys);
        $additional_info = [];
        $not_recom = $itinerary->itin_not_recom_opt;
        $exp = explode('-', $not_recom);
        foreach ($exp as $val) {
            $nr = $this->NotRecomModel->by_id(array('nr_id' => $val));
            $add = 'Not recommended for tourists ' . $nr->nr_title;
            array_push($additional_info, $add);
        }
        $this->data['additional_info'] = $additional_info;
        $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $itinerary->itin_creator_id));
        $this->data['itin_image'] = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $itinerary->itin_id)); 
        $this->data['itinerary'] = $itinerary;
    }

    private function fetch_record_book($keys) {
        $book = $this->BookingModel->by_id($keys);
        $this->data['turis'] = $this->UserDataModel->by_id(['user_id' => $book->tourist_id]);
        $this->data['turis_akun'] = $this->UserAccountModel->by_id(['user_id' => $book->tourist_id]);
        $this->data['book'] = $book;
    }

    function iPay88_signature($book_id) {
        $obj_id = array('book_id' => $book_id);
        $book = $this->BookingModel->by_id($obj_id);
        $signature = implode([
            $this->merchantKey,
            $this->merchantCode,
            $book->book_code,
            preg_replace('/[\.\,]/', '', $book->total_price_nominal),
            strtoupper($book->total_price_currency)
            // '||IPP:3||'
        ]);
       return base64_encode(hex2bin(sha1($signature)));
    }

    function hex2bin($hexSource) {
        for ($i=0;$i<strlen($hexSource);$i=$i+2) {
            $bin .= chr(hexdec(substr($hexSource,$i,2)));
        }
        return $bin;
    }

    public function detail($itin_id, $book_id) {
        $this->logged_in_member();
        $obj_id = array('itin_id' => $itin_id);
        $obj_bookid = array('book_id' => $book_id);
        $myid = $this->session->userdata('id');
        $this->data['merchantcode'] = $this->merchantCode;
        $this->data['merchantkey'] = $this->merchantkey;
        $this->preload();
        $this->fetch_record($obj_id);
        $this->fetch_record_book($obj_bookid);
        $this->data['signature'] = $this->iPay88_signature($book_id);
        // echo "<pre>";
        // print_r($this->data['signature']);die();
        $this->data['xmiles'] = $this->UserMilesModel->by_id(['user_id' => $myid]);
        if ($this->data['book']->book_status == 3 || $this->data['book']->book_status == 7 || $this->data['book']->book_status == 8) {
            $this->NotifBookingModel->delete(['user_id' => $myid, 'book_id' => $book_id, 'read_status' => 0]);
        }
        $this->template->display('booking_detail', $this->data);
    }

    public function response() {
        $data = array(
            'merchantcode' => $_REQUEST["MerchantCode"],
            'paymentid' => $_REQUEST["PaymentId"],
            'refno' => $_REQUEST["RefNo"],
            'amount' => $_REQUEST["Amount"],
            'ecurrency' => $_REQUEST["Currency"],
            'remark' => $_REQUEST["Remark"],
            'transid' => $_REQUEST["TransId"],
            'authcode' => $_REQUEST["AuthCode"],
            'estatus' => $_REQUEST["Status"],
            'errdesc' => @$_REQUEST["ErrDesc"],
            'signature' => $_REQUEST["Signature"],
            'xfield1' => @$_REQUEST["xfield1"]);

        $obj_id = array('book_code' => $_REQUEST["RefNo"]);
        $book = $this->BookingModel->by_id($obj_id);

        if ($_REQUEST["Status"] == 1) {
            $obj = array('transid' => $_REQUEST["TransId"],
                    'authcode' => $_REQUEST["AuthCode"],
                    'signature' => $_REQUEST["Signature"],
                    'book_status' => 4,
                    'book_paid_date' => date('Y-m-d'),
                    'book_paid' => $_REQUEST["Amount"]
                );
            $this->BookingModel->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Thank you, your payment was successful.','type_message'=>'success'));
            redirect(base_url().'booking/detail/'.$book->itin_id.'/'.$book->book_id);
        } else {
            $this->session->set_flashdata(array('message'=>'Sorry, your payment has failed. Please try again later.','type_message'=>'error'));
            redirect(base_url().'booking/detail/'.$book->itin_id.'/'.$book->book_id);
        }
        
    }

    public function response_seamless() {
        $data = json_decode($result, true);
        echo "<pre>";print_r($data);die();
        //Status Fail
        if ($data['Status'] == "0")
        {   
            // Write your code for update order status
            // Write your code for update private message
            // Write your code for redirect url to failed page
        } else {
            //Signature from iPay88
            $ipay88_signature   = $data['Signature'];

            //Generate Response Signature               
            $MechantKey_response    = "your-merchant-key";
            $MerchantCode_response  = "your-merchant-code";
            $PaymentId_response     = $data['PaymentId'];
            $Amount_response        = $data['Amount'];
            $Currency_response      = $data['Currency'];
            $Status_response        = $data['Status'];
            $merchant_signature     = "";

            $str = sha1($MechantKey_response.$MerchantCode_response.$PaymentId_response.$RefNo_response.$Amount_response.$Currency_response.$Status_response);      
            
            for ($i=0; $i<strlen($str); $i=$i+2)
            {
                $merchant_signature .= chr(hexdec(substr($str,$i,2)));
            }
         
            $merchant_signature = base64_encode($merchant_signature);

            //Signature match
            if($merchant_signature == $merchant_signature)
            {
                // Write your code for update order status
                // Write your code for update private message
                
                $checkout = $data['CheckoutURL'];
                
                $load_file_function = '<script language="JavaScript"> $(document).ready(function(){ document.ipay88_request_paramaters.submit();});</script>';

                echo $load_file_function;
                
                echo "<form  name='ipay88_request_paramaters' ACTION='".$checkout."' method='POST'>";
                echo "</form>";
            }

            //Signature is not match
            else                    
            {
                // Write your code for update order status
                // Write your code for update private message
                // Write your code for redirect url to failed page
            }       
        }
    }

    public function seamless() {
        $itemTransactions = array(
            array(  'id'        => $this->input->post('itin_id'),
                    'name'      => $this->input->post('ProdDesc'),
                    'quantity'  => '1', 
                    'amount'    => $this->input->post('Amount'),
                    'type'      => 'Itinerary',
                    'url'       => base_url().'itinerary/detail/'.$this->input->post('itin_id'))
            );
                                     
        //Shipping address          
        $ShippingAddress = array(
        'FirstName' => "PT Novelite",
        'LastName'  => "Indonesia",
        'Address'   => "Jl. Setrasari Mall B8",
        'City'      => "Bandung",
        'PostalCode'    => "40163",
        'Phone'     => "0222007563",
        'CountryCode'   => "ID",
        );

        //Billing address
        $BillingAddress = array(
        'FirstName' => "PT Novelite",
        'LastName'  => "Indonesia",
        'Address'   => "Jl. Setrasari Mall B8",
        'City'      => "Bandung",
        'PostalCode'    => "40163",
        'Phone'     => "0222007563",
        'CountryCode'   => "ID",
        );

        //Generate JSON             
        $ipay88_Obj = new stdClass();

        $ipay88_Obj->MerchantCode   = $this->input->post('MerchantCode');
        $ipay88_Obj->PaymentId      = "55";
        $ipay88_Obj->Currency       = "IDR";
        $ipay88_Obj->RefNo          = $this->input->post('RefNo');
        $ipay88_Obj->Amount         = $this->input->post('Amount');
        $ipay88_Obj->ProdDesc       = $this->input->post('ProdDesc');
        $ipay88_Obj->UserName       = $this->input->post('UserName');
        $ipay88_Obj->UserEmail      = $this->input->post('UserEmail');
        $ipay88_Obj->UserContact    = $this->input->post('UserContact');
        $ipay88_Obj->Remark         = "Transaction date ".date('Y-m-d H:i:s');
        $ipay88_Obj->Lang           = "UTF-8";
        $ipay88_Obj->ResponseURL    = base_url().'booking/response';
        $ipay88_Obj->BackendURL     = base_url().'booking/backend';
        $ipay88_Obj->Signature      = $this->input->post('Signature');
        $ipay88_Obj->xfield1        = "";
            
        $ipay88_Obj->itemTransactions   = $itemTransactions;                  
        $ipay88_Obj->ShippingAddress    = $ShippingAddress;
        $ipay88_Obj->BillingAddress     = $BillingAddress;

        $ipay88_JSON = json_encode($ipay88_Obj, JSON_UNESCAPED_SLASHES);
        $url = 'https://payment.ipay88.co.id/ePayment/WebService/PaymentAPI/Checkout';
        
        $options = array('http' => array('header' => "Content-type: application/json\r\n", 'method' => 'POST', 'content' => $ipay88_JSON));                                      
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $RefNo_response = $this->input->post('RefNo');

        $data = json_decode($result, true);
        // echo "<pre>";
        // print_r($data);die();
        //JSON Failed   
        if ($result === FALSE) {
            echo "gagal";          
        } else {
            $checkout = $data['CheckoutURL'];
        
            $load_file_function = '<script src="'.base_url().'assets/material-pro/assets/plugins/jquery/jquery.min.js"></script><script language="JavaScript"> $(document).ready(function(){ document.ipay88_request_paramaters.submit();});</script>';

            echo $load_file_function;
            
            echo "<form  name='ipay88_request_paramaters' ACTION='".$checkout."' method='POST'>";
            echo "</form>";     
        }
    }

    public function backend() {
        if (isset($_REQUEST['TransId'])) 
        {
            $signature_response = $_REQUEST['Signature'];
            //Generate signature    
            $MechantKey_response    = $this->merchantKey;
            $MerchantCode_response  = $this->merchantCode;
            $PaymentId_response     = $_REQUEST['PaymentId'];
            $RefNo_response         = trim(stripslashes($_REQUEST['RefNo']));
            $HashAmount_response    = str_replace(array(',','.'), "", $_REQUEST['Amount']);
            $Currency_response      = $_REQUEST['Currency'];
            $Status_response        = $_REQUEST['Status'];
            $error_response         = $_REQUEST['ErrDesc'];
            $transid_response       = $_REQUEST['TransId']; 
            $merchant_signature     = "";
        
            $merchant_encrypt       = sha1($MechantKey_response.$MerchantCode_response.$PaymentId_response.$RefNo_response.$HashAmount_response.$Currency_response.$Status_response);       
        
            for ($i=0; $i<strlen($merchant_encrypt); $i=$i+2){   
                $merchant_signature .= chr(hexdec(substr($merchant_encrypt,$i,2)));
            }        
            $merchant_signature_check = base64_encode($merchant_signature);

            //Payment success and signature match
            if ($_REQUEST['Status']=="1" && $merchant_signature_check==$signature_response) 
            {
                print_r("RECEIVEOK");exit;  
            } else {
                print_r("BACKEND FAILED");exit;
            }           
        }
        //redirect to base url
        else
        {
            //example
            print_r("gagal");
            die();
        }
    }
    
    public function reqdetail($itin_id, $book_id) {
        $this->preload();
        $this->logged_in_member();
        $obj_id = array('itin_id' => $itin_id);
        $obj_bookid = array('book_id' => $book_id);
        $myid = $this->session->userdata('id');
        
        $this->data['itin'] = $this->ItineraryModel->by_id(array('itin_id' => $itin_id));
        $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $myid));
        $this->fetch_record_book($obj_bookid);
        if ($this->data['book']->book_status == 4 || $this->data['book']->book_status == 7 || $this->data['book']->book_status == 8) {
            $this->NotifBookingModel->delete(['user_id' => $myid, 'book_id' => $book_id, 'read_status' => 0]);
        }
        $this->template->display('booking_request', $this->data);
    }

    private function validate() {           
        $this->form_validation->set_rules('itin_id', 'Itin Id', 'trim|required');
        $this->form_validation->set_rules('guide_id', 'Guide Id', 'trim|required');

        return $this->form_validation->run();
    }

    private function fetch_input_summary() {
        $data = array('itin_id' => $this->input->post('itin_id'),
                    'guide_id' => $this->input->post('guide_id'),
                    'duration' => $this->input->post('duration'),
                    'jmltourist' => $this->input->post('jmltourist'),
                    'start_date' => $this->input->post('stardate'),
                    'total_price' => $this->input->post('total_price'),
                    'itin_day' => $this->input->post('itin_day'),
                    'itin_duration' => $this->input->post('duration')
        );

        return $data;
    }

    private function fetch_input(){
        $data = array('itin_id' => $this->input->post('itin_id'),
                    'guide_id' => $this->input->post('guide_id'),
                    'tourist_id' => $this->session->userdata('id'),
                    'book_code' => $this->input->post('book_code'),
                    'start_date' => $this->input->post('start_date'),
                    'book_date' => date('Y-m-d'),
                    'total_tourist' => $this->input->post('total_tourist'),
                    'total_price_currency' => $this->input->post('total_price_currency'),
                    'total_price_nominal' => $this->input->post('total_price_nominal'),
                    'book_request' => $this->input->post('book_request'),
                    'book_status' => 1,
                    'book_hide_for_guide' => 0,
                    'book_hide_for_tourist' => 0,
                    'book_paid' => $this->input->post('total_price_nominal')
        );

        return $data;
    }

    public function summary(){
        $obj = $this->fetch_input_summary();
        if ($obj['guide_id'] == $this->session->userdata('id')){
            $this->session->set_flashdata(array('message'=>'You cannot send a booking request to your itinerary','type_message'=>'error'));
            redirect(base_url().'itinerary/detail/'.$obj['itin_id']);
        } else {
            if (!empty($obj['duration'])){
                $end_date = date('Y-m-d', strtotime('+'.$obj['duration'].' days', strtotime($obj['start_date'])));
                $obj['end_date'] = $end_date;
            }
            $this->data['itin'] = $this->ItineraryModel->by_id(array('itin_id' => $obj['itin_id']));
            $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $obj['guide_id']));
            $this->data['book'] = $obj;
            $this->data['code'] = $this->generate_code();
            $this->template->display('booking_summary',$this->data);
        }
    }

    public function add(){
        $obj = $this->fetch_input();
        $duration = $this->input->post('duration');
        $itin_day = $this->input->post('itin_day');

        if ($itin_day == 'md') {
            $obj['end_date'] = date('Y-m-d', strtotime('+'.($duration+1).' days', strtotime($obj['start_date'])));
        } else {
            $obj['end_date'] = date('Y-m-d', strtotime('+1 days', strtotime($obj['start_date'])));
        }
        if ($this->validate() != false) {
            $this->BookingModel->insert($obj);
            $insert_id = $this->db->insert_id();
            $this->send_notif_booking('Booking Request', $insert_id, $obj['guide_id'], '2');
            $this->send_mail_to_guide($insert_id, 'request');
            $this->send_mail_to_tourist($insert_id, 'request');
            $this->session->set_flashdata(array('message'=>'Your booking request has been sent.<br> You will be notified once your Guide Reviews your request.','type_message'=>'success'));
            redirect(base_url().'itinerary/detail/'.$obj['itin_id']);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['book'] = (object) $obj;
            $this->template->display('syspanel/news/news_insert', $this->data);
        }
    }

    private function fetch_data($book_id) {
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $itin = $this->ItineraryModel->by_id(['itin_id' => $book->itin_id]);
        $guide = $this->UserDataModel->by_id(['user_id' => $book->guide_id]);
        $tourist = $this->UserDataModel->by_id(['user_id' => $book->tourist_id]);
        $accountguide = $this->UserAccountModel->by_id(['user_id' => $book->guide_id]);
        $accountturis = $this->UserAccountModel->by_id(['user_id' => $book->tourist_id]);
        $this->data['book'] = $book;
        $this->data['itin'] = $itin;
        $this->data['guide'] = $guide;
        $this->data['tourist'] = $tourist;
        $this->data['accountguide'] = $accountguide;
        $this->data['accountturis'] = $accountturis;
    }

    private function config_mail() {
        $config_email = "henny.novelite@gmail.com";
        $config_password = "Novelite8899";    
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $config_email,
            'smtp_pass' => $config_password,
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config_email, 'Xploriant');
    }

    public function send_mail_to_guide($book_id, $type) {
        $this->config_mail();
        $this->fetch_data($book_id);
        $this->email->to($this->data['accountguide']->user_email); 
        if ($type == 'request') {
            $this->email->subject('Booking Request');
            $this->email->message($this->load->view('template_email/booking_request_guide', $this->data, true));  
        } elseif ($type == 'paid') {
            $this->email->subject('Booking Paid');
            $this->email->message($this->load->view('template_email/payment_completed_guide', $this->data, true));
        }
        $this->email->send();
    }

    public function send_mail_to_tourist($book_id, $type) {
        $this->config_mail();
        $this->fetch_data($book_id);
        $this->email->to($this->data['accountturis']->user_email); 
        if ($type == 'request') {
            $this->email->subject('Booking Request');
            $this->email->message($this->load->view('template_email/booking_request_tourist', $this->data, true));  
        } elseif ($type == 'accepted') {
            $this->email->subject('Booking Accepted');
            $this->email->message($this->load->view('template_email/booking_accepted', $this->data, true));
        } elseif ($type == 'declined') {
            $this->email->subject('Booking Declined');
            $this->email->message($this->load->view('template_email/booking_declined', $this->data, true));
        } elseif ($type == 'paid') {
            $this->email->subject('Booking Paid');
            $this->email->message($this->load->view('template_email/payment_completed_tourist', $this->data, true));
        }
        $this->email->send();
    }

    public function generate_code() {
        date_default_timezone_set('Asia/Jakarta');
        $code = date('ymdhis');
        $book = $this->BookingModel->fetch(null, null, null, false, array('book_code' => $code), null, null, false, false);
        if (!empty($book)) {
            $code = date('ymdhis');
        }
        
        return $code;
    }

    public function generate_token($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function cancel_book($book_id, $by_tourist = null) {
        if (!empty($by_tourist)) {
            $book_status = 7;
            $book_type = 2;
        } else {
            $book_status = 8;
            $book_type = 1;
        }
        $obj = ['book_status' => $book_status, 'book_cancel_date' => date('Y-m-d')];
        $obj_id = ['book_id' => $book_id];
        $this->BookingModel->update($obj, $obj_id);
        $this->send_notif_booking('Booking Canceled', $book_id, $book->tourist_id, $book_type);
        $this->session->set_flashdata(array('message'=>'Booking successfully canceled.','type_message'=>'success'));
        redirect('booking/mybook');
    }

    public function accept_book($book_id) {
        $obj = ['book_status' => 2, 'book_accepted_date'  => date('Y-m-d')];
        $obj_id = ['book_id' => $book_id];
        $this->BookingModel->update($obj, $obj_id);
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $this->NotifBookingModel->delete(['user_id' => $book->guide_id, 'book_id' => $book_id, 'notif_title' => 'Booking Request']);
        $this->send_notif_booking('Booking Accepted', $book_id, $book->tourist_id, '1');
        $this->send_mail_to_tourist($book_id, 'accepted');
        $this->session->set_flashdata(array('message'=>'Booking successfully accepted.','type_message'=>'success'));
        redirect('booking/mybook');   
    }

    public function reject_book($book_id) {
        $obj = ['book_status' => 3, 'book_rejected_date' => date('Y-m-d')];
        $obj_id = ['book_id' => $book_id];
        $this->BookingModel->update($obj, $obj_id);
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $this->NotifBookingModel->delete(['user_id' => $book->guide_id, 'book_id' => $book_id, 'notif_title' => 'Booking Request']);
        $this->send_notif_booking('Booking Rejected', $book_id, $book->tourist_id, '1');
        $this->send_mail_to_tourist($book_id, 'declined');
        $this->session->set_flashdata(array('message'=>'Booking successfully rejected.','type_message'=>'success'));
        redirect('booking/mybook');   
    }

        // With this precision (microsecond) ID will looks like '2di2adajgq6h'
        // $id = base_convert(microtime(false), 10, 36);

    public function make_payment($book_id) {
        $urlAPI = 'https://ecomm.cashlez.com/ecom-m-server/ws/getPaymentURL';
        $serverKey = '65319038483273579275326436543802';//confirm first to ops cashlez
        $username = 'nay.d033';
        $endpointId = 'novellite-xplorinat';

        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $custdata = $this->UserDataModel->by_id(['user_id' => $book->tourist_id]);
        $custacc = $this->UserAccountModel->by_id(['user_id' => $book->tourist_id]);
   
        //Example
        $test = new Request_dto;
        //EXAMPLE VALUES
        $test->amount = (empty($book->book_paid)?$book->total_price_nominal:$book->book_paid);
        $test->amountExtra = '0';
        $test->authorizationType = 'PAY';
        $test->integrationType = 'CASHLEZ_URL';
        $test->merchantPaymentSuccessURL = base_url().'booking/payment_success/'.$book_id;
        $test->merchantPaymentFailedURL = base_url().'booking/payment_failed';
        $test->currencyCode = strtoupper($book->total_price_currency);
        $test->decimalAmount = '0';
        $test->decimalAmountExtra = '0';
        $test->description = 'Description';
        $test->merchantTransactionId = 'OD01-000X-IS6YSPH7';
        $test->customerName = $custdata->user_fname . $custdata->user_lname;
        $test->customerEmailAddress = $custacc->user_email;
        $test->customerMobileNumber = $custdata->user_phone;
        $test->shippingAddressLine1 = '';
        $test->shippingAddressLine2 = '';
        $test->shippingCity = '';
        $test->shippingState = '';
        $test->shippingCountry = '';
        $test->shippingDistrict = '';
        $test->shippingVillage = '';
        $test->shippingPostcode = '';
        $test->billingAddressLine1 = '';
        $test->billingAddressLine2 = '';
        $test->billingCity = '';
        $test->billingState = '';
        $test->billingCountry = '';
        $test->billingDistrict = '';
        $test->billingVillage = '';
        $test->billingPostcode = '';
        $test->requestRecurringToken = 'N';

        $test->endpointType = 'AGGREGATOR-HASH';

        $test->timestamp = '1479787884961';
        $test->version = '1.5.0';
        $test->AdditionalData = [
          "AdditionalData2"=>"test2",
          "AdditionalData1"=>"test",
          "1234"=>"x x x"
        ];

        // CREDENTIALS
        $test->endpointId = $endpointId;
        $test->username = $username;
        $test->process($serverKey);

        $reqtest = new Response_dto;
        //Send the payment request now:
        $reqtest->process($test->response, $urlAPI);
        $response = $reqtest->response;
        // echo "<br/>Get Payment URL<br/>";
        // echo "<br/>REQUEST:<br/>" . json_encode($test);
        // echo "<br/><br/>RESPONSE:<br/>" . $response;

        $firstIndexURL = strpos($response, 'paymentPageURL')+17;
        $lastIndexURL = strpos($response, '"', $firstIndexURL);
        $responseURL = substr($response, $firstIndexURL, $lastIndexURL-$firstIndexURL);
        // echo "<br/><br/>URL :<br/>" . $responseURL;
        redirect($responseURL);
        // $myid = $this->session->userdata('id');
        // $token = base_convert(time(), 10, 36);
        // $obj = ['book_status' => 4, 'book_paid_date' => date('Y-m-d'), 'book_token' => $token];
        // $obj_id = ['book_id' => $book_id];
        // $this->BookingModel->update($obj, $obj_id);
        // $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        // $this->NotifBookingModel->delete(['user_id' => $myid, 'book_id' => $book_id, 'notif_title' => 'Booking Accepted']);
        // $this->send_notif_booking('Booking Paid', $book_id, $book->guide_id, '2');
        // $this->session->set_flashdata(array('message'=>'Booking successfully paid.','type_message'=>'success'));
        // redirect('booking/mybook'); 
    }

    public function payment_success($book_id) {
        $myid = $this->session->userdata('id');
        $token = base_convert(time(), 10, 36);
        $obj = ['book_status' => 4, 'book_paid_date' => date('Y-m-d'), 'book_token' => $token];
        $obj_id = ['book_id' => $book_id];
        $this->BookingModel->update($obj, $obj_id);
        $kurs = $this->KursModel->by_id(['konversi' => 'USD-IDR']);
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        if ($book->total_price_currency == 'idr') {
            $xmiles = round($book->total_price_nominal/$kurs->nilai2) * 5;      
        } else {
            $xmiles = round($book->total_price_nominal) * 5;
        }
        $x_usd_now = $xmiles * 0.01;
        $x_idr_now = round($x_usd_now * $kurs->nilai2);  
        $xmiles_exists = $this->UserMilesModel->by_id(['user_id' => $myid]);
        $data_xmiles = array(
                        'user_id' => $myid, 
                        'x_pts_now' => $xmiles, 
                        'x_pts_last' => @$xmiles_exists->x_pts_now,
                        'x_usd_now' => $x_usd_now,
                        'x_usd_last' => @$xmiles_exists->x_usd_now, 
                        'x_idr_now' => $x_idr_now,
                        'x_idr_last' => @$xmiles_exists->x_idr_now
                    );  
        if (empty($xmiles_exists)) {
            $this->UserMilesModel->insert($data_xmiles);
        } else {
            $this->UserMilesModel->update($data_xmiles, ['user_id' => $myid]);
        }
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $this->NotifBookingModel->delete(['user_id' => $myid, 'book_id' => $book_id, 'notif_title' => 'Booking Accepted']);
        $this->send_notif_booking('Booking Paid', $book_id, $book->guide_id, '2');
        $this->send_mail_to_guide($book_id, 'paid');
        $this->send_mail_to_tourist($book_id, 'paid');
        $this->session->set_flashdata(array('message'=>'Booking successfully paid.','type_message'=>'success'));
        redirect('booking/mybook');
    }

    public function payment_failed() {
        $this->session->set_flashdata(array('message'=>'Your payment failed. Please try again later or contact our billing service.','type_message'=>'error'));
        redirect('booking/mybook');
    }

    public function simulation($book_id) {
        $myid = $this->session->userdata('id');
        $token = base_convert(time(), 10, 36);
        $obj = ['book_status' => 4, 'book_paid_date' => date('Y-m-d'), 'book_token' => $token];
        $obj_id = ['book_id' => $book_id];
        $this->BookingModel->update($obj, $obj_id);
        $kurs = $this->KursModel->by_id(['konversi' => 'USD-IDR']);
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        if ($book->total_price_currency == 'idr') {
            $xmiles = round($book->total_price_nominal/$kurs->nilai2) * 5;
        } else {
            $xmiles = round($book->total_price_nominal) * 5;
        }
        $x_usd_now = $xmiles * 0.01;
        $x_idr_now = round($x_usd_now * $kurs->nilai2);
        $xmiles_exists = $this->UserMilesModel->by_id(['user_id' => $myid]);
        $data_xmiles = array(
                        'user_id' => $myid,
                        'x_pts_now' => $xmiles, 
                        'x_pts_last' => @$xmiles_exists->x_pts_now,
                        'x_usd_now' => $x_usd_now,
                        'x_usd_last' => @$xmiles_exists->x_usd_now, 
                        'x_idr_now' => $x_idr_now,
                        'x_idr_last' => @$xmiles_exists->x_idr_now
                    );
        if (empty($xmiles_exists)) {
            $this->UserMilesModel->insert($data_xmiles);
        } else {
            $this->UserMilesModel->update($data_xmiles, ['user_id' => $myid]);
        }
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $this->NotifBookingModel->delete(['user_id' => $myid, 'book_id' => $book_id, 'notif_title' => 'Booking Accepted']);
        $this->send_notif_booking('Booking Paid', $book_id, $book->guide_id, '2');
        $this->send_mail_to_guide($book_id, 'paid');
        $this->send_mail_to_tourist($book_id, 'paid');
        $this->session->set_flashdata(array('message'=>'Booking successfully paid.','type_message'=>'success'));
        redirect('booking/mybook');
    }

    public function start_tour() {
        $book_id = $this->input->post('book_id');
        $itin_id = $this->input->post('itin_id');
        $code = $this->input->post('code');
        $check = $this->BookingModel->by_id(['book_id' => $book_id, 'itin_id' => $itin_id, 'book_token' => $code]);
        if (!empty($check)) {
            $this->BookingModel->update(['book_status' => 5], ['book_id' => $book_id]);
            $this->session->set_flashdata(array('message'=>'Successfully. Tour started.','type_message'=>'success'));
        } else {
            $this->session->set_flashdata(array('message'=>'Code does not match. Please try again.','type_message'=>'error'));
        }
        redirect('booking/reqdetail/'.$itin_id.'/'.$book_id); 
    }

    //role type yang login : 1 = turis || role type 2 = guide
    public function end_tour($book_id, $role_type) {
        $this->preload();
        $this->logged_in_member();
        $myid = $this->session->userdata('id');
        $obj_id = ['book_id' => $book_id];
        $book = $this->BookingModel->by_id($obj_id);
        if ($role_type == 1) {
            $user_id = $book->tourist_id;
            $client = $book->guide_id;
            $this->BookingModel->update(['tourist_end_tour' => 1], $obj_id);
        } else {
            $user_id = $book->guide_id;
            $client = $book->tourist_id;
            $this->BookingModel->update(['guide_end_tour' => 1], $obj_id);
        }

        $book = $this->BookingModel->by_id($obj_id);
        if ($book->tourist_end_tour == 1 && $book->guide_end_tour == 1) {
            $this->BookingModel->update(['book_status' => 6], $obj_id);
        }

        $this->data['itin'] = $this->ItineraryModel->by_id(array('itin_id' => $book->itin_id));
        $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $client));
        $this->data['review'] = $this->ReviewModel->by_id(array('booking_id' => $book_id, 'user_id' => $user_id));
        $this->data['book'] = $book;
        $this->data['myid'] = $myid;
        $this->data['role_type'] = $role_type;
        $this->session->set_flashdata(array('message'=>'Thank you for traveling with us.','type_message'=>'success'));
        $this->template->display('booking_review', $this->data);
    }

    public function review_tour($book_id, $role_type) {
        $this->preload();
        $this->logged_in_member();
        $myid = $this->session->userdata('id');
        $obj_id = ['book_id' => $book_id];
        $book = $this->BookingModel->by_id($obj_id);
        if ($role_type == 1) {
            $user_id = $book->tourist_id;
            $client = $book->guide_id;
        } else {
            $user_id = $book->guide_id;
            $client = $book->tourist_id;
        }
        $this->data['itin'] = $this->ItineraryModel->by_id(array('itin_id' => $book->itin_id));
        $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $user_id));
        $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $client));
        $this->data['review'] = $this->ReviewModel->by_id(array('booking_id' => $book_id, 'user_id' => $user_id));
        $this->data['book'] = $book;
        $this->data['myid'] = $myid;
        $this->data['role_type'] = $role_type;
        $this->template->display('booking_review', $this->data);
    }

    public function send_review() {
        $obj = array('booking_id' => $this->input->post('book_id'),
                    'review_as' => $this->input->post('review_as'),
                    'user_id' => $this->input->post('user_id'),
                    'review_status' => $this->input->post('review_status'),
                    'review_to' => $this->input->post('review_to'),
                    'itin_id' => $this->input->post('itin_id'),
                    'review' => $this->input->post('review'),
                    'review_date' => date('Y-m-d'),
                    'rating' => $this->input->post('rating'),
                );
        $this->ReviewModel->insert($obj);
        $role_type = $this->input->post('role_type');
        $this->session->set_flashdata(array('message'=>'Thank you for your review.','type_message'=>'success'));
        redirect(base_url().'booking/review_tour/'.$obj['booking_id'].'/'.$role_type);
    }

    public function update_book_paid() {
        $book_id = $this->input->post('book_id');
        $book_paid = $this->input->post('book_paid');
        $this->BookingModel->update(['book_paid' => $book_paid], ['book_id' => $book_id]);
    }

}

?>