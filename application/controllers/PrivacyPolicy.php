<?php

class Privacypolicy extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'privacypolicy/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        // $this->load->model(array('MessageModel','TempImageModel','UserDataModel','UserAccountModel'));
        $this->data['page_title'] = "Privacy Policy";
    }
    
    public function index(){
        $lang = $this->input->get('lang');
        $this->data['lang'] = $lang;
        $this->template->display('privacy_policy',$this->data);
    }

}

?>