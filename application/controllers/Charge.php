<?php

class Charge extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'charge/');
        $this->data = array();
        init_generic_dao();
        // $this->load->library(array('Request_dto','Response_dto'));
        $this->data['page_title'] = "Charge";
    }
    
    public function index(){
        $urlAPI = 'https://ecomm.cashlez.com/ecom-m-server/ws/getPaymentURL';
        $serverKey = '65319038483273579275326436432699';//confirm first to ops cashlez
        $username = 'od-bwemal';
        $endpointId = 'bwemall';

        //Example
        $test = new Request_dto;
        //EXAMPLE VALUES
        $test->amount = '106';
        $test->amountExtra = '0';
        $test->authorizationType = 'PAY';
        $test->integrationType = 'CASHLEZ_URL';
        $test->merchantPaymentSuccessURL = 'http://localhost:8080/payment/success.php';
        $test->merchantPaymentFailedURL = 'http://localhost:8080/payment/failed.php';
        $test->currencyCode = 'IDR';
        $test->decimalAmount = '0';
        $test->decimalAmountExtra = '0';
        $test->description = 'Description';
        $test->merchantTransactionId = 'OD01-000X-IS6YSPH7';
        $test->customerName = 'Martin';
        $test->customerEmailAddress = 'riko@cashlez.com';
        $test->customerMobileNumber = '+628199999999';
        $test->shippingAddressLine1 = 'PT Cashlez Worlwide Indonesia';
        $test->shippingAddressLine2 = 'APL Tower 9th Floor Suite T8';
        $test->shippingCity = 'Jakarta';
        $test->shippingState = 'DKI Jakarta';
        $test->shippingCountry = 'Indonesia';
        $test->shippingDistrict = 'Tj. Duren selatan';
        $test->shippingVillage = 'Grogol Petamburan';
        $test->shippingPostcode = '11111';
        $test->billingAddressLine1 = 'PT Cashlez Worlwide Indonesia';
        $test->billingAddressLine2 = 'APL Tower 9th Floor Suite T8';
        $test->billingCity = 'Jakarta';
        $test->billingState = 'DKI Jakarta';
        $test->billingCountry = 'Indonesia';
        $test->billingDistrict = 'Tj. Duren selatan';
        $test->billingVillage = 'Grogol Petamburan';
        $test->billingPostcode = '11111';
        $test->requestRecurringToken = 'N';

        $test->endpointType = 'AGGREGATOR-HASH';

        $test->timestamp = '1479787884961';
        $test->version = '1.5.0';
        $test->AdditionalData = [
          "AdditionalData2"=>"test2",
          "AdditionalData1"=>"test",
          "1234"=>"x x x"
        ];

        // CREDENTIALS
        $test->endpointId = $endpointId;
        $test->username = $username;
        $test->process($serverKey);

        $reqtest = new Response_dto;
        //Send the payment request now:
        $reqtest->process($test->response, $urlAPI);
        $response = $reqtest->response;
        echo "<br/>Get Payment URL<br/>";
        echo "<br/>REQUEST:<br/>" . json_encode($test);
        echo "<br/><br/>RESPONSE:<br/>" . $response;

        $firstIndexURL = strpos($response, 'paymentPageURL')+17;
        $lastIndexURL = strpos($response, '"', $firstIndexURL);
        $responseURL = substr($response, $firstIndexURL, $lastIndexURL-$firstIndexURL);
        echo "<br/><br/>URL :<br/>" . $responseURL;
    }

}

?>