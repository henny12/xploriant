<?php

class Dashboard extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/dashboard/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_admin();
        $this->load->library('template_admin');
        $this->load->model(array('NotRecomModel','TempImageModel','ItineraryModel','ItineraryImageModel','UserDataModel'));
        $this->data['page_title'] = "Home";
    }
    
    public function index(){
        $this->fetch_data(20, 0, null);
        $this->template_admin->display('admin/dashboard',$this->data);
    }

    private function fetch_data($limit, $offset, $key) {
        $itinerary = $this->ItineraryModel->fetch($limit, $offset, null, true, null, $key);
        $arr_itin = [];
        if(!empty($itinerary)){
            foreach ($itinerary as $val) {
                $image = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id));
                $merge = array_merge((array)$val, array('image' => $image));
                array_push($arr_itin, (object)$merge);
            }
        }
        $this->data['itinerary'] = $arr_itin;
        $this->data['total_rows'] = $this->ItineraryModel->fetch(null, null, null, true, null, $key, null, true);
    }

}

?>