<?php

class Account extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/account/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_admin();
        $this->load->library('template_admin');
        $this->load->model(array('UserAccountModel','UserDataModel','PersonalInterestModel'));
        $this->data['page_title'] = "Account";
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['personal_interest_list'] = $this->PersonalInterestModel->fetch();
    }

    private function validate() {           
        $this->form_validation->set_rules('user_fname', 'First Name', 'trim|required|max_length[100]');
        return $this->form_validation->run();
    }
    
    public function index(){
        $this->preload();
        $user_arr = [];
        $user_acc = $this->UserAccountModel->fetch();
        if (!empty($user_acc)){
            foreach ($user_acc as $val) {
                $user_data = $this->UserDataModel->by_id(['user_id' => $val->user_id]);
                $user_merge = array_merge((array)$val, (array)$user_data);
                array_push($user_arr, (object)$user_merge);
            }
        }
        $this->data['account'] = $user_arr;
        $this->template_admin->display('admin/list_account', $this->data);
    }

    public function change_status($user_id) {
        $user_id = ['user_id' => $user_id];
        $data = ['user_status' => $this->input->post('status'), 'comment_suspend' => $this->input->post('comment')];
        $this->UserAccountModel->update($data, $user_id);
        $this->session->set_flashdata(array('message'=>'Status has been changed.','type_message'=>'success'));
        redirect('admin/account');
    }

    public function account_status($user_id){
        $this->data['status'] = $this->UserAccountModel->by_id(array('user_id' => $user_id));
        $this->data['user_id'] = $user_id;
        $result['html_page'] = $this->load->view('admin/status_account', $this->data, TRUE);
        $result['status'] = $this->data['status']->user_status;
        echo json_encode($result);
    }

    public function fetch_record($keys) {
        $account = $this->UserAccountModel->by_id($keys);
        $data =  $this->UserDataModel->by_id($keys);
        $this->data['user'] = (object) array_merge((array)$account, (array)$data);
    }

    private function fetch_input() {
        $data = array('user_fname' => $this->input->post('user_fname'),
                    'user_lname' => $this->input->post('user_lname'),
                    'user_nationality' => $this->input->post('user_nationality'),
                    'user_residence' => $this->input->post('user_residence'),
                    'user_lang' => $this->input->post('user_lang'),
                    'user_desc' => $this->input->post('user_desc')
                );
        return $data;
    }

    public function edit($user_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('admin_id');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('user_id' => $user_id);

        if ($this->validate() != false) {
            $this->UserDataModel->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/edit_account', $this->data);
        }
    }

}

?>