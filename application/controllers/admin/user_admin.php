<?php

class User_admin extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/user_admin/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_admin();
        $this->load->library('template_admin');
        $this->load->model(array('AdminModel'));
        $this->data['page_title'] = "Account";
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }
    
    public function index() {
        $this->preload();
        $this->data['user_admin'] = $this->AdminModel->fetch();
        $this->template_admin->display('admin/list_admin', $this->data);
    }
}

?>