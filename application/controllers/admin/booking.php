<?php

class Booking extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/booking/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_admin();
        $this->load->library('template_admin');
        $this->load->model(array('BookingModel'));
        $this->data['page_title'] = "Account";
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }
    
    public function index(){
        $this->preload();
        $this->data['booking'] = $this->BookingModel->fetch();
        $this->template_admin->display('admin/list_booking', $this->data);
    }

    public function change_status($book_id) {
        $user_id = ['book_id' => $itin_id];
        $data = ['book_status' => $this->input->post('status'), 'comment_suspend' => $this->input->post('comment')];
        $this->ItineraryModel->update($data, $user_id);
        $this->session->set_flashdata(array('message'=>'Status has been changed.','type_message'=>'success'));
        redirect('admin/booking');
    }

    public function booking_status($book_id){
        $this->data['status'] = $this->ItineraryModel->by_id(array('book_id' => $book_id));
        $this->data['book_id'] = $book_id;
        $result['html_page'] = $this->load->view('admin/status_booking', $this->data, TRUE);
        $result['status'] = $this->data['status']->book_status;
        echo json_encode($result);
    }
}

?>