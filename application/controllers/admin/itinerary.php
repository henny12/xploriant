<?php

class Itinerary extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/itinerary/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_admin();
        $this->load->library('template_admin');
        $this->load->model(array('ItineraryModel','UserDataModel','ItineraryDetailModel'));
        $this->data['page_title'] = "Account";
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    private function validate() {           
        $this->form_validation->set_rules('itin_title', 'Title', 'trim|required');
        return $this->form_validation->run();
    }
    
    public function index(){
        $this->preload();
        $itin_arr = [];
        $itinerary = $this->ItineraryModel->fetch();
        if (!empty($itinerary)) {
            foreach ($itinerary as $val) {
                $guide = $this->UserDataModel->by_id(['user_id' => $val->itin_creator_id]);
                $itin_merge = array_merge((array) $val, ['guide_name' => $guide->user_fname.' '.$guide->user_lname, 'guide_residence' => $guide->user_residence]);
                array_push($itin_arr, (object) $itin_merge);
            }
        }
        $this->data['itinerary'] = $itin_arr;
        $this->template_admin->display('admin/list_itinerary', $this->data);
    }

    public function change_status($itin_id) {
        $user_id = ['itin_id' => $itin_id];
        $data = ['itin_status' => $this->input->post('status'), 'comment_suspend' => $this->input->post('comment')];
        $this->ItineraryModel->update($data, $user_id);
        $this->session->set_flashdata(array('message'=>'Status has been changed.','type_message'=>'success'));
        redirect('admin/itinerary');
    }

    public function itinerary_status($itin_id){
        $this->data['status'] = $this->ItineraryModel->by_id(array('itin_id' => $itin_id));
        $this->data['itin_id'] = $itin_id;
        $result['html_page'] = $this->load->view('admin/status_itinerary', $this->data, TRUE);
        $result['status'] = $this->data['status']->itin_status;
        echo json_encode($result);
    }

    public function fetch_record($keys) {
        $itinerary = $this->ItineraryModel->by_id($keys);
        $detail =  $this->ItineraryDetailModel->fetch(null, null,'itd_day', true, null, $keys, null, false, false);
        $guide = $this->UserDataModel->by_id(['user_id' => $itinerary->itin_creator_id]);
        $this->data['itin'] = (object) array_merge((array)$itinerary, (array)$guide, ['itin_detail' => $detail]);
        echo "<pre>";
        print_r($this->data['itin']);die();
    }

    private function fetch_input() {
        $data = array('user_fname' => $this->input->post('user_fname'),
                    'user_lname' => $this->input->post('user_lname'),
                    'user_nationality' => $this->input->post('user_nationality'),
                    'user_residence' => $this->input->post('user_residence'),
                    'user_lang' => $this->input->post('user_lang'),
                    'user_desc' => $this->input->post('user_desc')
                );
        return $data;
    }

    public function edit($itin_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('admin_id');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('itin_id' => $itin_id);

        if ($this->validate() != false) {
            $this->UserDataModel->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/edit_itinerary', $this->data);
        }
    }
}

?>