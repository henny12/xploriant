<?php

class Auth extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/auth/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        $this->load->model(array('AdminModel','OauthModel'));
        $this->data['page_title'] = "Login";
        $this->data['body_class'] = "signup-page sidebar-collapse";
    }
    
    public function login() {
        $this->load->view('admin/login', $this->data);
    }

    public function login_process() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->AdminModel->check_login($username, $password);
        if ($result != false){
            $admin = $this->AdminModel->by_id(['admin_user' => $username]);
            $userdata = [
                    'admin_id'  => $admin->admin_id,
                    'username'  => $admin->admin_user,
                    'fullname'  => $admin->admin_name,
                    'logged_in_admin' => 'TRUE'
            ];
            $this->session->set_userdata($userdata);
            redirect('admin/dashboard');
        } else {
            $this->session->set_flashdata(array('message'=>'Login failed. Your email is not registered!','type_message'=>'error'));
            $this->template->display('admin/login', $this->data);
        }
    }

    public function logout() {
        $clear = array('admin_id','username','fullname','logged_in_admin');
        $this->session->unset_userdata($clear);
        $this->session->set_flashdata(array('message'=>'Logout success','type_message'=>'success'));
        redirect('admin/auth/login');
    }
}

?>