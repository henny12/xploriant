<?php
ob_start();
class Crontour extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'crontour/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        $this->load->model(array('MessageModel','BookingModel','UserDataModel','UserAccountModel','UserMilesModel','KursModel','ItineraryModel',''));
    }
    
    public function index() {
        $myid = $this->session->userdata('id');
        $booking_paid = $this->BookingModel->fetch(null, null, null, false, null, array('book_status' => 4));
        if (!empty($booking_paid)) {
            foreach ($booking_paid as $bp) {
                $dayminstart = date('Y-m-d', strtotime('-1 days', strtotime($bp->start_date)));
                //check day -24 hours to start tour
                if ($dayminstart == date('Y-m-d') && empty($bp->day_min24_mail)) {
                    $this->send_mail_to_guide($bp->book_id, 'd-24hours');
                    $this->send_mail_to_tourist($bp->book_id, 'd-24hours');
                    $this->BookingModel->update(['day_min24_mail' => 1], ['book_id' => $bp->book_id]);
                }
                //check start tour
                if ($bp->start_date == date('Y-m-d') && empty($bp->day_start_mail)) {
                    $this->send_mail_to_guide($bp->book_id, 'start_tour');
                    $this->send_mail_to_tourist($bp->book_id, 'start_tour');
                    $this->BookingModel->update(['day_start_mail' => 1], ['book_id' => $bp->book_id]);
                }
            }
        }

        $booking_start = $this->BookingModel->fetch(null, null, null, false, null, array('book_status' => 5));
        if (!empty($booking_start)) {
            foreach ($booking_start as $bs) {
                $dayplusend = date('Y-m-d', strtotime('+1 days', strtotime($bs->end_date)));
                //check end tour
                if ($dayplusend == date('Y-m-d') && empty($bp->day_end_mail)) {
                    $this->send_mail_to_guide($bp->book_id, 'end_tour');
                    $this->send_mail_to_tourist($bp->book_id, 'end_tour');
                    $this->BookingModel->update(['day_end_mail' => 1], ['book_id' => $bp->book_id]);
                }
            }
        }
        return 1;
    }

    public function send_mail_to_guide($book_id, $type) {
        $this->config_mail();
        $this->fetch_data($book_id);
        $this->email->to($this->data['accountguide']->user_email); 
        if ($type == 'd-24hours') {
            $this->email->subject('D-24 Hours');
            $this->email->message($this->load->view('template_email/d_24hours_guide', $this->data, true));  
        } elseif ($type == 'start_tour') {
            $this->email->subject('Start Tour');
            $this->email->message($this->load->view('template_email/start_date_guide', $this->data, true));
        } elseif ($type == 'end_tour') {
            $this->email->subject('End Tour');
            $this->email->message($this->load->view('template_email/end_date_guide', $this->data, true));
        }
        $this->email->send();
    }

    public function send_mail_to_tourist($book_id, $type) {
        $this->config_mail();
        $this->fetch_data($book_id);
        $this->email->to($this->data['accountturis']->user_email); 
        if ($type == 'd-24hours') {
            $this->email->subject('D-24 Hours');
            $this->email->message($this->load->view('template_email/d_24hours_tourist', $this->data, true));  
        } elseif ($type == 'start_tour') {
            $this->email->subject('Start Tour');
            $this->email->message($this->load->view('template_email/start_date_tourist', $this->data, true));
        } elseif ($type == 'end_tour') {
            $this->email->subject('End Tour');
            $this->email->message($this->load->view('template_email/end_date_tourist', $this->data, true));
        }
        $this->email->send();
    }

    private function fetch_data($book_id) {
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $itin = $this->ItineraryModel->by_id(['itin_id' => $book->itin_id]);
        $guide = $this->UserDataModel->by_id(['user_id' => $book->guide_id]);
        $tourist = $this->UserDataModel->by_id(['user_id' => $book->tourist_id]);
        $accountguide = $this->UserAccountModel->by_id(['user_id' => $book->guide_id]);
        $accountturis = $this->UserAccountModel->by_id(['user_id' => $book->tourist_id]);
        $this->data['book'] = $book;
        $this->data['itin'] = $itin;
        $this->data['guide'] = $guide;
        $this->data['tourist'] = $tourist;
        $this->data['accountguide'] = $accountguide;
        $this->data['accountturis'] = $accountturis;
    }

    private function config_mail() {
        $config_email = "henny.novelite@gmail.com";
        $config_password = "Novelite8899";    
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => $config_email,
            'smtp_pass' => $config_password,
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config_email, 'Xploriant');
    }

}

?>