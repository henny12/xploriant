<?php
ob_start();
class Payment extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'payment/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        $this->load->model(array('MessageModel','BookingModel','UserDataModel','UserAccountModel','UserMilesModel','KursModel'));
        $this->data['page_title'] = "Payment";
    }
    
    public function success($book_id) {
        $myid = $this->session->userdata('id');
        $token = base_convert(time(), 10, 36);
        $obj = ['book_status' => 4, 'book_paid_date' => date('Y-m-d'), 'book_token' => $token];
        $obj_id = ['book_id' => $book_id];
        $this->BookingModel->update($obj, $obj_id);
        $book = $this->BookingModel->by_id(['book_id' => $book_id]);
        $this->NotifBookingModel->delete(['user_id' => $myid, 'book_id' => $book_id, 'notif_title' => 'Booking Accepted']);
        $this->send_notif_booking('Booking Paid', $book_id, $book->guide_id, '2');
        $this->session->set_flashdata(array('message'=>'Booking successfully paid.','type_message'=>'success'));
        redirect('booking/mybook');
    }

    public function failed() {
        $this->session->set_flashdata(array('message'=>'Your payment failed. Please try again later or contact our billing service.','type_message'=>'error'));
        redirect('booking/mybook');
    }

    

}

?>