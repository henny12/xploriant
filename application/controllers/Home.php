<?php
ob_start();
class Home extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'home/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        $this->load->model(array('NotRecomModel','TempImageModel','ItineraryModel','ItineraryImageModel','UserDataModel','ReviewModel','TopDestinationModel'));
        $this->data['page_title'] = "Home";
    }
    
    public function index(){
        $this->fetch_data(24  , 0, ['itin_status' => 1]);
        $this->template->display('home',$this->data);
    }

    private function fetch_data($limit, $offset, $key) {
        $itinerary = $this->ItineraryModel->fetch($limit, $offset, null, true, null, $key);
        $arr_itin = [];
        if(!empty($itinerary)){
            foreach ($itinerary as $val) {
                $image = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id));
                $total_review = $this->ReviewModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id), null, true);
                $merge = array_merge((array)$val, array('image' => $image, 'total_review' => $total_review));
                array_push($arr_itin, (object)$merge);
            }
        }
        $this->data['top_desti'] = $this->TopDestinationModel->fetch(11, 0, 'td_order', true, null, ['td_status' => 1]);
        $this->data['itinerary'] = $arr_itin;
        $this->data['total_rows'] = $this->ItineraryModel->fetch(null, null, null, true, null, $key, null, true);
    }

}

?>