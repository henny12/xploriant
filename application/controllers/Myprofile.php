<?php

class Myprofile extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'myprofile/');
        $this->data = array();
        init_generic_dao();
        $this->logged_in_member();
        $this->load->library('template');
        $this->load->model(array('UserAccountModel','UserDataModel','PersonalInterestModel','TempImageModel','ItineraryModel','ItineraryImageModel'));
        $this->data['page_title'] = "My Profile";
        $this->path_thumb = "media/profpic_thumb/";
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['personal_interest_list'] = $this->PersonalInterestModel->fetch();
    }
    
    public function index(){
        $this->preload();
        $this->fetch_data(20, 0, null);
        $user_id = $this->session->userdata('id');
        $user_acc = $this->UserAccountModel->by_id(array('user_id' => $user_id));
        $user_data = $this->UserDataModel->by_id(array('user_id' => $user_id));
        $merge = array_merge((array)$user_acc, (array)$user_data);
        $this->data['user'] = (object) $merge;
        $this->template->display('myprofile',$this->data);
    }

    private function fetch_data($limit, $offset, $key) {
        $key = array('itin_creator_id' => $this->session->userdata('id'), 'itin_status' => 1);
        $itinerary = $this->ItineraryModel->fetch($limit, $offset, null, true, null, $key);
        $arr_itin = [];
        if(!empty($itinerary)){
            foreach ($itinerary as $val) {
                $image = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id));
                $merge = array_merge((array)$val, array('image' => $image));
                array_push($arr_itin, (object)$merge);
            }
        }
        $this->data['itinerary'] = $arr_itin;
        $this->data['total_rows'] = $this->ItineraryModel->fetch(null, null, null, true, null, $key, null, true);
    }

    private function fetch_input() {
        $data = array('user_fname' => $this->input->post('user_fname'),
            'user_lname' => $this->input->post('user_lname'),
            'user_email' => $this->input->post('user_email'),
            'user_gender' => $this->input->post('user_gender'),
            'user_nationality' => $this->input->post('user_nationality'),
            'user_residence' => $this->input->post('user_residence'),
            'user_dob' => $this->input->post('user_dob'),
            'user_email' => $this->input->post('user_email'),
            'user_personal_id' => $this->input->post('user_personal_id'),
            'account_status' => $this->input->post('account_status'));

        return $data;
    }

    public function upload_profpic(){
        $croped_image = $_POST['image'];
        list($type, $croped_image) = explode(';', $croped_image);
        list(, $croped_image)      = explode(',', $croped_image);
        $croped_image = base64_decode($croped_image);
        $image_name = md5(date('Y-m-d').time()).'.png';
        // upload cropped image to server 
        file_put_contents($this->path_thumb.$image_name, $croped_image);
        $obj = array('user_photo' => $image_name);
        $obj_id = array('user_id' => $this->session->userdata('id'));
        $this->UserDataModel->update($obj, $obj_id);
        $data = json_encode($image_name);
        echo $data;
    }

    public function edit_account($user_id) {
        $data = ['user_fname' => $this->input->post('fname'),
                'user_lname' => $this->input->post('lname'),
                'user_personal_id' => $this->input->post('personalid'),
                'updated_on' => date('Y-m-d h:i:s'),
                'updated_by' => $this->session->userdata('id')];

        $this->UserDataModel->update($data, ['user_id' => $user_id]);
        $this->session->set_flashdata(array('message'=>'Data has been changed.','type_message'=>'success'));
        redirect('myprofile');
    }

    public function change_password($user_id) {
        $current_pass = $this->input->post('currentpassword');
        $account = $this->UserAccountModel->by_id(['user_id' => $user_id]);
        if ($this->OauthModel->VerifyPassword($current_pass,$account->user_password) == 1) {
            $new_password = $this->OauthModel->HashPassword($this->input->post('newpassword'));
            $this->UserAccountModel->update(['user_password' => $new_password], ['user_id' => $user_id]);
            $this->session->set_flashdata(array('message'=>'Password has been changed.','type_message'=>'success'));
            redirect('myprofile');
        } else {
            $this->session->set_flashdata(array('message'=>'Your current password is wrong.','type_message'=>'error'));
            redirect('myprofile');
        }
    }

    public function edit_about($user_id) {
        $data = ['user_gender' => $this->input->post('gender'),
                'user_dob' => $this->input->post('dob'),
                'user_nationality' => $this->input->post('nationality'),
                'user_residence' => $this->input->post('residence'),
                'user_lang' => $this->input->post('language'),
                'user_desc' => $this->input->post('desc'),
                'user_interests' => (!empty($this->input->post('interest')))?implode("-",$this->input->post('interest')):""
            ];

        $this->UserDataModel->update($data, ['user_id' => $user_id]);
        $this->session->set_flashdata(array('message'=>'Data has been changed.','type_message'=>'success'));
        redirect('myprofile');
    }

}

?>