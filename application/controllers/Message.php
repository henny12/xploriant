<?php

class Message extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'message/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        $this->load->model(array('MessageModel','TempImageModel','UserDataModel','UserAccountModel'));
        $this->data['page_title'] = "Message";
    }
    
    public function index(){
        $this->template->display('message',$this->data);
    }

}

?>