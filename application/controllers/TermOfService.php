<?php

class Termofservice extends MY_Controller {
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'termofservice/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        // $this->load->model(array('MessageModel','TempImageModel','UserDataModel','UserAccountModel'));
        $this->data['page_title'] = "Term Of Service";
    }
    
    public function index(){
        $lang = $this->input->get('lang');
        $this->data['lang'] = $lang;
        $this->template->display('term_service',$this->data);
    }



}

?>