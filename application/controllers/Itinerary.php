<?php
ob_start();
class Itinerary extends MY_Controller {

    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'itinerary/');
        $this->data = array();
        init_generic_dao();
        $this->load->library('template');
        $this->load->model(array('PersonalInterestModel','NotRecomModel','TempImageModel','ItineraryModel','ItineraryImageModel','UserDataModel','ItineraryDetailModel','ReviewModel', 'TopDestinationModel'));
        $this->data['page_title'] = "Be a Guide";
    }

    private function validate() {           
        $this->form_validation->set_rules('title', 'Title', 'trim|required');

        return $this->form_validation->run();
    }

    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['personal_interest_list'] = $this->PersonalInterestModel->fetch();
        $this->data['not_recommended_list'] = $this->NotRecomModel->fetch();
    }
    
    public function index(){
        $this->template->display('itinerary_list',$this->data);
    }

    private function fetch_data($limit, $offset, $key) {
        $active = ['itin_status' => 1];
        $itinerary = $this->ItineraryModel->fetch($limit, $offset, null, true, $key, $active, null);
        $arr_itin = [];
        if(!empty($itinerary)){
            foreach ($itinerary as $val) {
                $image = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id));
                $total_review = $this->ReviewModel->fetch(null, null, null, true, null, array('itin_id' => $val->itin_id), null, true);
                $merge = array_merge((array)$val, array('image' => $image, 'total_review' => $total_review));
                array_push($arr_itin, (object)$merge);
            }
        }
        $this->data['itinerary'] = $arr_itin;
        $this->data['total_rows'] = $this->ItineraryModel->fetch(null, null, null, true, $key, $active, null, true);
    }

    private function fetch_record($keys){
        $itinerary = $this->ItineraryModel->by_id($keys);
        $additional_info = [];
        $not_recom = $itinerary->itin_not_recom_opt;
        $exp = explode('-', $not_recom);
        foreach ($exp as $val) {
            $nr = $this->NotRecomModel->by_id(array('nr_id' => $val));
            $add = 'Not recommended for tourists ' . $nr->nr_title;
            array_push($additional_info, $add);
        }

        $itin_detail_md = [];
        $multiday = $this->ItineraryDetailModel->get_day($itinerary->itin_id);
        if (!empty($multiday)){
            foreach ($multiday as $val) {
                $itin_detail = $this->ItineraryDetailModel->fetch(null, null, 'itd_start_time' , true, null, array('itin_id' => $itinerary->itin_id, 'itd_day' => $val->itd_day));
                $merge = array_merge(array('itd_day'=> $val->itd_day, 'detail' => $itin_detail));
                array_push($itin_detail_md, (object) $merge);
                
            }
        }
        $this->data['myid'] = $this->session->userdata('id');
        $this->data['additional_info'] = $additional_info;
        $this->data['guide'] = $this->UserDataModel->by_id(array('user_id' => $itinerary->itin_creator_id));
        $this->data['itin_image'] = $this->ItineraryImageModel->fetch(null, null, null, true, null, array('itin_id' => $itinerary->itin_id)); 
        $this->data['itinerary'] = $itinerary;
        $this->data['itinerary_detail'] = $this->ItineraryDetailModel->get_detail($itinerary->itin_id);
        $this->data['itinerary_detail_md'] = $itin_detail_md;
    }

    public function detail($itin_id) {
        $obj_id = array('itin_id' => $itin_id);
        
        $this->preload();
        $this->fetch_record($obj_id);
        $this->template->display('itinerary_detail', $this->data);
    }

    public function delete($itin_id) {
        $obj_id = ['itin_id' => $itin_id];
        $data = ['itin_status' => 2];
        $this->ItineraryModel->update($data, $obj_id);
        $this->session->set_flashdata(array('message'=>'Data has been deleted.','type_message'=>'success'));
        redirect('myprofile');
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_itin');
        if ($this->input->post('search')) {
            $key = array(
                'itin_location' => strtolower($this->input->post('location')),
                'itin_title' => strtolower($this->input->post('location'))
            );
            $this->session->set_userdata(array('filter_itin' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
    
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template->display('itinerary_list', $this->data);
    }

    public function top_destination($td_title, $page = 1) {
        $key = $this->session->userdata('filter_itin');
        $key = array(
                'itin_location' => strtolower($td_title),
                'itin_title' => strtolower($td_title)
            );
        $this->session->set_userdata(array('filter_itin' => $key));
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
}

?>