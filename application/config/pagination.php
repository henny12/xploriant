<?php

$config['num_links'] = 2;

$config['limit_pagination'] = 4;

$config['use_page_numbers'] = TRUE;
$config['query_string_segment'] = 'page';

$config['full_tag_open']   = '<div class="pagging text-center"><nav aria-label="Page navigation example"><ul class="pagination pagination-primary">';
$config['full_tag_close']  = '</ul></nav></div>';

$config['first_link']      = 'First'; 
$config['first_tag_open']  = '<li class="page-item"><span class="page-link">';
$config['first_tag_close'] = '</span></li>';

$config['last_link']       = 'Last'; 
$config['last_tag_open']   = '<li class="page-item"><span class="page-link">';
$config['last_tag_close']  = '</span></li>';

$config['next_link']       = '<i class="fa fa-angle-double-right pageb" aria-hidden="true"></i>'; 
$config['next_tag_open']   = '<li class="page-item"><span class="page-link">';
$config['next_tag_close']  = '</span></li>';

$config['prev_link']       = '<i class="fa fa-angle-double-left pageb" aria-hidden="true"></i>'; 
$config['prev_tag_open']   = '<li class="page-item"><span class="page-link">';
$config['prev_tag_close']  = '</span></li>';

$config['cur_tag_open']    = '<li class="page-item active"><span class="page-link">';
$config['cur_tag_close']   = '<span class="sr-only">(current)</span></span></li>';
 
$config['num_tag_open']    = '<li class="page-item"><span class="page-link">';
$config['num_tag_close']   = '</span></li>';





?>