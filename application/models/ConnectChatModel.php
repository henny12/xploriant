<?php
class ConnectChatModel extends Generic_dao {

    public function table_name() {
        return Tables::$connect_message;
    }

    public function field_map() {
        return array(
            'cm_id' => 'cm_id',
            'cm_user_id' => 'cm_user_id',
            'cm_connected_to' => 'cm_connected_to',
            'read_status' => 'read_status',
            'last_chat' => 'last_chat'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    
}

?>