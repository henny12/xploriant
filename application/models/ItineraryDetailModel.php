<?php
class ItineraryDetailModel extends Generic_dao {

    public function table_name() {
        return Tables::$itinerary_detail;
    }
    
    public function field_map() {
        return array(
            'itd_id' => 'itd_id',
            'itin_id' => 'itin_id',
            'itd_start_time' => 'itd_start_time',
            'itd_end_time' => 'itd_end_time',
            'itd_title' => 'itd_title',
            'itd_desc' => 'itd_desc',
            'itd_day' => 'itd_day'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    public function get_detail($itin_id) {
        $sql = "select * from itinerary_detail where itin_id = '$itin_id' order by itd_day, itd_start_time";
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

    public function get_day($itin_id) {
        $sql = "select itd_day from itinerary_detail where itin_id = '$itin_id' group by itd_day order by itd_day, itd_start_time";
        $query = $this->ci->db->query($sql);
        return $query->result();
    }
}

?>