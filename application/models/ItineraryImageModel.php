<?php
class ItineraryImageModel extends Generic_dao {

    public function table_name() {
        return Tables::$itinerary_image;
    }

    public function field_map() {
        return array(
            'itin_img_id' => 'itin_img_id',
            'itin_img_name' => 'itin_img_name',
            'itin_id' => 'itin_id',
            'updated_by' => 'updated_by',
            'updated_on' => 'updated_on'
        );
    }

    public function __construct() {
        parent::__construct();
    }
}

?>