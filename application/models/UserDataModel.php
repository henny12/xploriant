<?php
class UserDataModel extends Generic_dao {

    public function table_name() {
        return Tables::$user_data;
    }

    public function field_map() {
        return array(
            'user_id' => 'user_id',
            'user_fname' => 'user_fname',
            'user_lname' => 'user_lname',
            'user_gender' => 'user_gender',
            'user_nationality' => 'user_nationality',
            'user_residence' => 'user_residence',
            'user_dob' => 'user_dob',
            'user_personal_id' => 'user_personal_id',
            'user_interests' => 'user_interests',
            'user_lang' => 'user_lang',
            'user_desc' => 'user_desc',
            'user_photo' => 'user_photo',
            'user_phone' => 'user_phone',
            'updated_by' => 'updated_by',
            'updated_on' => 'updated_on'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    public function PictureUrlById($id) {  
        $query = $this->ci->db->query("select user_id, user_photo from ".$this->table_name()." where user_id='$id' limit 1");
        $res = $query->row_array();
        if(!empty($res['user_photo'])){
            return base_url('media/profpic_thumb/'.$res['user_photo']);
        }else{
            return base_url('public/images/user-icon.jpg');
        }
    }

}

?>