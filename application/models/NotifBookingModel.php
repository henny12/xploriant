<?php
class NotifBookingModel extends Generic_dao {

    public function table_name() {
        return Tables::$notif_booking;
    }

    public function field_map() {
        return array(
            'notif_id' => 'notif_id',
            'notif_title' => 'notif_title',
            'book_id' => 'book_id',
            'user_id' => 'user_id',
            'book_type' => 'book_type',
            'read_status' => 'read_status',
            'created_at' => 'created_at'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>