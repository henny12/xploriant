<?php
class KursModel extends Generic_dao {

    public function table_name() {
        return Tables::$kurs;
    }

    public function field_map() {
        return array(
            'konversi' => 'konversi',
            'nilai1' => 'nilai1',
            'nilai2' => 'nilai2',
            'last_update' => 'last_update'
        );
    }

    public function __construct() {
        parent::__construct();
    }
}

?>