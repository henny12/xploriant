<?php
class TopDestinationModel extends Generic_dao {

    public function table_name() {
        return Tables::$top_destination;
    }

    public function field_map() {
        return array(
            'td_id' => 'td_id',
            'td_title' => 'td_title',
            'td_image' => 'td_image',
            'td_desc' => 'td_desc',
            'td_status' => 'td_status',
            'td_order' => 'td_order'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>