<?php
class NotRecomModel extends Generic_dao {

    public function table_name() {
        return Tables::$not_recommended;
    }

    public function field_map() {
        return array(
            'nr_id' => 'nr_id',
            'nr_title' => 'nr_title',
            'nr_desc' => 'nr_desc',
            'nr_status' => 'nr_status'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>