<?php
class TempImageModel extends Generic_dao {

    public function table_name() {
        return Tables::$temp_image;
    }

    public function field_map() {
        return array(
            'temp_id' => 'temp_id',
            'temp_img_name' => 'temp_img_name',
            'temp_img_token' => 'temp_img_token'
        );
    }

    public function __construct() {
        parent::__construct();
    }
}

?>