<?php
class ReviewModel extends Generic_dao {

    public function table_name() {
        return Tables::$review;
    }

    public function field_map() {
        return array(
            'review_id' => 'review_id',
            'booking_id' => 'booking_id',
            'review_as' => 'review_as',
            'rating' => 'rating',
            'review' => 'review',
            'user_id' => 'user_id',
            'review_date' => 'review_date',
            'review_status' => 'review_status',
            'review_to' => 'review_to',
            'itin_id' => 'itin_id'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    public function avg_value($booking_id){
        $sql = "select avg(trim(rating)) as value from " . $this->table_name() . " where booking_id = '$booking_id'";
        $result = $this->ci->db->query($sql)->result();
        return $result[0];
    }
}

?>