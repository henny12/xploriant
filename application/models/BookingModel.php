<?php

class BookingModel extends Generic_dao {

    public function table_name() {
        return Tables::$booking;
    }

    public function field_map() {
        return array(
            'book_id' => 'book_id',
            'book_code' => 'book_code',
            'itin_id' => 'itin_id',
            'guide_id' => 'guide_id',
            'book_date' => 'book_date',
            'start_date' => 'start_date',
            'end_date' => 'end_date',
            'total_tourist' => 'total_tourist',
            'total_price_currency' => 'total_price_currency',
            'total_price_nominal' => 'total_price_nominal',
            'book_request' => 'book_request',
            'book_status' => 'book_status',
            'book_paid' => 'book_paid',
            'tourist_id' => 'tourist_id',
            'updated_on' => 'updated_on',
            'book_accepted_date' => 'book_accepted_date',
            'book_rejected_date' => 'book_rejected_date',
            'book_paid_date' => 'book_paid_date',
            'book_cancel_date' => 'book_cancel_date',
            'book_token' => 'book_token',
            'guide_end_tour' => 'guide_end_tour',
            'tourist_end_tour' => 'tourist_end_tour',
            'book_hide_for_guide' => 'book_hide_for_guide',
            'book_hide_for_tourist' => 'book_hide_for_tourist',
            'day_min24_mail' => 'day_min24_mail',
            'day_start_mail' => 'day_start_mail',
            'day_end_mail' => 'day_end_mail',
            'transid' => 'transid',
            'authcode' => 'authcode',
            'signature' => 'signature'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>