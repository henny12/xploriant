<?php
class ItineraryModel extends Generic_dao {

    public function table_name() {
        return Tables::$itinerary;
    }

    public function field_map() {
        return array(
            'itin_id' => 'itin_id',
            'itin_day' => 'itin_day',
            'itin_location' => 'itin_location',
            'itin_duration' => 'itin_duration',
            'itin_places' => 'itin_places',
            'itin_categories' => 'itin_categories',
            'itin_title' => 'itin_title',
            'itin_overview' => 'itin_overview',
            'itin_min_tourist' => 'itin_min_tourist',
            'itin_max_tourist' => 'itin_max_tourist',
            'itin_what_to_bring' => 'itin_what_to_bring',
            'itin_included_meals' => 'itin_included_meals',
            'itin_accom_included' => 'itin_accom_included',
            'itin_accom_min_rate' => 'itin_accom_min_rate',
            'itin_included_fees' => 'itin_included_fees',
            'itin_not_recom_opt' => 'itin_not_recom_opt',
            'itin_cancellation' => 'itin_cancellation',
            'itin_price_currency' => 'itin_price_currency',
            'itin_price_nominal' => 'itin_price_nominal',
            'itin_avg_ratings' => 'itin_avg_ratings',
            'itin_total_bookings' => 'itin_total_bookings',
            'itin_status' => 'itin_status',
            'itin_creator_id' => 'itin_creator_id',
            'itin_accom_night' => 'itin_accom_night',
            'itin_accom_desc' => 'itin_accom_desc',
            'itin_thumb' => 'itin_thumb',
            'itin_last_booked' => 'itin_last_booked',
            'comment_suspend' => 'comment_suspend',
            'updated_on' => 'updated_on',
            'updated_by' => 'updated_by',
            'created_on' => 'created_on',
            'is_deleted' => 'is_deleted'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>