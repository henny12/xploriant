<?php
class PersonalInterestModel extends Generic_dao {

    public function table_name() {
        return Tables::$personal_interest;
    }

    public function field_map() {
        return array(
            'pe_id' => 'pe_id',
            'pe_name' => 'pe_name',
            'pe_desc' => 'pe_desc'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>