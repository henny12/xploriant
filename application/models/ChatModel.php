<?php
class ChatModel extends Generic_dao {

    public function table_name() {
        return Tables::$chat;
    }

    public function field_map() {
        return array(
            'id' => 'id',
            'sender_id' => 'sender_id',
            'receiver_id' => 'receiver_id',
            'message' => 'message',
            'attachment_name' => 'attachment_name',
            'file_ext' => 'file_ext',
            'mime_type' => 'mime_type',
            'message_date_time' => 'message_date_time',
            'ip_address' => 'ip_address'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    public function get_last($sender_id, $receiver_id) {
        $sql = "select * from chat where (sender_id = '$sender_id' and receiver_id = '$receiver_id') or (sender_id = '$receiver_id' and receiver_id = '$sender_id') order by id desc limit 1";
        $query = $this->ci->db->query($sql);
        return $query->row();
    }

}

?>