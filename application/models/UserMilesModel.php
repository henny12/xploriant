<?php
class UserMilesModel extends Generic_dao {

    public function table_name() {
        return Tables::$user_miles;
    }

    public function field_map() {
        return array(
            'user_id' => 'user_id',
            'x_pts_now' => 'x_pts_now',
            'x_pts_last' => 'x_pts_last',
            'x_usd_now' => 'x_usd_now',
            'x_usd_last' => 'x_usd_last',
            'x_idr_now' => 'x_idr_now',
            'x_idr_last' => 'x_idr_last',
            'updated_on' => 'updated_on'
        );
    }

    public function __construct() {
        parent::__construct();
    }

}

?>