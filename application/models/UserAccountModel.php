<?php
class UserAccountModel extends Generic_dao {

    public function table_name() {
        return Tables::$user_account;
    }

    public function field_map() {
        return array(
            'user_id' => 'user_id',
            'user_email' => 'user_email',
            'user_password' => 'user_password',
            'user_status' => 'user_status',
            'last_login' => 'last_login',
            'code' => 'code',
            'comment_suspend' => 'comment_suspend',
            'ip_address' => 'ip_address',
            'created_by' => 'created_by',
            'created_on' => 'created_on',
            'updated_by' => 'updated_by',
            'updated_on' => 'updated_on'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    public function check_login($email, $password) {
        $password = md5($password);
        $query = $this->ci->db->query("select * from ".$this->table_name()." where user_email='$email' and user_password='$password'");
        return ($query->num_rows() == 1);
    }

    public function Authentication_Check($email){  
        $query = $this->ci->db->query("select user_id from ".$this->table_name()." where user_email='$email' limit 1");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function Read_User_Information($id) {
        $query = $this->ci->db->query("select * from user_data d, user_account a where d.user_id = a.user_id and d.user_id = '$id'");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return false;
        }
    }
}

?>