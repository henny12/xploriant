<?php
class AdminModel extends Generic_dao {

    public function table_name() {
        return Tables::$admin;
    }

    public function field_map() {
        return array(
            'admin_id' => 'admin_id',
            'admin_user' => 'admin_user',
            'admin_pass' => 'admin_pass',
            'admin_status' => 'admin_status',
            'admin_name' => 'admin_name',
            'last_login' => 'last_login'
        );
    }

    public function __construct() {
        parent::__construct();
    }

    public function check_login($username, $password) {
        $password = md5($password);
        $query = $this->ci->db->query("select * from ".$this->table_name()." where admin_user='$username' and admin_pass='$password'");
        return ($query->num_rows() == 1);
    }
}

?>