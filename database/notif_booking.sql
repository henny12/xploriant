/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : xploriant

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 20/08/2019 09:42:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for notif_booking
-- ----------------------------
DROP TABLE IF EXISTS `notif_booking`;
CREATE TABLE `notif_booking`  (
  `notif_id` int(11) NOT NULL AUTO_INCREMENT,
  `notif_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `book_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `book_type` smallint(11) NULL DEFAULT NULL COMMENT '1 = my bookings, 2 = tourist booking',
  `read_status` smallint(11) NULL DEFAULT NULL COMMENT '0 = unread, 1 = read',
  `created_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`notif_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
