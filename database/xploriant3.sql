/*
 Navicat Premium Data Transfer

 Source Server         : postgre-10
 Source Server Type    : PostgreSQL
 Source Server Version : 100006
 Source Host           : localhost:5432
 Source Catalog        : xploriant
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100006
 File Encoding         : 65001

 Date: 09/05/2019 12:22:13
*/


-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS "public"."booking";
CREATE TABLE "public"."booking" (
  "book_id" int4 NOT NULL,
  "itin_id" int4,
  "guest_id" int4,
  "book_date" date,
  "start_date" date,
  "total_tourist" int4,
  "total_price_currency" varchar(255) COLLATE "pg_catalog"."default",
  "total_price_nominal" int4,
  "book_request" text COLLATE "pg_catalog"."default",
  "book_status" int2,
  "book_paid" int4
)
;
COMMENT ON COLUMN "public"."booking"."book_request" IS 'description';
COMMENT ON COLUMN "public"."booking"."book_status" IS '1=pending, 2=accepted, 3=rejected, 4=paid, 5=finished';

-- ----------------------------
-- Table structure for calender
-- ----------------------------
DROP TABLE IF EXISTS "public"."calender";
CREATE TABLE "public"."calender" (
  "cal_id" int4 NOT NULL DEFAULT nextval('calender_cal_id_seq'::regclass),
  "user_id" int4,
  "cal_date" date,
  "itin_id" int4,
  "cal_desc" text COLLATE "pg_catalog"."default",
  "cal_status" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for cancellation
-- ----------------------------
DROP TABLE IF EXISTS "public"."cancellation";
CREATE TABLE "public"."cancellation" (
  "cancel_id" int4 NOT NULL DEFAULT nextval('cancel_cancel_id_seq'::regclass),
  "booking_id" int4,
  "cancel_date" date,
  "refund_currency" varchar(255) COLLATE "pg_catalog"."default",
  "refund_nominal" varchar(255) COLLATE "pg_catalog"."default",
  "cancel_status" int4
)
;

-- ----------------------------
-- Table structure for category_tour
-- ----------------------------
DROP TABLE IF EXISTS "public"."category_tour";
CREATE TABLE "public"."category_tour" (
  "cat_id" int4 NOT NULL DEFAULT nextval('category_tour_cat_id_seq'::regclass),
  "cat_name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of category_tour
-- ----------------------------
INSERT INTO "public"."category_tour" VALUES (1, 'photography');
INSERT INTO "public"."category_tour" VALUES (2, 'adventure');
INSERT INTO "public"."category_tour" VALUES (3, 'hiking and trekking');
INSERT INTO "public"."category_tour" VALUES (4, 'food (culinary tours)');
INSERT INTO "public"."category_tour" VALUES (5, 'sightseeing');
INSERT INTO "public"."category_tour" VALUES (6, 'arts and culture (cultu');
INSERT INTO "public"."category_tour" VALUES (7, 'history (historic tours)');
INSERT INTO "public"."category_tour" VALUES (8, 'shopping');
INSERT INTO "public"."category_tour" VALUES (9, 'theme parks');
INSERT INTO "public"."category_tour" VALUES (10, 'diving and snorkeling');
INSERT INTO "public"."category_tour" VALUES (11, 'island tours');
INSERT INTO "public"."category_tour" VALUES (12, 'sports');
INSERT INTO "public"."category_tour" VALUES (13, 'performances');
INSERT INTO "public"."category_tour" VALUES (14, 'spirituality (spiritual tours)');
INSERT INTO "public"."category_tour" VALUES (15, 'nightlife');
INSERT INTO "public"."category_tour" VALUES (16, 'workshops');

-- ----------------------------
-- Table structure for included_fees
-- ----------------------------
DROP TABLE IF EXISTS "public"."included_fees";
CREATE TABLE "public"."included_fees" (
  "ifee_id" int4 NOT NULL DEFAULT nextval('included_fees_ifee_id_seq'::regclass),
  "ifee_title" varchar(255) COLLATE "pg_catalog"."default",
  "ifee_desc" text COLLATE "pg_catalog"."default",
  "ifee_status" int2
)
;

-- ----------------------------
-- Records of included_fees
-- ----------------------------
INSERT INTO "public"."included_fees" VALUES (1, 'Entrance Tickets', NULL, NULL);
INSERT INTO "public"."included_fees" VALUES (2, 'Transportation', NULL, NULL);
INSERT INTO "public"."included_fees" VALUES (3, 'Insurance', NULL, NULL);

-- ----------------------------
-- Table structure for included_meals
-- ----------------------------
DROP TABLE IF EXISTS "public"."included_meals";
CREATE TABLE "public"."included_meals" (
  "imeal_id" int2 NOT NULL DEFAULT nextval('included_meals_imeal_id_seq'::regclass),
  "imeal_title" varchar(255) COLLATE "pg_catalog"."default",
  "imeal_status" int2
)
;

-- ----------------------------
-- Records of included_meals
-- ----------------------------
INSERT INTO "public"."included_meals" VALUES (1, 'breakfast', NULL);
INSERT INTO "public"."included_meals" VALUES (2, 'lunch', NULL);
INSERT INTO "public"."included_meals" VALUES (3, 'dinner', NULL);

-- ----------------------------
-- Table structure for itinerary
-- ----------------------------
DROP TABLE IF EXISTS "public"."itinerary";
CREATE TABLE "public"."itinerary" (
  "itin_id" int4 NOT NULL DEFAULT nextval('itinerary_itin_id_seq'::regclass),
  "itin_day" varchar(10) COLLATE "pg_catalog"."default",
  "itin_location" varchar(255) COLLATE "pg_catalog"."default",
  "itin_duration" int4,
  "itin_places" text COLLATE "pg_catalog"."default",
  "itin_activities" text COLLATE "pg_catalog"."default",
  "itin_categories" text COLLATE "pg_catalog"."default",
  "itin_title" varchar(255) COLLATE "pg_catalog"."default",
  "itin_overview" text COLLATE "pg_catalog"."default",
  "itin_min_tourist" int4,
  "itin_max_tourist" int4,
  "itin_what_to_bring" text COLLATE "pg_catalog"."default",
  "itin_included_meals" text COLLATE "pg_catalog"."default",
  "itin_accom_included" bool,
  "itin_accom_min_rate" int2,
  "itin_included_fees" varchar(255) COLLATE "pg_catalog"."default",
  "itin_not_recom_opt" varchar(255) COLLATE "pg_catalog"."default",
  "itin_cancellation" int2,
  "itin_price_currency" varchar(10) COLLATE "pg_catalog"."default",
  "itin_price_nominal" varchar(255) COLLATE "pg_catalog"."default",
  "itin_avg_ratings" float4,
  "itin_total_bookings" varchar(255) COLLATE "pg_catalog"."default",
  "itin_status" int2,
  "updated_on" timestamp(6),
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "itin_creator_id" int4,
  "itin_accom_night" int4,
  "itin_accom_desc" text COLLATE "pg_catalog"."default",
  "itin_thumb" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."itinerary"."itin_day" IS 'single day (sd) dan multi day (md)';
COMMENT ON COLUMN "public"."itinerary"."itin_duration" IS 'with hours';
COMMENT ON COLUMN "public"."itinerary"."itin_places" IS 'dipisah menggunakan -';
COMMENT ON COLUMN "public"."itinerary"."itin_activities" IS 'dipisah menggunakan -';
COMMENT ON COLUMN "public"."itinerary"."itin_included_meals" IS 'dipisah menggunakan -';
COMMENT ON COLUMN "public"."itinerary"."itin_cancellation" IS 'with day';
COMMENT ON COLUMN "public"."itinerary"."itin_creator_id" IS 'user_id';

-- ----------------------------
-- Records of itinerary
-- ----------------------------
INSERT INTO "public"."itinerary" VALUES (5, 'md', 'bandung, indonesia', NULL, 'Dago-Braga-Gedung Sate-Lembang', 'Lorem Ipsum 1-Lorem Ipsum 2-Lorem Ipsum 3', '1-4-6-7', 'Liburan 3D4N Di Bandung', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 2, 4, 'Baju-Celana-Payung', 'breakfast-lunch-lunch', 'f', 3, 'ticket-transport', '1-2', 4, 'idr', '2000000', NULL, NULL, NULL, NULL, NULL, 1, 4, 'Lorem Ipsum A', '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO "public"."itinerary" VALUES (14, 'md', 'bali, indonesia', NULL, 'qqq-www-eee', 'aaa-bbb-ccc', '1-5-9', 'liburan 3d4n di bali', 'dsfsdfsdf dsffdfsdfsdf', 1, 5, 'ddd-fff-ggg', 'breakfast-lunch', 't', NULL, 'ticket-transport', '1-2', 5, 'idr', '30000000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'deaedb5efc5f21387561af3326f3f394.png');
INSERT INTO "public"."itinerary" VALUES (8, 'md', 'bali, indonesia', NULL, 'aaa-bbb-ccc', 'qqq-www-eee', '1-5-11', 'Ubud Rice Terrace', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'ccc-ddd-fff', 'breakfast-lunch', 'f', 4, 'ticket-transport', '1-3', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 1, 2, 'Lorem Ipsum A', '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO "public"."itinerary" VALUES (4, 'sd', 'bandung, indonesia', 8, 'Dago-Lembang-Punclut', 'Lorem Ipsum-Lorem Ipsum 2-Lorem Ipsum 3', '1-2-3', 'Liburan seharian di Bandung', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 2, 5, 'Baju-Celana', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-4-5', 5, 'idr', '800000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO "public"."itinerary" VALUES (13, 'sd', 'bali, indonesia', 0, NULL, NULL, NULL, 'sdfsdf', 'ghfghfghgfh', 0, 0, NULL, NULL, 'f', NULL, NULL, NULL, 0, 'idr', '199997', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO "public"."itinerary" VALUES (10, 'md', 'bali, indonesia', NULL, 'bbb-nnn-mmm', 'fff-ggg-hhh', '1-2-3-4-6-8-9-10', 'Everything Bali (Complete Bali Tour)', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'qqq-www-eee', 'lunch-lunch', 'f', 4, 'transport-insurance', '3-4-5', 5, 'idr', '12000000', NULL, NULL, NULL, NULL, NULL, 1, 7, 'Lorem Ipsum M', '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO "public"."itinerary" VALUES (9, 'md', 'bali, indonesia', NULL, 'zzz-xxx-ccc', 'aaa-sss-ddd', '1-2-6', 'Tamblingan - Munduk Adventure', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'qqq-www-eee', 'breakfast-lunch-lunch', 'f', 2, 'ticket-transport', '1-2', 5, 'idr', '1200000', NULL, NULL, NULL, NULL, NULL, 1, 3, 'Lorem Ipsum V', NULL);
INSERT INTO "public"."itinerary" VALUES (7, 'sd', 'bali, indonesia', 2, 'place a-place b-place c', 'swimming-snorkling', '1-2-9', 'Dewata Beach Adventure', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'aaa-bbb-ccc', 'breakfast-lunch', 'f', NULL, 'ticket-transport', '1-4', 5, 'idr', '200000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO "public"."itinerary" VALUES (11, 'sd', 'bali, indonesia', 1, 'sss-aaa-qqq', 'eee-rrr-ttt', '5-6-7', 'Sightseeing to Garuda Wisnu Kencana', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'yyy-uuu-iii', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-2', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO "public"."itinerary" VALUES (16, 'sd', 'bandung, indonesia', 4, 'huiuhi', 'uiui', '1-5-9', 'xgdfgdfg', 'dfLorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 3, 'huiuhi', 'breakfast-lunch', NULL, NULL, 'ticket-transport', '2-4', 3, 'idr', '199997', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '7c174a067b4416f697b622f9599d6a0d.png');
INSERT INTO "public"."itinerary" VALUES (6, 'sd', 'batam, indonesia', 8, 'Place 1-Place 2-Place 3', 'Shopping-Swimming', '1-8-15', 'Liburan Sehari Di Batam', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'aaa-bbb-ccc', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-3', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO "public"."itinerary" VALUES (12, 'md', 'bali, indonesia', NULL, 'mmm-nnn-bbb', 'qqq-www-eee', '1-2-10-11', 'Kuta - Sanur - Tanjung Benoa Experience', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'ttt-yyy-uuu', 'breakfast-lunch', 'f', 3, 'transport-insurance', '1-2-3', 5, 'idr', '4000000', NULL, NULL, NULL, NULL, NULL, 1, 5, 'Lorem Ipsum M', '72ec2b682bfb99935a5b85008c4516eb.png');

-- ----------------------------
-- Table structure for itinerary_image
-- ----------------------------
DROP TABLE IF EXISTS "public"."itinerary_image";
CREATE TABLE "public"."itinerary_image" (
  "itin_img_id" int4 NOT NULL DEFAULT nextval('itinerary_image_itin_img_id_seq'::regclass),
  "itin_img_name" varchar(255) COLLATE "pg_catalog"."default",
  "itin_id" int4,
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "updated_on" timestamp(6)
)
;

-- ----------------------------
-- Records of itinerary_image
-- ----------------------------
INSERT INTO "public"."itinerary_image" VALUES (13, 'd107cf33a106b5ad2729f6873c1cb9e9.jpg', 4, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (14, '54de197dc0fc6d7c5bdb7a42735f7065.jpg', 4, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (15, 'ca0f96b550a696275fa8ca1172d26e57.jpg', 4, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (16, '93157ad2476a5c5f3eb73f1672b9e9ff.jpg', 6, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (17, 'd8e109881fd4aa2547550de4c9446faa.jpg', 6, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (18, '7bc89e4b2a35afb3ddf8a76c3819ec6a.jpg', 6, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (19, '4e73ddf3674ee443e34145db232f0dd7.jpg', 6, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (20, '622224797c825493b8b80077461a93da.jpg', 7, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (21, '4ecf919e3fb6732f4d90b69421bb063f.jpg', 7, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (22, '9ca67083445aa521dd7bac0ef5b9727c.jpg', 7, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (23, '8a13a0cc07f69c307a9f71a6d300d915.jpg', 8, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (24, '856e46f1598b958590554d20c2f5057b.jpg', 8, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (25, '3f014c1a5e2db20bfe3a9310c6fd13c9.jpg', 8, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (26, '04fa65ada2778ddfc916a8eb37345542.jpg', 12, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (27, '7ea654b6d8096e0a35aa12670ff499c7.jpg', 12, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (28, 'b2abfc213da21516bd3ed6d93478d0af.jpg', 12, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (29, '607604a783c7362ea12efb367a5b9ae2.jpg', 14, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (30, 'f69bea43adaf6f7a6aeb8ecae58652f7.jpg', 14, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (31, '0c235ed851592054aa1ce2ad4f949e0e.jpg', 14, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (32, '29d157054045c2528346000471ded15b.jpg', 14, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (33, '287b2399e273b02627d7d890febb8c5e.jpg', 14, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (34, '7507539d918b0adf913b4bfc0747c5df.jpg', 14, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (35, '7bc85892d5e42acdebe0dadbd762dd42.jpg', 16, NULL, NULL);
INSERT INTO "public"."itinerary_image" VALUES (36, '6c3ba1a78a8450c99138cb92fc548495.jpg', 16, NULL, NULL);

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS "public"."language";
CREATE TABLE "public"."language" (
  "lang_id" int4 NOT NULL DEFAULT nextval('language_lang_id_seq'::regclass),
  "lang_name" varchar(100) COLLATE "pg_catalog"."default",
  "lang_code" varchar(10) COLLATE "pg_catalog"."default",
  "lang_status" int2
)
;

-- ----------------------------
-- Table structure for not_recommended
-- ----------------------------
DROP TABLE IF EXISTS "public"."not_recommended";
CREATE TABLE "public"."not_recommended" (
  "nr_id" int4 NOT NULL DEFAULT nextval('not_recommended_nr_id_seq'::regclass),
  "nr_title" varchar(255) COLLATE "pg_catalog"."default",
  "nr_desc" text COLLATE "pg_catalog"."default",
  "nr_status" int2
)
;

-- ----------------------------
-- Records of not_recommended
-- ----------------------------
INSERT INTO "public"."not_recommended" VALUES (4, 'w/ Cardiac Problems', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (5, 'w/ Back Problems', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (6, 'w/ Children', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (1, '< 17 Years Old', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (2, '< 21 Years Old', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (3, '> 60 Years Old', NULL, NULL);

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS "public"."payment";
CREATE TABLE "public"."payment" (
  "payment_id" int4 NOT NULL DEFAULT nextval('payment2_payment_id_seq'::regclass),
  "booking_id" int4,
  "payment_method" varchar(255) COLLATE "pg_catalog"."default",
  "total_paid" int4,
  "payment_date" date
)
;

-- ----------------------------
-- Table structure for personal_interest
-- ----------------------------
DROP TABLE IF EXISTS "public"."personal_interest";
CREATE TABLE "public"."personal_interest" (
  "pe_id" int4 NOT NULL DEFAULT nextval('personal_interest_pe_id_seq'::regclass),
  "pe_name" varchar(255) COLLATE "pg_catalog"."default",
  "pe_desc" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of personal_interest
-- ----------------------------
INSERT INTO "public"."personal_interest" VALUES (1, 'photography', NULL);
INSERT INTO "public"."personal_interest" VALUES (2, 'adventure', NULL);
INSERT INTO "public"."personal_interest" VALUES (3, 'hiking and trekking', NULL);
INSERT INTO "public"."personal_interest" VALUES (4, 'food', NULL);
INSERT INTO "public"."personal_interest" VALUES (5, 'sightseeing', NULL);
INSERT INTO "public"."personal_interest" VALUES (6, 'arts and culture', NULL);
INSERT INTO "public"."personal_interest" VALUES (7, 'history', NULL);
INSERT INTO "public"."personal_interest" VALUES (8, 'shopping', NULL);
INSERT INTO "public"."personal_interest" VALUES (9, 'theme parks', NULL);
INSERT INTO "public"."personal_interest" VALUES (10, 'diving and snorkeling', NULL);
INSERT INTO "public"."personal_interest" VALUES (11, 'island tours', NULL);
INSERT INTO "public"."personal_interest" VALUES (12, 'sports', NULL);
INSERT INTO "public"."personal_interest" VALUES (13, 'performances', NULL);
INSERT INTO "public"."personal_interest" VALUES (14, 'spirituality', NULL);
INSERT INTO "public"."personal_interest" VALUES (15, 'nightlife', NULL);
INSERT INTO "public"."personal_interest" VALUES (16, 'workshops', NULL);

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS "public"."review";
CREATE TABLE "public"."review" (
  "review_id" int4 NOT NULL DEFAULT nextval('review_review_id_seq'::regclass),
  "booking_id" int4,
  "review_as" varchar(255) COLLATE "pg_catalog"."default",
  "rating" varchar(255) COLLATE "pg_catalog"."default",
  "review" varchar(255) COLLATE "pg_catalog"."default",
  "user_id" int4,
  "review_date" date,
  "review_status" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."review"."review_as" IS 'guide / tourist';

-- ----------------------------
-- Table structure for temp_image
-- ----------------------------
DROP TABLE IF EXISTS "public"."temp_image";
CREATE TABLE "public"."temp_image" (
  "temp_id" int4 NOT NULL DEFAULT nextval('temp_image_temp_id_seq'::regclass),
  "temp_img_name" text COLLATE "pg_catalog"."default",
  "temp_img_token" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_account";
CREATE TABLE "public"."user_account" (
  "user_id" int4 NOT NULL DEFAULT nextval('user_account_user_id_seq'::regclass),
  "user_email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "user_password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "user_status" int2,
  "created_by" varchar(100) COLLATE "pg_catalog"."default",
  "created_on" timestamp(6),
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "updated_on" timestamp(6)
)
;

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO "public"."user_account" VALUES (1, 'henny.alfianti@gmail.com', '115c660420fa693c0f833cb36d8f3deb', 1, 'member', '2019-04-15 08:01:22', NULL, NULL);

-- ----------------------------
-- Table structure for user_data
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_data";
CREATE TABLE "public"."user_data" (
  "user_id" int4 NOT NULL,
  "user_fname" varchar(200) COLLATE "pg_catalog"."default",
  "user_lname" varchar(200) COLLATE "pg_catalog"."default",
  "user_gender" varchar(10) COLLATE "pg_catalog"."default",
  "user_nationality" varchar(255) COLLATE "pg_catalog"."default",
  "user_residence" varchar(255) COLLATE "pg_catalog"."default",
  "user_dob" varchar(255) COLLATE "pg_catalog"."default",
  "user_personal_id" text COLLATE "pg_catalog"."default",
  "user_interests" varchar(255) COLLATE "pg_catalog"."default",
  "user_lang" varchar(255) COLLATE "pg_catalog"."default",
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "updated_on" timestamp(6),
  "user_desc" text COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."user_data"."user_gender" IS 'M = male, F = female';
COMMENT ON COLUMN "public"."user_data"."user_personal_id" IS 'nomor ktp';

-- ----------------------------
-- Records of user_data
-- ----------------------------
INSERT INTO "public"."user_data" VALUES (1, 'Henny', 'Alfianti', 'F', 'Indonesian', 'Bandung, Indonesia', '1993-08-27', '7364835763485734', NULL, 'Indonesian, English, Mandarin', NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lobortis sed sem a sodales. In hac habitasse platea dictumst. In hac habitasse platea dictumst. Fusce eu est metus. Integer vulputate nulla justo, sed rhoncus augue interdum a. Proin a odio sit amet sem finibus eleifend eu vel tortor. Sed eget venenatis dolor. Pellentesque et placerat purus.');

-- ----------------------------
-- Table structure for user_miles
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_miles";
CREATE TABLE "public"."user_miles" (
  "user_id" int4 NOT NULL,
  "x_miles_now" int8,
  "x_miles_last" int8,
  "updated_on" timestamp(6)
)
;

-- ----------------------------
-- Primary Key structure for table booking
-- ----------------------------
ALTER TABLE "public"."booking" ADD CONSTRAINT "booking_pkey" PRIMARY KEY ("book_id");

-- ----------------------------
-- Primary Key structure for table calender
-- ----------------------------
ALTER TABLE "public"."calender" ADD CONSTRAINT "calender_pkey" PRIMARY KEY ("cal_id");

-- ----------------------------
-- Primary Key structure for table cancellation
-- ----------------------------
ALTER TABLE "public"."cancellation" ADD CONSTRAINT "cancel_pkey" PRIMARY KEY ("cancel_id");

-- ----------------------------
-- Primary Key structure for table category_tour
-- ----------------------------
ALTER TABLE "public"."category_tour" ADD CONSTRAINT "category_tour_pkey" PRIMARY KEY ("cat_id");

-- ----------------------------
-- Primary Key structure for table included_fees
-- ----------------------------
ALTER TABLE "public"."included_fees" ADD CONSTRAINT "included_fees_pkey" PRIMARY KEY ("ifee_id");

-- ----------------------------
-- Primary Key structure for table included_meals
-- ----------------------------
ALTER TABLE "public"."included_meals" ADD CONSTRAINT "included_meals_pkey" PRIMARY KEY ("imeal_id");

-- ----------------------------
-- Primary Key structure for table itinerary
-- ----------------------------
ALTER TABLE "public"."itinerary" ADD CONSTRAINT "itinerary_pkey" PRIMARY KEY ("itin_id");

-- ----------------------------
-- Primary Key structure for table itinerary_image
-- ----------------------------
ALTER TABLE "public"."itinerary_image" ADD CONSTRAINT "itinerary_image_pkey" PRIMARY KEY ("itin_img_id");

-- ----------------------------
-- Primary Key structure for table language
-- ----------------------------
ALTER TABLE "public"."language" ADD CONSTRAINT "language_pkey" PRIMARY KEY ("lang_id");

-- ----------------------------
-- Primary Key structure for table not_recommended
-- ----------------------------
ALTER TABLE "public"."not_recommended" ADD CONSTRAINT "not_recommended_pkey" PRIMARY KEY ("nr_id");

-- ----------------------------
-- Primary Key structure for table payment
-- ----------------------------
ALTER TABLE "public"."payment" ADD CONSTRAINT "payment2_pkey" PRIMARY KEY ("payment_id");

-- ----------------------------
-- Primary Key structure for table personal_interest
-- ----------------------------
ALTER TABLE "public"."personal_interest" ADD CONSTRAINT "personal_interest_pkey" PRIMARY KEY ("pe_id");

-- ----------------------------
-- Primary Key structure for table review
-- ----------------------------
ALTER TABLE "public"."review" ADD CONSTRAINT "review_pkey" PRIMARY KEY ("review_id");

-- ----------------------------
-- Primary Key structure for table temp_image
-- ----------------------------
ALTER TABLE "public"."temp_image" ADD CONSTRAINT "temp_image_pkey" PRIMARY KEY ("temp_id");

-- ----------------------------
-- Primary Key structure for table user_account
-- ----------------------------
ALTER TABLE "public"."user_account" ADD CONSTRAINT "user_account_pkey" PRIMARY KEY ("user_id", "user_email");

-- ----------------------------
-- Primary Key structure for table user_data
-- ----------------------------
ALTER TABLE "public"."user_data" ADD CONSTRAINT "user_data_pkey" PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table user_miles
-- ----------------------------
ALTER TABLE "public"."user_miles" ADD CONSTRAINT "user_miles_pkey" PRIMARY KEY ("user_id");
