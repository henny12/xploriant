/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : xploriant

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 26/07/2019 16:27:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `admin_pass` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `admin_status` smallint(255) NULL DEFAULT NULL,
  `admin_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_login` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`admin_id`, `admin_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'Henny Alfianti', NULL);

-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking`  (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `itin_id` int(11) NULL DEFAULT NULL,
  `guide_id` int(11) NULL DEFAULT NULL,
  `book_date` date NULL DEFAULT NULL,
  `start_date` date NULL DEFAULT NULL,
  `total_tourist` int(11) NULL DEFAULT NULL,
  `total_price_currency` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_price_nominal` int(11) NULL DEFAULT NULL,
  `book_request` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'description',
  `book_status` smallint(6) NULL DEFAULT NULL COMMENT '1=pending, 2=accepted, 3=rejected, 4=paid, 5=finished',
  `book_paid` int(11) NULL DEFAULT NULL,
  `tourist_id` int(11) NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  `updated_on` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`book_id`, `book_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of booking
-- ----------------------------
INSERT INTO `booking` VALUES (5, '190618103702', 6, 12, '2019-06-18', '2019-06-18', 2, 'idr', 16000000, 'test abc', 1, NULL, 1, '2019-06-18', '2019-07-18 13:42:03');
INSERT INTO `booking` VALUES (6, '190618103924', 8, 1, '2019-06-18', '2019-06-19', 2, 'idr', 16000000, 'rsyrf fhfdgh dfgdsert', 1, NULL, 12, '2019-06-22', '2019-07-17 13:42:07');
INSERT INTO `booking` VALUES (7, '190618104016', 12, 1, '2019-06-18', '2019-06-20', 2, 'idr', 8000000, 'jhgf kjfdgdfg dfjkghdfjg dfg', 1, NULL, 12, '2019-06-23', '2019-07-15 13:42:11');

-- ----------------------------
-- Table structure for calender
-- ----------------------------
DROP TABLE IF EXISTS `calender`;
CREATE TABLE `calender`  (
  `cal_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `cal_date` date NULL DEFAULT NULL,
  `itin_id` int(11) NULL DEFAULT NULL,
  `cal_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cal_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cal_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cancellation
-- ----------------------------
DROP TABLE IF EXISTS `cancellation`;
CREATE TABLE `cancellation`  (
  `cancel_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NULL DEFAULT NULL,
  `cancel_date` date NULL DEFAULT NULL,
  `refund_currency` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `refund_nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cancel_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cancel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for category_tour
-- ----------------------------
DROP TABLE IF EXISTS `category_tour`;
CREATE TABLE `category_tour`  (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category_tour
-- ----------------------------
INSERT INTO `category_tour` VALUES (1, 'photography');
INSERT INTO `category_tour` VALUES (2, 'adventure');
INSERT INTO `category_tour` VALUES (3, 'hiking and trekking');
INSERT INTO `category_tour` VALUES (4, 'food (culinary tours)');
INSERT INTO `category_tour` VALUES (5, 'sightseeing');
INSERT INTO `category_tour` VALUES (6, 'arts and culture (cultu');
INSERT INTO `category_tour` VALUES (7, 'history (historic tours)');
INSERT INTO `category_tour` VALUES (8, 'shopping');
INSERT INTO `category_tour` VALUES (9, 'theme parks');
INSERT INTO `category_tour` VALUES (10, 'diving and snorkeling');
INSERT INTO `category_tour` VALUES (11, 'island tours');
INSERT INTO `category_tour` VALUES (12, 'sports');
INSERT INTO `category_tour` VALUES (13, 'performances');
INSERT INTO `category_tour` VALUES (14, 'spirituality (spiritual tours)');
INSERT INTO `category_tour` VALUES (15, 'nightlife');
INSERT INTO `category_tour` VALUES (16, 'workshops');

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `attachment_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_ext` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mime_type` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `message_date_time` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES (30, 1, 12, 'hi jon please send my project file report', '', '', '', '2018-06-13 17:28:40', '::1');
INSERT INTO `chat` VALUES (31, 12, 1, 'ok', '', '', '', '2018-06-13 17:28:45', '::1');
INSERT INTO `chat` VALUES (32, 1, 12, 'wait..', '', '', '', '2018-06-13 17:28:47', '::1');
INSERT INTO `chat` VALUES (33, 12, 1, 'NULL', 'Proejct_report_presenation.pptx', '.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '2018-06-13 17:29:11', '::1');
INSERT INTO `chat` VALUES (34, 1, 11, 'ok thanks', '', '', '', '2018-06-13 17:30:21', '::1');
INSERT INTO `chat` VALUES (35, 1, 11, 'open the file', '', '', '', '2018-06-13 17:30:34', '::1');
INSERT INTO `chat` VALUES (36, 12, 11, 'please send images', '', '', '', '2018-06-13 17:31:22', '::1');
INSERT INTO `chat` VALUES (37, 11, 1, 'ok', '', '', '', '2018-06-13 17:31:27', '::1');
INSERT INTO `chat` VALUES (38, 12, 1, 'wait bro..', '', '', '', '2018-06-13 17:31:33', '::1');
INSERT INTO `chat` VALUES (39, 1, 12, 'NULL', '21_preview.png', '.png', 'image/png', '2018-06-13 17:31:43', '::1');
INSERT INTO `chat` VALUES (40, 12, 1, 'you got it', '', '', '', '2018-06-13 17:32:05', '::1');
INSERT INTO `chat` VALUES (41, 1, 11, 'yes', '', '', '', '2018-06-13 17:32:10', '::1');
INSERT INTO `chat` VALUES (42, 12, 11, 'thanks', '', '', '', '2018-06-13 17:32:16', '::1');
INSERT INTO `chat` VALUES (43, 1, 12, 'some pdf file send', '', '', '', '2018-06-13 17:32:33', '::1');
INSERT INTO `chat` VALUES (44, 12, 1, 'NULL', 'Invoice.pdf', '.pdf', 'application/pdf', '2018-06-13 17:33:03', '::1');
INSERT INTO `chat` VALUES (47, 11, 1, 'fdgdfg', '', '', '', '2018-11-26 08:43:17', '::1');
INSERT INTO `chat` VALUES (48, 12, 1, 'halo', '', '', '', '2018-11-26 08:44:20', '::1');
INSERT INTO `chat` VALUES (49, 1, 12, 'iya', '', '', '', '2018-11-26 08:44:32', '::1');
INSERT INTO `chat` VALUES (50, 11, 12, 'apa kabar', '', '', '', '2018-11-26 08:44:56', '::1');
INSERT INTO `chat` VALUES (51, 12, 11, 'baik', '', '', '', '2018-11-26 08:45:00', '::1');
INSERT INTO `chat` VALUES (52, 1, 12, 'anda sendiri ?', '', '', '', '2018-11-26 08:45:06', '::1');
INSERT INTO `chat` VALUES (53, 1, 12, 'tetst', '', '', '', '2019-07-03 08:06:43', '::1');
INSERT INTO `chat` VALUES (54, 1, 12, 'ttt', '', '', '', '2019-07-03 08:06:48', '::1');
INSERT INTO `chat` VALUES (55, 1, 11, 'ttt', '', '', '', '2019-07-03 09:42:04', '::1');
INSERT INTO `chat` VALUES (56, 1, 12, 'tetetete', '', '', '', '2019-07-03 09:48:19', '::1');
INSERT INTO `chat` VALUES (57, 1, 11, 'testttt', '', '', '', '2019-07-04 08:54:02', '::1');
INSERT INTO `chat` VALUES (58, 1, 11, 'test', '', '', '', '2019-07-05 01:56:56', '::1');
INSERT INTO `chat` VALUES (59, 1, 11, 'test', '', '', '', '2019-07-05 01:57:40', '::1');
INSERT INTO `chat` VALUES (60, 1, 12, 'sdfsf', '', '', '', '2019-07-05 03:58:33', '::1');
INSERT INTO `chat` VALUES (61, 1, 12, 'tet', '', '', '', '2019-07-05 03:59:10', '::1');
INSERT INTO `chat` VALUES (62, 1, 11, 'test', '', '', '', '2019-07-05 04:03:11', '::1');
INSERT INTO `chat` VALUES (63, 1, 12, 'test', '', '', '', '2019-07-05 04:05:12', '::1');
INSERT INTO `chat` VALUES (64, 1, 12, 'test', '', '', '', '2019-07-05 04:13:10', '::1');
INSERT INTO `chat` VALUES (65, 1, 11, 'err', '', '', '', '2019-07-05 04:27:09', '::1');
INSERT INTO `chat` VALUES (66, 1, 11, 'dfgdfg', '', '', '', '2019-07-05 04:50:16', '::1');
INSERT INTO `chat` VALUES (67, 1, 0, 'dsfdf', '', '', '', '2019-07-05 04:51:54', '::1');
INSERT INTO `chat` VALUES (68, 1, 12, 'fdff', '', '', '', '2019-07-05 04:52:01', '::1');
INSERT INTO `chat` VALUES (69, 1, 11, 'test', '', '', '', '2019-07-05 04:53:57', '::1');
INSERT INTO `chat` VALUES (70, 1, 12, 'testt', '', '', '', '2019-07-05 04:54:16', '::1');
INSERT INTO `chat` VALUES (71, 1, 11, 'test', '', '', '', '2019-07-05 05:02:17', '::1');
INSERT INTO `chat` VALUES (72, 1, 12, 'aff', '', '', '', '2019-07-05 08:13:07', '::1');
INSERT INTO `chat` VALUES (73, 1, 11, 'aaa', '', '', '', '2019-07-05 08:19:57', '::1');
INSERT INTO `chat` VALUES (74, 1, 11, 'rer', '', '', '', '2019-07-18 09:22:28', '::1');

-- ----------------------------
-- Table structure for connect_message
-- ----------------------------
DROP TABLE IF EXISTS `connect_message`;
CREATE TABLE `connect_message`  (
  `cm_id` int(11) NOT NULL AUTO_INCREMENT,
  `cm_user_id` int(11) NULL DEFAULT NULL,
  `cm_connected_to` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cm_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of connect_message
-- ----------------------------
INSERT INTO `connect_message` VALUES (1, 1, '12');
INSERT INTO `connect_message` VALUES (2, 1, '11');
INSERT INTO `connect_message` VALUES (3, 11, '1');
INSERT INTO `connect_message` VALUES (4, 11, '12');
INSERT INTO `connect_message` VALUES (5, 12, '1');
INSERT INTO `connect_message` VALUES (6, 12, '11');

-- ----------------------------
-- Table structure for included_fees
-- ----------------------------
DROP TABLE IF EXISTS `included_fees`;
CREATE TABLE `included_fees`  (
  `ifee_id` int(11) NOT NULL AUTO_INCREMENT,
  `ifee_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ifee_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ifee_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`ifee_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of included_fees
-- ----------------------------
INSERT INTO `included_fees` VALUES (1, 'Entrance Tickets', NULL, NULL);
INSERT INTO `included_fees` VALUES (2, 'Transportation', NULL, NULL);
INSERT INTO `included_fees` VALUES (3, 'Insurance', NULL, NULL);

-- ----------------------------
-- Table structure for included_meals
-- ----------------------------
DROP TABLE IF EXISTS `included_meals`;
CREATE TABLE `included_meals`  (
  `imeal_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `imeal_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `imeal_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`imeal_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of included_meals
-- ----------------------------
INSERT INTO `included_meals` VALUES (1, 'breakfast', NULL);
INSERT INTO `included_meals` VALUES (2, 'lunch', NULL);
INSERT INTO `included_meals` VALUES (3, 'dinner', NULL);

-- ----------------------------
-- Table structure for itinerary
-- ----------------------------
DROP TABLE IF EXISTS `itinerary`;
CREATE TABLE `itinerary`  (
  `itin_id` int(11) NOT NULL AUTO_INCREMENT,
  `itin_day` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'single day (sd) dan multi day (md)',
  `itin_location` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_duration` int(11) NULL DEFAULT NULL COMMENT 'with hours',
  `itin_places` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'dipisah menggunakan -',
  `itin_categories` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_overview` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_min_tourist` int(11) NULL DEFAULT NULL,
  `itin_max_tourist` int(11) NULL DEFAULT NULL,
  `itin_what_to_bring` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_included_meals` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'dipisah menggunakan -',
  `itin_accom_included` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_accom_min_rate` smallint(6) NULL DEFAULT NULL,
  `itin_included_fees` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_not_recom_opt` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_cancellation` smallint(6) NULL DEFAULT NULL COMMENT 'with day',
  `itin_price_currency` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_price_nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_avg_ratings` float NULL DEFAULT NULL,
  `itin_total_bookings` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_status` smallint(6) NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_creator_id` int(11) NULL DEFAULT NULL COMMENT 'user_id',
  `itin_accom_night` int(11) NULL DEFAULT NULL,
  `itin_accom_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_thumb` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_last_booked` timestamp(0) NULL DEFAULT NULL,
  `created_on` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`itin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of itinerary
-- ----------------------------
INSERT INTO `itinerary` VALUES (1, 'sd', 'bandung, indonesia', 8, 'test q-test w-test e', '1-6-10', 'test aaa', 'test aaa test aaa test aaa test aaa', 1, 3, 'eytr-ettr-qrewr', 'breakfast', NULL, NULL, 'transport-insurance', '1-4', 7, 'idr', '10000000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'f8128e9edff7621a896bab891f076bfd.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (4, 'sd', 'bandung, indonesia', 8, 'Dago-Lembang-Punclut', '1-2-3', 'Liburan seharian di Bandung', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 2, 5, 'Baju-Celana', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-4-5', 5, 'idr', '800000', NULL, NULL, NULL, NULL, NULL, 1, 3, NULL, 'c6d68bf68b422c51c1203c3e5bf951ad.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (5, 'md', 'bandung, indonesia', 4, 'Dago-Braga-Gedung Sate-Lembang', '1-4-6-7', 'Liburan 3D4N Di Bandung', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 2, 4, 'Baju-Celana-Payung', 'breakfast-lunch-lunch', 'f', 3, 'ticket-transport', '1-2', 4, 'idr', '2000000', NULL, NULL, NULL, NULL, NULL, 1, 4, 'Lorem Ipsum A', '8044155e94712a677b73ca7dd2bbd567.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (6, 'sd', 'batam, indonesia', 8, 'Place 1-Place 2-Place 3', '1-8-15', 'Liburan Sehari Di Batam', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'aaa-bbb-ccc', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-3', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 12, 2, NULL, 'e65bdd8e7ee97160ab4247296c563029.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (7, 'sd', 'bali, indonesia', 2, 'place a-place b-place c', '1-2-9', 'Dewata Beach Adventure', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'aaa-bbb-ccc', 'breakfast-lunch', 'f', NULL, 'ticket-transport', '1-4', 5, 'idr', '200000', NULL, NULL, NULL, NULL, NULL, 12, 3, NULL, '72ec2b682bfb99935a5b85008c4516eb.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (8, 'md', 'bali, indonesia', 3, 'aaa-bbb-ccc', '1-5-11', 'Ubud Rice Terrace', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'ccc-ddd-fff', 'breakfast-lunch', 'f', 4, 'ticket-transport', '1-3', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 1, 2, 'Lorem Ipsum A', 'be07bbd20d329f73f9a91f8e072906d4.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (9, 'md', 'bali, indonesia', 3, 'zzz-xxx-ccc', '1-2-6', 'Tamblingan - Munduk Adventure', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'qqq-www-eee', 'breakfast-lunch-lunch', 'f', 2, 'ticket-transport', '1-2', 5, 'idr', '1200000', NULL, NULL, NULL, NULL, NULL, 12, 3, 'Lorem Ipsum V', 'b0ed90b3d2585e47938b77c14454b4d7.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (10, 'md', 'bali, indonesia', 2, 'bbb-nnn-mmm', '1-2-3-4-6-8-9-10', 'Everything Bali (Complete Bali Tour)', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'qqq-www-eee', 'lunch-lunch', 'f', 4, 'transport-insurance', '3-4-5', 5, 'idr', '12000000', NULL, NULL, NULL, NULL, NULL, 1, 7, 'Lorem Ipsum M', '7c35b5f15d02e15d1f77ccd1879bc4f1.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (12, 'md', 'bali, indonesia', 3, 'mmm-nnn-bbb', '1-2-10-11', 'Kuta - Sanur - Tanjung Benoa Experience', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'ttt-yyy-uuu', 'breakfast-lunch', 'f', 3, 'transport-insurance', '1-2-3', 5, 'idr', '4000000', NULL, NULL, NULL, NULL, NULL, 1, 5, 'Lorem Ipsum M', '89e8204991b777b884e37083b8326a50.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (14, 'md', 'bali, indonesia', 2, 'qqq-www-eee', '1-5-9', 'liburan 3d4n di bali', 'dsfsdfsdf dsffdfsdfsdf', 1, 5, 'ddd-fff-ggg', 'breakfast-lunch', 't', NULL, 'ticket-transport', '1-2', 5, 'idr', '30000000', NULL, NULL, NULL, NULL, NULL, 1, 2, NULL, '9891cc092373b7ec0fe3db635ea05286.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (16, 'sd', 'bandung, indonesia', 4, 'huiuhi', '1-5-9', 'Lembang Bandung', 'dfLorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 3, 'huiuhi', 'breakfast-lunch', 't', NULL, 'ticket-transport', '2-4', 3, 'idr', '199997', NULL, NULL, NULL, NULL, NULL, 12, 3, NULL, 'f642748e8240fdfc0b97e6316aa57ee7.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (17, 'md', 'bali, indonesia', 3, 'Place A-Place B-Place C', '2-5-6-7', 'sightseeing to garuda wisnu kencana', 'orem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'Bring 1-Bring 2-Bring 3', 'breakfast-lunch-lunch', 'f', 3, 'ticket-transport-insurance', '1-2', 5, 'idr', '3500000', NULL, NULL, NULL, NULL, NULL, 12, 3, 'Hotel A', 'ee96a01a7c077245dc73b035172d9062.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (18, 'sd', 'bandung, indonesia', 5, 'Place A-Place B-Place C', '1-4-6', 'wisata lembang - dago', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'Bring A-Bring B-Bring C', 'lunch', NULL, NULL, 'ticket-transport-insurance', '2-3-4', 5, 'idr', '500000', NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, '9e5a49ebe92f9fdaae0e1b8f4a331b2f.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (21, 'md', 'bandung, indonesia', 4, 'test 1-test 2-test 3', '1-5-9', 'liburan seru di bandung', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', 1, 4, 'test a-test b-test c', 'breakfast-lunch', '0', 4, 'transport-insurance', '1-4', 6, 'idr', '12000000', NULL, NULL, NULL, NULL, NULL, 1, 4, 'Hotel ABC', '1226401e4f6fc576efc72ca7555d90a9.png', '2019-07-18 11:01:20', '2019-07-18 11:29:31');
INSERT INTO `itinerary` VALUES (22, 'sd', 'bandung, indonesia', 8, 'ddd-eee-fff', '1-6-9', 'liburan sehari di bandung', 'fdjffgjf fgfgjdfj', 1, 5, 'aaa-ccc-vvv', 'breakfast-lunch', NULL, NULL, 'ticket-transport', '1-2', 3, 'idr', '2000000', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '15ae186ee3a4fc8e5f2821530e2d610c.png', NULL, NULL);

-- ----------------------------
-- Table structure for itinerary_detail
-- ----------------------------
DROP TABLE IF EXISTS `itinerary_detail`;
CREATE TABLE `itinerary_detail`  (
  `itd_id` int(11) NOT NULL AUTO_INCREMENT,
  `itin_id` int(11) NULL DEFAULT NULL,
  `itd_start_time` time(0) NULL DEFAULT NULL,
  `itd_end_time` time(0) NULL DEFAULT NULL,
  `itd_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itd_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itd_day` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`itd_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of itinerary_detail
-- ----------------------------
INSERT INTO `itinerary_detail` VALUES (1, 1, '11:00:00', '18:00:00', 'test 3', 'kfjghdfkjg lskhflgh khfkjhdfh fkhlfj', NULL);
INSERT INTO `itinerary_detail` VALUES (2, 21, '09:00:00', '12:00:00', 'test 1', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', 1);
INSERT INTO `itinerary_detail` VALUES (3, 21, '12:00:00', '15:00:00', 'test 2', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', 1);
INSERT INTO `itinerary_detail` VALUES (4, 21, '15:00:00', '17:00:00', 'test 3', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', 1);
INSERT INTO `itinerary_detail` VALUES (5, 21, '09:00:00', '12:00:00', 'test 4', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', 2);
INSERT INTO `itinerary_detail` VALUES (6, 21, '12:00:00', '15:00:00', 'test 5', 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ', 2);
INSERT INTO `itinerary_detail` VALUES (7, 21, '13:00:00', '15:00:00', 'test 2.1', 'lorem ipsum', 1);
INSERT INTO `itinerary_detail` VALUES (8, 1, '10:00:00', '11:00:00', 'test 1', 'lorem ipsum', NULL);
INSERT INTO `itinerary_detail` VALUES (9, 22, '04:05:00', '04:12:00', 'test 1', 'fdfgjfjfgjfgj', NULL);
INSERT INTO `itinerary_detail` VALUES (10, 22, '04:23:00', '15:33:00', 'test 2', 'dfhsfjfgjfg', NULL);

-- ----------------------------
-- Table structure for itinerary_image
-- ----------------------------
DROP TABLE IF EXISTS `itinerary_image`;
CREATE TABLE `itinerary_image`  (
  `itin_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `itin_img_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_id` int(11) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`itin_img_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of itinerary_image
-- ----------------------------
INSERT INTO `itinerary_image` VALUES (1, 'b2cbc348aaff36a40a9585781cc595e2.jpg', 0, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (13, 'd107cf33a106b5ad2729f6873c1cb9e9.jpg', 4, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (14, '54de197dc0fc6d7c5bdb7a42735f7065.jpg', 4, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (15, 'ca0f96b550a696275fa8ca1172d26e57.jpg', 4, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (16, '93157ad2476a5c5f3eb73f1672b9e9ff.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (17, 'd8e109881fd4aa2547550de4c9446faa.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (18, '7bc89e4b2a35afb3ddf8a76c3819ec6a.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (19, '4e73ddf3674ee443e34145db232f0dd7.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (20, '622224797c825493b8b80077461a93da.jpg', 7, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (21, '4ecf919e3fb6732f4d90b69421bb063f.jpg', 7, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (22, '9ca67083445aa521dd7bac0ef5b9727c.jpg', 7, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (23, '8a13a0cc07f69c307a9f71a6d300d915.jpg', 8, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (24, '856e46f1598b958590554d20c2f5057b.jpg', 8, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (25, '3f014c1a5e2db20bfe3a9310c6fd13c9.jpg', 8, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (26, '04fa65ada2778ddfc916a8eb37345542.jpg', 12, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (27, '7ea654b6d8096e0a35aa12670ff499c7.jpg', 12, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (28, 'b2abfc213da21516bd3ed6d93478d0af.jpg', 12, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (29, '607604a783c7362ea12efb367a5b9ae2.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (30, 'f69bea43adaf6f7a6aeb8ecae58652f7.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (31, '0c235ed851592054aa1ce2ad4f949e0e.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (32, '29d157054045c2528346000471ded15b.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (33, '287b2399e273b02627d7d890febb8c5e.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (34, '7507539d918b0adf913b4bfc0747c5df.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (35, '7bc85892d5e42acdebe0dadbd762dd42.jpg', 16, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (36, '6c3ba1a78a8450c99138cb92fc548495.jpg', 16, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (37, '06ad8386c52a5b69ca12154d03488d81.jpg', 17, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (38, '2fe0334255a375045e33458b6f3fb254.jpg', 17, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (39, 'd42c9fce4a8548380bca4c03697262fa.jpg', 17, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (40, '06ad8386c52a5b69ca12154d03488d81.jpg', 10, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (41, '2fe0334255a375045e33458b6f3fb254.jpg', 10, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (42, 'd42c9fce4a8548380bca4c03697262fa.jpg', 10, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (43, '7bc85892d5e42acdebe0dadbd762dd42.jpg', 9, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (44, '6c3ba1a78a8450c99138cb92fc548495.jpg', 9, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (45, '5d1ff5d40f936486ba60dfd0df007478.jpg', 22, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (46, '510cab69d00196c71403943a15d2053f.jpg', 22, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (47, '28af35925303f74773e089d369ec0bf2.jpg', 22, NULL, NULL);

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language`  (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lang_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lang_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`lang_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for not_recommended
-- ----------------------------
DROP TABLE IF EXISTS `not_recommended`;
CREATE TABLE `not_recommended`  (
  `nr_id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nr_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nr_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`nr_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of not_recommended
-- ----------------------------
INSERT INTO `not_recommended` VALUES (1, '< 17 Years Old', NULL, NULL);
INSERT INTO `not_recommended` VALUES (2, '< 21 Years Old', NULL, NULL);
INSERT INTO `not_recommended` VALUES (3, '> 60 Years Old', NULL, NULL);
INSERT INTO `not_recommended` VALUES (4, 'w/ Cardiac Problems', NULL, NULL);
INSERT INTO `not_recommended` VALUES (5, 'w/ Back Problems', NULL, NULL);
INSERT INTO `not_recommended` VALUES (6, 'w/ Children', NULL, NULL);

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NULL DEFAULT NULL,
  `payment_method` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_paid` int(11) NULL DEFAULT NULL,
  `payment_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`payment_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for personal_interest
-- ----------------------------
DROP TABLE IF EXISTS `personal_interest`;
CREATE TABLE `personal_interest`  (
  `pe_id` int(11) NOT NULL AUTO_INCREMENT,
  `pe_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pe_desc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of personal_interest
-- ----------------------------
INSERT INTO `personal_interest` VALUES (1, 'photography', NULL);
INSERT INTO `personal_interest` VALUES (2, 'adventure', NULL);
INSERT INTO `personal_interest` VALUES (3, 'hiking and trekking', NULL);
INSERT INTO `personal_interest` VALUES (4, 'food', NULL);
INSERT INTO `personal_interest` VALUES (5, 'sightseeing', NULL);
INSERT INTO `personal_interest` VALUES (6, 'arts and culture', NULL);
INSERT INTO `personal_interest` VALUES (7, 'history', NULL);
INSERT INTO `personal_interest` VALUES (8, 'shopping', NULL);
INSERT INTO `personal_interest` VALUES (9, 'theme parks', NULL);
INSERT INTO `personal_interest` VALUES (10, 'diving and snorkeling', NULL);
INSERT INTO `personal_interest` VALUES (11, 'island tours', NULL);
INSERT INTO `personal_interest` VALUES (12, 'sports', NULL);
INSERT INTO `personal_interest` VALUES (13, 'performances', NULL);
INSERT INTO `personal_interest` VALUES (14, 'spirituality', NULL);
INSERT INTO `personal_interest` VALUES (15, 'nightlife', NULL);
INSERT INTO `personal_interest` VALUES (16, 'workshops', NULL);

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review`  (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NULL DEFAULT NULL,
  `review_as` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'guide / tourist',
  `rating` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `review` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `review_date` date NULL DEFAULT NULL,
  `review_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`review_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for temp_image
-- ----------------------------
DROP TABLE IF EXISTS `temp_image`;
CREATE TABLE `temp_image`  (
  `temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `temp_img_name` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `temp_img_token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`temp_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_status` smallint(6) NULL DEFAULT NULL COMMENT '1 = active, 0 = not active/blm verify, 2 = suspended',
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `last_login` timestamp(0) NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `comment_suspend` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`user_id`, `user_email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES (1, 'henny.alfianti@gmail.com', '$2y$12$SBZMP.yKPZJTIYI7vQ9J7.RnAk8X8qsz7ufWYwPnSLzPQY5/N9bnO', 1, 'member', '2019-04-15 08:01:22', NULL, NULL, '2019-07-26 01:23:17', NULL, NULL, 'test abc');
INSERT INTO `user_account` VALUES (11, 'dsfsdf@jhfdg.fgkj', '115c660420fa693c0f833cb36d8f3deb', 2, 'member', '2019-05-16 07:58:13', NULL, NULL, '2019-07-18 09:51:49', NULL, NULL, NULL);
INSERT INTO `user_account` VALUES (12, 'user.guide1@gmail.com', '115c660420fa693c0f833cb36d8f3deb', 1, 'member', '2019-05-16 08:08:09', NULL, NULL, '2019-07-18 09:51:51', NULL, NULL, NULL);
INSERT INTO `user_account` VALUES (28, 'henny.lime@gmail.com', '$2y$12$0Tm5.ZxTKfRdNZFE1b4YUu.BUI5cvNhNlcsQugAGxzfU2V4l3/3L2', 1, 'member', '2019-07-24 08:33:27', NULL, NULL, '2019-07-26 02:27:57', 'fMPaEC1xqi8z', '::1', NULL);

-- ----------------------------
-- Table structure for user_data
-- ----------------------------
DROP TABLE IF EXISTS `user_data`;
CREATE TABLE `user_data`  (
  `user_id` int(11) NOT NULL,
  `user_fname` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_lname` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_gender` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'M = male, F = female',
  `user_nationality` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_residence` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_dob` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_personal_id` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'nomor ktp',
  `user_interests` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_lang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `user_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_photo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_data
-- ----------------------------
INSERT INTO `user_data` VALUES (1, 'Henny', 'Alfianti', 'F', 'Indonesian', 'Bandung, Indonesia', '1993-08-27', '12456477686787', '1-2-3', 'Indonesian, English', '1', '2019-07-25 07:05:31', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lobortis sed sem a sodales. In hac habitasse platea dictumst. In hac habitasse platea dictumst. Fusce eu est metus. Integer vulputate nulla justo, sed rhoncus augue interdum a. Proin a odio sit amet sem finibus eleifend eu vel tortor. Sed eget venenatis dolor. Pellentesque et placerat purus.', 'fb630753d66e9fedbaf3ef4c09f5e8ee.png');
INSERT INTO `user_data` VALUES (11, 'Test', 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fb630753d66e9fedbaf3ef4c09f5e8ee.png');
INSERT INTO `user_data` VALUES (12, 'User', 'Guide 1', NULL, NULL, NULL, '1993-09-12', NULL, NULL, NULL, NULL, NULL, NULL, 'fb630753d66e9fedbaf3ef4c09f5e8ee.png');
INSERT INTO `user_data` VALUES (28, 'User', 'Satu', NULL, NULL, NULL, '1993-08-27', '111111111111111', NULL, NULL, '28', '2019-07-24 09:57:01', NULL, NULL);

-- ----------------------------
-- Table structure for user_miles
-- ----------------------------
DROP TABLE IF EXISTS `user_miles`;
CREATE TABLE `user_miles`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `x_miles_now` bigint(20) NULL DEFAULT NULL,
  `x_miles_last` bigint(20) NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `source` int(5) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  `is_email_verify` int(11) NOT NULL,
  `name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `first_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `last_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alternateEmail` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mobile_no` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `picture_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `profile_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `vendor_file` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dob` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `gender` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `about` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `address_2` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `country` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `language` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `state` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pincode` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lastlogged` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `modified` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 198 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (127, 'admin@ca.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeFJqcpURF2QOrH4vqMEQELe9wDMLfZYe', 'Admin', 1, 1, 0, 'Admin', 'Super', 'Admin', 'admin@admin.com', '', '', '', 'eligibility-jump.png', '', '', '', '', 'asdfsdfsdfsdf', '', 'Helosdf', '', '', '', '', '', '302039', '::1', '2018-03-21 15:53:01', '', '2018-03-22 07:31:43');
INSERT INTO `users` VALUES (184, 'vendor1@ca.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeFJqcpURF2QOrH4vqMEQELe9wDMLfZYe', 'Vendor', 0, 1, 1, 'Vendor 1 xyz', 'Vendor 1', 'xyz', 'vendor1@xyz.com', '', '', '', '4.png', '', '[\"27042018105604_test - Copy (2).png\",\"27042018105604_test - Copy (3) - Copy.png\"]', '', '', '', '', '', '', '', '', '', '', '', '::1', '2018-04-27 10:56:05', '', '');
INSERT INTO `users` VALUES (185, 'vendor2@ca.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeFJqcpURF2QOrH4vqMEQELe9wDMLfZYe', 'Vendor', 0, 1, 1, 'Vendor 2 xyz', 'Vendor 2', 'xyz', 'vendor2@xyz.com', '', '', '', '', '', '[\"27042018105632_message-bar-chart3.png\",\"27042018105632_test - Copy (2).png\"]', '', '', '', '', '', '', '', '', '', '', '', '::1', '2018-04-27 10:56:33', '', '');
INSERT INTO `users` VALUES (196, 'client1@ca.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeFJqcpURF2QOrH4vqMEQELe9wDMLfZYe', 'Client_cs', 0, 1, 1, 'Client 1 xyz', 'Client 1', 'xyz', 'client1@xyz.com', '', '', '', '1.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '::1', '2018-04-27 10:56:05', '', '');
INSERT INTO `users` VALUES (197, 'client2@ca.com', '$2y$12$RyMmZVcqPEt9X2lJbHg1PeFJqcpURF2QOrH4vqMEQELe9wDMLfZYe', 'Client_cs', 0, 1, 1, 'Client 2 xyz', 'Client 2', 'xyz', 'client2@xyz.com', '', '', '', '2.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '::1', '2018-04-27 10:56:05', '', '');

SET FOREIGN_KEY_CHECKS = 1;
