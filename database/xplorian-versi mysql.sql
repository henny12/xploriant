/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : xploriant

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 28/06/2019 16:45:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking`  (
  `book_id` int(11) NOT NULL,
  `book_code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `itin_id` int(11) NULL DEFAULT NULL,
  `guide_id` int(11) NULL DEFAULT NULL,
  `book_date` date NULL DEFAULT NULL,
  `start_date` date NULL DEFAULT NULL,
  `total_tourist` int(11) NULL DEFAULT NULL,
  `total_price_currency` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_price_nominal` int(11) NULL DEFAULT NULL,
  `book_request` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'description',
  `book_status` smallint(6) NULL DEFAULT NULL COMMENT '1=pending, 2=accepted, 3=rejected, 4=paid, 5=finished',
  `book_paid` int(11) NULL DEFAULT NULL,
  `tourist_id` int(11) NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`book_id`, `book_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of booking
-- ----------------------------
INSERT INTO `booking` VALUES (5, '190618103702', 6, 12, '2019-06-18', '2019-06-18', 2, 'idr', 16000000, 'test abc', 1, NULL, 1, '2019-06-18');
INSERT INTO `booking` VALUES (6, '190618103924', 8, 1, '2019-06-18', '2019-06-19', 2, 'idr', 16000000, 'rsyrf fhfdgh dfgdsert', 1, NULL, 12, '2019-06-22');
INSERT INTO `booking` VALUES (7, '190618104016', 12, 1, '2019-06-18', '2019-06-20', 2, 'idr', 8000000, 'jhgf kjfdgdfg dfjkghdfjg dfg', 1, NULL, 12, '2019-06-23');

-- ----------------------------
-- Table structure for calender
-- ----------------------------
DROP TABLE IF EXISTS `calender`;
CREATE TABLE `calender`  (
  `cal_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `cal_date` date NULL DEFAULT NULL,
  `itin_id` int(11) NULL DEFAULT NULL,
  `cal_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `cal_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cal_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cancellation
-- ----------------------------
DROP TABLE IF EXISTS `cancellation`;
CREATE TABLE `cancellation`  (
  `cancel_id` int(11) NOT NULL,
  `booking_id` int(11) NULL DEFAULT NULL,
  `cancel_date` date NULL DEFAULT NULL,
  `refund_currency` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `refund_nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cancel_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cancel_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for category_tour
-- ----------------------------
DROP TABLE IF EXISTS `category_tour`;
CREATE TABLE `category_tour`  (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category_tour
-- ----------------------------
INSERT INTO `category_tour` VALUES (1, 'photography');
INSERT INTO `category_tour` VALUES (2, 'adventure');
INSERT INTO `category_tour` VALUES (3, 'hiking and trekking');
INSERT INTO `category_tour` VALUES (4, 'food (culinary tours)');
INSERT INTO `category_tour` VALUES (5, 'sightseeing');
INSERT INTO `category_tour` VALUES (6, 'arts and culture (cultu');
INSERT INTO `category_tour` VALUES (7, 'history (historic tours)');
INSERT INTO `category_tour` VALUES (8, 'shopping');
INSERT INTO `category_tour` VALUES (9, 'theme parks');
INSERT INTO `category_tour` VALUES (10, 'diving and snorkeling');
INSERT INTO `category_tour` VALUES (11, 'island tours');
INSERT INTO `category_tour` VALUES (12, 'sports');
INSERT INTO `category_tour` VALUES (13, 'performances');
INSERT INTO `category_tour` VALUES (14, 'spirituality (spiritual tours)');
INSERT INTO `category_tour` VALUES (15, 'nightlife');
INSERT INTO `category_tour` VALUES (16, 'workshops');

-- ----------------------------
-- Table structure for included_fees
-- ----------------------------
DROP TABLE IF EXISTS `included_fees`;
CREATE TABLE `included_fees`  (
  `ifee_id` int(11) NOT NULL,
  `ifee_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ifee_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ifee_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`ifee_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of included_fees
-- ----------------------------
INSERT INTO `included_fees` VALUES (1, 'Entrance Tickets', NULL, NULL);
INSERT INTO `included_fees` VALUES (2, 'Transportation', NULL, NULL);
INSERT INTO `included_fees` VALUES (3, 'Insurance', NULL, NULL);

-- ----------------------------
-- Table structure for included_meals
-- ----------------------------
DROP TABLE IF EXISTS `included_meals`;
CREATE TABLE `included_meals`  (
  `imeal_id` smallint(6) NOT NULL,
  `imeal_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `imeal_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`imeal_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of included_meals
-- ----------------------------
INSERT INTO `included_meals` VALUES (1, 'breakfast', NULL);
INSERT INTO `included_meals` VALUES (2, 'lunch', NULL);
INSERT INTO `included_meals` VALUES (3, 'dinner', NULL);

-- ----------------------------
-- Table structure for itinerary
-- ----------------------------
DROP TABLE IF EXISTS `itinerary`;
CREATE TABLE `itinerary`  (
  `itin_id` int(11) NOT NULL,
  `itin_day` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'single day (sd) dan multi day (md)',
  `itin_location` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_duration` int(11) NULL DEFAULT NULL COMMENT 'with hours',
  `itin_places` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'dipisah menggunakan -',
  `itin_activities` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'dipisah menggunakan -',
  `itin_categories` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_overview` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_min_tourist` int(11) NULL DEFAULT NULL,
  `itin_max_tourist` int(11) NULL DEFAULT NULL,
  `itin_what_to_bring` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_included_meals` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'dipisah menggunakan -',
  `itin_accom_included` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_accom_min_rate` smallint(6) NULL DEFAULT NULL,
  `itin_included_fees` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_not_recom_opt` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_cancellation` smallint(6) NULL DEFAULT NULL COMMENT 'with day',
  `itin_price_currency` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_price_nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_avg_ratings` float NULL DEFAULT NULL,
  `itin_total_bookings` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_status` smallint(6) NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_creator_id` int(11) NULL DEFAULT NULL COMMENT 'user_id',
  `itin_accom_night` int(11) NULL DEFAULT NULL,
  `itin_accom_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `itin_thumb` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`itin_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of itinerary
-- ----------------------------
INSERT INTO `itinerary` VALUES (4, 'sd', 'bandung, indonesia', 8, 'Dago-Lembang-Punclut', 'Lorem Ipsum-Lorem Ipsum 2-Lorem Ipsum 3', '1-2-3', 'Liburan seharian di Bandung', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 2, 5, 'Baju-Celana', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-4-5', 5, 'idr', '800000', NULL, NULL, NULL, NULL, NULL, 1, 3, NULL, 'c6d68bf68b422c51c1203c3e5bf951ad.png');
INSERT INTO `itinerary` VALUES (5, 'md', 'bandung, indonesia', 4, 'Dago-Braga-Gedung Sate-Lembang', 'Lorem Ipsum 1-Lorem Ipsum 2-Lorem Ipsum 3', '1-4-6-7', 'Liburan 3D4N Di Bandung', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 2, 4, 'Baju-Celana-Payung', 'breakfast-lunch-lunch', 'f', 3, 'ticket-transport', '1-2', 4, 'idr', '2000000', NULL, NULL, NULL, NULL, NULL, 1, 4, 'Lorem Ipsum A', '8044155e94712a677b73ca7dd2bbd567.png');
INSERT INTO `itinerary` VALUES (6, 'sd', 'batam, indonesia', 8, 'Place 1-Place 2-Place 3', 'Shopping-Swimming', '1-8-15', 'Liburan Sehari Di Batam', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'aaa-bbb-ccc', 'lunch-lunch', 'f', NULL, 'ticket-transport', '1-3', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 12, 2, NULL, 'e65bdd8e7ee97160ab4247296c563029.png');
INSERT INTO `itinerary` VALUES (7, 'sd', 'bali, indonesia', 2, 'place a-place b-place c', 'swimming-snorkling', '1-2-9', 'Dewata Beach Adventure', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'aaa-bbb-ccc', 'breakfast-lunch', 'f', NULL, 'ticket-transport', '1-4', 5, 'idr', '200000', NULL, NULL, NULL, NULL, NULL, 12, 3, NULL, '72ec2b682bfb99935a5b85008c4516eb.png');
INSERT INTO `itinerary` VALUES (8, 'md', 'bali, indonesia', 3, 'aaa-bbb-ccc', 'qqq-www-eee', '1-5-11', 'Ubud Rice Terrace', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'ccc-ddd-fff', 'breakfast-lunch', 'f', 4, 'ticket-transport', '1-3', 5, 'idr', '8000000', NULL, NULL, NULL, NULL, NULL, 1, 2, 'Lorem Ipsum A', 'be07bbd20d329f73f9a91f8e072906d4.png');
INSERT INTO `itinerary` VALUES (9, 'md', 'bali, indonesia', 3, 'zzz-xxx-ccc', 'aaa-sss-ddd', '1-2-6', 'Tamblingan - Munduk Adventure', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'qqq-www-eee', 'breakfast-lunch-lunch', 'f', 2, 'ticket-transport', '1-2', 5, 'idr', '1200000', NULL, NULL, NULL, NULL, NULL, 12, 3, 'Lorem Ipsum V', 'b0ed90b3d2585e47938b77c14454b4d7.png');
INSERT INTO `itinerary` VALUES (10, 'md', 'bali, indonesia', 2, 'bbb-nnn-mmm', 'fff-ggg-hhh', '1-2-3-4-6-8-9-10', 'Everything Bali (Complete Bali Tour)', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'qqq-www-eee', 'lunch-lunch', 'f', 4, 'transport-insurance', '3-4-5', 5, 'idr', '12000000', NULL, NULL, NULL, NULL, NULL, 1, 7, 'Lorem Ipsum M', '7c35b5f15d02e15d1f77ccd1879bc4f1.png');
INSERT INTO `itinerary` VALUES (12, 'md', 'bali, indonesia', 3, 'mmm-nnn-bbb', 'qqq-www-eee', '1-2-10-11', 'Kuta - Sanur - Tanjung Benoa Experience', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 5, 'ttt-yyy-uuu', 'breakfast-lunch', 'f', 3, 'transport-insurance', '1-2-3', 5, 'idr', '4000000', NULL, NULL, NULL, NULL, NULL, 1, 5, 'Lorem Ipsum M', '89e8204991b777b884e37083b8326a50.png');
INSERT INTO `itinerary` VALUES (14, 'md', 'bali, indonesia', 2, 'qqq-www-eee', 'aaa-bbb-ccc', '1-5-9', 'liburan 3d4n di bali', 'dsfsdfsdf dsffdfsdfsdf', 1, 5, 'ddd-fff-ggg', 'breakfast-lunch', 't', NULL, 'ticket-transport', '1-2', 5, 'idr', '30000000', NULL, NULL, NULL, NULL, NULL, 1, 2, NULL, '9891cc092373b7ec0fe3db635ea05286.png');
INSERT INTO `itinerary` VALUES (16, 'sd', 'bandung, indonesia', 4, 'huiuhi', 'uiui', '1-5-9', 'Lembang Bandung', 'dfLorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 3, 'huiuhi', 'breakfast-lunch', 't', NULL, 'ticket-transport', '2-4', 3, 'idr', '199997', NULL, NULL, NULL, NULL, NULL, 12, 3, NULL, 'f642748e8240fdfc0b97e6316aa57ee7.png');
INSERT INTO `itinerary` VALUES (17, 'md', 'bali, indonesia', 3, 'Place A-Place B-Place C', 'Do A-Do B-Do C', '2-5-6-7', 'sightseeing to garuda wisnu kencana', 'orem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'Bring 1-Bring 2-Bring 3', 'breakfast-lunch-lunch', 'f', 3, 'ticket-transport-insurance', '1-2', 5, 'idr', '3500000', NULL, NULL, NULL, NULL, NULL, 12, 3, 'Hotel A', 'ee96a01a7c077245dc73b035172d9062.png');
INSERT INTO `itinerary` VALUES (18, 'sd', 'bandung, indonesia', 5, 'Place A-Place B-Place C', 'Do 1-Do 2-Do 3', '1-4-6', 'wisata lembang - dago', 'Lorem ipsum dolor sit amet, et nec duis epicuri, vix id ridens electram. Mucius impedit meliore ne mea, te modo meis mel. Ei eius viderer sapientem mel, eu nobis pericula usu. Ea tantas blandit ius, erant utroque in eos, audiam ponderum voluptatum ut sed. Exerci percipitur consectetuer no vix, ius vocent quaeque omittantur ei. Duo suas facilis accusamus in, an timeam feugait volutpat per, qui at nullam definitionem.', 1, 4, 'Bring A-Bring B-Bring C', 'lunch', NULL, NULL, 'ticket-transport-insurance', '2-3-4', 5, 'idr', '500000', NULL, NULL, NULL, NULL, NULL, 12, NULL, NULL, '9e5a49ebe92f9fdaae0e1b8f4a331b2f.png');

-- ----------------------------
-- Table structure for itinerary_image
-- ----------------------------
DROP TABLE IF EXISTS `itinerary_image`;
CREATE TABLE `itinerary_image`  (
  `itin_img_id` int(11) NOT NULL,
  `itin_img_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `itin_id` int(11) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`itin_img_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of itinerary_image
-- ----------------------------
INSERT INTO `itinerary_image` VALUES (13, 'd107cf33a106b5ad2729f6873c1cb9e9.jpg', 4, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (14, '54de197dc0fc6d7c5bdb7a42735f7065.jpg', 4, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (15, 'ca0f96b550a696275fa8ca1172d26e57.jpg', 4, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (16, '93157ad2476a5c5f3eb73f1672b9e9ff.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (17, 'd8e109881fd4aa2547550de4c9446faa.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (18, '7bc89e4b2a35afb3ddf8a76c3819ec6a.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (19, '4e73ddf3674ee443e34145db232f0dd7.jpg', 6, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (20, '622224797c825493b8b80077461a93da.jpg', 7, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (21, '4ecf919e3fb6732f4d90b69421bb063f.jpg', 7, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (22, '9ca67083445aa521dd7bac0ef5b9727c.jpg', 7, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (23, '8a13a0cc07f69c307a9f71a6d300d915.jpg', 8, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (24, '856e46f1598b958590554d20c2f5057b.jpg', 8, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (25, '3f014c1a5e2db20bfe3a9310c6fd13c9.jpg', 8, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (26, '04fa65ada2778ddfc916a8eb37345542.jpg', 12, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (27, '7ea654b6d8096e0a35aa12670ff499c7.jpg', 12, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (28, 'b2abfc213da21516bd3ed6d93478d0af.jpg', 12, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (29, '607604a783c7362ea12efb367a5b9ae2.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (30, 'f69bea43adaf6f7a6aeb8ecae58652f7.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (31, '0c235ed851592054aa1ce2ad4f949e0e.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (32, '29d157054045c2528346000471ded15b.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (33, '287b2399e273b02627d7d890febb8c5e.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (34, '7507539d918b0adf913b4bfc0747c5df.jpg', 14, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (35, '7bc85892d5e42acdebe0dadbd762dd42.jpg', 16, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (36, '6c3ba1a78a8450c99138cb92fc548495.jpg', 16, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (37, '06ad8386c52a5b69ca12154d03488d81.jpg', 17, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (38, '2fe0334255a375045e33458b6f3fb254.jpg', 17, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (39, 'd42c9fce4a8548380bca4c03697262fa.jpg', 17, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (40, '06ad8386c52a5b69ca12154d03488d81.jpg', 10, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (41, '2fe0334255a375045e33458b6f3fb254.jpg', 10, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (42, 'd42c9fce4a8548380bca4c03697262fa.jpg', 10, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (43, '7bc85892d5e42acdebe0dadbd762dd42.jpg', 9, NULL, NULL);
INSERT INTO `itinerary_image` VALUES (44, '6c3ba1a78a8450c99138cb92fc548495.jpg', 9, NULL, NULL);

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language`  (
  `lang_id` int(11) NOT NULL,
  `lang_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lang_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lang_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`lang_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `id` int(11) NOT NULL,
  `name` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ke` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `avatar` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `message` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipe` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `date` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES (1, 'bachors', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'Awesome â¤ï¸ https://github.com/bachors/Chat-Realtime', '', 'rooms', '2018-08-30 09:21:19');
INSERT INTO `messages` VALUES (2, 'heri', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'test', '', 'rooms', '2019-06-24 16:57:04');
INSERT INTO `messages` VALUES (3, 'heri', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'test', '', 'rooms', '2019-06-24 16:57:09');
INSERT INTO `messages` VALUES (4, 'heri', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'fdgdfg', '', 'rooms', '2019-06-24 16:57:16');
INSERT INTO `messages` VALUES (5, 'heri', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'sffsf', '', 'users', '2019-06-24 16:57:31');
INSERT INTO `messages` VALUES (6, 'heri', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'qqq', '', 'users', '2019-06-24 16:57:41');
INSERT INTO `messages` VALUES (7, 'henny', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'haloÂ â˜ºï¸', '', 'users', '2019-06-24 16:58:53');
INSERT INTO `messages` VALUES (8, 'henny', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'test', '', 'users', '2019-06-24 16:59:03');
INSERT INTO `messages` VALUES (9, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'yoyo', '', 'users', '2019-06-24 16:59:19');
INSERT INTO `messages` VALUES (10, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'test', '', 'users', '2019-06-24 16:59:30');
INSERT INTO `messages` VALUES (11, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'fdhfgh', '', 'users', '2019-06-24 16:59:43');
INSERT INTO `messages` VALUES (12, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'fghfghfgh', '', 'users', '2019-06-24 16:59:45');
INSERT INTO `messages` VALUES (13, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'fgfgj', '', 'users', '2019-06-24 16:59:48');
INSERT INTO `messages` VALUES (14, 'henny', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'fghfgh', '', 'rooms', '2019-06-24 17:00:10');
INSERT INTO `messages` VALUES (15, 'henny', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'testppp', '', 'users', '2019-06-25 08:50:14');
INSERT INTO `messages` VALUES (16, 'henny', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'haloo', '', 'users', '2019-06-25 08:51:00');
INSERT INTO `messages` VALUES (17, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'test\ndfg', '', 'users', '2019-06-25 08:55:34');
INSERT INTO `messages` VALUES (18, 'henny', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'sdfdsf', '', 'rooms', '2019-06-25 08:57:14');
INSERT INTO `messages` VALUES (19, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'sdfdsf', '', 'users', '2019-06-25 08:57:18');
INSERT INTO `messages` VALUES (20, 'henny', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'sdfdsf', '', 'rooms', '2019-06-25 09:02:08');
INSERT INTO `messages` VALUES (21, 'henny', 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', 'test', '', 'users', '2019-06-25 09:02:19');
INSERT INTO `messages` VALUES (22, 'skjgf', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'jkdfhsdjfdfsdf', '', 'rooms', '2019-06-25 09:07:02');
INSERT INTO `messages` VALUES (23, 'skjgf', 'Public', 'https://avatars3.githubusercontent.com/u/4948333', 'sdfsdf', '', 'rooms', '2019-06-25 09:07:05');
INSERT INTO `messages` VALUES (24, 'skjgf', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'ndfbfb', '', 'users', '2019-06-25 09:07:16');
INSERT INTO `messages` VALUES (25, 'skjgf', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'dsfsdfdsf', '', 'users', '2019-06-25 09:07:19');
INSERT INTO `messages` VALUES (26, 'hans', 'henny', 'https://avatars3.githubusercontent.com/u/4948333', 'test', '', 'users', '2019-06-25 15:01:33');
INSERT INTO `messages` VALUES (27, 'hans', 'henny', 'https://avatars3.githubusercontent.com/u/4948333', 'hgdfhg', '', 'users', '2019-06-25 15:01:45');
INSERT INTO `messages` VALUES (28, 'hans', 'henny', 'https://avatars3.githubusercontent.com/u/4948333', 'gfj', '', 'users', '2019-06-25 15:02:10');
INSERT INTO `messages` VALUES (29, 'hans', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'dgdfg', '1561689781633ZY9N8.jpg', 'users', '2019-06-28 09:43:10');
INSERT INTO `messages` VALUES (30, 'hans', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'dfgdgfg', '', 'users', '2019-06-28 09:43:18');
INSERT INTO `messages` VALUES (31, 'henny', 'heri', 'https://avatars3.githubusercontent.com/u/4948333', 'dgfdfg', '', 'users', '2019-06-28 09:43:39');
INSERT INTO `messages` VALUES (32, 'henny', 'hans', 'https://avatars3.githubusercontent.com/u/4948333', 'dgfg', '', 'users', '2019-06-28 09:43:47');

-- ----------------------------
-- Table structure for not_recommended
-- ----------------------------
DROP TABLE IF EXISTS `not_recommended`;
CREATE TABLE `not_recommended`  (
  `nr_id` int(11) NOT NULL,
  `nr_title` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nr_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nr_status` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`nr_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of not_recommended
-- ----------------------------
INSERT INTO `not_recommended` VALUES (1, '< 17 Years Old', NULL, NULL);
INSERT INTO `not_recommended` VALUES (2, '< 21 Years Old', NULL, NULL);
INSERT INTO `not_recommended` VALUES (3, '> 60 Years Old', NULL, NULL);
INSERT INTO `not_recommended` VALUES (4, 'w/ Cardiac Problems', NULL, NULL);
INSERT INTO `not_recommended` VALUES (5, 'w/ Back Problems', NULL, NULL);
INSERT INTO `not_recommended` VALUES (6, 'w/ Children', NULL, NULL);

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment`  (
  `payment_id` int(11) NOT NULL,
  `booking_id` int(11) NULL DEFAULT NULL,
  `payment_method` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_paid` int(11) NULL DEFAULT NULL,
  `payment_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`payment_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for personal_interest
-- ----------------------------
DROP TABLE IF EXISTS `personal_interest`;
CREATE TABLE `personal_interest`  (
  `pe_id` int(11) NOT NULL,
  `pe_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pe_desc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pe_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of personal_interest
-- ----------------------------
INSERT INTO `personal_interest` VALUES (1, 'photography', NULL);
INSERT INTO `personal_interest` VALUES (2, 'adventure', NULL);
INSERT INTO `personal_interest` VALUES (3, 'hiking and trekking', NULL);
INSERT INTO `personal_interest` VALUES (4, 'food', NULL);
INSERT INTO `personal_interest` VALUES (5, 'sightseeing', NULL);
INSERT INTO `personal_interest` VALUES (6, 'arts and culture', NULL);
INSERT INTO `personal_interest` VALUES (7, 'history', NULL);
INSERT INTO `personal_interest` VALUES (8, 'shopping', NULL);
INSERT INTO `personal_interest` VALUES (9, 'theme parks', NULL);
INSERT INTO `personal_interest` VALUES (10, 'diving and snorkeling', NULL);
INSERT INTO `personal_interest` VALUES (11, 'island tours', NULL);
INSERT INTO `personal_interest` VALUES (12, 'sports', NULL);
INSERT INTO `personal_interest` VALUES (13, 'performances', NULL);
INSERT INTO `personal_interest` VALUES (14, 'spirituality', NULL);
INSERT INTO `personal_interest` VALUES (15, 'nightlife', NULL);
INSERT INTO `personal_interest` VALUES (16, 'workshops', NULL);

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review`  (
  `review_id` int(11) NOT NULL,
  `booking_id` int(11) NULL DEFAULT NULL,
  `review_as` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'guide / tourist',
  `rating` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `review` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `review_date` date NULL DEFAULT NULL,
  `review_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`review_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for temp_image
-- ----------------------------
DROP TABLE IF EXISTS `temp_image`;
CREATE TABLE `temp_image`  (
  `temp_id` int(11) NOT NULL,
  `temp_img_name` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `temp_img_token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`temp_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of temp_image
-- ----------------------------
INSERT INTO `temp_image` VALUES (57, 'b2cbc348aaff36a40a9585781cc595e2.jpg', '0.5188242486289285');
INSERT INTO `temp_image` VALUES (58, 'adcae3b07dba87aacc458f3f51306d40.jpg', '0.942049747086767');
INSERT INTO `temp_image` VALUES (59, 'b20f8d2a9cf3e370186c1d1ba19e19eb.jpg', '0.5777385114289997');
INSERT INTO `temp_image` VALUES (60, '8505d83cd7c4e576779107912bad2183.jpg', '0.9209138329421984');
INSERT INTO `temp_image` VALUES (61, 'ff64660857e10cb6230aef1f3fd5a767.jpg', '0.40916996567723096');
INSERT INTO `temp_image` VALUES (62, '6e734d7fb34a08b33d72afb03f7c2aea.jpg', '0.7272497611193474');
INSERT INTO `temp_image` VALUES (63, '178adb789cdf046f7f9f6b16975dc095.jpg', '0.4456104787563544');

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account`  (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_status` smallint(6) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `last_login` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_online` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `user_email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES (1, 'henny.alfianti@gmail.com', '115c660420fa693c0f833cb36d8f3deb', 1, 'member', '2019-04-15 08:01:22', NULL, NULL, NULL, NULL);
INSERT INTO `user_account` VALUES (11, 'dsfsdf@jhfdg.fgkj', '115c660420fa693c0f833cb36d8f3deb', 1, 'member', '2019-05-16 07:58:13', NULL, NULL, NULL, NULL);
INSERT INTO `user_account` VALUES (12, 'user.guide1@gmail.com', '115c660420fa693c0f833cb36d8f3deb', 1, 'member', '2019-05-16 08:08:09', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_data
-- ----------------------------
DROP TABLE IF EXISTS `user_data`;
CREATE TABLE `user_data`  (
  `user_id` int(11) NOT NULL,
  `user_fname` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_lname` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_gender` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'M = male, F = female',
  `user_nationality` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_residence` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_dob` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_personal_id` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'nomor ktp',
  `user_interests` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_lang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `user_desc` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_photo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_data
-- ----------------------------
INSERT INTO `user_data` VALUES (1, 'Henny', 'Alfianti', 'F', 'Indonesian', 'Bandung, Indonesia', '1993-08-27', '7364835763485734', NULL, 'Indonesian, English, Mandarin', NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lobortis sed sem a sodales. In hac habitasse platea dictumst. In hac habitasse platea dictumst. Fusce eu est metus. Integer vulputate nulla justo, sed rhoncus augue interdum a. Proin a odio sit amet sem finibus eleifend eu vel tortor. Sed eget venenatis dolor. Pellentesque et placerat purus.', 'fb630753d66e9fedbaf3ef4c09f5e8ee.png');
INSERT INTO `user_data` VALUES (12, 'User', 'Guide 1', NULL, NULL, NULL, '1993-09-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user_miles
-- ----------------------------
DROP TABLE IF EXISTS `user_miles`;
CREATE TABLE `user_miles`  (
  `user_id` int(11) NOT NULL,
  `x_miles_now` bigint(20) NULL DEFAULT NULL,
  `x_miles_last` bigint(20) NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `ke` int(11) NOT NULL,
  `name` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `avatar` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `login` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`ke`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'bachors', 'https://avatars3.githubusercontent.com/u/4948333', '2018-08-30 09:21:27', 'online');
INSERT INTO `users` VALUES (2, 'alvhie', 'https://avatars3.githubusercontent.com/u/4948333', '2019-06-24 16:56:02', 'offline');
INSERT INTO `users` VALUES (3, 'heri', 'https://avatars3.githubusercontent.com/u/4948333', '2019-06-24 16:56:58', 'offline');
INSERT INTO `users` VALUES (4, 'henny', 'https://avatars3.githubusercontent.com/u/4948333', '2019-06-28 10:07:00', 'offline');
INSERT INTO `users` VALUES (5, 'skjgf', 'https://avatars3.githubusercontent.com/u/4948333', '2019-06-25 09:06:47', 'offline');
INSERT INTO `users` VALUES (6, 'hans', 'https://avatars3.githubusercontent.com/u/4948333', '2019-06-28 09:27:43', 'offline');

SET FOREIGN_KEY_CHECKS = 1;
