/*
 Navicat Premium Data Transfer

 Source Server         : postgre-10
 Source Server Type    : PostgreSQL
 Source Server Version : 100006
 Source Host           : localhost:5432
 Source Catalog        : xploriant
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100006
 File Encoding         : 65001

 Date: 08/04/2019 13:28:38
*/


-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS "public"."booking";
CREATE TABLE "public"."booking" (
  "book_id" int4 NOT NULL,
  "itin_id" int4,
  "guest_id" int4,
  "book_date" date,
  "start_date" date,
  "total_tourist" int4,
  "total_price_currency" varchar(255) COLLATE "pg_catalog"."default",
  "total_price_nominal" int4,
  "book_request" text COLLATE "pg_catalog"."default",
  "book_status" int2,
  "book_paid" int4
)
;
COMMENT ON COLUMN "public"."booking"."book_request" IS 'description';
COMMENT ON COLUMN "public"."booking"."book_status" IS '1=pending, 2=accepted, 3=rejected, 4=paid, 5=finished';

-- ----------------------------
-- Table structure for calender
-- ----------------------------
DROP TABLE IF EXISTS "public"."calender";
CREATE TABLE "public"."calender" (
  "cal_id" int4 NOT NULL DEFAULT nextval('calender_cal_id_seq'::regclass),
  "user_id" int4,
  "cal_date" date,
  "itin_id" int4,
  "cal_desc" text COLLATE "pg_catalog"."default",
  "cal_status" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for cancellation
-- ----------------------------
DROP TABLE IF EXISTS "public"."cancellation";
CREATE TABLE "public"."cancellation" (
  "cancel_id" int4 NOT NULL DEFAULT nextval('cancel_cancel_id_seq'::regclass),
  "booking_id" int4,
  "cancel_date" date,
  "refund_currency" varchar(255) COLLATE "pg_catalog"."default",
  "refund_nominal" varchar(255) COLLATE "pg_catalog"."default",
  "cancel_status" int4
)
;

-- ----------------------------
-- Table structure for category_tour
-- ----------------------------
DROP TABLE IF EXISTS "public"."category_tour";
CREATE TABLE "public"."category_tour" (
  "cat_id" int4 NOT NULL DEFAULT nextval('category_tour_cat_id_seq'::regclass),
  "cat_name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of category_tour
-- ----------------------------
INSERT INTO "public"."category_tour" VALUES (1, 'photography');
INSERT INTO "public"."category_tour" VALUES (2, 'adventure');
INSERT INTO "public"."category_tour" VALUES (3, 'hiking and trekking');
INSERT INTO "public"."category_tour" VALUES (4, 'food (culinary tours)');
INSERT INTO "public"."category_tour" VALUES (5, 'sightseeing');
INSERT INTO "public"."category_tour" VALUES (6, 'arts and culture (cultu');
INSERT INTO "public"."category_tour" VALUES (7, 'history (historic tours)');
INSERT INTO "public"."category_tour" VALUES (8, 'shopping');
INSERT INTO "public"."category_tour" VALUES (9, 'theme parks');
INSERT INTO "public"."category_tour" VALUES (10, 'diving and snorkeling');
INSERT INTO "public"."category_tour" VALUES (11, 'island tours');
INSERT INTO "public"."category_tour" VALUES (12, 'sports');
INSERT INTO "public"."category_tour" VALUES (13, 'performances');
INSERT INTO "public"."category_tour" VALUES (14, 'spirituality (spiritual tours)');
INSERT INTO "public"."category_tour" VALUES (15, 'nightlife');
INSERT INTO "public"."category_tour" VALUES (16, 'workshops');

-- ----------------------------
-- Table structure for included_fees
-- ----------------------------
DROP TABLE IF EXISTS "public"."included_fees";
CREATE TABLE "public"."included_fees" (
  "ifee_id" int4 NOT NULL DEFAULT nextval('included_fees_ifee_id_seq'::regclass),
  "ifee_title" varchar(255) COLLATE "pg_catalog"."default",
  "ifee_desc" text COLLATE "pg_catalog"."default",
  "ifee_status" int2
)
;

-- ----------------------------
-- Records of included_fees
-- ----------------------------
INSERT INTO "public"."included_fees" VALUES (1, 'Entrance Tickets', NULL, NULL);
INSERT INTO "public"."included_fees" VALUES (2, 'Transportation', NULL, NULL);
INSERT INTO "public"."included_fees" VALUES (3, 'Insurance', NULL, NULL);

-- ----------------------------
-- Table structure for included_meals
-- ----------------------------
DROP TABLE IF EXISTS "public"."included_meals";
CREATE TABLE "public"."included_meals" (
  "imeal_id" int2 NOT NULL DEFAULT nextval('included_meals_imeal_id_seq'::regclass),
  "imeal_title" varchar(255) COLLATE "pg_catalog"."default",
  "imeal_status" int2
)
;

-- ----------------------------
-- Records of included_meals
-- ----------------------------
INSERT INTO "public"."included_meals" VALUES (1, 'breakfast', NULL);
INSERT INTO "public"."included_meals" VALUES (2, 'lunch', NULL);
INSERT INTO "public"."included_meals" VALUES (3, 'dinner', NULL);

-- ----------------------------
-- Table structure for itinerary
-- ----------------------------
DROP TABLE IF EXISTS "public"."itinerary";
CREATE TABLE "public"."itinerary" (
  "itin_id" int4 NOT NULL DEFAULT nextval('itinerary_itin_id_seq'::regclass),
  "itin_day" varchar(10) COLLATE "pg_catalog"."default",
  "itin_location" varchar(255) COLLATE "pg_catalog"."default",
  "itin_duration" int4,
  "itin_places" text COLLATE "pg_catalog"."default",
  "itin_activites" text COLLATE "pg_catalog"."default",
  "itin_categories" text COLLATE "pg_catalog"."default",
  "itin_title" varchar(255) COLLATE "pg_catalog"."default",
  "itin_overview" text COLLATE "pg_catalog"."default",
  "itin_min_tourist" int4,
  "itin_max_tourist" int4,
  "itin_what_to_bring" text COLLATE "pg_catalog"."default",
  "itin_included_meals" varchar(255) COLLATE "pg_catalog"."default",
  "itin_accom_included" varchar(255) COLLATE "pg_catalog"."default",
  "itin_accom_min_rate" int2,
  "itin_included_fees" varchar(255) COLLATE "pg_catalog"."default",
  "itin_not_recommended" varchar(255) COLLATE "pg_catalog"."default",
  "itin_cancellation" int2,
  "itin_price_currency" varchar(10) COLLATE "pg_catalog"."default",
  "itin_price_nominal" varchar(255) COLLATE "pg_catalog"."default",
  "itin_avg_ratings" float4,
  "itin_total_bookings" varchar(255) COLLATE "pg_catalog"."default",
  "itin_status" int2,
  "updated_on" timestamp(6),
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "itin_creator_id" int4
)
;
COMMENT ON COLUMN "public"."itinerary"."itin_day" IS 'single day (sd) dan multi day (md)';
COMMENT ON COLUMN "public"."itinerary"."itin_duration" IS 'with hours';
COMMENT ON COLUMN "public"."itinerary"."itin_places" IS 'dipisah menggunakan -';
COMMENT ON COLUMN "public"."itinerary"."itin_activites" IS 'dipisah menggunakan -';
COMMENT ON COLUMN "public"."itinerary"."itin_cancellation" IS 'with day';
COMMENT ON COLUMN "public"."itinerary"."itin_creator_id" IS 'user_id';

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS "public"."language";
CREATE TABLE "public"."language" (
  "lang_id" int4 NOT NULL DEFAULT nextval('language_lang_id_seq'::regclass),
  "lang_name" varchar(100) COLLATE "pg_catalog"."default",
  "lang_code" varchar(10) COLLATE "pg_catalog"."default",
  "lang_status" int2
)
;

-- ----------------------------
-- Table structure for not_recommended
-- ----------------------------
DROP TABLE IF EXISTS "public"."not_recommended";
CREATE TABLE "public"."not_recommended" (
  "nr_id" int4 NOT NULL DEFAULT nextval('not_recommended_nr_id_seq'::regclass),
  "nr_title" varchar(255) COLLATE "pg_catalog"."default",
  "nr_desc" text COLLATE "pg_catalog"."default",
  "nr_status" int2
)
;

-- ----------------------------
-- Records of not_recommended
-- ----------------------------
INSERT INTO "public"."not_recommended" VALUES (1, 'Under 17', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (2, 'Under 21', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (3, 'Above 60', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (4, 'Cardiac Problems', NULL, NULL);
INSERT INTO "public"."not_recommended" VALUES (5, 'Back Problems', NULL, NULL);

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS "public"."payment";
CREATE TABLE "public"."payment" (
  "payment_id" int4 NOT NULL DEFAULT nextval('payment2_payment_id_seq'::regclass),
  "booking_id" int4,
  "payment_method" varchar(255) COLLATE "pg_catalog"."default",
  "total_paid" int4,
  "payment_date" date
)
;

-- ----------------------------
-- Table structure for personal_interest
-- ----------------------------
DROP TABLE IF EXISTS "public"."personal_interest";
CREATE TABLE "public"."personal_interest" (
  "pe_id" int4 NOT NULL DEFAULT nextval('personal_interest_pe_id_seq'::regclass),
  "pe_name" varchar(255) COLLATE "pg_catalog"."default",
  "pe_desc" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of personal_interest
-- ----------------------------
INSERT INTO "public"."personal_interest" VALUES (1, 'photography', NULL);
INSERT INTO "public"."personal_interest" VALUES (2, 'adventure', NULL);
INSERT INTO "public"."personal_interest" VALUES (3, 'hiking and trekking', NULL);
INSERT INTO "public"."personal_interest" VALUES (4, 'food', NULL);
INSERT INTO "public"."personal_interest" VALUES (5, 'sightseeing', NULL);
INSERT INTO "public"."personal_interest" VALUES (6, 'arts and culture', NULL);
INSERT INTO "public"."personal_interest" VALUES (7, 'history', NULL);
INSERT INTO "public"."personal_interest" VALUES (8, 'shopping', NULL);
INSERT INTO "public"."personal_interest" VALUES (9, 'theme parks', NULL);
INSERT INTO "public"."personal_interest" VALUES (10, 'diving and snorkeling', NULL);
INSERT INTO "public"."personal_interest" VALUES (11, 'island tours', NULL);
INSERT INTO "public"."personal_interest" VALUES (12, 'sports', NULL);
INSERT INTO "public"."personal_interest" VALUES (13, 'performances', NULL);
INSERT INTO "public"."personal_interest" VALUES (14, 'spirituality', NULL);
INSERT INTO "public"."personal_interest" VALUES (15, 'nightlife', NULL);
INSERT INTO "public"."personal_interest" VALUES (16, 'workshops', NULL);

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS "public"."review";
CREATE TABLE "public"."review" (
  "review_id" int4 NOT NULL DEFAULT nextval('review_review_id_seq'::regclass),
  "booking_id" int4,
  "review_as" varchar(255) COLLATE "pg_catalog"."default",
  "rating" varchar(255) COLLATE "pg_catalog"."default",
  "review" varchar(255) COLLATE "pg_catalog"."default",
  "user_id" int4,
  "review_date" date,
  "review_status" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_account";
CREATE TABLE "public"."user_account" (
  "user_id" int4 NOT NULL DEFAULT nextval('user_account_user_id_seq'::regclass),
  "user_email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "user_password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "user_status" int2,
  "created_by" varchar(100) COLLATE "pg_catalog"."default",
  "created_on" timestamp(6),
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "updated_on" timestamp(6)
)
;

-- ----------------------------
-- Table structure for user_data
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_data";
CREATE TABLE "public"."user_data" (
  "user_id" int4 NOT NULL,
  "user_fname" varchar(200) COLLATE "pg_catalog"."default",
  "user_lname" varchar(200) COLLATE "pg_catalog"."default",
  "user_gender" varchar(10) COLLATE "pg_catalog"."default",
  "user_nationality" varchar(255) COLLATE "pg_catalog"."default",
  "user_residence" varchar(255) COLLATE "pg_catalog"."default",
  "user_dob" varchar(255) COLLATE "pg_catalog"."default",
  "user_personal_id" int4,
  "user_interests" varchar(255) COLLATE "pg_catalog"."default",
  "user_lang" varchar(255) COLLATE "pg_catalog"."default",
  "updated_by" varchar(100) COLLATE "pg_catalog"."default",
  "updated_on" timestamp(6)
)
;
COMMENT ON COLUMN "public"."user_data"."user_personal_id" IS 'nomor ktp';

-- ----------------------------
-- Table structure for user_miles
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_miles";
CREATE TABLE "public"."user_miles" (
  "user_id" int4 NOT NULL,
  "x_miles_now" int8,
  "x_miles_last" int8,
  "updated_on" timestamp(6)
)
;

-- ----------------------------
-- Primary Key structure for table booking
-- ----------------------------
ALTER TABLE "public"."booking" ADD CONSTRAINT "booking_pkey" PRIMARY KEY ("book_id");

-- ----------------------------
-- Primary Key structure for table calender
-- ----------------------------
ALTER TABLE "public"."calender" ADD CONSTRAINT "calender_pkey" PRIMARY KEY ("cal_id");

-- ----------------------------
-- Primary Key structure for table cancellation
-- ----------------------------
ALTER TABLE "public"."cancellation" ADD CONSTRAINT "cancel_pkey" PRIMARY KEY ("cancel_id");

-- ----------------------------
-- Primary Key structure for table category_tour
-- ----------------------------
ALTER TABLE "public"."category_tour" ADD CONSTRAINT "category_tour_pkey" PRIMARY KEY ("cat_id");

-- ----------------------------
-- Primary Key structure for table included_fees
-- ----------------------------
ALTER TABLE "public"."included_fees" ADD CONSTRAINT "included_fees_pkey" PRIMARY KEY ("ifee_id");

-- ----------------------------
-- Primary Key structure for table included_meals
-- ----------------------------
ALTER TABLE "public"."included_meals" ADD CONSTRAINT "included_meals_pkey" PRIMARY KEY ("imeal_id");

-- ----------------------------
-- Primary Key structure for table itinerary
-- ----------------------------
ALTER TABLE "public"."itinerary" ADD CONSTRAINT "itinerary_pkey" PRIMARY KEY ("itin_id");

-- ----------------------------
-- Primary Key structure for table language
-- ----------------------------
ALTER TABLE "public"."language" ADD CONSTRAINT "language_pkey" PRIMARY KEY ("lang_id");

-- ----------------------------
-- Primary Key structure for table not_recommended
-- ----------------------------
ALTER TABLE "public"."not_recommended" ADD CONSTRAINT "not_recommended_pkey" PRIMARY KEY ("nr_id");

-- ----------------------------
-- Primary Key structure for table payment
-- ----------------------------
ALTER TABLE "public"."payment" ADD CONSTRAINT "payment2_pkey" PRIMARY KEY ("payment_id");

-- ----------------------------
-- Primary Key structure for table personal_interest
-- ----------------------------
ALTER TABLE "public"."personal_interest" ADD CONSTRAINT "personal_interest_pkey" PRIMARY KEY ("pe_id");

-- ----------------------------
-- Primary Key structure for table review
-- ----------------------------
ALTER TABLE "public"."review" ADD CONSTRAINT "review_pkey" PRIMARY KEY ("review_id");

-- ----------------------------
-- Primary Key structure for table user_account
-- ----------------------------
ALTER TABLE "public"."user_account" ADD CONSTRAINT "user_account_pkey" PRIMARY KEY ("user_id", "user_email");

-- ----------------------------
-- Primary Key structure for table user_data
-- ----------------------------
ALTER TABLE "public"."user_data" ADD CONSTRAINT "user_data_pkey" PRIMARY KEY ("user_id");

-- ----------------------------
-- Primary Key structure for table user_miles
-- ----------------------------
ALTER TABLE "public"."user_miles" ADD CONSTRAINT "user_miles_pkey" PRIMARY KEY ("user_id");
