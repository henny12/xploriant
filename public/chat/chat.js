$(function() {

  var guide_id = getAllUrlParams().guide_id;
  var guide_name = getAllUrlParams().guide_name; 
  if (guide_id) {
    ChatSection(1);
    var res = guide_name.replace(/\+/g, " ");
    var receiver_id = guide_id;
    $('#ReciverId_txt').val(receiver_id);
    $('#ReciverName_txt').html(res);
    GetChatHistory(receiver_id,1);
    GetUserHistory();
  }
  // console.log(param);

$('.message').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
       sendTxtMessage($(this).val());
    }
});
$('.btnSend').click(function(){
       sendTxtMessage($('.message').val());
});
// $('.selectVendor').click(function(){
//   ChatSection(1);
//       var receiver_id = $(this).attr('id');
//       console.log(receiver_id);
//     // alert(receiver_id);
//     $('#ReciverId_txt').val(receiver_id);
//     $('#ReciverName_txt').html($(this).attr('title'));
    
//     GetChatHistory(receiver_id,1);
        
// });
$('.upload_attachmentfile').change(function(){
  
  DisplayMessage('<div class="spiner"><i class="fa fa-circle-o-notch fa-spin"></i></div>');
  ScrollDown();
  
  var file_data = $('.upload_attachmentfile').prop('files')[0];
  var receiver_id = $('#ReciverId_txt').val();   
    var form_data = new FormData();
    form_data.append('attachmentfile', file_data);
  form_data.append('type', 'Attachment');
  form_data.append('receiver_id', receiver_id);
  
  $.ajax({
                url: 'chat-attachment/upload', 
                dataType: 'json',  
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                        
                type: 'post',
                success: function(response){
          
          $('.upload_attachmentfile').val('');
          GetChatHistory(receiver_id);
        },
        error: function (jqXHR, status, err) {
               // alert('Local error callback');
        }
   });
  
});
$('.ClearChat').click(function(){
       var receiver_id = $('#ReciverId_txt').val();
          $.ajax({
              //dataType : "json",
                url: 'chat-clear?receiver_id='+receiver_id,
              success:function(data)
              {
                 GetChatHistory(receiver_id);    
              },
              error: function (jqXHR, status, err) {
               // alert('Local error callback');
              }
          });
        
});

 

}); ///end of jquery

function ViewAttachment(message_id){
//alert(message_id);
      /*$.ajax({
              //dataType : "json",
                url: 'view-chat-attachment?message_id='+message_id,
              success:function(data)
              {
                     
              },
              error: function (jqXHR, status, err) {
               // alert('Local error callback');
              }
          });*/
}
function ViewAttachmentImage(image_url,imageTitle){
  $('#modelTitle').html(imageTitle); 
  $('#modalImgs').attr('src',image_url);
  $('#myModalImg').modal('show');
}

function ChatSection(status){
  //chatSection
  if(status==0){
    $('#chatSection :input').attr('disabled', true);
    } else {
        $('#chatSection :input').removeAttr('disabled');
    }   
}
ChatSection(1);


function ScrollDown(){
  var elmnt = document.getElementById("content");
    var h = elmnt.scrollHeight + 1000;
   $('#content').animate({scrollTop: h}, 1000);
}
window.onload = ScrollDown();

function DisplayMessage(message){
  var Sender_Name = $('#Sender_Name').val();
  var Sender_ProfilePic = $('#Sender_ProfilePic').val();
  
    var str = '<div class="direct-chat-msg right">';
        str+='<div class="direct-chat-info clearfix">';
         str+='<span class="direct-chat-name pull-right">'+Sender_Name ;
         str+='</span><span class="direct-chat-timestamp pull-left"></span>'; //23 Jan 2:05 pm
         str+='</div><img class="direct-chat-img" src="'+Sender_ProfilePic+'" alt="">';
         str+='<div class="direct-chat-text">'+message;
         str+='</div></div>';
    $('#dumppy').append(str);
}

function sendTxtMessage(message){
  var messageTxt = message.trim();
  if(messageTxt!=''){
    //console.log(message);
    DisplayMessage(messageTxt);
    
        var receiver_id = $('#ReciverId_txt').val();
        $.ajax({
              dataType : "json",
              type : 'post',
              data : {messageTxt : messageTxt, receiver_id : receiver_id },
              url: 'send-message',
              success:function(data)
              {
                GetChatHistory(receiver_id)    
              },
              error: function (jqXHR, status, err) {
               // alert('Local error callback');
              }
          });
          
    
    
    ScrollDown();
    $('.message').val('');
    $('.message').focus();
  }else{
    $('.message').focus();
  }
}

function GetChatHistory(receiver_id, open){

  if (open === 1){
        ScrollDown();
    }
        $.ajax({
              //dataType : "json",
                url: 'get-chat-history-vendor?receiver_id='+receiver_id,
              success:function(data)
              {
                $('#dumppy').html(data);
              // ScrollDown();  
              },
              error: function (jqXHR, status, err) {
               // alert('Local error callback');
              }
          });
}

function GetUserHistory(){
  // console.log("test");
  // $('#refresh').load(location.href + ' #time');
    // $('.inbox_chat').load(location.href + ' .inbox_chat');
    $.ajax({
        //dataType : "json",
        url: 'get-user-history',
        success:function(data)
        {
          $('.inbox_chat').html(data);
        // ScrollDown();  
        },
        error: function (jqXHR, status, err) {
         // alert('Local error callback');
        }
    });
}

setInterval(function(){ 
  GetUserHistory();
  // $('#test').trigger('click');
    var receiver_id = $('#ReciverId_txt').val();
  if(receiver_id!=''){
    GetChatHistory(receiver_id);
    
  }
}, 3000);

// setInterval(function(){
//     $('#test').trigger('click');
// }, 10);

$(document).on('click', '.selectVendor', function() {
  ChatSection(1);
      var receiver_id = $(this).attr('id');
      console.log(receiver_id);
    // alert(receiver_id);
    $('#ReciverId_txt').val(receiver_id);
    $('#ReciverName_txt').html($(this).attr('title'));
    
    GetChatHistory(receiver_id,1);
});


function getAllUrlParams(url) {
  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  // we'll store the parameters here
  var obj = {};
  // if query string exists
  if (queryString) {
    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];
    // split our query string into its component parts
    var arr = queryString.split('&');
    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');
      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      if (typeof paramValue === 'string') paramValue = paramValue;
      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {
        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];
        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }

  return obj;
}