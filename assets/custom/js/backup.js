/***********************************************************************
* For create itinerary page
***********************************************************************/
jQuery(document).ready(function() {
    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    // For select 2
    $(".select2").select2();
    $('.selectpicker').selectpicker();

    //Bootstrap-TouchSpin
    $(".vertical-spin").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'ti-plus',
        verticaldownclass: 'ti-minus'
    });
    var vspinTrue = $(".vertical-spin").TouchSpin({
        verticalbuttons: true
    });
    if (vspinTrue) {
        $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
    }
    $("input[name='tch1']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $("input[name='tch2']").TouchSpin({
        min: -1000000000,
        max: 1000000000,
        stepinterval: 50,
        maxboostedstep: 10000000,
        prefix: '$'
    });
    $(".tch3").TouchSpin();
    $("input[name='tch3_22']").TouchSpin({
        initval: 40
    });
    $("input[name='tch5']").TouchSpin({
        prefix: "pre",
        postfix: "post"
    });

    // For multiselect
    $('#pre-selected-options').multiSelect();
    $('#optgroup').multiSelect({
        selectableOptgroup: true
    });
    $('#public-methods').multiSelect();
    $('#select-all').click(function() {
        $('#public-methods').multiSelect('select_all');
        return false;
    });
    $('#deselect-all').click(function() {
        $('#public-methods').multiSelect('deselect_all');
        return false;
    });
    $('#refresh').on('click', function() {
        $('#public-methods').multiSelect('refresh');
        return false;
    });
    $('#add-option').on('click', function() {
        $('#public-methods').multiSelect('addOption', {
            value: 42,
            text: 'test 42',
            index: 0
        });
        return false;
    });
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

$("#basic_checkbox_1").change( function(){
    if($(this).is(':checked')){
        $("#night").hide();
        $("#desc").hide();
        $("#minrate").hide();
        $("#accom_night").attr("disabled", true);
        $("#accom_desc").attr("disabled", true);
        $("#rating-input").attr("disabled", true);
    }else{
        $("#night").show();
        $("#desc").show();
        $("#minrate").show();
        $("#accom_night").attr("disabled", false);
        $("#accom_desc").attr("disabled", false);
        $("#rating-input").attr("disabled", false);
    }
});

$("#mintourist").change( function(){
    var min = $(this).val();
    $("#maxtourist").val(min);
    var max = $("#maxtourist").val();
    if(max <= min){
        $(".btn-min-for-maxtour").attr("disabled", true);
    }else{
        $(".btn-min-for-maxtour").attr("disabled", false);
    }
});

$("#maxtourist").change( function(){
    var min = $("#mintourist").val();
    var max = $(this).val();
    if(max <= min){
        $(".btn-min-for-maxtour").attr("disabled", true);
    }else{
        $(".btn-min-for-maxtour").attr("disabled", false);
    }
});

$(document).ready(function () {
    $('#carouselExampleIndicators2').find('.carousel-item').first().addClass('active');
});

$("#total-tourist").change( function(){
    var jmltourist = $(this).val();
    var price = $("#itin-price").val();
    var total = jmltourist * price;

    $("#total-price").val(total);
    $("#txt-total-price").text('IDR '+formatNumber(total));
});
/***********************************************************************
* For checkbox option with button
***********************************************************************/
$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length === 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});

/***********************************************************************
* For add content itinerary
***********************************************************************/
$('#placewego').on('click', function (){
    var place = $('#place').val();
    var cf = $("#data_place > tr:last-child").find('input.conter-forgood').data('con');
    var ln = (typeof cf !== "undefined")?(cf+1):1;
    if ($("#data_place > tr").length === 5) {
        return;
    }
    var tr = '<tr>'+
        '<td>'+
        '<div class="input-group mb-1">'+
            '<input type="text" data-con="'+ln+'" class="form-control input_add" name="dplace[]" value="'+place+'">'+
                '<div class="input-group-append">'+
                    '<button class="btn btn-outline-secondary remove_selection" type="button">-</button>'+
                '</div></div>'+
        '</td>'+
        '</tr>';
    $('#data_place').append(tr);
    $('#place').val('');
});

$('#thingwedo').on('click', function (){
    var wedo = $('#wedo').val();
    var cf = $("#data_wedo > tr:last-child").find('input.conter-forgood').data('con');
    var ln = (typeof cf !== "undefined")?(cf+1):1;
    if ($("#data_wedo > tr").length === 5) {
        return;
    }
    var tr = '<tr>'+
        '<td>'+
        '<div class="input-group mb-1">'+
            '<input type="text" data-con="'+ln+'" class="form-control input_add" name="dwedo[]" value="'+wedo+'">'+
                '<div class="input-group-append">'+
                    '<button class="btn btn-outline-secondary remove_selection" type="button">-</button>'+
                '</div></div>'+
        '</td>'+
        '</tr>';
    $('#data_wedo').append(tr);
    $('#wedo').val('');
});

$('#whattobring').on('click', function (){
    var tobring = $('#tobring').val();
    var cf = $("#data_tobring > tr:last-child").find('input.conter-forgood').data('con');
    var ln = (typeof cf !== "undefined")?(cf+1):1;
    if ($("#data_wedo > tr").length === 5) {
        return;
    }
    var tr = '<tr>'+
        '<td>'+
        '<div class="input-group mb-1">'+
            '<input type="text" data-con="'+ln+'" class="form-control input_add" name="dtobring[]" value="'+tobring+'">'+
                '<div class="input-group-append">'+
                    '<button class="btn btn-outline-secondary remove_selection" type="button">-</button>'+
                '</div></div>'+
        '</td>'+
        '</tr>';
    $('#data_tobring').append(tr);
    $('#tobring').val('');
});

$(document).on('click','button.remove_selection', function(){
  $(this).parents('tr').remove();
});

/***********************************************************************
* For multiple upload
***********************************************************************/
if ($('.dropzone').length) {
    // $("div.dropzone").dropzone({ url: "/file/post" });
    Dropzone.autoDiscover = false;

    var foto_upload= new Dropzone(".dropzone",{
    url: "<?= base_url().'guide/upload_image'; ?>",
    maxFilesize: 2,
    method:"post",
    acceptedFiles:"image/*",
    paramName:"userfile",
    dictInvalidFileType:"This type of file is not permitted",
    addRemoveLinks:true,
    });

    //Event ketika Memulai mengupload
    foto_upload.on("sending",function(a,b,c){
        a.token=Math.random();
        c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
    });

    //Event ketika foto dihapus
    foto_upload.on("removedfile",function(a){
        var token=a.token;
        $.ajax({
            type:"post",
            data:{token:token},
            url:"<?= base_url().'guide/remove_img'; ?>",
            cache:false,
            dataType: 'json',
            success: function(){
                console.log("The image has been deleted");
            },
            error: function(){
                console.log("Error");

            }
        });
    });
}  

/***********************************************************************
* For rating star
***********************************************************************/
$(document).ready(function(){
    var $inp = $('#rating-input');          
    //$inp.attr('value','4');
    $inp.rating({
            min: 0,
            max: 5,
            step: 1,
            size: 'sm',
            showClear: false
        });
    // $inp.on('rating.change', function () {
    //     alert('Nilai rating : '+$('#rating-input').val());
    // });
});

/***********************************************************************
* For crop photo
***********************************************************************/    
// Start upload preview image
$("#fig").hide();
var $uploadCrop, tempFilename, rawImg, imageId;
function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('.upload-demo').addClass('ready');
          $('#cropImagePop').modal('show');
          rawImg = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }else {
        swal("Failed");
    }
}

$uploadCrop = $('#upload-demo').croppie({
    viewport: {
      width: 300,
      height: 310,
    },
    enforceBoundary: false,
    enableExif: true
});

$('#cropImagePop').on('shown.bs.modal', function(){
    console.log("masuk");
    // alert('Shown pop');
    $uploadCrop.croppie('bind', {
        url: rawImg
    }).then(function(){
        console.log('jQuery bind complete');
    });
});

$('.item-img').on('change', function () { imageId = $(this).data('id'); tempFilename = $(this).val();

$('#cancelCropBtn').data('id', imageId); readFile(this); });

$('#cropImageBtn').on('click', function (ev) {
    $uploadCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        size: {width: 300, height: 310}
    }).then(function (resp) {
        $('#item-img-output').attr('src', resp);
        // $('#fig').show();
        var profpic = $('.profpic').val();
        if(profpic != 1 || profpic == null){
            $.ajax({
                url: "<?= base_url().'guide/upload'?>",
                type: "POST",
                data: {"image":resp},
                success: function (data) {
                    html = '<img src="' + resp + '" />';
                    $("#upload-image-i").html(html);
                    $("#thumb").val(data);
                    $('#cropImagePop').modal('hide');
                }
            });
        }else{
            $.ajax({
                url: "<?= base_url().'myprofile/upload_profpic'?>",
                type: "POST",
                data: {"image":resp},
                success: function (data) {
                    html = '<img src="' + resp + '" />';
                    $("#upload-image-i").html(html);
                    $("#thumb").val(data);
                    $('#cropImagePop').modal('hide');
                }
            });
            location.reload();
        }
        
      $('#cropImagePop').modal('hide');
    });
});
// End upload preview image  
    
/***********************************************************************
* For collapse profile edit
***********************************************************************/
$(document).ready(function () {
    var aktif = true;
    var aktif_pass = true;
    $("#collapse-init1").click(function(){
        if(aktif){
            aktif = false;
            $(this).text('Cancel Edit Profile');
            $(".fname").attr("disabled", false);
            $(".lname").attr("disabled", false);
            $(".email").attr("disabled", false);
            $(".personalid").attr("disabled", false);
            $(".reg-btn").show();
        }else{
            aktif = true;
            $(this).text('Edit Your Profile');
            $(".fname").attr("disabled", true);
            $(".lname").attr("disabled", true);
            $(".email").attr("disabled", true);
            $(".personalid").attr("disabled", true);
            $(".reg-btn").hide();
        }
    });

    $("#collapse-changepass").click(function(){
        if(aktif_pass){
            aktif_pass = false;
            $(this).text('Cancel Change Password');
            $(".changepass").show();
        }else{
            aktif_pass = true;
            $(this).text('Change Your Password');
            $(".changepass").hide();
        }
    });
});