/***********************************************************************
* For dropdown datepicker
***********************************************************************/
$('#datepicker').datepicker({
    format: "yyyy-mm-dd",
    todayHighlight: true,
    autoclose: true,
    startDate: truncateDate(new Date())
})

function truncateDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}
/***********************************************************************
* For modal login, register and forgot
***********************************************************************/
$('#forgotpassword-modal').on('shown.bs.modal', function () {
    $('#login-modal').modal('hide')
});

$('#starttour-modal').on('shown.bs.modal', function (e) {
    var bookId = $(e.relatedTarget).data('book-id');
    var itinId = $(e.relatedTarget).data('itin-id');
    $(e.currentTarget).find('input[name="book_id"]').val(bookId);
    $(e.currentTarget).find('input[name="itin_id"]').val(itinId);
});


// var myBookId = $(this).data('id');
//      $(".modal-body #bookId").val( myBookId );

$('#login-modal').on('shown.bs.modal', function () {
    $('#register-modal').modal('hide');
    $('#forgotpassword-modal').modal('hide');
    $('#cart-modal').modal('hide');
});

$('#register-modal').on('shown.bs.modal', function () {
    $('#login-modal').modal('hide')
});

$('#deleteModal').on('show.bs.modal', function(e) {
    $(this).find('.danger').attr('onclick', 'location.href=\"' + $(e.relatedTarget).data('href') + '\"');
});

$('#cancelModal').on('show.bs.modal', function(e) {
    $(this).find('.danger').attr('onclick', 'location.href=\"' + $(e.relatedTarget).data('href') + '\"');
});
/***********************************************************************
* For validate confirm password
***********************************************************************/
jQuery(document).ready(function() {
    var type = document.getElementsByClassName('type')[0].value;
    var password = document.getElementById(type+'-password');  
    var confirm_password = document.getElementById(type+'-confirm');

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
});


function show_pass() {
    var x = document.getElementById("password");
    var y = document.getElementById("password2");

    if (x.type === "password" || y.type === "password2") {
        x.type = "text";
        y.type = "text";
    } else {
        x.type = "password";
        y.type = "password";
    }
}

$(document).ready(function () {
    setInterval(function() {
        $("#navbarSupportedContent").load(location.href + " #navbarSupportedContent");  
        // $("#start-tour").load(location.href + " #start-tour");     
    }, 3000);
});

$(document).ready(function(){
    $(".get-token").click(function(){
        $(".token").toggle();
        $(".ket-token").toggle();
    });
});

