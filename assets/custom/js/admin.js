var baseUrl = 'http://localhost/xploriant/admin';
// DataTable
$(document).ready(function() {
    $('#myTable').DataTable();
});

//Change Account Status
$(document).on('click', '.chstat_account', function (){
    var id = $(this).data('id');
    var link = baseUrl + '/account/change_status/' + id;
    console.log(link);
    $.ajax({
            url: baseUrl +'/account/account_status/' + id,
            dataType: 'json',
            type: 'POST',
            data: {id: id},
            success: function (result) {
            console.log('et');
              $('#changestatus-modal').find('.body-status').html(result.html_page);
              $('#changestatus-modal').modal('show');
              $('#changestatus-modal').attr('action', link);
            }
        });
});

//Change Itinerary Status
$(document).on('click', '.chstat_itin', function (){
    var id = $(this).data('id');
    var link = baseUrl + '/itinerary/change_status/' + id;
    console.log(link);
    $.ajax({
            url: baseUrl +'/itinerary/itinerary_status/' + id,
            dataType: 'json',
            type: 'POST',
            data: {id: id},
            success: function (result) {
            console.log('et');
              $('#changestatus-modal').find('.body-status').html(result.html_page);
              $('#changestatus-modal').modal('show');
              $('#changestatus-modal').attr('action', link);
            }
        });
});

//Change Booking Status
$(document).on('click', '.chstat_book', function (){
    var id = $(this).data('id');
    var link = baseUrl + '/booking/change_status/' + id;
    console.log(link);
    $.ajax({
            url: baseUrl +'/booking/booking_status/' + id,
            dataType: 'json',
            type: 'POST',
            data: {id: id},
            success: function (result) {
            console.log('et');
              $('#changestatus-modal').find('.body-status').html(result.html_page);
              $('#changestatus-modal').modal('show');
              $('#changestatus-modal').attr('action', link);
            }
        });
});